#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='Astrobot',
      package_dir={"":"src"},
      packages = find_packages("src"),
      package_data = {
          'astrobot': ['data/*.ecsv','data/*']
      },
      version='0.1.2',
      description='Astronomy planning, execution, and pipeline tools',
      author='Donald Gavel',
      author_email='donald.gavel@gmail.com',
      url='http://www.not_ready_yet.com/astrobot/',
      keywords = [],
      classifiers = [],
      python_requires = '>=3.8',
      install_requires = ['numpy',
                          'scipy',
                          'astropy >= 4.1',
                          'astroquery >= 0.4',
                          'pandas',
                          'matplotlib',
                          'tqdm',
                          'pytz',
                          'PyYAML',
                          'xlrd',
                          'pyds9',
                         ]
     )
