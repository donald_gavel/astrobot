********
Astrobot
********

Astrobot is a suite of software that will streamline and automate many of the tasks of
astronomical observing including planning, operation, data archiving, and scientific
productivity evaluation
of the observatory.

Background
##########

The project originated with the idea of making the Lick Observatory 3-meter
telescope and its adaptive optics instrument into a more automated and
efficient observing system.

The complexity of operating the AO instrument during nighttime observing led to
investigating how the latest technologies could improve the system's overall consistency
of performance and improve its observing efficiency. It was quickly realized that 80% of the
actions needed to accomplish these goals would overlap with generically improving observatory
operations with machine automation. Tasks such as observation planning, precision telescope
pointing and science field acquisition, automating the calibrations, and moderninzing the
database to facilitate data mining and pipelined data reduction can be instantiated with
computer algorithms. These tools can be used to benefit observing with any instruments
used on any general observing telescope.

Objective
#########

Our goal is to upgrade and alogrithmitize the astronomical observing process,
from planning to data redution.  The developed suite of integrated tools is intended
to be useful to the entire astronomy community, and will be made available in the open domain.

Concept
#######

The diagram below shows our initial analysis of the work flow of the observing process.
In an automated work flow, ideally, a researcher only submits a list of science targets
and the observatory takes care of the rest, ultimately providing a data product to the
researcher. Also ideally, the observer has the ability to preview the process and
participates in various manual adjustments to the program as desired. Hopefully the
algorithmic system eventually becomes robust enough to minimize the need for manual
adjustment.

.. image:: astronomy_workflow.png

Plan of Development
###################

* The basic plan is to develop the overall software and standardization framework that will enable an observation workflow to exectute robotically. This will done by applying existing tools, as described above, and filling in the gaps where needed.

* Initial stages of development will focus on early releases of test software, perhaps containing immediately useful tools that could support a semi-robotic, or "suggestion mode," process.

* The interfaces to the telescope and instrument hardware will need clear definition and standardized interfaces that allow control, monitoring, error recovery, etc.

Standards
#########

The framework will require considerable effort in defining standards.

* The input target list formats should accomodate a wide range of possible observation programs and observation requirments, and be straightforward to set up by an observer with minimal drudge effort.

* The scheduler and operations knowledge base will need a framework definition, including hooks for observatory specific procedures. The construction effort will require input from experienced observers and instrument and telescope operators.

* The database will need definition (in database speak: "schema") that is compatible with existing databases and enable the future data mining and processing tools to work fluently. The database should contain all the science and calibration data collected by the instrument as well as instrument monitoring diagnostic data and observatory operations logs.

* Much can be learned and borrowed from existing experience with automated observatories.

Existing tools
==============

Fortunately, many useful component tools already exist, released in open-source form, so we don't have to develop everything from scratch. So the main job will be to tie it together in a package that facilitates automating the work-flow.

Licensing of value-added components will be with a GPL or similar type of open source licensing, so that the techniques and standards developed by the astrobot project are available to the open community. Under the Python infrastructure, incorporated software retains its original license porvisions and preserves a path to the original source and its updates.

Python
~~~~~~

The python programming environment provides an excellent computing platform for the astrobot framework. Python has an impressive well-developed scientific computing capability and is also now becoming widely adopted for machine learning and big-data analysis. Python comes with a vibrant ecosystem of user-contributed open-source add-on packages. Python sits on top of, and is easily interfaced to C and C++. It also interfaces easily to the Unix, Macintosh, and Windows operating systems, and has tool packages for handling web based applications, web communications, access to web databases etc..  Python has capability to operate in a multithreaded and distributed processor environment plus there are tools for instantiating massive parallel calculation on GPUs.

Subsystem packages
~~~~~~~~~~~~~~~~~~

* The **astropy** package and affilitated projects **astroplan** and **astroquery** provide a basis for the astronomical planning process. Software developed by the **astrometry.net** project can help automate telescope pointing and science field acquisition.

* Databases and data mining tools are mostly covered by the SQL infrastructure and the recent developments in database support in python (e.g. **SQLalchemy**, **pandas**).

* Machine learning technology (with public python packages like **TensorFlow** and **PyTorch**) will be able to assist in chosing instrument setup (and in the case of AO, the actual real-time control), and assist in data mining and discerning patterns in images and spectra. Some astronomy processing tools already utilize machin-learning technology: **Starfinder**, **Galfit**, and **astrometry.net** use machine-learning and neural net like techniques.

Package Requirements
####################

The package requirements are defined in setup.py. Required packages will be downloaded dinring installation.

Installation
############

This should be simple::

    pip install astrobot

Code will be distributed on PyPI, and souce available on Bitbucket.com.

**But** nothing is ever that simple. Several non-python libraries may also have to be installed using your computer system's package manager (e.g. HDF5, SQL,...). Some system configuration and maintenance will also be required.

Hardware control connection (for example telescope pointing control) will require coding that is particular to the observatory hardware drivers. The local site will need a suitable deployment test plan and system for operational safety.

Tests
#####

A suite of self-tests can be separately loaded and run.

Examples and Documentation
##########################

Example use and documentation will be made available via links to online files, wiki (Atlasian Confluence), papers (composed on Overleaf.com, posted on arXiv). Low level documentation will be on readthedocs.org
