.. Astrobot documentation master file, created by
   sphinx-quickstart on Tue Jun  9 15:39:20 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Astrobotic
==========

Document version 1.0

Contents:

.. toctree::
    :maxdepth: 2

    strehl

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

