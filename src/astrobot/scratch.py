'''scratch
'''
import astrobot.control as c
import numpy as np
import astropy.units as u
from astrobot.oprint import pprint
isscalar = c.isscalar
plt = c.plt
pi = np.pi

reload(c)
globals().update(c.AOdefs())

Hdm.name = 'H_{dm}({\it{f}})'
w = 2*pi*1230*u.rad/u.s
tau = 1.50*u.ms
sig = c.Signal(lambda t: np.exp(-t/tau)*np.sin(w*t)*u.Unit('volt'),name='sig(t)')
sig.plot(tf = 10*u.ms)
fig = plt.gcf()
T = 10*sig.samples.dt
sigsh = sig.sample_and_hold(T=T)
sigsh.plot(fig=fig)
sigF = sig.spectrum()
sigF.abs().plot(fmax=50*u.kHz)
fig = plt.gcf()
sigshF = sigsh.spectrum()
sigshF.abs().plot(fig=fig,fmax=50*u.kHz)
shF = Hdm(T=T)
qF = sigF*shF
qF.abs().plot(fmax=50*u.kHz)
sigs = sig.sample(T=T)
sigsF = 10*sigs.spectrum()
sigsF.abs().plot(fmax=50*u.kHz)

# control of sample and hold spectrum
reload(c)
globals().update(c.AOdefs())
ph = c.Sphi.instance(name='phi',dt=.1*u.ms)
phsh = ph.sample_and_hold(T=1*u.ms)
ph.plot(label='ph'); fig=plt.gcf()
phsh.plot(fig=fig,label='phsh')

q = (ph.spectrum()*Hc).signal()
q.plot()

t = np.linspace(0*u.ms,102.3*u.ms,1024)
x = (t > 10*u.ms)*(t < 20*u.ms)*u.rad
ph = c.Signal((t,x))
r = (ph.spectrum()*Hc(gamma=.9)).signal()
r = r - r(0*u.s)
r.plot()

# optimum control
ph = c.Sphi.instance(name='phi',dt=.1*u.ms,n=10240) # doesn't take a tf argument
t = ph.samples.t
x = ph.samples.x
w = ( t < 500*u.ms ) * ( t > 10*u.ms )*u.Quantity(1.0)
x = x * w
ph = c.Signal((t,x),name='\phi')
w = c.Signal((t,w))
#ph.plot()
r = (ph.spectrum()*Hc(gamma=.99)).signal()
#r.plot()

Hol = Hdm*Hc*Hdel*Hr #*Hwfs*Hdm
Hol.setattrs(name = 'AO open loop transfer function',
         formula = 'Hc(s)*Hdel(s)*Hr(s)',
         latex_name = '$H_{ol}(s)$',
         latex_formula = '$H_c(s) H_\\delta(s) H_r(s)$')
g = TF(lambda s,g=.5: g,
       name = 'g',
       long_name = 'loop gain',
       latex_name = '$g$',
       latex_formula = '$g$')
Hcl = 1 / (1 + g*Hol)
Hcl.setattrs(name = 'AO closed loop transfer function',
         formula='1/(1+g*Hol(s))',
         latex_name = '$H_{cl}(s)$',
         latex_formula = '$1 / \\left( 1 + g H_{ol}(s) \\right)$',
         loop_transfer = g*Hol)

ph1 = ph.sample_and_hold(T=1*u.ms)
ph1.plot(label='ph S+H');fig=plt.gcf()
ph.plot(label='ph',fig=fig)
gs = [.1,.2,.3,.4,.5]
for g in gs:
    r = (ph1.spectrum()*Hcl(gamma=.99,g=g)).signal()
    r.plot(label=f'r g={g}',fig=fig)
    ms = ((r.samples.x*w.samples.x)**2).sum()/w.samples.x.sum() # need a windowed rms function
    rms = np.sqrt(ms)
    print(f'gain:{g} rms: {rms}')
# best gain
g = 0.4
ph1.plot(label='ph S+H');fig=plt.gcf()
ph.plot(label='ph',fig=fig)
r = (ph1.spectrum()*Hcl(gamma=.9,g=g)).signal()
r.plot(label=f'r g={g}',fig=fig)


# convert instance to spectrum then back to signal
reload(c)
globals().update(c.AOdefs())
ph = c.Sphi.instance(name='phi',dt=1*u.ms)
phF = ph.spectrum()
q = phF.signal()
qF = q.spectrum()
q1 = 1*q
q1F = q1.spectrum(dt=200*u.ms,n=1024) # Spectrum.from_signal isnores the sampling arguments

ph.plot(label='ph');fig = plt.gcf()
q.plot(fig=fig,label='q');fig = plt.gcf()
q1.plot(fig=fig,label='q1')

phF.plot(label='phF');fig = plt.gcf()
qF.plot(label='qF',fig=fig)
q1F.plot(label='q1F',fig=fig)

# plot open and closed loop signals, spectra, spectra ratio, and closed loop transfer function
reload(c)
globals().update(c.AOdefs())
ph = c.Sphi.instance(name='phi',dt=1*u.ms)
phF = ph.spectrum()
phcF = phF*Hcl
phc = phcF.signal()
ph.plot()
phc.plot(fig=1)
phF.abs().plot()
phcF.abs().plot(fig=2)
(phcF/phF).abs().plot()
Hcl.plot()


reload(c)
globals().update(c.AOdefs())
dt = (pi/10)*u.s
ph = c.Signal(lambda t: np.sin(t*u.rad/u.s)*np.exp(-t/(7*u.s))*(t<128*dt))
#ph.gen_samples(dt=dt,n=256)
ph.plot(dt=dt,n=256,label='ph',fig=1)
phF = ph.spectrum()
phF.plot(label='phF',fig=2)
q = phF.signal(dt=dt*1.1,n=256)
q.plot(label='q',fig=1)
qF = q.spectrum()
qF.plot(label='qF',fig=2)


reload(c)
globals().update(c.AOdefs())
ph = c.Sphi.instance(name='phi')
phF = ph.spectrum()
df = phF.samples.df
RR = c.PowerSpectrum(phF.abs2()*df)
rF = phF*Hcl
phF.plot(label='phF');fig=plt.gcf()
rF.plot(label='rf',fig=fig)

(rF/phF).plot()

r = rF.signal()
ph.plot(label='ph');fig=plt.gcf()
r.plot(label='r',fig=fig)

RR.plot(label='RR');fig=plt.gcf()
c.Sphi.plot(label='Sphi',fig = fig)

reload(c)
globals().update(c.AOdefs())
data = []
fmax = 500*u.Hz
n = 1024
fig=None
ntest=10
for k in range(ntest):
    ph = c.Sphi.instance()
    phF = ph.spectrum()
    df = phF.samples.df
    RR = c.PowerSpectrum(phF*phF.conj()*df) # equation (36)
    RR.gen_samples()#(fmax=fmax,n=n//2) # Objects should be savvy about limits on f
    data.append(RR.samples.F)
    RR.plot(fig=fig,fmax=fmax)
    fig = plt.gcf()
data = u.Quantity(data)
mRR = np.mean(data,axis=0)
f = RR.samples.f
c.PowerSpectrum((f,mRR)).plot(fig=fig,color='red')
c.Sphi.plot(fig=fig,color='black')

# sampling a signal
ph = c.Sphi.instance(T=10*u.us,n=102400)
t = np.arange(0,1,.01)*u.s
x = ph(t)
phs = c.Signal((t,x))
ph.plot()
phs.plot()

# how I'd want to do a time simulation
ph = c.Sphi.instance(t=10*u.us,n=102400)
Hol = Hwfs * Hr * Hdel * Hc * Hdm # Hwfs and Hdm are defined with sample-and-holds
loop = c.Loop(-Hol)
system = ph > loop
system.simulate(t0=0*u.s, tf=1*u.s, dt=10*u.us)

class SystemBlock():
    '''define a class of items that can be used
    in a time simuation
    '''
    record_buffer_size = 10000
    valid_kinds = ['standard-block','generator','multi-input']
    
    def __init__(self,**kwargs):
        self.name = '?'
        self.x = 0.
        self.y = 0.
        self.record = np.array([0.]*self.record_buffer_size)
        self.through = kwargs.pop('through',False)
        self.kind = kwargs.pop('kind','standard-block')
        n = kwargs.pop(n,0)
        x = np.array([0.]*n)
        self.x,self.n = x,n
        self.input = kwargs.pop('input',None)
        if self.input is None and self.kind == 'multi-input':
            self.input = []
        self.output = kwargs.pop('output',None)
        self.set(**kwargs)
    
    @property
    def pp(self):
        oprint.pprint(self)
    
    @property
    def source(self):
        return inspect.getsource(self.f)

    def set(self,**kwargs):
        '''set the attributes of the system block
        '''
        for key,val in kwargs.items(): self.__dict__[key] = val
        return self
    
    def reset(self,x0=0.):
        '''reset the state to x0 and clear all the history
        '''
        x,y,n = self.x,self.y,self.n
        assert isscalar(x0) or len(x0) == n
        x = x*0+x0
        y = 0
        self.x,self.y = x,y
        self.record = 0*self.record
        
    def __call__(self,*args):
        return self.f(self,*args)

'''System Block function definitions -
    These generate the system output y and change of state x as a funtion of the input u.
Rules:
    The output is first calculated as a function of the ~present~ state and ~present~ input,
    then the state is updated.
    Any throughput of input to output is explicit  (for example D != 0 in a state-space model).
    The resulting output (y) is stored in the object, so it can be read out at any subsequent
    time without triggering a state transistion.
Clocking:
    t is the global clock time. Each block function may depend on t, and any internal clock-oriented
    attributes (such as T, the interval at which it triggers a state transition)
'''
def stateSpacef(self,u):
    A,B,C,D,x = self.A, self.B, self.C, self.D, self.x
    y = C*x + D*u
    x = A*x + B*u
    self.x,self.y = x,y
    return y
def integratorf(self,u):
    gamma, x, k, T = self.gamma, self.x, self.T
    if t%T == 0:
        y = x + u
        x = gamma*(x+u)
        self.x,self.y = x,y
    return y
def delayf(self,u):
    x = self.x
    y = x[-1]
    x = np.roll(q,1)
    x[0] = u
    self.x,self.y = x,y
    return y
def adderf(self,x1,x2):
    s = self.signs
    y = s[0]*x1 +s[1]*x2
    self.y = y
    return y
def boxcarf(self,u):
    x,n = self.x,self.n
    y = x.sum()/n
    x = np.roll(x,1)
    x[0] = u
    self.x,self.y = x,y
    return y
def sampleholdf(self,u):
    n,T = self.n,self.T
    y = x
    if t%T == 0:
        x = u
    self.x,self.y = x,y
    return y
def disturbf(self,u):
    y = np.random.normal()
    self.y = y
    return y

'''System Block instances -
'''
T = 1*u.ms
dt = 0.1*u.ms
tau = 800*u.us

abcd_sys = SystemBlock(name = 'asys',f = stateSpacef,descr='5 step delay represented in state space',
    A = np.matrix(np.diag(np.ones(4),1)),
    B = np.matrix([0,0,0,0,1.]).T,
    C = np.matrix([1.,0,0,0,0]),
    D = 0,
    x = np.matrix([0,0,0,0,0]).T,
    n = 5,
    )
control_law = SystemBlock(name='control_law',f = integratorf,descr='leaky integrator control law',
                   gamma=1.,
                   n = 1,
                   through = True,
                   )
readout_delay = SystemBlock(name='readout_delay',f = delayf,descr='CCD readout delay',
                            T=T,
                            n=int(T/dt),
                            )
compute_delay = SystemBlock(name='compute_delay',f = delayf,descr='compute delay',
                    tau = tau,
                    n = int(tau/dt),
                    )
adder = SystemBlock(name='adder',f = adderf,descr='two-input +/- difference',
                    signs = (+1,-1),
                    kind = 'multi-input',
                    through = True,
                    )
WFS_samplehold = SystemBlock(name='WFS',f = sampleholdf,descr='WFS sample and hold',
                         n = int(T/dt),
                         )
DM_samplehold = SystemBlock(name='DM',f=sampleholdf,descr='DM sample and hold',
                            n = int(T/dt),
                            )
boxcar = SystemBlock(name='boxcar',f=boxcarf,descr='boxcar average',
                     n = int(T/dt),
                     )
disturb = SystemBlock(name='disturb',f = disturbf,
                      kind = 'generator',
                      )

def connect(*args):
    for a1,a2 in zip(args[:-1],args[1:]):
        a1.output = a2
        if a2.kind == 'multi-input':
            a2.input.append(a1)
        else:
            a2.input = a1
    return args[0]

connect( disturb, adder, boxcar, WFS_samplehold, readout_delay, compute_delay, control_law, DM_samplehold, adder)

compute_delay.set(tau=300*u.us,n = 3,x = np.array([0.]*3))
connect(disturb, boxcar, WFS_samplehold,readout_delay,  control_law, DM_samplehold)
DM_samplehold.output = None
run(disturb,nt=100)
tt = np.arange(0,k)*dt
c.Signal((tt, disturb.record[0:k]*u.Unit(''))).plot(label='disturb'); fig = plt.gcf()
c.Signal((tt, (boxcar.record[0:k]-2)*u.Unit(''))).plot(label='boxcar',fig=fig)
c.Signal((tt, (WFS_samplehold.record[0:k]-4)*u.Unit(''))).plot(label='WFS',fig=fig)
c.Signal((tt, (readout_delay.record[0:k]-6)*u.Unit(''))).plot(label='read delay',fig=fig)
c.Signal((tt, (control_law.record[0:k]-8)*u.Unit(''))).plot(label='control',fig=fig)
c.Signal((tt, (DM_samplehold.record[0:k]-10)*u.Unit(''))).plot(label='DM',fig=fig)

    
connect( adder, delay, asys, cont )
#connect((disturb,cont),adder)
adder.input = (disturb,cont)
disturb.output = adder
cont.output = adder

def connection_diagram(arg,seen=[]):
    if arg in seen:
        print(arg.name)
        return
    seen.append(arg)
    print(arg.name,end='->')
    if hasattr(arg,'output'):
        connection_diagram(arg.output,seen=seen)
    else:
        return

#Hc.f = types.MethodType(f,Hc)

# develop PowerSpectrum instance realization code
reload(c)
globals().update(c.AOdefs())

self = c.Sphi
i = 1j
n = 1024 # this is the size of the resulting time domain sequence
T = 2*u.ms
fn = ( 1/(2*T)).to(u.Hz)
f0 = 0*u.Hz
self.gen_samples(fmax=fn,n=n//2+1)
f,S = (self.samples[x][:-1] for x in ['f','F'])
dt = T
df = (1/(n*T)).to(u.Hz)

re,im = np.random.normal(size=(2,n//2))
N = (re + i*im)/np.sqrt(2)
N[0] = re[0]
R = np.sqrt(S/df)*N # equation 36
rn = np.fft.irfft(R,n=n)*n*df # n to undo the normalization, df to do the Reimann sum
# Parseval test
st = np.sum(rn*rn)*dt
R2 = np.block([R,0,np.flip(R[1:n//2]).conj()])
sf2 = np.sum(R2*R2.conj()).real*df
sf = 2*(np.sum(R*R.conj())-R[0]*R[0]/2).real*df

rn2 = np.fft.ifft(R2).real*n*df
t = np.arange(0,n)*dt
Q = (np.fft.fft(rn))
print(all(np.isclose(Q[0:n//2],R)))
# Parseval
st = np.sum(rn*rn)*n
sf = np.sum(R2*R2.conj()).real*df
print(np.isclose(st,sf))

Sr = R.abs()**2*df # formula 36 solved for Sr

n = 8
dt = 1
df = 1/(n*dt)
R = np.array([0,1,2,0])
R2 = np.array([0,1,2,0, 0,0,2,1])
rn = np.fft.irfft(R,n=n)
rn2 = np.fft.ifft(R2).real

# "Scheme 1" and "Scheme 2"

# Scheme 1:
exme()
T = 1*u.ms
dt= T/10
system = atmos
delay = SystemBlock(name='t_delay',f=delayf,n = 10)
controller = control_law.copy()

system.connect(adder,WFSexpose,sampler,delay,controller,DM,adder)
#system.connect(WFSexpose,sampler)
for scheme in [1,2]:
    for gain in [0.4,1.0]:
        if scheme == 2:
            controller.gain = gain
            system.clock = SystemClock(T=T,dt=dt).set(signals=[(WFSexpose,0,20,'on'),
                                                               (WFSexpose,11,20,'off'),
                                                               (sampler,10,20),
                                                               (controller,0,20),
                                                               (DM,0,20)])
        elif scheme == 1:
            controller.gain = gain
            system.clock = SystemClock(T=T,dt=dt).set(signals=[(WFSexpose,0,10,'on'),
                                                               (sampler,10,10),
                                                               (controller,0,10),
                                                               (DM,0,10)])
        #WFSexpose.clock = system.clock
        
        system.run(nt=7000).multiplot(offset=20,title='scheme%s, gain=%s'%(scheme,controller.gain))
