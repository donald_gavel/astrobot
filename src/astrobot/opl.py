# opl.py - optics plane
#   stores a wavefront or optical element
#   for wave optics simulation

import numpy as np
from astropy.units import Quantity
import astropy.units as u
from astrobot import img,dimg
import matplotlib.pyplot as plt


class OpticBase(Quantity):
    def __new__(cls, *args, **kwargs):
        self = super().__new__(cls, *args)
        for key, val in kwargs.items():
            setattr(self, key, val)
        return self

    @property
    def inform(self):   # doesn't break the Quantity.info method
        print(f'dtype = {self.dtype}')
        print(f'shape = {self.shape}')
        print(f'class = {self.__class__}')
        print('attributes:')
        for k,v in self.__dict__.items():
            print(f'    {k} = {v}')
        #pprint.pp(self.__dict__,width=1)
        return

    @property
    def magnitude(self):
        return np.abs(self)

    @property
    def peak(self):
        """The peak of absolute values
        """
        return self.magnitude.max()

    @property
    def ssq(self):
        """The sum of absolute values
        """
        return float((self.magnitude ** 2).sum())

    def normalize(self, nph=1.):
        """set the total throughput to 1
        """
        r = self * np.sqrt(nph / self.ssq)
        r.nph = nph
        return r

    def show(self,**kwargs):
        """display 2-d image
        """
        dimg.show(self, **kwargs)


class Optic(OpticBase):
    @classmethod
    def aperture(cls, shape, c=None, r=None, dx=1., **kwargs):
        '''Generate a 2-d image with a disk of ones inside radius r and centered at c, zero outside

        Parameters
        ----------
        c : tuple of 2 integers
            The x,y center position of the center of the circle, in pixels.
            Defaults to the center of the image.
        r : Quantity
            The radius of the circle.
            Defaults to (unitless) 1/2 the image's smallest dimension
        dx : Quantity
            The pixel size.
            Defaults to (unitless) 1.

        r and dx must have equivalent units.

        '''
        if r is not None:
            r = r / dx
        else:
            r = shape[0] / 2
        n, m = shape
        if Quantity(dx).isscalar:
            dx = Quantity([dx, dx])
        x0 = -Quantity(shape) * dx / 2
        kwargs.update({'r':r,'c':c,'dx':dx,'x0':x0})
        self = super().__new__(cls,img.circle(shape,c=c,r=r),**kwargs)
        return self

    @classmethod
    def disk(cls,shape,**kwargs):
        return 1 - Optic.aperture(shape,**kwargs)

class Wavefront(OpticBase):
    def ffprop(self):
        """propagate a wavefront to the far field
        """
        if hasattr(self, 'nph'):
            nph = self.nph
        else:
            nph = self.ssq
        r = img.ft(self).normalize(nph)
        return r

    def prop(self, L):
        """propagate a wavefront from this plane
        by a distance L using Fraunhofer approximation
        to Fresnel integral
        """
        raise NotImplementedError('Opl.prop')


class Intensity(Quantity):
    pass


# A coronagraph consists of a pupil plane, a focal plane with
# beam stop, and a second pupil plane with a Lyot stop


n = 100
shape = (n, n)
D = 50
r = D / 2
lyot_r = 0.9 * r


def prop_light():
    #    optics planes
    pupil = Optic.aperture(shape, r=r, name='pupil')
    wf = Wavefront(pupil,lam=1.0*u.micron)
    wf_fp = wf.ffprop()
    cspot = Optic.disk(shape,r=2,name='Focal_Plane_Stop')
#     pp2 = pp1*Opl.circle(shape,r = lyot_r).set(name = 'Lyot_Stop')
#     # wavefronts
#     w = pp1.normalize().ffprop().set(name = 'Field_at_first_focal_plane',t)
#     w2 = (w*fp1).ffprop().set(name = 'beam_after_c-stop')
#     w3 = (w2*pp2).set(name = 'beam_after_Lyot_stop')
#     w4 = w3.ffprop().set(name = 'psf_after_Lyot_stop')

    globals().update(locals())

def test():
    q = Opl.circle((100, 100), c=(50.5, 50.5), r=25)
    qs = Opl.circle((100, 100), c=(50.5, 50.5), r=5)
    q = q - qs
    plt.figure(1)
    plt.cla()
    plt.imshow(q, cmap='gray', origin='lower')
    plt.show()
