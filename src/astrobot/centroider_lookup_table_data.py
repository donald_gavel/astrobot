import pandas as pd
import numpy as np
import scipy
from scipy.interpolate import UnivariateSpline
import astropy.units as u
import matplotlib.pyplot as plt

df = pd.DataFrame(columns = ['r00','mode','cent','ns','nexp','spl'])

ns=25
nexp=10
mode = '16x'
pixelscale = {'16x':1.56*u.arcsec/u.pix, '8x':2.06*u.arcsec/u.pix}[mode]

def append(**kwargs):
    global df
    row = {'mode':mode,'cent':'COG','ns':ns,'nexp':nexp}
    tcki = kwargs.pop('tcki',0) # don't include the inverse, we recomputed it later using a much finer sampling and exact fit of the forward spline
    tck = kwargs.pop('tck',0)
    spl = UnivariateSpline._from_tck(tck)
    kwargs['spl'] = spl
    row.update(kwargs)
    rowdf = pd.DataFrame([row])
    df = df.append(row,ignore_index=True)

append(r00 = 7*u.cm,
       tck = ([-2.34, -2.34, -2.34, -2.34, -1.17, -0.58,  0.  ,  1.17,  2.34,  2.34,
               2.34,  2.34],
               [-1.21, -1.35, -0.47, -0.54,  0.33,  0.44,  1.33,  1.2 ,  0.  ,  0.  ,
               0.  ,  0.  ],
               3),
       tcki = ([-1.2 , -1.2 , -1.2 , -1.2 , -1.07, -0.87, -0.58, -0.52,  0.22,  0.47,
                0.74,  1.08,  1.13,  1.21,  1.21,  1.21,  1.21,  1.21],
               [-2.35e+00, -1.65e+00, -1.92e+00, -1.36e+00, -1.48e+00,  1.52e-03,
                -2.14e-01,  7.17e-01,  1.61e+00,  1.45e+00,  1.95e+00,  3.89e+00,
                1.72e+00,  2.14e+00, -2.10e+00, -1.91e-01, -2.04e-01, 1.55e+00],
               3),
        )

append(r00=10*u.cm,
       tck = ([-2.34, -2.34, -2.34, -2.34, -1.17, -0.58,  0.  ,  0.58,
               1.17,  2.34,  2.34,  2.34,  2.34],
              [-1.28, -1.47, -0.45, -0.54,  0.  ,  0.52,  0.51,  1.44,
               1.28,  0.  ,  0.  ,  0.  , -1.29],
              3),
       tcki = ([-1.31e+00, -1.31e+00, -1.31e+00, -1.31e+00, -1.23e+00, -1.09e+00, -9.04e-01, -7.29e-01,
               -5.96e-01, -5.29e-01, -4.94e-01, -4.40e-01, -3.30e-01, -1.76e-01, -6.74e-04,  1.73e-01,
               3.25e-01, 4.36e-01,  5.00e-01,  5.47e-01,  6.21e-01,  7.51e-01,  9.18e-01,  1.09e+00,
               1.23e+00,  1.30e+00,  1.30e+00,  1.30e+00,  1.30e+00],
               [-2.15e+00, -2.59e+00, -1.41e+00, -1.94e+00, -1.50e+00, -1.40e+00, -1.22e+00, -1.06e+00,
               -7.25e-01, -5.16e-01, -3.67e-01, -1.82e-01, -1.26e-03,  1.82e-01,  3.68e-01,  5.27e-01,
               7.43e-01,  1.03e+00,  1.22e+00,  1.40e+00,  1.51e+00,  1.93e+00,  1.42e+00,  2.60e+00,
               2.15e+00,  0.00e+00,  0.00e+00,  0.00e+00,  0.00e+00],
               3),
      )

append(r00 = 12*u.cm,
       tck = ([-2.34, -2.34, -2.34, -2.34, -1.17, -0.58,  0.  ,  0.58,  1.17,  2.34,
               2.34,  2.34,  2.34],
               [-1.26, -1.57, -0.38, -0.57,  0.03,  0.56,  0.41,  1.6 ,  1.25,  0.  ,
                0.  ,  0.  , -1.33],
               3),
       tcki = ([-1.28, -1.28, -1.28, -1.28, -1.11, -0.92, -0.57, -0.5 , -0.48, -0.44,
                -0.37,  0.23,  0.39,  0.45,  0.45,  0.5 ,  0.59,  0.72,  1.14,  1.28,
                1.28,  1.32,  1.32,  1.32,  1.32],
               [-2.34e+00, -1.56e+00, -1.91e+00, -1.40e+00, -1.42e+00, -1.06e+00,
                -7.65e-01, -4.70e-01, -1.51e-01,  1.05e-02,  3.55e-01,  4.29e-01,
                9.00e-01,  1.01e+00,  1.21e+00,  1.52e+00,  1.57e+00,
                1.90e+00,  2.43e+00, -2.66e+02,  2.14e+00,  0.00e+00,  0.00e+00,
                0.00e+00, -2.33e+00],
               3),
      )

append(r00 = 15*u.cm,
       tck = ([-2.34, -2.34, -2.34, -2.34, -1.17, -0.58,  0.  ,  0.58,  1.17,
               1.76,  2.34,  2.34,  2.34,  2.34],
               [-1.29, -1.61, -0.36, -0.57,  0.05,  0.56,  0.41,  1.25,  1.43,
                1.3 ,  0.  ,  0.  , -1.26, -1.69],
               3),
       tcki = ([-1.31, -1.31, -1.31, -1.31, -1.14, -0.92, -0.7 , -0.56, -0.51,
                -0.47, -0.47, -0.36, -0.2 ,  0.02,  0.23,  0.39,  0.46,  0.48,
                0.51,  0.57,  0.73,  0.95,  1.17,  1.31,  1.35,  1.35,  1.35,
                1.35],
               [-2.34, -1.18, -2.14, -1.48, -1.27, -1.49, -0.8 , -1.18,  0.98,
                -1.31,  0.28, -0.11,  0.11,  0.51,  0.28,  0.9 ,  0.98,  1.32,
                1.39,  1.35,  2.36,  0.79,  2.58,  2.15, -2.16, -1.52, -1.84,
               -0.53],
               3),
      )

append(r00=20*u.cm,
       tck = ([-2.34, -2.34, -2.34, -2.34, -1.76, -1.17, -0.58,  0.  ,  0.39,
               0.58,  1.17,  1.76,  2.34,  2.34,  2.34,  2.34],
               [-1.33, -1.44, -1.24, -0.37, -0.62, -0.08,  0.38,  0.53,  0.39,
                         1.27,  1.44,  1.32, -1.37, -1.47, -0.22, -0.49],
               3),
       tcki = ([-1.34, -1.34, -1.34, -1.34, -1.15, -0.92, -0.69, -0.56, -0.49,
               -0.49, -0.46, -0.39, -0.21,  0.02,  0.25,  0.4 ,  0.47,  0.47,
               0.51,  0.57,  0.73,  0.96,  1.18,  1.32,  1.35,  1.35,  1.35,
               1.35],
               [-2.34, -1.29, -2.28, -0.53, -4.34,  3.47, -6.38,  1.41, -3.16,
               1.95, -1.52,  1.49, -3.4 ,  6.97, -6.49,  4.32, -1.62,  3.55,
               0.15,  1.65,  2.47,  0.56,  2.74,  2.14, -2.26, -1.46, -1.88,
               -0.38],
               3),
      )

append(r00 = 22*u.cm,
       tck = ([-2.34, -2.34, -2.34, -2.34, -1.76, -1.17, -0.58,  0.  ,  0.58,
               1.17,  1.76,  2.34,  2.34,  2.34,  2.34],
               [-1.33, -1.45, -1.25, -0.35, -0.63,  0.02,  0.62,  0.37,  1.29,
               1.43,  1.33,  0.  , -1.38, -1.49, -0.21],
               3),
       tcki = ([-1.34, -1.34, -1.34, -1.34, -1.15, -0.93, -0.7 , -0.56, -0.49,
                -0.48, -0.47, -0.38, -0.21,  0.02,  0.25,  0.4 ,  0.47,  0.48,
                0.49,  0.58,  0.73,  0.96,  1.18,  1.32,  1.36,  1.36,  1.36,
                1.36],
               [-2.34e+00, -1.14e+00, -2.15e+00, -1.38e+00, -1.59e+00, -9.62e-01,
                -1.41e+00, -7.78e-01, -1.20e-01, -6.64e-01, -3.29e-02, -6.45e-04,
                3.66e-02,  5.39e-01,  2.36e-01,  7.64e-01,  1.32e+00,
                1.02e+00,  1.55e+00,  1.31e+00,  2.35e+00,  8.45e-01,  2.63e+00,
                2.15e+00, -2.16e+00, -1.44e+00, -1.95e+00, -3.16e-01],
               3),
      )

def plots():
    '''plot all the forward and inverse splines
    and nonlinearity curves (departure from straight line)
    '''
    fig1 = plt.figure()
    ax1 = plt.gca()
    fig2 = plt.figure()
    ax2 = plt.gca()
    fig3 = plt.figure()
    ax3 = plt.gca()
    tips = np.linspace(-1.5,1.5,ns)*pixelscale*u.pix
    xs = np.linspace(-1.2,1.2,ns)
    linfit = (1.4/2.34)*tips.value
    for idx,row in df.iterrows():
       r0 = row.r00
       spl = row.spl
       spli = row.spli
       label='$r_0$ = %s'%r0.round(2)
       plt.figure(fig1.number)
       plt.plot(tips,spl(tips),label=label)
       plt.figure(fig2.number)
       plt.plot(tips,spl(tips)-linfit,label=label)
       plt.figure(fig3.number)
       plt.plot(spli(xs),xs,label=label)
    for fig in [fig1,fig2,fig3]:
       plt.figure(fig.number)
       plt.legend()
       plt.grid(True)
       ax = fig.axes[0]
       ax.set_xlabel('tip, arcsec')
    ax1.set_title('Forward splines')
    ax1.set_ylabel('centroider output')
    xlim = ax1.get_xlim()
    ylim = ax1.get_ylim()
    ax2.set_title('Forward nonlinearity')
    ax2.set_ylabel('nonlinearity')
    ax2.set_xlim(xlim)
    ax3.set_title('Inverse splines')
    ax3.set_ylabel('centroider output')
    ax3.set_xlim(xlim)
    ax3.set_ylim(ylim)

def plot(idx):
    '''plot the forward and inverse splines for a particular r0
    on one graph
    '''
    tips = np.linspace(-1.5,1.5,ns)*pixelscale*u.pix
    xs = np.linspace(-1.2,1.2,ns)
    linfit = (1.4/2.34)*tips.value
    row = df.loc[idx]
    spl = row.spl
    spli = row.spli
    fig = plt.figure()
    plt.plot(tips,spl(tips),label='forward')
    plt.plot(spli(xs),xs,label='inverse')
    plt.plot(tips,linfit,linestyle='--',color='black')
    ax = plt.gca()
    ax.set_xlabel('tip, arcsec')
    ax.set_ylabel('centroider output')
    ax.set_title('$r_0$ = %s'%row.r00.round(2))
    plt.legend()
    plt.grid(True)
    
def monotone(x,y):
    '''Makes the sequence of y's monotonic by throwing out x,y points
    that are not increasing values of y.
    Returns the monotonic x,y sequence
    '''
    rx = []
    ry = []
    y_last = y[0]
    for xe,ye in zip(x,y):
        if ye > y_last:
            ry.append(ye)
            rx.append(xe)
            y_last = ye
    return rx,ry

def inverses(n=500):
    '''Computes the inverse splines for each of the splines in the
    database. The inverse spline exactly fits a finely sampled set of points in the
    forward spline. n is the number of samples.
    '''
    
    global df
    try:
        df2 = pd.DataFrame(columns = ['spli'])
        for idx in df.index:
            spl = df.loc[idx].spl
            tips = np.linspace(-1.5,1.5,n)*pixelscale*u.pix
            y = spl(tips)
            tipsm,ym = monotone(tips,y)
            tipsm = u.Quantity(tipsm)
            ym = u.Quantity(ym)
            spli = UnivariateSpline(ym,tipsm,s=0)
            row = {'spli':spli}
            df2 = df2.append(row,ignore_index=True)
        df = pd.concat([df,df2],axis=1)
    finally:
        globals().update(locals())

inverses()
