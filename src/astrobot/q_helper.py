'''Extend the astropy.units.Quantity objects with
additional methods and properties useful for image analysis
and optical wave propagation
'''

from functools import wraps # This convenience func preserves name and docstring
import astropy.units as u
import astropy.io.fits as fits
import numpy as np
import inspect
import traceback

try:
    import pyds9
except Exception as err:
    pass

import matplotlib.pyplot as plt
from matplotlib.figure import Figure

from astrobot import dimg
from astrobot import img
from astrobot import oprint
from astrobot.dotdict import DotDict
from astrobot.tcolor import tcolor

plt.ion()
verbose = False
msgs = []
def mprint(*args,**kwargs):
    msgs.append(*args)
    if verbose: print(*args,**kwargs)
    
# new properties

u.Quantity.angle = property(lambda self: np.angle(self).asQ(self))
u.Quantity.phase = property(lambda self: np.angle(self).asQ(self))
u.Quantity.magnitude = property(lambda self: np.abs(self).asQ(self))
u.Quantity.mag = property(lambda self: np.abs(self).asQ(self))

u.Quantity.shapexy = property(lambda self: u.Quantity(self.shape).astype(int)[::-1])
u.Quantity.physical_size = property(lambda self: self.dx*self.shapexy)
u.Quantity.width = property(lambda self: self.physical_size[0])
u.Quantity.height = property(lambda self: self.physical_size[1])
u.Quantity.xmin = property(lambda self: self.x0[0])
u.Quantity.xmax = property(lambda self: self.x0[0]+self.dx[0]*self.shapexy[0])
u.Quantity.ymin = property(lambda self: self.x0[1])
u.Quantity.ymax = property(lambda self: self.x0[1]+self.dx[1]*self.shapexy[1])


u.Quantity.pp = property(lambda self: self.pprint())
u.Quantity.hdr = property(lambda self: DotDict(self.__dict__))

# modified properties

if not hasattr(u.Quantity,'up_real'):
    mprint ('<q_helper> changing u.Quantity.real')
    u.Quantity.up_real = getattr(u.Quantity,'real')
    u.Quantity.real = property(lambda self: self.up_real.asQ(self))
if not hasattr(u.Quantity,'up_imag'):
    mprint ('<q_helper> changing u.Quantity.imag')
    u.Quantity.up_imag = getattr(u.Quantity,'imag')
    u.Quantity.imag = property(lambda self: self.up_imag.asQ(self))
    
# -- define the decorators --

def add_class_method(cls,wfunc=None):
    def decorator(func):
        @wraps(func)
        def wrapper(*args,**kwargs):
            return func(*args,**kwargs)
        if wfunc is not None:
            doc = '{} is a constructor for {}.{}'.format(func.__name__,cls.__module__,cls.__name__)
            doc += '\n' + '(wraps {}.{})'.format(wfunc.__module__,wfunc.__name__)
            doc += '\n' + wfunc.__doc__
            wrapper.__doc__ = doc
        setattr(cls,func.__name__,wrapper)
        return wrapper
    return decorator
    
def add_method(cls,wfunc=None):
    # https://medium.com/@mgarod/dynamically-add-a-method-to-a-class-in-python-c49204b85bd6
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            #print('<wrapper> {}'.format(func.__name__))
            return func(*args, **kwargs)
        if wfunc is not None:
            doc = '(wraps {}.{})'.format(wfunc.__module__,wfunc.__name__)
            doc += '\n' + wfunc.__doc__
            wrapper.__doc__ = doc
        setattr(cls,func.__name__,wrapper)
        return wrapper
    return decorator

# -------- new methods --------

defaults = dict((
    ('axes-dx',1.),
    ('prop-dx',1.*u.mm),
    ('prop-wavelength',0.5*u.micron),
))

@add_method(u.Quantity)
def _default_axes_properties(self):
    '''Set up minimal axis paramters in a quantity if it doesn't have
    them already: dx, x0
    '''
    n = self.shapexy
    if not hasattr(self,'dx'):
        dx = u.Quantity(defaults['axes-dx'])
        if self.ndim > 1: dx = u.Quantity([dx,]*self.ndim)
        self.dx = dx
    if not hasattr(self,'x0'):
        if isinstance(self.dx,list):
            self.x0 = [0.*dx for dx in self.dx]
        else:
            self.x0 = 0*self.dx
        #self.x0 = -(n//2)*self.dx
    return self

@add_method(u.Quantity)
def _default_propagation_properties(self,warn=False):
    '''Set up the minimal properties needed for propagating
    as a wavefront: dx, x0, wavelength
    '''
    assert self.ndim == 2,'optical propagation only valid for 2-d arrays'
    n = self.shapexy
    if not hasattr(self,'dx'):
        dx = defaults['prop-dx']
        if warn: print(f'dx not a property, supplying default {dx}')
        self.dx = u.Quantity([dx,]*2)
    elif isinstance(self.dx,list):
        if all([dx[k].unit.is_equivalent(u.m) for k in [0,1]]):
            self.dx = u.Quantity(self.dx)
        else:
            raise Exception('dx list elements must all have units of length')
    else: # Quantity
        assert self.dx.unit.is_equivalent(u.m),'dx Quantity must have units of length'
        
    if not hasattr(self,'x0'):
        self.x0 = -(n//2)*self.dx
    elif isinstance(self.x0,list):
        if all([self.x0[k].unit.is_equivalent(u.m) for k in [0,1]]):
            self.x0 = u.Quantity(self.x0)
        else:
            raise Exception('x0 list must all have units of length')
    else: # Quantity
        assert self.dx.unit.is_equivalent(u.m),'x0 Quantity must have units of length'
    
    if not hasattr(self,'wavelength'):
        wavelength = defaults['prop-wavelength']
        if warn: print(f'wavelength is not a property, supplying default {wavelength}')
        self.wavelength = wavelength
        
    return self

@add_method(u.Quantity)
def _asQ(r,q):
    r = u.Quantity(r)
    r.__dict__.update(q.__dict__)
    return r

@add_method(u.Quantity)
def asQ(self,other):
    '''Copy all attributes from other to self
    other than the unit
    '''
    d = other.__dict__
    for k,v in d.items():
        if k != '_unit':
            setattr(self,k,v)
    return self

@add_method(u.Quantity)
def pprint(self):
    '''Pretty-print the Quantity's attributes
    '''
    oprint.pprint(self)
    
code = '''
@ add_method(u.Quantity)
def __func__(self,*args,**kwargs):
    r = img.__func__(self,*args,**kwargs)
    r = u.Quantity(r,self.unit).asQ(self)
    return r
__func__.__doc__ = '(wraps img.__func__)\\n'+img.__func__.__doc__
'''

for name in ['depiston','detilt','minmax','shift']:
    excode = code.replace('__func__',name)
    exec(excode)

@add_class_method(u.Quantity)
def arange(x0,xf,dx):
    xunit = x0.unit
    r = np.arange(x0/xunit,xf/xunit,dx/xunit)*xunit
    return r

@add_class_method(u.Quantity,img.circle)
def circle(shape,c=None,r=None,dx=1):
    '''Generate a 2-d image with a disk of ones inside radius r and centered at c, zero outside
    
    Parameters
    ----------
    c : tuple of 2 integers
        The x,y center position of the center of the circle, in pixels.
        Defaults to the center of the image.
    r : Quantity
        The radius of the circle.
        Defaults to (unitless) 1/2 the image's smallest dimension
    dx : Quantity
        The pixel size.
        Defaults to (unitless) 1.
    
    r and dx must have equivalent units.

    '''
    if r is not None:
        r = r/dx
    n,m = shape
    dx = u.Quantity([dx,dx])
    x0 = -u.Quantity(shape)*dx/2
    self = u.Quantity(img.circle(shape,c=c,r=r)).set_attributes(dx=dx,x0=x0)
    return self

@add_class_method(u.Quantity)
def rectangle(shape,wh,origin=None,dx=1):
    '''create a rectangle
    
    Parameters
    ----------
    shape : tuple
        The shape of the final array
    wh : Quantity, 2 elements
        The shape of the rectangle, [width,height], in the same units as dx
    origin : Quantity, 2 elements
        The x,y location, in dx-like units, of the bottom left corner of the rectangle.
        If None, the rectangle is centered
    dx : Quantity
        The size of one pixel (the same in both axes)
    '''
    n,m = shape
    dx = u.Quantity([dx,dx])
    x0 = -u.Quantity(shape)*dx/2
    w,h = wh
    x = (np.arange(n)-n/2)*dx[0]
    y = (np.arange(m)-m/2)*dx[1]
    x,y = np.meshgrid(x,y)
    ap = np.where((x.abs() < (w/2)) & (y.abs() < (h/2)),1,0)
    ap = u.Quantity(ap).set_attributes(dx=dx,x0=x0)
    if origin is not None:
        if not isinstance(wh,u.Quantity): wh = u.Quantity(wh)
        shift = tuple(int(np.floor(x)) for x in (origin+wh/2)/dx)
        shift = tuple(reversed(shift)) # makes x left-right, y up-down
        ap = np.roll(ap,shift,axis=(0,1)).asQ(ap)
    return ap
    
@add_method(u.Quantity)
def xap(self,ap=None):
    '''multiply a quantity by its aperture.
    
    Parameters
    ----------
    ap : Quantity
        Optional aperture, which will replace the object's ap and
        then multiply by it
    
    Returns
    -------
    Quantity
        The modified object. Note that this is a side-effect, the object
        itself and possibly the object's ap will be changed
    '''
    if hasattr(self,'ap'):
        return (self*self.ap).asQ(self)
    else:
        if ap is not None:
            self.ap = ap
            return self.xap()
        else:
            return self

@add_method(u.Quantity)
def centroid(self,ap=None):
    '''find the centroid of data by center-of-mass
    '''
    self._default_axes_properties()
    if hasattr(self,'ap'):
        ap = self.ap
    else:
        ap = None
    cx,cy = img.centroid(self,ap=ap)
    return self.x0 + self.dx*[cx,cy]

@add_method(u.Quantity)
def to_da(self,unit):
    '''same as Quantity.to, but with dimensionless_angles() eqivalencies
    '''
    return self.to(unit,equivalencies=u.dimensionless_angles()).asQ(self)

@add_method(u.Quantity)
def axto(self,unit):
    '''change the x,y axes scale in a 2d array to the specified unit.
    This changes dx and x0 members in place.
    '''
    self.dx = self.dx.to(unit)
    self.x0 = self.x0.to(unit)
    return self

@add_method(u.Quantity,img.ft)
def ft(self):
    '''(wraps img.ft)
    '''
    self._default_axes_properties()
    n = self.shapexy
    
    r = img.ft(self)
    
    r = u.Quantity(r,self.unit).asQ(self)
    r.dx = 1/(n*self.dx)
    r.x0 = -(n/2)*r.dx
    return r

@add_method(u.Quantity,img.ftinv)
def ftinv(self):
    '''(wraps img.ftinv)
    '''
    self._default_axes_properties()
    n = self.shapexy
    
    r = img.ftinv(self)
    
    r = u.Quantity(r,self.unit).asQ(self)
    r.dx = 1/(n*self.dx)
    r.x0 = -(n/2)*r.dx
    return r

@add_method(u.Quantity,img.zoom)
def zoomby(self,zoom_factor,shape='same',order=3,center=None):
    '''Zoom the image by a factor, resampling to a new pixel scale.
    The new pixel scale is the ratio of the original pixel scale to the zoom factor.
    The routine allows for specifying the shape of the resulting
    image array, and the image is either cropped or zeropadded to fit.
    
    Parameters
    ----------
    a : Quantity, 2-D
        The image
    zoom_factor : scalar (Quantity, float, or int)
        The ratio of the old dx to the new dx,
        i.e. zoom_factor = 2 takes old dx=1 to new dx=0.5
    shape : 2-tuple, str, or None
        None: return a reshaped array that fits all the data in the original image.
            This can get large if the zoom_factor is large.
        'same': returns an array the same shape as the original image.
            The original image is cropped or zero-padded accordingly.
        2-tuple: a desired shape
            The original image is cropped or zero-padded accordingly.
    center : 2-tuple of ints, or Quantity, or None
        2-tuple: pixel indices of zoom center (row,column)
        Quantity: the physical position of the zoom center (with dx like units)
        None: assume the center of the image is the center of the zoom
    
    Returns
    -------
    2-D Quantity
        The zoomed image
    
    (wraps img.zoom)
    '''
    self._default_axes_properties()
    if isinstance(center,(tuple,list)) and all([isinstance(j,int) for j in center]):
        pass
    elif center is None:
        pass
    else:
        pcenter = center
        center = (pcenter-self.x0)/self.dx # pixel address
        center = list(reversed([int(c) for c in center])) # now an integer pixel address in row,column order
    
    r = img.zoom(self,zoom_factor,shape=shape,order=order,center=center)
    
    r = u.Quantity(r,self.unit).asQ(self)
    r.dx = [self.dx[k]/zoom_factor for k in [0,1]]
    if center is None:
        center = self.shapexy//2 # zoomed on the middle
    center = list(reversed(center)) # center is the row,column address, convert to x,y order
    pcenter = [center[k]*self.dx[k] + self.x0[k] for k in [0,1]] # convert to physical center
    r.x0 = [pcenter[k] - r.dx[k]*r.shapexy[k]/2 for k in [0,1]] # physical location of new lower left point
    r.pcenter = pcenter
    r.pcenter_doc = 'physical position of center pixel'
    return r

@add_method(u.Quantity)
def zoomto(self,size,center=None):
    '''crop and zoom in on an image to a new size 
    using interpolation. Size is in physical units (dx-like).
    Center of zoom is the center of the original image unless
    an optional center parameter is provided.

    Parameters
    ----------
    size : scalar Quantity
        physical width of the new image (dx-like units).
        Note: size must be smaller than the width of the starting image
    center : Quantity, 2 elements
        physical x,y center of the zoom
    
    Returns
    -------
    2-D Quantity
        A new Quantity cropped, zoomed in, and interpolated on the original
        so it has the same number of pixels as the original.
    '''
    if center is not None:
        raise NotImplementedError('<zoomto> center not implented yet')
    self._default_axes_properties()
    if isinstance(self.dx, list): self.dx = u.Quantity(self.dx)
    if isinstance(self.x0, list): self.x0 = u.Quantity(self.x0)
    assert self.ndim == 2, 'to zoom, the image must be 2-d'
    assert self.dx[0] == self.dx[1], f'zooming an image requires a square pixel dx[0]==dx[1] (dx={self.dx})'
    olddx = self.dx
    newdx = size/self.shapexy
    zoom_factor = (olddx/newdx).decompose()
    cropsize = (self.shape/zoom_factor).astype(int)
    cropsize = ((cropsize+1)//2)*2
    #cropsize = [int(((x+1)//2)*2) for x in cropsize]
    #cropsize = ((cropsize+1)//2)*2 # next even integer size
    #cropsize = [int(x) for x in cropsize] # make it a list of integers for crop()
    # this changed new dx, so
    self_cropped = self.crop(size=tuple([x for x in cropsize]))
    size = cropsize*olddx
    newdx = (size/self.shapexy)
    zoom_factor = (olddx/newdx).decompose()[0]
    #assert zoom_factor > 1, 'new size must be smaller than the original size'
    r = self_cropped.zoomby(float(zoom_factor),shape=None).zeropad(self.shape)
    return r    
    
@add_method(u.Quantity)
def zeropad(self,n=None,pad=None):
    '''(wraps img.zeropad)
    '''
    self._default_axes_properties()
    n = u.Quantity(n).astype(int)
    r = img.zeropad(self,n=n,pad=pad)
    
    r = u.Quantity(r,self.unit).asQ(self)
    nr = r.shapexy
    r.x0 = [self.x0[k] - (nr[k]-n[k])*r.dx[k]/2 for k in range(self.ndim)]
    return r
zeropad.__doc__ += '\n' + img.zeropad.__doc__
    
@add_method(u.Quantity)
def crop(self,center=None,size=None,fill=0.0):
    '''(wraps img.crop)
    crop a 2D image to size
    
    Parameters
    ----------
    center : 2-tuple of ints or Quantity
        ints: pixel (row,column) that is to be the new center
        Quantity: (x,y) physical location of center (in dx-like units)
        defaults to the middle
    size : 2-tuple of ints or Quantity
        ints: the new shape (#rows,#cols)
        Quantity: (x,y) physical size of the new image (in dx-like units)
        defaults to 1/2 the present size
    fill : float or Quantity (with same units as original array)
        value to fill in if crop goes outside original array

    '''
    n = self.shapexy
    dx = self.dx
    x0 = self.x0
    if center is None:
        center = tuple(int(x) for x in n//2)
    if size is None:
        size = tuple(int(x) for x in n//2)
    
    _center = center
    _size = size
    if isinstance(center,u.Quantity):
        _center = tuple(int(x) for x in list(reversed((center-x0)/dx)))
    if isinstance(size,u.Quantity):
        _size = tuple(int(x) for x in list(reversed(size/dx)))
    if isinstance(fill,u.Quantity):
        _fill = fill.value
    else:
        _fill = fill
    
    r = img.crop(self,_center,_size,fill=_fill)
    
    r = u.Quantity(r,self.unit).asQ(self)
    # repair x0
    center = u.Quantity(list(reversed(_center)))
    size = u.Quantity(list(reversed(_size)))
    r.x0 = x0 + (center - size/2)*dx
    r.dx = dx
    return r

@add_method(u.Quantity)
def blur(self,p,kernelType='block'):
    '''wraps img.blur
    '''
    r = img.blur(a,p,kernelType=kernelType)
    r = u.Quantity(r).asQ(self)*self.unit
    return r
    
@add_method(u.Quantity)
def rms(self,ap=None):
    '''(wraps img.rms)'''
    r = img.rms(self,ap)
    r = u.Quantity(r,self.unit).asQ(self)
    return r
rms.__doc__ += '\n' + img.rms.__doc__

@add_method(u.Quantity)
def planeFit(self,*args,**kwargs):
    '''(wraps img.planeFit)'''
    self._default_axes_properties()
    
    z,piston,tip,tilt = img.planeFit(self,*args,**kwargs)
    
    z = u.Quantity(z,self.unit).asQ(self)
    piston = u.Quantity(piston,self.unit)
    tip = u.Quantity(tip,self.unit)/u.pix
    tilt = u.Quantity(tilt,self.unit)/u.pix
    return (z,dict(zip(['piston','tip','tilt'],[piston,tip,tilt])))
planeFit.__doc__ += '\n' + img.planeFit.__doc__

@add_method(u.Quantity)
def show(self,*args,**kwargs):
    '''(wraps img.show)'''
    if np.iscomplexobj(self):
        print('WARNING: complex data type. Displaying the magnitude')
        z = self.mag.asQ(self)
        z.show(*args,**kwargs)
        return
    self._default_axes_properties()
    if isinstance(self.x0[0],u.Quantity):
        self.x0 = [x.to(y.unit) for x,y in zip(self.x0,self.dx)] # self.x0.to(self.dx.unit) # prioritize dx units
    if 'kind' in kwargs and kwargs['kind'] == 'surface':
        plt.figure()
        ax = plt.axes(projection='3d') # plt.gca(projection='3d') depricated
        self._default_axes_properties()
        n = self.shapexy.value
        x0 = self.x0
        dx = self.dx
        if (n<50).any():
            zoom_factor = 50//n.min() + 1
            z = img.zoom(self,zoom_factor)
        else:
            z = self
        z = img.congrid(z,(50,50)) # 50x50 is the default size of linspace
        z = u.Quantity(z).asQ(self)*self.unit
        z.dx = [dx[k]*n[k]/50 for k in [0,1]]
        z.x0 = [-q*(50//2) for q in z.dx]
        x,y = [np.linspace(x0[k],x0[k]+n[k]*dx[k]) for k in [0,1]]
        x,y = np.meshgrid(x,y)
        surf = ax.plot_surface(x,y,z)
        ax.set_xlabel('X {}'.format(dx[0].unit))
        ax.set_ylabel('Y {}'.format(dx[1].unit))
        ax.set_zlabel('Z {}'.format(self.unit))
        name = ''
        units = str(self.unit)
        if hasattr(self,'name'):
            name = self.name
        if units != '':
            name += ', '+units
        ax.set_title(name)
        ax.view_init(elev=50,azim=-100)
        plt.draw()
        return
    elif 'kind' in kwargs:
        kwargs.pop('kind')
    
    if hasattr(self,'ismatrix') and self.ismatrix==True:
        self.x0 = [0,self.shape[0]]
        self.dx = [1,-1]
        kwargs['origin'] = 'upper'
    dimg.show(self,*args,**kwargs)
show.__doc__ += '\n' + dimg.show.__doc__

@add_method(u.Quantity)
def plot(self,*args,**kwargs):
    '''Plot a Quantity if it is one-dimensional
    '''
    if self.ndim > 1:
        self.show(**kwargs)
        return
    self._default_axes_properties()
    self.x0 = self.x0.to(self.dx.unit) # prioritize dx units
    
    fig = kwargs.pop('fig',None)
    title = kwargs.pop('title',None)
    if title is None and hasattr(self,'name'): title = self.name
    xlabel = kwargs.pop('xlabel','') + ', '
    xlabel = (xlabel + str(self.dx.unit)).strip(', ')
    ylabel = kwargs.pop('ylabel','') + ', '
    ylabel = (ylabel + str(self.unit)).strip(', ')
    str(self.unit)
    set_fig(fig)
    x0 = self.x0
    dx = self.dx
    if not x0.isscalar: x0 = x0[0]
    if not dx.isscalar: dx = dx[0]
    n = self.size
    x = np.arange(0,n)*dx + x0
    if np.iscomplexobj(self):
        yr = self.real
        yi = self.imag
        label = kwargs.pop('label','')
        plt.plot(x,yr,label=label+' real part',**kwargs)
        plt.plot(x,yi,label=label+' imag part',**kwargs)
        plt.legend()
    else:
        plt.plot(x,self,**kwargs)
        if 'label' in kwargs: plt.legend()
    ax = plt.gca()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    plt.grid(True)

@add_method(u.Quantity)
def point(self,p):
    '''Return the value a position p=(x,y)
    The Quantity must be 2-D with dx and x0 attributes
    
    Parameters
    ----------
    p : Quantity of length n, where n is the number of dimensions
        The position of the point. It must have dx-like units
    '''
    assert self.ndim == 2,'point only works on 2-D arrays'
    if not isinstance(p,u.Quantity): p = u.Quantity(p)
    x,y = p
    ny,nx = self.shape
    dx,dy = self.dx
    x0,y0 = self.x0
    ix = int((x-x0)/dx)
    iy = int((y-y0)/dy)
    return self[iy,ix]

@add_method(u.Quantity)
def slice(self,x=None,y=None):
    '''Return a 1-D slice of a 2-D array
    
    Parameters
    ----------
    x : Quantity
        The x position of a vertical slice, or a (min,max) range of x values in a horizontal slice
    y : Quantity
        The y position of a horizontal slice, or a (min,max) range of y values in a vertical slice
    '''
    ny,nx = self.shape
    dx,dy = self.dx
    x0,y0 = self.x0
    if x is None: x = u.Quantity([x0,x0+nx*dx]) # default sliced axis to full range
    if y is None: y = u.Quantity([y0,y0+ny*dy])
    if not isinstance(x,u.Quantity): x = u.Quantity(x) # allows tuples and lists
    if not isinstance(y,u.Quantity): y = u.Quantity(y)
    assert x.isscalar or y.isscalar,'slice can only be 1-D'
    if x.isscalar:
        kind = 'vertical'
        if len(y) == 1: # let ymax be the limit
            ymin = y[0]
            ymax = y0 + ny*dy
        else:
            ymin,ymax = y
        y = np.arange(ymin.value,ymax.value,dy.value)*dy.unit
    elif y.isscalar:
        kind = 'horizontal'
        if len(x) == 1: # let xmax be the limit
            xmin = x[0]
            xmax = x0 + nx*dx
        else:
            xmin,xmax = x
        x = np.arange(xmin.value,xmax.value,dx.value)*dx.unit

    indx = ((x-x0)/dx).astype(int)
    indy = ((y-y0)/dy).astype(int)
    r = self[indy,indx].asQ(self)
        
    if kind == 'horizontal':
        r.dx = dx
        r.x0 = xmin
        r.slice_info = {'kind':kind,'y':y}
    elif kind == 'vertical':
        r.dx = dy
        r.x0 = ymin
        r.slice_info = {'kind':kind,'x':x}
    
    return r

@add_method(u.Quantity)
def integral(self):
    '''Integrate the Quantity over all dimension using Reimann sum integration
    .. math::
    
        \\Sum Q_{ij\\ldots} \\Delta x_1 \\Delta x_2 \\ldots
    
    '''
    ndim = self.ndim
    dv = self.dx.value.prod()*self.dx.unit**ndim
    return(self.sum()*dv)
    
@add_method(u.Quantity)
def abs(self):
    '''take the absolute value of a complex Quantity
    Copies all the original object's properties to the result
    '''
    return np.abs(self).asQ(self)

@add_method(u.Quantity)
def abs2(self):
    '''take the absolute value squared of a complex Quantity
    '''
    return (np.abs(self)**2).asQ(self)

@add_method(u.Quantity)
def normalized(self,norm='max'):
    '''normalize the Quantity
    
    Parameters
    ----------
    norm : str
        'max' or 'peak' normalize to the peak value
        'sum' normalize so the sum = 1
        'integral' normalize so the integral = 1
    
    Returns
    -------
    Returns the normalized Quantity; does not modify the original
    Quantity.
    '''
    if norm in ['max','peak']:
        r = (self/self.max()).asQ(self)
    elif norm == 'sum':
        r = (self/self.sum()).asQ(self)
    elif norm == 'integral':
        r = (self/self.integral()).asQ(self)
    else:
        raise ValueError('invalid normalization %s'%norm)
    return r
        
@add_method(u.Quantity)
def set_attributes(self,**kwargs):
    '''Set or change any attributes on the Quantity.
    Attributes are given by keyword arguments in the call.
    
    Example
    -------
    >>> q.set_attributes(name='foo',wavelength=2*u.micron,bling=False)
    
    Returns
    -------
    Quantity
        The initial object, but with attributes set.
        Note: Side-effect is to -change- the attributes of self (not create a copy).
    '''
    self.__dict__.update(kwargs)
    return self

@add_method(u.Quantity)
def set(self,**kwargs):
    '''Set or change any attributes on the Quantity.
    Attributes are given by keyword arguments in the call.
    
    Example
    -------
    >>> q.set(name='foo',wavelength=2*u.micron,bling=False)
    
    Returns
    -------
    Quantity
        The initial object, but with attributes set.
        Note: Side-effect is to -change- the attributes of self (not create a copy).
    
    Note
    ----
    Equivalent to set_attributes
    '''
    return self.set_attributes(**kwargs)
    
@add_method(u.Quantity)
def oft(self,L=None):
    '''Optical Fourier transform.
    Fraunhofer approximation to the Huygens-Fresnel integral,
    propagating a complex optical wavefront to the far-field.
    
    Example
    -------
    >>> wf_ff = oft(wf)
    
    Parameters
    ----------
    self : Quantity
        a 2D wavefront having attributes dx, x0, and wavelength
    
    L : Quantity
        optional propagation distance, a scalar with length units.
        If L is provided, the return wavefront has dx and x0 in
        length units (i.e. it is a focal plane at distance L)
        as opposed to in angle units.
    
    Returns
    -------
    Quantity
        The propagated wavefront. It's dx and x0 parameters will
        depend on the presence of the L argument: dx and x0
        will have units of plate-scale (length) if L is provided,
        otherwise dx and x0 will have units of angle.
    
    See Also
    --------
    fresnel, fresnelft
    '''
    n = self.shapexy
    _default_propagation_properties(self,warn=True)
    
    b = self.ft()
    
    # calculate the far-field pixel size
    b.asQ(self)
    dx = self.wavelength/(n*self.dx)
    dx = dx.to('rad',equivalencies=u.dimensionless_angles())
    if L is not None:
        dx *= L/u.rad
    b.x0 = -(n//2)*dx
    b.dx = dx
    b.algo = 'oft'
    name = self.name.rstrip()+' ' if hasattr(self,'name') else ''
    name = name + 'propagated to far-field'
    if L is not None:
        name += ' (L = {:.3g})'.format(L)
        b.L = L
    b.name = name
    
    return b

@add_method(u.Quantity)
def fresnelft(self,L=None,ABCD=None,check=True):
    '''Propagate a wavefront from z=0 to z=L.
    Best for use where L > ~2% of Rayleigh range.
    For shorter distances, use :method:`fresnel` instead.
    
    Example
    -------
    >>> wfL = wf0.fresnelft(L=100.*u.m)
        
    Parameters
    ----------
    self : Quantity
        A complex wavefront defined at z=0 with properties
        dx, x0, and wavelength
        
    L : Quantity
        Propagation distance, with units of length
    
    ABCD : tuple
        Optional ABCD matrix parameters describing the optical system.
        This is a 4-tuple of Quantities with units (dimensionless,length,1/length,dimensionless).
        If given, the second element (B) supercedes L
        
    Returns
    -------
    Quantity
        The propagated wavefront at z=L
    
    Notes
    -----
    A,B,C,D - are the ABCD matrix parameters of the intervening
    optical system between 0 and L (see Siegman, Lasers, Ch. 15)[1]_
    Examples:
        free space: A=1,     B=L, C=0,    D=1
        lens:       A=1-L/f, B=L, C=-1/f, D=1
    
    See Also
    --------
    fresnel, oft
    
    References
    ----------
    .. [1] A. E. Siegman, "Lasers", 1986.
    '''
    _default_propagation_properties(self,warn=True)
    if ABCD is None:
        ABCD = (1,L,0/u.m,1)
    A,B,C,D = ABCD
    if L is None:
        L = B
    n = self.shapexy
    lam = self.wavelength
    i = 1j
    du = self.dx
    x,y = [np.arange(-n[j]/2,n[j]/2)*du[j] for j in [0,1]]
    x,y = np.meshgrid(x,y)
    r2 = x**2+y**2
    ph = 2*np.pi*(A/(2*B*lam))*r2

    wfL = (self*np.exp(i*ph)).ftinv()
    
    dx = B*lam/(n*du)
    x,y = [np.arange(-n[j]/2,n[j]/2)*dx[j] for j in [0,1]]
    x,y = np.meshgrid(x,y)
    r2 = x**2+y**2    
    phL = 2*np.pi*(D/(2*B*lam))*r2
    wfL *= i*np.exp(-i*phL)
    wfL = u.Quantity(wfL,self.unit)
    wfL.wavelength = lam
    wfL.dx = dx.to(u.mm)
    wfL.x0 = -(n//2)*wfL.dx
    name = self.name.rstrip()+' ' if hasattr(self,'name') else ''
    wfL.L = L
    wfL.ABCD = (A,B,C,D)
    wfL.algo = 'fresnelft'
    wfL.name = name + 'after fresnelft-propagating {:.3g}'.format(L)
    return wfL

@add_method(u.Quantity)
def fresnel(self,L=None):
    '''Fresnel's approximation to Huygens' integral
    for a complex wavefront propagating paraxially from a plane at
    z=0 to a plane at z=L. The Fresnel operator works best for
    very short propagations (L < ~2% of Rayleigh range). For
    longer propagations, use :method:`fresnelft` or method:`oft`
    Reference: Siegman[1]_, Ch 16, eqn (79)
    
    Example
    -------
    >>> wfL = wf.fresnel(L=1.*u.m)
    
    Parameters
    ----------
    self : Quantity
        A complex wavefront defined at z=0 with properties
        dx, x0, and wavelength
        
    L : Quantity
        The propagation distance, a scalar with units of length
    
    Returns
    -------
    Quantity
        A complex wavefront after propagation to z=L.
    
    See Also
    --------
    fresnelft, oft
    
    References
    ----------
    .. [1] A. E. Siegman, "Lasers", 1986.

    '''
    _default_propagation_properties(self,warn=True)
    i = 1j
    lam = self.wavelength
    k = 2*np.pi/lam
    n = self.shapexy
    
    fwf0 = self.ft()
    
    dk = 2*np.pi*fwf0.dx
    k0 = 2*np.pi*fwf0.x0
    kf = k0 + n*dk
    x,y = [np.arange(-n[j]//2,n[j]//2)*dk[j] for j in [0,1]]
    kx,ky = np.meshgrid(x,y)
    kperp2 = kx*kx + ky*ky
    kernel = np.exp(i*kperp2*L/(2*k))
    
    wfL = (fwf0*kernel).asQ(fwf0).ftinv()
    
    wfL.L = L
    wfL.algo = 'fresnel'
    name = self.name.rstrip()+' ' if hasattr(self,'name') else ''
    wfL.name = name + 'after fresnel-propagating {:.3g}'.format(L)
    return wfL
    
@add_method(u.Quantity)
def prop_check(self,D=None):
    lam = self.wavelength
    if D is None:
        D = (self.dx*self.shapexy).max()
    RayleighRange = (D**2/lam).to(u.m)
    L = RayleighRange/10.
    FresnelZone = np.sqrt(lam*L).to(u.mm)
    print('The exit aperture diameter is ~{}'.format(D))
    print('The RayleighRange is {}'.format(RayleighRange))
    print('at 10% of the Rayleigh range the Fresnel zone is {}'.format(FresnelZone))
    print('for 0 < L < 0.02 x RR, use fresnel(...)')
    print('for 0.02 x RR < L < RR, use fresnelft(...)')
    print('if L > RR, use oft(...)')
    return

@add_method(u.Quantity)
def enclosing_diameter(self):
    '''Find the smallest circle that encloses all the non-zero points.
    This is useful for finding the diameter of apertures.
    '''
    x,y = (u.Quantity(np.nonzero(self)).T*self.dx + self.x0).T
    r2 = (x-x.mean())**2 + (y-y.mean())**2
    D = 2.*np.sqrt(r2.max())
    return D

@add_method(u.Quantity)
def pixel_to_point(self,ixiy):
    '''find the axis point location of pixel number
    
    Parameters
    ----------
    ixiy : list of integers
        indices into the array, e.g. [ix,iy]
        Position <Quantity>.x0 is the middle of the [0,0] pixel.
        Pixels are [fastindex,slowindex], which is reverse of array indexing.
        Pixels are numbered starting at 0,0 for lower left corner.
        There is no assumption that pixel numbers are in bounds of the array.
    
    Returns
    -------
    Quantity
        The point location
    '''
    return self.x0 + ixiy*self.dx

@add_method(u.Quantity)
def point_to_pixel(self,xy):
    '''find the array index location that contains the position
    
    Parameters
    ----------
    xy : Quantity
        the point location, e.g.[x,y]
        xy must be a Quantity with units of the same kind as
        attributes <Quantity>.dx and x0.
    
    Returns
    -------
    list of integers
        Pixels are numbered starting at 0,0 for lower left corner.
        There is no assumption that returned pixel number(s) are in bounds of the array.
        Position <Quantity>.x0 is the middle of the [0,0] pixel.
        Pixel numbers are [fastindex,slowindex], which is reverse of array indexing.
    '''
    return [int(x) for x in (xy - self.x0)/self.dx]

# -------- modified methods -----------

# unary ops

if not hasattr(u.Quantity,'up_copy'):
    mprint('<q_helper> changing u.Quantity.copy')
    u.Quantity.up_copy = getattr(u.Quantity,'copy')
def __copy__(self,*args,**kwargs):
    return self.up_copy().asQ(self)
setattr(u.Quantity,'copy',__copy__)

if not hasattr(u.Quantity,'up_neg'):
    mprint ('<q_helper> changing u.Quantity.__neg__')
    u.Quantity.up_neg = getattr(u.Quantity,'__neg__')
def __neg__(self):
    return self.up_neg().asQ(self)
setattr(u.Quantity,'__neg__',__neg__)

if not hasattr(u.Quantity,'up_pow'):
    mprint('<q_helper> changing u.Quantity.__pow__')
    u.Quantity.up_pow = getattr(u.Quantity,'__pow__')
def __pow__(self,p):
    return self.up_pow(p).asQ(self)
setattr(u.Quantity,'__pow__',__pow__)    

if not hasattr(u.Quantity,'up_clip'):
    mprint('<q_helper> changing u.Quantity.clip')
    u.Quantity.up_clip = getattr(u.Quantity,'clip')
def clip(self,min=None,max=None,out=None,**kwargs):
    return self.up_clip(min=min,max=max,out=out).asQ(self)
setattr(u.Quantity,'clip',clip)    
    
# binary ops
ops = ['add','sub','mul','truediv']

code = '''
if not hasattr(u.Quantity,'up_***'):
    mprint('<q_helper> changing u.Quantity.__***__')
    u.Quantity.up_*** = getattr(u.Quantity,'__***__')
    def __***__(self,other):
        if isinstance(other,u.Quantity):
            return self.up_***(other).asQ(self)
        else:
            r = self.up_***(other)
            if isinstance(r,u.Quantity):
                r = r.asQ(self)
            return r
    setattr(u.Quantity,'__***__',__***__)
'''

for name in ops:
    excode = code.replace('***',name)
    exec(excode)

# ---------- fits helpers ---------
isscalar = lambda x: (isinstance(x,u.Quantity) and x.isscalar) or isinstance(x,(int,float,complex))

@add_method(u.Quantity)
def to_fits(self,filename,**kwargs):
    '''write a Quantity to a fits file
    '''
    data = self.value
    hdu = fits.PrimaryHDU(data)
    hdu.header.append(('name',self.name))
    hdu.header.append(('_unit',f'{self.unit:FITS}'))
    ndim = self.ndim
    for k in range(ndim):
        dx = self.dx[k]
        hdu.header.append((f'dx_{k}',dx.value,f'{dx.unit:FITS}'))
    for k in range(ndim):
        x0 = self.x0[k]
        hdu.header.append((f'x0_{k}',x0.value,f'{x0.unit:FITS}'))
    keys = [key for key in self.__dict__.keys() if key not in ['_unit','name','dx','x0']]
    for key in keys:
        val = getattr(self,key)
        if isinstance(val,str):
            hdu.header.append((key,val))
        elif isscalar(val):
            if isinstance(val,u.Quantity):
                hdu.header.append((key,val.value,f'{val.unit:FITS}'))
            else:
                hdu.header.append((key,val))
    overwrite = kwargs.get('overwrite',False)
    hdu.writeto(filename,overwrite=overwrite)

@add_class_method(u.Quantity)
def from_fits(filename,**kwargs):
    hdu = fits.open(filename)
    data = hdu[0].data
    hdr = hdu[0].header
    unit = hdr['_unit']
    self = u.Quantity(data,unit)
    self.name = hdr['name']
    ndim = self.ndim
    dxv = []
    for k in range(ndim):
        dx = hdr[f'dx_{k}']
        dx_unit = hdr.comments[f'dx_{k}']
        dxv.append(dx)
    self.dx = u.Quantity(dxv,dx_unit)
    x0v = []
    for k in range(ndim):
        x0 = hdr[f'x0_{k}']
        x0_unit = hdr.comments[f'x0_{k}']
        x0v.append(x0)
    self.x0 = u.Quantity(x0v,x0_unit)
    keys = [key.lower() for key in hdr.keys() if key not in ['SIMPLE', 'BITPIX', 'NAXIS', 'NAXIS1', 'NAXIS2', 'EXTEND', 'NAME', '_UNIT']]
    for key in keys:
        if key.startswith('dx_') or key.startswith('x0_'):
            continue
        val = hdr[key]
        unit = hdr.comments[key]
        unit = u.Unit(unit,parse_strict='silent')
        if isinstance(unit,u.UnrecognizedUnit):
            setattr(self,key,val)
        elif isscalar(val):
            setattr(self,key,u.Quantity(val,unit))
        elif isinstance(val,str):
            setattr(self,key,val)
    return self

@add_method(u.Quantity)
def ds9(self):
    assert self.ndim == 2
    self._default_axes_properties()
    if not hasattr(self,'name'): self.name = '(unnamed)'
    self.object = self.name
    bunit = f'{self._unit:FITS}'
    if bunit == '': bunit = 'ADU'
    self.bunit = bunit
    self.crpix1 = 1
    self.crpix2 = 1
    self.crval1 = self.x0[0].value
    self.crval2 = self.x0[1].value
    self.cdelt1 = self.dx[0].value
    self.cdelt2 = self.dx[1].value
    self.ctype1 = f'{self.x0[0].unit:FITS}'
    self.ctype2 = f'{self.x0[1].unit:FITS}'
    filename = 'tempds9.fits'
    self.to_fits(filename,overwrite=True)
    try:
        d = pyds9.DS9()
    except:
        print(f'{tcolor.warning} Apparently module pyds9 is not available.{tcolor.end}')
        print(f'You can still view the image with an external ds9 viewer on file {filename}')
        return
    d.set(f'file {filename}')
    d.set('view physical no')

# ---------- matplotlib helpers ----------
def get_figs():
    '''Get a dictionary of all the figures that have names,
    keyed by name
    '''
    nums = plt.get_fignums()
    figs = [plt.figure(num) for num in nums]
    r = [(x.name,x) for x in figs if hasattr(x,'name')]
    return dict(r)

def get_fig(f):
    '''Retrieve an existing figure by name, number, or Figure instance
    '''
    namedfigs = get_figs()
    fignums = plt.get_fignums()
    if isinstance(f,Figure):
        if f.number in fignums:
            return f
        else:
            return None
    elif isinstance(f,int):
        if f in fignums:
            f = plt.figure(f)
            return f
        else:
            return None
    elif isinstance(f,str):
        if f in namedfigs:
            return namedfigs[f]
        else:
            return None

def set_fig(name):
    '''Retrieve an existing, or create a new figure by name.
    '''
    fig = get_fig(name)
    if not fig:
        fig = plt.figure()
        fig.name = name
    plt.figure(fig.number)
    return fig

# --------- local suport functions ----------

def nameof(var):
    callers_local_vars = inspect.currentframe().f_back.f_locals.items()
    return [k for k, v in callers_local_vars if v is var][0]

#---------- tests ------------

def oft_test():
    global a,b,p
    n = 1024
    dx = u.Quantity([0.1,0.1],'mm')
    x0 = -dx*n/2
    
    name = 'Initial wavefront: flattop, circular aperture'
    a = u.Quantity(img.circle((n,n),r=n/10.)).set_attributes(name=name,dx=dx,x0=x0)
    a.pprint()
    a.show()
    
    name = 'Far-Field image'
    b = oft(a).set_attributes(name=name)
    b.pprint()
    p = (b.mag**2).crop(size=(100,100))
    p.dx,p.x0 = [x.to('urad') for x in [p.dx,p.x0]]
    p.show(scale=('pow',.4))
    p.show(kind='surface')
    p.pprint()
    globals().update(locals())

def fresnelft_test():
    w = u.Quantity.circle((256,256),r=50)
    w.wavelength = 0.5*u.micron
    w.dx = [1.,1.]*u.mm
    w.x0 = w.x0*w.dx
    RR = 31752.*u.m
    f = [.01,.02,.04,.05,.07,.1,.2,.5]
    r = [w.fresnelft(L=x*RR).mag.zoomto(256*u.mm) for x in f]
    rz = [x.zeropad((256,256)) for x in r]
    rzn = [x*w.sum()/x.sum() for x in rz]
    m = np.block(rzn)
    m.name = '{} x RR'.format(f)
    m.show()
    globals().update(locals())

def prop_test():
    w = u.Quantity.circle((256,256),r=50)
    w.wavelength = 0.5*u.micron
    w.dx = [1.,1.]*u.mm
    w.x0 = w.x0*w.dx
    RR = 31752.*u.m
    f = [.005,.01,.05,.1,.5,.8]
    r1 = [w.fresnelft(L=x*RR).mag.zoomto(256*u.mm) for x in f]
    r2 = [w.fresnel(L=x*RR).mag for x in f]
    r1 = [x.zeropad((256,256)) for x in r1]
    r1n = [x*w.sum()/x.sum() for x in r1]
    r1n[0] /= 2
    r2n = [x*w.sum()/x.sum() for x in r2]
    m = u.Quantity(np.block([r1n,r2n]))
    m.name = '{} x RR'.format(f)
    fig = plt.figure(figsize=[10.,10./(len(f)/2)]) #[10.94, 3.64])
    plt.axes([0,0,1,1])
    plt.axis('off')
    m.clip(0.,1.).asQ(m).show(fig=fig.number)
    for alg,y in zip(['Fresnelft','Fresnel'],[220,480]):
        plt.text(20,y,alg,color='white')
    for i,ff in enumerate(f):
        x = i*256 + 128
        plt.text(x,35,'{:.3g}'.format(ff),horizontalalignment='center',color='white')
    globals().update(locals())

# examples from https://realpython.com/primer-on-python-decorators/

def do_twice(func):
    #@wraps(func)
    def wrapper_do_twice(*args, **kwargs):
        func(*args, **kwargs)
        return func(*args, **kwargs)
    return wrapper_do_twice

@do_twice
def return_greeting(name):
    print("Creating greeting")
    return f"Hi {name}"

