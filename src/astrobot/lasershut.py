'''lasershut.py - laser shutdown airmass plots
Given the target list and laser shutdown file for each target from space command,
provide a tool to plot airmass with green and red parts of the curves to indicate
allowed and disallowed laser propagation times
'''
import os
import numpy as np
import pandas as pd
import dateutil.parser as dparser
import pytz
from collections import deque
import astropy.units as u
from astropy.coordinates import SkyCoord, EarthLocation, AltAz
from astropy.time import Time

from astrobot.tcolor import tcolor,gprint,wprint,eprint

import matplotlib.pyplot as plt

plt.ion()

top = os.path.expanduser('~/Desktop/laser shutdowns')

class TargList(object):
    '''an object that contains the night's target list
    and associated set of target shutdown databases
    '''
    def __init__(self,tag = '2021Jul17UT',site='lick'):
        fn = os.path.join(top,tag+'.txt')
        df = pd.read_csv(fn,sep=' ',header=None)
        df.columns = ['ind','name','ra','dec']
        df.set_index('ind',inplace=True)
        site = EarthLocation.of_site(site) # lick.geodetic to view
        date = dparser.parse(tag,fuzzy=True)
        self.tag = tag
        self.site = site
        self.date = Time(date)
        self.df = df
        
    def airmass(self,obj,fig=None,color='green'):
        '''generate the airmass data for an object.
        
        Argument:
        ---------
        obj : int or str
            If an int, use that as the index into the target
            database. If a string, lookup up the object by name
            in the target database.
        '''
        if isinstance(obj,list):
            clrs = deque(['g','b','c','m','k','orange'])
            fig = None
            for ob in obj:
                self.airmass(ob,fig=fig,color=clrs[0])
                clrs.rotate(-1)
                fig = plt.gcf().number
            return
        elif isinstance(obj,int):
            r = self.df.loc[obj]
        elif isinstance(obj,str):
            r = self.df[self.df['name']==obj]
            if len(r) > 0:
                r = r.iloc[0]
            else:
                eprint(f'object {obj} not found in database')
                return
        fn = os.path.join(top,self.tag,r['name']+'.lsm') # file name of shutdown times list
        sdb = pd.read_csv(fn,sep=' ',header=None,skiprows=2)
        sdb.columns = ['start','stop','on','off']
        epoch = Time(1970,format='decimalyear')
        
        ra,dec = r.ra,r.dec
        c = SkyCoord(ra,dec,unit=(u.deg,u.deg))
        midnight = self.date #+ 1*u.day # tag date is the UT date
        timezone = self.site.info.meta['timezone']
        midnight = utc_from_local(midnight,tz=timezone) #'America/Los_Angeles')
        plt.figure(fig)
        label = f'{r.name}: {r["name"]}'
        lastrow = len(sdb)-1
        for ind,row in sdb.iterrows():
            start = row['start']*u.second
            stop = row['stop']*u.second
            n = max(2,int( (stop-start)/(1*u.minute) ))
            seconds = np.linspace(start,stop,n)
            delta_midnight = (seconds+epoch-midnight).to(u.hour)
            frame = AltAz(obstime=midnight+delta_midnight,location=self.site)
            amass = c.transform_to(frame).secz
            amass = u.Quantity([x if x>0 else np.nan for x in amass])
            if ind != 0: label=None
            plt.plot(delta_midnight,amass,color=color,label=label)
            
            if ind != lastrow:
                start = row['stop']*u.second
                stop = start + row['off']*u.second
                n = max(2,int( (stop-start)/(1*u.minute) ))
                seconds = np.linspace(start,stop,n)
                delta_midnight = (seconds+epoch-midnight).to(u.hour)
                frame = AltAz(obstime=midnight+delta_midnight,location=self.site)
                amass = c.transform_to(frame).secz
                amass = u.Quantity([x if x>0 else np.nan for x in amass])
                plt.plot(delta_midnight,amass,'r',linewidth=3)
            
        plt.legend()
        if fig is None:
            ax = plt.gca()
            ax.set_xlim(-4,5)
            ax.set_ylim(0,3)
            plt.grid('on')
            date = self.date.strftime('%Y-%m-%d')
            plt.xlabel(f'hours from mignight {date}')
            plt.ylabel('airmass')
            plt.title(f'Laser Shutdowns')
        
def utc_from_local(time,ambiguous='DST',tz='America/Los_Angeles'):
    '''convert local time to utc time.
    
    Note:
    -----
    There is an ambiguous one hour of local time in the fall at the
    end of daylight savings time - local time 1-2 am is repeated, once
    as DST and again as PST.
    '''
    if isinstance(time,Time):
        time = time.to_datetime()
    local = pytz.timezone(tz)
    is_dst = {'DST':True,'PDT':True,'PST':False,'ST':False}.get(ambiguous) # defaults to None
    try:
        local_dt = local.localize(time, is_dst=is_dst)
    except pytz.AmbiguousTimeError:
        wprint(f'time {time} is ambigous with respect to standard or daylight savings time, assuming DST')
        local_dt = local.localize(time, is_dst=True)
    return Time(local_dt.astimezone(pytz.utc))

