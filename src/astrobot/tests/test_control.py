# test_control.py
from astrobot.control import *

# Spectrum tests
def test_Spectrum(f0=10*u.Hz,damping = 0.1,figs=[None,None],plot=True):
    fig1,fig2 = figs
    i = 1j
    pi = np.pi
    w0 = 2*pi*f0
    xi = complex(damping)
    s1 = -w0*(xi + i*np.sqrt(1-xi**2))
    s2 = -w0*(xi - i*np.sqrt(1-xi**2))
    f1 = s1/(2*pi*i)
    f2 = s2/(2*pi*i)
    a = ( -1/(2*pi)**2 )*u.Hz
    Ff = Spectrum(lambda f,a=a,f1=f1,f2=f2: a/((f-f1)*(f-f2)),
                   name='F(f) - f domain',
                   latex_name = '$F(f)$',
                   latex_formula = '$-4 \\pi^2 / (f-f_1)(f-f_2)$',
                   poles = [f1,f2],
                   zeros = [],
                   )
    
    Fs = Spectrum(lambda s,s1=s1,s2=s2: 1*u.Hz/((s-s1)*(s-s2)),
                   name='F(s) - s domain',
                   latex_name = '$F(s)$',
                   latex_formula = '$1 / (s-s_1)(s-s_2)$',
                   poles = [s1,s2],
                   zeros = [],
                   )
    if plot:
        Ff.plot(n=10000,title='F(f) - f domain',fig=fig1)
        fig1 = plt.gcf()
        Fs.plot(n=10000,title='F(s) - s domain',fig=fig2)
        fig2 = plt.gcf()

    t,x = Ff.to_signal()
    if plot:
        time_domain_plot((t,x.real),title = '${\\cal F}^{-1}\{$'+Ff.name+'$\}(t)$')
    t,x = Fs.to_signal()
    if plot:
        time_domain_plot((t,x.real),title = '${\\cal F}^{-1}\{$'+Fs.name+'$\}(t)$')

    if plot:
        if matplotlib.get_backend() in ['Qt5Agg','Qt4Agg','TkAgg']:
            tile_figures.tile_figures()
    globals().update(locals())

# TransferFunction tests
def test_TF():
    """Open and closed loop plots for several gains
    """

    if 'Hol' not in globals():
        globals().update(AOdefs())
        
    gains = np.arange(.1,1.1,.1)
    
    for gain in gains:
        (gain*Hol).bode_plot(label='g={:.1f}'.format(gain),title='$H_{OL}$')
        plt.pause(.001)
    
    for gain in gains:
        (gain*Hol).nyquist_plot(label='g={:.1f}'.format(gain),title='$H_{OL}$')
        plt.pause(.001)

    for gain in gains:
        (gain*Hol).nichols_chart(label='g={:.1f}'.format(gain),title='$H_{OL}$')
        plt.pause(.001)

    for gain in gains:
        Hcl(g=gain).abs().plot(label='g={:.1f}'.format(gain),fig='Hcl',title='$H_{CL}$')
        plt.pause(.001)
    
    df = pd.DataFrame(columns = ['gain','stability'])
    stability = []
    for gain in gains:
        stability.append('stable' if (gain*Hol).is_stable else 'unstable')
    
    df['gain'] = gains
    df['stability'] = stability
    df.index = ['']*len(df)
    print(df)

def test_TF2():
    H = TransferFunction(lambda s: 1/s)
    g = 1*u.kg
    G = g*H
    G2 = H*g
    print(G(s)==G2(s))
    q = (1+1j)*u.s
    Q = q - H
    Q2 = H - q
    print(Q(s) == -Q2(s))
    globals().update(locals())
    
def examples():
    # examples
    TF = TransferFunction
    # using string definitions
    HL = TF('(1-exp(-s*T))/(s*T)')
    HR = TF('exp(-s*T)')
    Hdel = TF('exp(-s*tau)',tau=800*u.us)
    HC = TF('1/(1-gamma*exp(-s*T))',gamma=1)
    HDM = TF('(1-exp(-s*T))/(s*T)')
    Hol = HDM*HC*Hdel*HR*HL
    Hol = TF('HDM(s)*HC(s)*Hdel(s)*HR(s)*HL(s)',HDM=HDM,HC=HC,Hdel=Hdel,HR=HR,HL=HL)
    Hcl = TF('1/(1+g*Hol(s))',g=0.5,Hol=Hol)
    
    HL.name='Wavefront sensor stare (sample and hold)'
    HR.name='Sensor readout delay'
    Hdel.name='Compute delay'
    HC.name='Feedback control law'
    HDM.name='Deformable mirror (sample and hold)'
    Hol.name = 'AO open loop transfer function'
    Hcl.name = 'AO closed loop transfer function'
    
    # using lambda definitions, with keywords for parameters:
    T = 1*u.ms
    tau = 800*u.us
    HL = TF(lambda s,T=T: (1-exp(-s*T))/(s*T))
    HR = TF(lambda s,T=T: exp(-s*T))
    Hdel = TF(lambda s,tau=tau: exp(-s*tau))
    HC = TF(lambda s,gamma=1,T=T: (1/(1-gamma*exp(-s*T))))
    HDM = TF(lambda s,T=T: (1-exp(-s*T))/(s*T))
    Hol = HDM*HC*Hdel*HR*HL
    Hol = TF(lambda s,T=T,gamma=1,g=.5,tau=tau: g*HDM(s,T=T)*HC(s,gamma=gamma,T=T)*Hdel(s,tau=tau)*HR(s,T=T)*HL(s,T=T))
    Hcl = TF(lambda s,T=T,gamma=1,g=.5,tau=tau: 1/(1+g*Hol(s,T=T,gamma=gamma,g=1,tau=tau)))
    # you can change the attribute in the TF, then call it
    g = 1
    f = 10*u.Hz
    s = 2*pi*i*f
    # definition with full set of internal parameters
    gH = TF(lambda s,g=g,T=1*u.s: g*s*T)
    # using the internal defaults:
    gH(s)
    # <Quantity 0.+62.83185307j>
    # changing the internal default
    gH.g = 2
    gH(s)
    # <Quantity 0.+125.66370614j>
    # or pass it in as a keyword
    gH(s,g=3)
    # <Quantity 0.+188.49555922j>
    
    # you can define intermediate functions and pass them in
    def zi(s,T): return exp(-s*T)
    HL = TF(lambda s,T=T: (1-zi(s,T))/(s*T))
    
    # using lambda definitions, rely on the outer scope to supply parameters
    T = 1*u.ms
    zi = lambda s: exp(-s*T)
    HL = TF(lambda s: g*(1-zi(s))/(s*T)) # no internal parameters
    g = 0.5
    HL(s)
    # <Quantity 0.49967108-0.0157028j>
    g = 1
    HL(s)
    # <Quantity 0.99934216-0.03140559j>
    
    #restore HL so we don't break Hol and Hcl defined before
    HL = TF(lambda s,T=T: (1-exp(-s*T))/(s*T))
    globals().update(locals())

# PowerSpectrum tests
def test_PS():
    lam = 500*u.nm
    v = Sphi.v
    r0 = Sphi.r0
    f0 = Sphi.f0
    Sphi.plot()
    # position the spectrum window
    plt.pause(.1)
    try:
        width = plt.gcf().canvas.manager.window.winfo_width()
        plt.gcf().canvas.manager.window.wm_geometry('+0+0')
    except:
        pass # need TkAgg to do that
    # example use
    t,r = Sphi.noise(check=True)
    rn = Signal((t,r*lam/(2*pi*u.rad)))
    rn.plot()
    plt.title('Random noise generated from power spectrum'+'\n'+'$\\lambda=${0}, $v=${1}, $r_0=${2}'.format(lam,v,r0))
    # position the signal window
    try:
        plt.gcf().canvas.manager.window.wm_geometry('+{}+0'.format(width))
    except:
        pass # need TkAgg to do that
    var2 = Sphi.integral(one_side=False)
    var1 = Sphi.integral(one_side=True)
    df = 500.*u.Hz/1024
    print(var1,np.sqrt(var1),' one-side')
    print(var2,np.sqrt(var2),' two-side')
    print(var1-var2,Sphi(0*u.Hz)*df,' difference')
    
    # known analytic integral for the von Karman spectrum
    Sint = c3*(v/r0)**(5/3)*f0**(-5/3)*gamma(1/2)*gamma(5/6)/(2*gamma(4/3))
    Sint = Sint.decompose()
    print('Known analytic integral of von Karman spectrum:')
    print(Sint)
    globals().update(locals())
    
z_accum_ = None
def test_PSnoise(n=1024*16,ns=100,plot=True,nso=1,lam=500*u.nm):
    '''measure the variance of the generated noise
    and compare to expected
    '''
    PSdefs()
    if nso>1: # multiple experiment groups
        for jj  in range(nso):
            r = test_PSnoise(n=n,ns=ns,plot=True,nso=1)
            print('{}/{}'.format(jj,nso),r)
            plt.draw()
            plt.pause(.01)
        return
    var = Sphi.integral(n=n) # the integral of the power spectrum
    z = []
    for k in range(ns): # for 100 realizations...
        t,rn = Sphi.noise(n=n)  # generate random noise rn(t)
        msr = (rn*rn).mean()
        varr = rn.var()
        z.append(msr) # mean squares for each realization
    z = u.Quantity(z)
    if plot:
        k = (2*pi*u.rad)/lam
        r = rn/k # convert random noise from radians to opd (eg nanometers), r(t)
        t = t.to('s')
        sd = np.sqrt(var)/k # var is the variance from the power spectrum
        
        name = 'noise' # plot one of the noise vs time realizations, r(t)
        fig = get_fig(name)
        if not fig:
            fig  = plt.figure()
            fig.name = name
        plt.figure(fig.number)
        plt.title(name)
        plt.plot(t,r)
        plt.xlabel('$t$, seconds')
        plt.ylabel('$\phi(t)$' + ' ' + str(r.unit))
        plt.grid(True)
        # put std dev from the power spectrum as dashed lines
        plt.gca().axhline(y=sd.value,color='black',linestyle='dashed')
        plt.gca().axhline(y=-sd.value,color='black',linestyle='dashed')

        name = 'noiseVar' # plot experimental mean-square for the 100 realizations
        fig = get_fig(name)
        if not fig:
            fig = plt.figure()
            fig.name = name
        plt.figure(fig.number)
        plt.title(name)
        plt.plot(z,'x')
        plt.gca().axhline(y=var.value)
        plt.ylabel('radians squared')
        plt.grid(True)

        global z_accum_ # plot a histogram of the experimental noise variances
        if z_accum_ is None:
            z_accum_ = []
        z_accum_ += list(z.value) # accumlate the histogram over multiple experiment groups
        name = 'noiseHist'
        fig = get_fig(name)
        if not fig:
            fig = plt.figure()
            fig.name = name
        plt.figure(fig.number)
        plt.clf()
        plt.title(name)
        plt.hist(z_accum_,bins=500)
        plt.xlabel('radians squared')
        plt.grid(True)
        
        backend = matplotlib.get_backend()
        if backend in ['Qt5Agg','Qt4Agg','TkAgg']:
            tile_figures.tile_figures(2,2)
    
    print('Variance from power spectrum: {}'.format(var))
    print('Mean variance of {} experimental noise realizations: {}'.format(ns,z.mean()))
    return (var,z.mean())

# Tuner (optimizer) tests
def test_opt(fmax=4000*u.Hz, n=1024, fig=None, label=None, **kwargs):
    '''test AO optimization procedure.
    Integrate the power spectrum of the closed-loop residual
    and find the optimum values for gain and integrator gain (gamma)
    '''
    
    debug = kwargs.pop('debug',False)
    plot = kwargs.pop('plot',False)
    
    conditions = DotDict({'r0': 10*u.cm,
                      'v': 2*u.m/u.s,
                      'L0': 10*u.m,
                      'psf': {'shape':'gaussian', 'sigma':0.5*u.arcsec},
                      'flux': {'n':500.*u.ct,'d':20*u.cm,'T':1*u.ms},
                      'lam0': 550*u.nm,
                     })
    conditions.r0.lam0 = conditions.lam0
    
    aoParams = {'fs': 1*u.kHz,
                    'g': 0.5,
                    'gamma': 0.998,
                    'tau': 800*u.us,
                    'd': 20*u.cm,
                    'D': 3*u.m,
                    'lam_wfs': 0.7*u.micron,
                    'read_noise': 4*u.ct,
                    }
    
    co = kwargs.pop('co',conditions)
    ao = kwargs.pop('ao',aoParams)
    for key in kwargs:
        if key in co:
            co[key] = kwargs[key]
        elif key in ao:
            ao[key] = kwargs[key]
    
    r0,v,L0,psf,flux,lam0 = co.values()
    fs,g,gamma,tau,d,D,lam_wfs, read_noise = ao.values()
    
    T = (1/fs).to(u.ms)
    f0 = v/L0
    #flux =  flux['n']/(flux['d']**2*flux['T'])
    #N = flux*d**2*T
    
    Sphi_ = Sphi(f0=f0,v=v,r0=r0)
    
    sigma_m,sigma_n = phase_sigma(flux=flux, T=T, d=d, D=D, psf=psf, read_noise=read_noise)
    sigma_m = (2*pi/lam0)*sigma_m
        
    Sn = PowerSpectrum(lambda f: (sigma_m**2)*T.to_da(u.rad**2/u.Hz),
                        name='noise power spectrum',
                        formula='sigma_m**2 T for all f',
                        latex_name = '$S_n(f)$',
                        latex_formula = '$\\sigma_m^2 T$',
                        )
        
    def func(args,plot=False,debug=False):
        # this is the function to minimize
        g, gamma = args
        
        Hrej = Hcl(g=g, gamma=gamma)
        Hn = g*Hol(gamma=gamma)*Hcl(g=g, gamma=gamma)
        
        S = Hrej.abs2()*Sphi_ + Hn.abs2()*Sn
        
        if plot or debug:
            S1 = Hrej.abs2()*Sphi_
            S2 = Hn.abs2()*Sn
        
        if plot:
            S.plot(fmax=fmax,label='total')
            fig = plt.gcf()
            S1.plot(fmax=fmax,label='atmos part',fig=fig)
            S2.plot(fmax=fmax,label='noise part',fig=fig)
        
        if debug:
            k = 2*pi*u.rad/lam0
            r = {'atmos part':np.sqrt(S1.integral(fmax=fmax,n=n))/k,
                 'noise part':np.sqrt(S2.integral(fmax=fmax,n=n))/k,
                 'total' :np.sqrt( S.integral(fmax=fmax,n=n))/k,
                 'N' : N,
                 }
            return r
        
        return S.integral(fmax=fmax,n=n).value
    
    if debug:
        print('conditions')
        oprint.pprint(co)
        print('AOparams')
        oprint.pprint(ao)
        print('rms residual wavefront error')
        r = func((g,gamma),plot=plot,debug=debug)
        oprint.pprint(r)
        return func
    
    # optimization
    #  gain constrained to positive and less than the gain margin
    #  integrator gain constrained to >0.5 and strictly less than one
    
    else:
        r = optimize.minimize(func,(0,.999),bounds=[(0,.67),(.5,.999)])
        
        print(r)
        g,gamma = r.x
        Scl_opt = Scl(g=g,gamma=gamma)
        Scl_u = Scl(g=g+.1,gamma=gamma)
        Scl_d = Scl(g=g-.1,gamma=gamma)
        kwargs = {'fmax':fmax,'n':n,'fig':fig}
        
        # now plot the open and closed loop power spectra,
        # and compare with slight variations around the optimized parameters
        exfig = get_fig(fig)
        if not exfig:
            Sphi.plot(label='open loop',**kwargs)
        kwargs['fig'] = plt.gcf()
        if not label: label = 'opt closed loop'
        Scl_opt.plot(label=label,**kwargs)
        Scl_u.plot(color='gray',linestyle='dashed',**kwargs)
        Scl_d.plot(color='gray',linestyle='dashed',**kwargs)
        kwargs.pop('fig')
        
        lam = 500*u.nm
        mse_ol = Sphi.integral(**kwargs)**(1/2)*lam/(2*pi*u.rad)
        mse_cl = Scl_opt.integral(**kwargs)**(1/2)*lam/(2*pi*u.rad)
        print('open loop: ',mse_ol)
        print('closed loop: ',mse_cl)
        print('optimum gain = ',g)
        print('optimum gamma = ',gamma)
        print('closed loop at gain+0.1: ',Scl_u.integral(**kwargs)**(1/2)*lam/(2*pi*u.rad))
        print('closed loop at gain-0.1: ',Scl_d.integral(**kwargs)**(1/2)*lam/(2*pi*u.rad))

def test_opt2():
    fss = u.Quantity([1000,500,200,100,50],u.Hz)
    fig = None
    mses = []
    for fs in fss:
        label = str(fs)
        ol,cl = test_opt(fs=fs,label=label,fig=fig)
        mses.append(cl)
        fig = plt.gcf()
        plt.pause(.1)
    
    mses = u.Quantity(mses)
    
    plt.figure()
    plt.plot(fss,mses)
    ax = plt.gca()
    ax.set_xlabel('frame sample rate')
    ax.set_ylabel('RMS error, {}'.format(cl.unit))
    ax.set_title('RMS error vs WFS frame rate')
    plt.grid(True)
    
    tile_figures.tile_figures(2,1)

def AOloop(fs=1*u.kHz,g=0.5,gamma=0.999):
    T = (1/fs).to(u.s)
    Hcl_r = Hcl(T=T,gamma=gamma,g=g)
    Hn_r = Hn(T=T,gamma=gamma,g=g)
    return (Hcl_r,Hn_r)

def test_opt3(noise=10*u.nm**2/u.Hz, r0=10*u.cm, v=10*u.m/u.s, fs=1000*u.Hz):
    from scipy import optimize
    lam = 500*u.nm
    k = 2*pi*u.rad/lam
    Sphi = vonKarmanSpectrum(r0=r0,v=v)/k**2
    Sn = PowerSpectrum(lambda f: unity(f)*noise)
    
    def resid_var(args):
        g,gamma = args
        Hcl,Hn = AOloop(fs=fs,g=g,gamma=gamma)
        Scl = PowerSpectrum(lambda f: Sphi(f)*Hcl(2*pi*i*f).abs()**2,name='phase rejection residual')
        Sncl  = PowerSpectrum(lambda f: Sn(f)*Hn(2*pi*i*f).abs()**2,name='closed loop noise spectrum')
        var = (Scl+Sncl).integral(use_scipy=False).value
        return(var)
    
    r = optimize.minimize(resid_var,(0,.999),bounds=[(0,.67),(.5,.999)])
    g,gamma = r.x
    Scl = PowerSpectrum(lambda f: Sphi(f)*Hcl(2*i*pi*f).abs()**2,name='phase rejection residual')
    Sncl  = PowerSpectrum(lambda f: Sn(f)*Hn(2*i*pi*f).abs()**2,name='closed loop noise spectrum')
    sigma_cl = (Scl+Sncl).integral(use_scipy=False)**(1/2)
    sigma_ol = (Sphi+Sn).integral(use_scipy=False)**(1/2)
    
    try:
        (Sphi+Sn).plot(label='open loop',color='red',linestyle='-')
        fig = plt.gcf()
        Sphi.plot(fig=fig,label='Sphi part',color='red',linestyle='--')
        Sn.plot(fig=fig,label='Sn part',color='red',linestyle='-.')
        (Scl+Sncl).plot(fig=fig,label='closed loop',color='green',linestyle='-')
        Scl.plot(fig=fig,label='Sphi part',color='green',linestyle='--')
        Sncl.plot(fig=fig,label='Sn part',color='green',linestyle='-.')
        ax = plt.gca()
        fig.suptitle('')
        ax.set_title('Optimized Closed Loop Spectra')
    except:
        globals().update(locals())
    
    return r,sigma_ol,sigma_cl

# Signal tests
def test_Signal():
    x = Signal(lambda t,tau=300.*u.ms,t0=100*u.ms: np.exp(-t/tau)*np.sin(2*pi*u.rad*t/t0)*u.V,
               formula = '$x(t) = e^{-t/\\tau}\sin(2\pi t/t_{0})$',
               name = 'Test Signal',
              )
    x._gen_samples()
    x.plot()
    fig1 = plt.gcf()
    x.name = 'Renamed'
    x.plot()
    fig2 = plt.gcf()
    
    if plt.get_backend() == 'TkAgg':
        plt.figure(fig1.number)
        cfm1 = plt.get_current_fig_manager()
        cfm1.window.geometry('+605+31')
        plt.figure(fig2.number)
        cfm2 = plt.get_current_fig_manager()
        cfm2.window.geometry('+750+286')
        cfm2.window.attributes('-topmost',True)
    
    globals().update(locals())

def test_Signal2():
    t,x = Sphi.noise(n=1024*16)
    # important to get a lot of samples to get the noise variance close to the Sphi integral
    rns = Signal((t,x))
    rns.plot()
    rns.plot(t0=4*u.s,tf=16*u.s,n=4000)
    rns.plot(t0=4*u.s,tf=5*u.s,n=4000)
    # todo: Fourier transform of the signal - to compute a sample power spectrum
    globals().update(locals())

def test_Signal3(n=1,plot=True,fig=None):
    try:
        if n>0:
            iSphi = Sphi.integral()
            print('integral of Sphi: {}'.format(iSphi))
            iFFs = []
            for k in range(n):
                iFF = test_Signal3(n=0,plot=plot,fig=fig)
                if plot:
                    fig = plt.gcf()
                iFFs.append(iFF)
            if plot:
                Sphi.plot(fig=fig)
                plt.title('von Karman spectrum vs random generator')
            iFFs = u.Quantity(iFFs)
            print('mean: {}, std: {}'.format(iFFs.mean(),iFFs.std()))
        else:
            t,x = Sphi.noise(n=1024*16)
            rns = Signal((t,x))
            f,X = rns.FourierTransform()
            n = len(f)
            fpositive = f>=0
            f = f[fpositive]
            X = X[fpositive]
            df = f[1]-f[0]
            FF = Spectrum((f,2*X*X.conj()*df)) # factor of 2 to count both sides of spectrum
            if plot:
                FF.plot(fig=fig)
                plt.pause(.01)
            # compute the integral
            Fs = FF._original_samples.F/2. # and a factor of 2 to uncount the left side
            iFF = ( Fs.abs().sum() + Fs[1:].abs().sum() )*df #... so we can use this special formula to count both sides
            print('integral of FF: {}'.format(iFF))
            return iFF
    
    finally:
        globals().update(locals())

# Temporal X Spatial power spectrum tests
globals().update(AOdefs())
def test_3d():
    print('sampling',flush=True)
    DM2.sample()
    Hol.sample()
    S = DM2.samples.S
    print('computing OLTF',flush=True)
    OLTF = u.Quantity([Hol(f)*S for f in Hol.samples.f])
    print('computing CLTF',flush=True)
    g = 0.5
    CLTFnoise = g*OLTF/(1+g*OLTF)
    CLTFrej = 1/(1+g*OLTF)
    f = Hol.samples.f
    frequency_domain_plot((f,CLTFnoise[:,32,32]),title='CLTFnoise')
    frequency_domain_plot((f,CLTFrej[:,32,32]),title='CLTFrej')
    Hnoise = g*Hol/(1+g*Hol)
    Hrej = 1/(1+g*Hol)
    frequency_domain_plot((f,Hnoise(f)),title='Hnoise')
    frequency_domain_plot((f,Hrej(f)),title='Hrej')
    globals().update(locals())

# formatter
def test_formatter():
    x = np.linspace(-np.pi, 3*np.pi,500)
    plt.figure()
    plt.plot(x, np.cos(x))
    plt.title(r'Multiples of $\pi$')
    ax = plt.gca()
    ax.grid(True)
    ax.set_aspect(1.0)
    ax.axhline(0, color='black', lw=2)
    ax.axvline(0, color='black', lw=2)
    ax.xaxis.set_major_locator(plt.MultipleLocator(np.pi / 2))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(np.pi / 12))
    ax.xaxis.set_major_formatter(plt.FuncFormatter(multiple_formatter()))
    plt.show()

# ---------- comprehensive testing -------------
def tests():
    test_TF()
    test_PS()
    test_PSnoise()
    #test_opt()
    test_Signal()
    test_Signal2()
    test_Signal3()
    test_formatter()
    

