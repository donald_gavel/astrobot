#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Deformable mirrors module.

Version 2 uses GPU to drive the mirror

"""

import os
import sys
import numpy as np
import configparser
from pkg_resources import resource_filename
from astropy import units as u

from astrobot import dotdict
from astrobot import q_helper
from astrobot import oprint
from astrobot import img
from astrobot import dimg

import pyopencl as cl
import pyopencl.array as cl_array
import pyopencl.clmath as cl_math
import reikna.cluda as cluda
from reikna.fft import FFT
import matplotlib.pyplot as plt
import matplotlib.patches as patches

rc_configFile = resource_filename('astrobot','data/dm.cfg')

class DM(object):
    """
    Base Deformable mirror class
    """
    def __init__(self,configFile=rc_configFile,sections=[]):
        cp = configparser.ConfigParser()
        cp.optionxform = str # keep the keys case sensitive
        cp.read(configFile)
        self.config = cp
    
        for sect in sections:
            _d =  dotdict.typize(dict(self.config.items(sect)))
            self.__dict__.update(_d)

    def pprint(self,**kwargs):
        """Pretty-print the object's instance variables and descriptions
        """
        oprint.pprint(self,**kwargs)
    
    @property
    def pp(self):
        self.pprint()
        
class MEMS(DM):
    """
    MEMS DM
    """    
    def __init__(self,dx=1.*u.cm):
        """create a Tweeter deformable_mirror object
        specific to the Boston Micromachines 32x32 DM
        
        Parameters
        ----------
        dx : Quantity
            size of a fine-grid pixel on the pupil plane
        
        >>> w = wfs.Wfs()
        >>> tw = Tweeter(dx=w.du, d=w.d/2.)
        """
        super().__init__(sections = ['BMC_KiloDM','ShaneAO_KiloDM'])
        da = self.spaced
        self.lim = np.kron(np.ones(self.nacts),self.command_range)
        self.stroke_per_DN = self.stroke / self.command_range.ptp()
        self.magnification = ( self.d / self.spaced ).decompose()
        self.magnification.doc = 'pupil (de)magnification ratio'
        # ------ Fine surface grid -------
        self.dx_fine = dx
        self.padded = True
        pad = 0
        if self.padded:
            pad = 1.
        self.n_fine = img.nextpow2(int(np.round((self.n_across+pad)* (self.d / self.dx_fine).decompose())))
        ph = np.zeros((self.n_fine,self.n_fine))
        self.surface = u.Quantity(ph,u.nm)
        self.surface.dx = self.dx_fine
        self.surface.x0 = -u.Quantity([1,1])*self.dx_fine*self.n_fine/2
        # ----- Locations of the actuators -----
        x = (np.arange(0,self.n_across) - self.n_across/2)
        y = x
        actLocs = []
        for xx in x:
            for yy in y:
                actLocs.append((xx,yy))
        self.actLocs = u.Quantity(actLocs)*self.d
        self.actLocs_fine = (self.actLocs / self.dx_fine).value.astype(int) + self.n_fine//2
        n = self.n_fine
        self.actLocsIndices = list(self.actLocs_fine[:,0]*n + self.actLocs_fine[:,1])
        self.actLocsIndices_doc = '1d index of actLocs into dm surface array'
        self._definePoke()

    def show(self,ax=None):
        '''show the DM and actuator locations in a graphic.
        An option is to put the display on top of another
        object display, such as the wavefront sensor.
        
        Parameters
        ----------
        ax : :class:`~matplotlib.axes.Axes` object
            The graphic object on top of which to draw the tweeter display.
            If None, a new figure is created.

        Returns
        -------
        axis
            The :class:`~matplotlib.axes.Axes` object on which the figure is drawn.
        '''
        if ax is None:
            a = u.Quantity(np.zeros_like(self.surface.value))
            a.name = 'Tweeter actuator locations'
            a *= 0.
            n,m = a.shape
            a.dx = self.surface.dx.to(u.m)
            a.x0 = self.surface.x0.to(u.m)
            dimg.show(a)
            ax = plt.gca()
            
        d = self.n_across*self.d.to(u.m).value
        s = self.d.to(u.m).value/2.
        ax.add_patch(patches.Rectangle((-d/2-s,-d/2-s),d,d,fill=False,edgecolor='red'))
        for x,y in self.actLocs.to(u.m):
            plt.plot(x,y,marker='o',markersize=1,color='red')
        plt.draw()
        return ax

    def _definePoke(self,sigma=1.4):
        """Generate a generic poke for the actuator
        on the fine grid. These are sin(x)/x functions
        with a Gaussian envelope.
        
        Parameters
        ----------
        sigma : float
            A Gaussian decay parameter for the poke, in units
            of actuator spacing
        """
        n = self.n_fine
        a = self.d.to(u.m)
        poke = np.zeros((n,n))
        x = (np.arange(n) - n/2)*self.dx_fine.to(u.m)
        x,y = np.meshgrid(x,x)
        r2 = x**2 + y**2
        s2 = (sigma*a)**2
        poke = -np.sinc((x/a).to_da(u.rad))*np.sinc((y/a).to_da(u.rad))*np.exp(-r2/s2)
        self.influenceFcn = u.Quantity(poke)
        self.influenceFcn.name = 'Tweeter poke'
        self.influenceFcn.dx = u.Quantity([self.dx_fine,]*2)
        self.influenceFcn.x0 = -self.influenceFcn.dx*n/2
        poke_f = img.ft(poke)
        self.influenceFcn_f = u.Quantity(poke_f)
        self.influenceFcn_f.name = 'FT of Tweeter poke'
        self.influenceFcn_f.dx = 1./(self.influenceFcn.dx*self.n_fine)
        self.influenceFcn_f.x0 = -u.Quantity([1,1])*self.influenceFcn_f.dx*n/2

    def actuate(self,a):
        '''actuate the deformable mirror given a list
        of actuator values. The result of actuation shows
        up on self.surface.
        
        Parameters
        ----------
        a : list of floats
            The actuator command values. These are assumed to be
            in units of digital number (u.DN) and within the
            tweeter actuation range (self.actDNRange)
        '''
        self.z[:] = 0.
        self.z.flat[self.actLocsIndices] = a[0:self.nacts]
        self._gpu_convolve()
    
    def _gpu_prep(self,device=1):
        '''prepare the gpu for subsequent Fourier Transform
        convolve operations
        
        device 1 is the GPU device on the MacBook Pro
        '''
        global platform, devs, ctx, queue, api, thr, fft, fftc
        assert sys.version_info[0] == 3,'_gpu_convolve can only be used with Python3'
        n = self.n_fine
        self.z = np.zeros((n,n)).astype(np.complex64)
        platform = cl.get_platforms()[0]
        devs = platform.get_devices()
        ctx = cl.Context(devices=[devs[device]])
        queue = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)
        api = cluda.ocl_api()
        thr = api.Thread(queue)
        z = np.ones((n,n)).astype(np.complex64)
        fft = FFT(d)
        fftc = fft.compile(thr)
        f = self.influenceFcn_f.astype(np.complex64)
        self.gpu_f = cl_array.to_device(queue,f)
    
    def _gpu_convolve(self):
        '''execute the GPU based convolution of pokes with
        commands
        '''
        gpu_z = cl_array.to_device(queue,self.z)
        mult(qpu_z,gpu_f)
        fftc(gpu_z,gpu_z)
        self.z = gpu_z.get()
        
