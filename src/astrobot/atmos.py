#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Atmos.py
Atmosphere opject for use with
AO simulations and analysis
"""
from __future__ import print_function
import sys
import numpy as np
import pandas as pd
from astropy import units as u
import time
import tqdm
from scipy.ndimage.interpolation import rotate
from scipy.optimize import least_squares
from scipy.interpolate import interp1d

from astrobot import q_helper
from astrobot import oprint
from astrobot.dotdict import DotDict
from astrobot import img
from astrobot import dimg

inf = float('Infinity')
nan = float('NaN')
# allows round(u.Quantity) to work on an array of length 1
u.Quantity.__round__ = lambda x: round(x.decompose().value)
# do not redefine the already-defined round method
#u.Quantity.round = lambda x: x.__round__()

def Qrange(*args):
    '''arange function that can take astropy Quantites
    '''
    try:
        unit = args[0].unit
        vargs = [x.to(unit).value for x in args]
    except:
        raise Exception('all arguments must be Quantities with convertable units')
    return np.arange(*vargs)*unit

def ceil(x):
    if isinstance(x,u.Quantity):
        #return x.ceil()
        return int(np.ceil(x).decompose())
    else:
        return int(np.ceil(x))

def normedL1(x):
    r = x / x.sum()
    if isinstance(r,u.Quantity):
        return r.decompose().value
    else: # np.array
        return r
u.Quantity.normedL1 = lambda x: normedL1(x)

seeing = DotDict({
    'r0': 10.*u.cm,
    'lam0': 0.5*u.micron,
    'L0': inf*u.m,
    'theta0':nan,
    'Cn2_profile': '',
    'wind_profile': '',
    'wind_magnitude':10.*u.m/u.s,
    'wind_direction':0.*u.degree,
})

seeing_layers = DotDict({
    'r0': 10.*u.cm,
    'lam0': 0.5*u.micron,
    'L0': inf*u.m,
    'theta0':nan,
    'nlayers': 3,
    'Cn2_profile': '',
    'wind_profile': '',
    'altitude': u.Quantity([0.,5.,10.],u.km),
    'Cn2': [1.,1.,1.],
    'wind_magnitude':u.Quantity([5.,20.,10.],u.m/u.s),
    'wind_direction':u.Quantity([0.,180.,45.],u.degree),
})

telescope = DotDict({
    'name': 'Shane 3m',
    'D': 3.0*u.m,
    'Ds': 0.8*u.m,
    'fno': 17.,
    'f': 51.*u.m,
    'platescale': (1.*u.radian/(51.*u.m)).to(u.arcsec/u.mm),
})

options = DotDict({'pretty_print':'short'})

def radiansphase(lam):
    '''creates an equivalence for unit conversion (see astropy.units)'''
    if isinstance(lam,u.Quantity):
        lam = lam.to('m').value
    return [(u.radian, u.m,
            lambda x: x*lam/(2*np.pi),
            lambda x: x*2*np.pi/lam
            )]

def pprint(obj):
    """
    Pretty-print the object's contents
    """
    oprint.pprint(obj,short=True)
    print(DotDict(sorted(obj.__dict__.items())))

u.Quantity.ppprint = pprint
u.Quantity.show = dimg.show

class Screen(object):
    """The Screen class defines a single screen of atmospheric turbulence
    as characterized by a vonKarman spectrum
    """
    
    def __init__(self,seeing=seeing, telescope=telescope, n=512, du = 1.*u.cm):
        """
        :param DotDict seeing: DotDict having Quantities 'r0' and 'L0'
        :param DotDict telescope: DotDict having str 'name', and Quantities 'D' and 'Ds'
        :param int n: number of pixels across the fine grid
        :param Quantity du: size of one fine grid pixel
        """
        if telescope:
            self.telescope = telescope
            rp = float(telescope.D / du) / 2.
            rs = float(telescope.Ds / du) / 2.
            self.ap = img.circle((n,n),r=rp) - img.circle((n,n),r=rs)
        self.n = n
        self.du = du
        self.du.doc = 'spatial grid sampling'
        self.seeing = seeing.copy()
        self.seeing.seeing = (seeing.lam0/seeing.r0).to(u.arcsec,equivalencies=u.dimensionless_angles())
        #self.dtheta.doc = 'size of one pixel on the PSF image'
    
    def __repr__(self):
        if options.pretty_print == 'short':
            return super(Screen,self).__repr__()
        return oprint.pprint(self,out='string')
        
    def pprint(self,**kwargs):
        """:py:func:`Pretty-print <oprint.pprint>` the object, displaying the
        names and values of all the attributes.
        """
        pprint(self,**kwargs)
    
    @property
    def pp(self):
        self.pprint()
        
    def show(self,**kwargs):
        """Display the atmosphere's phase screen
        
        :param dict kwargs: arguments passed to the display routine
        """
        self.screen.show(**kwargs)

    def set_seed(self, seed=1):
        """Set the random number generator seed
        
        :param int seed: random seed
        """
        np.random.seed(seed)
        u = np.random.get_state()
        self.randomState = {'seed':seed,
                         'algo':u[0],
                         'keys':u[1],
                         'pos':u[2],
                         'has_gauss':u[3],
                         'cached_gaussian':u[4],
        }
        
    def gen_screen(self,shape=None,nsh=0):
        """generate a random phase screen
        
        :param tuple shape: 2-tuple of the x and y dimensions of the screen
        :param int nsh: number of sub-harmonics (used to improve low
            spatial frequency power, see Jahansson and Gavel, SPIE 2200, 1994)

        """
        if hasattr(self,'psf'): delattr(self,'psf')
        if hasattr(self,'mtf'): delattr(self,'mtf')
        
        if shape is not None:
            self.shape = shape
        else:
            self.shape = (self.n,self.n)
        self.nsh = nsh
        
        self._generate_screen()
        return self
    
    def _generate_filter(self):
        """generate the spectrum filter for generating
        Kolmogorov random phase screens from white noise.
        """
        n,m = self.shape # (rows,cols) = (ny, nx)
        du = self.du.to('meter').value
        r0 = self.seeing.r0.to('meter').value
        L0 = self.seeing.L0.to('meter').value
        nsh = self.nsh
        
        dkx = 2 * np.pi / (m*du)
        dky = 2 * np.pi / (n*du)
        kx = np.arange(m/2+1)*dkx
        ky = (np.arange(n) - n/2)*dky
        kx,ky = np.meshgrid(kx,ky)
        k0 = 2 * np.pi / L0
        k2 = np.power(kx,2) + np.power(ky,2) + (k0 ** 2.0)
        k2[n//2,0] = 1.0
        
        f = np.sqrt(0.023) * np.power(2.0 * np.pi / r0,5./6.) * np.power(k2,-11./12.) * np.sqrt(dkx * dky)
        
        f[n//2,0] = 0.0
        
        if nsh > 0:
            shf = np.zeros((8,nsh))
            for i in range(nsh):
                dkx = dkx/3.
                dky = dky/3.
                k2 = dkx**2 + dky**2 + k0**2
                shf[0,i] = k2**(-11./12.)*np.sqrt(dkx*dky)
                shf[1,i] = (dky**2+k0**2)**(-11./12.)*np.sqrt(dkx*dky)
                shf[2,i] = k2**(-11./12.)*np.sqrt(dkx*dky)
                shf[3,i] = (dkx**2+k0**2)**(-11./12.)*np.sqrt(dkx*dky)
                shf[4,i] = (dkx**2+k0**2)**(-11./12.)*np.sqrt(dkx*dky)
                shf[5,i] = k2**(-11./12.)*np.sqrt(dkx*dky)
                shf[6,i] = (dky**2+k0**2)**(-11./12.)*np.sqrt(dkx*dky)
                shf[7,i] = k2**(-11./12.)*np.sqrt(dkx*dky)
            shf = shf*np.sqrt(0.023)*(2*np.pi)**(5./6.)*r0**(-5./6.)
        else:
            shf = None
        
        self.shf = shf
        
        self.filter = u.Quantity(f)
        self.filter.unit_info = 'normalized to one at zero frequency'
        self.filter.name = 'screen spectral filter'
        self.filter.longName = 'Kolmogorov Spectral Filter'
        self.filter.dx = u.Quantity([1./(n*self.du),1./(m*self.du)])
        self.filter.x0 = -n//2*self.filter.dx
        self.filter.axis_names = ('kx','ky','k')
    
    def _generate_screen(self,seed=None):
        """Generate a random phase screen
        
        Note: there is an uncertainty about a factor of root 2
        For now we are *not* dividing the complex noise by root 2
        so the generated rms phase values come out close to expected by Noll's coefficients
        """
        if seed is not None:
            self.seed = seed

        if not hasattr(self,'filter'):
            self._generate_filter()
        
        n,m = self.shape
        shf = self.shf
        du = self.du.to('meter').value
        i = 1j
        
        size = (n,m//2+1)
        frn = (np.random.normal(size=size) + i*np.random.normal(size=size))/np.sqrt(2.)
        kx = [0,m//2] # 4 corner coefficients are real
        for ky in [0,n//2]: frn[ky,kx] += np.conj(frn[ky,kx])
        
        fs = frn*self.filter
        fs = np.roll(fs,n//2,axis=0)
        #s = np.fft.irfft2(fs)*np.prod(self.shape) # astropy v3
        s = np.fft.irfft2(fs.value)*np.prod(self.shape) # astropy v4

        # subharmonics
        shn = np.random.normal(4)
        if shf is not None:
            dkx = 2.0*np.pi/(n*du)
            dky = 2.0*np.pi/(m*du)
            y, x = (np.mgrid[0:n,0:m]).astype(np.float) * du
            rn = np.ones((8,),dtype=np.complex)
            nsh = shf.shape[1]
            for i in range(nsh):            
                rn[0:4] = shn
                rn[7] = np.conj(rn[0])
                rn[6] = np.conj(rn[1])
                rn[5] = np.conj(rn[2])
                rn[4] = np.conj(rn[3])
                dkx = dkx/3.0
                dky = dky/3.0
                s = s + rn[0]*shf[0,i]*np.exp(1j*(-dkx*x-dky*y))
                s = s + rn[1]*shf[1,i]*np.exp(1j*(-dky*y))
                s = s + rn[2]*shf[2,i]*np.exp(1j*(dkx*x-dky*y))
                s = s + rn[3]*shf[3,i]*np.exp(1j*(-dkx*y))
                s = s + rn[4]*shf[4,i]*np.exp(1j*(dkx*y))
                s = s + rn[5]*shf[5,i]*np.exp(1j*(-dkx*x+dky*y))
                s = s + rn[6]*shf[6,i]*np.exp(1j*(dky*y))
                s = s + rn[7]*shf[7,i]*np.exp(1j*(dkx*x+dky*y))
        
        self.screen = u.Quantity(s.real,u.radian)
        self.screen = self.screen.to('nm',equivalencies=radiansphase(self.seeing.lam0))
        self.screen.name = 'atmos phase'
        self.screen.longName = 'Random Atmospheric Phase Screen'
        self.screen.dx = u.Quantity([self.du,self.du])
        self.screen.x0 = -n//2*self.screen.dx
        self.screen.seeing = self.seeing
        self.screen.axis_names = ('X','Y')
        
    def calc_rms(self,D):
        '''calculate the expected rms of a screen on a
        diameter D aperture
        '''
        D = as_ufloat(D,'meters')
        rms = (D/self.r0)**(5./3.)
        self.rms_pr = ufloat(np.sqrt(1.1*rms),'radians',wavelength=self.lam0)
        self.rms_pr_doc = 'expected rms, piston removed'
        self.rms_pttr = ufloat(np.sqrt(0.1*rms),'radians',wavelength=self.lam0)
        self.rms_pttr_doc = 'expected rms, piston tip tilt removed'
        self.rms_D = D
        self.rms_D_doc = 'aperture for expected rms calcs'
        r = dict([(x,self.__dict__[x]) for x in self.__dict__.keys() if x.startswith('rms_')])
        return r        
    
    def calc_psf(self,lam=None,short_exposure=False,near=.9):
        """calculate the point spread function of just the atmosphere or the
        atmosphere + telescope
        
        :param float dtheta: fine pixel sampling of the psf, arcsec
        :param float lam: wavelength of light, microns
        :param boolean short_exposure: compute the short-exposure PSF (tilt removed).
            Default is long-exposure, which has long term tilts included
        :param float near: near field parameter (1 for near field, 0.5 for far field)

        Near field is in the sense that
        the atmosphweric aberrations are close to the telescope
        or, r >> (L*lambda)^(1/2)
        where du<r<d is transverse positions on the aperture,
        L is the propagation distance from turbulence
        to telescope and lambda is the wavelength.
        I.e. nearness of the field is in relation to
        the propagation distance needed
        to cause significant scintillation.
        See Fried, JOSA V.56, n.10, 1966.
        
        The result is an instance psf is created or modified.
        """
        n = self.n
        du = self.du
        if (lam is None):
            lam = self.seeing.lam0
        dtheta = (lam/(n*du)).to(u.arcsec,equivalencies=u.dimensionless_angles())
        
        self.calc_mtf(lam=lam,short_exposure=short_exposure,near=near)
        psfa = np.abs(img.ft(self.mtf))
        psfa = psfa/np.sum(psfa)
        
        self.psf = u.Quantity(psfa)
        self.psf.name = 'atmos psf'
        self.psf.longName = 'Atmosphere Point Spread Function'
        self.psf.comment = 'normalized to integrate to one'
        self.psf.dx = [dtheta]*2
        self.psf.dx_doc = 'size of pixel on PSF grid'
        self.psf.x0 = [-n/2*dx for dx in self.psf.dx]
        self.psf.wavelength = lam
        self.psf.wavelength_doc = 'wavelength of the imaged light'

    def calc_mtf(self,lam=None,short_exposure=False,near=0.9):
        """calculate the modulation transfer function (MTF)
        of just the atmosphere or the atmosphere + telescope
        
        :param float lam: wavelength of light, microns
        :param boolean short_exposure: compute the short-exposure PSF (tilt removed).
            Default is long-exposure, which has long term tilts included
        :param float near: near field parameter (1 for near field, 0.5 for far field)
            See Fried, JOSA V.56, n.10, 1966.
        :param float d: size of the aperture to assume if computing the
            short exposure case, in meters. Must be specified
            if short_exposure = True

        Near field is in the sense that
        the atmosphweric aberrations are close to the telescope
        or, r >> (L*lambda)^(1/2)
        where du<r<d is transverse positions on the aperture,
        L is the propagation distance from turbulence
        to telescope and lambda is the wavelength.
        I.e. nearness of the field is in relation to
        the propagation distance needed
        to cause significant scintillation.
        See Fried, JOSA V.56, n.10, 1966.
        
        """
        n = self.n
        if (lam is None):
            lam = self.seeing.lam0
        r0 = self.seeing.r0*(lam/self.seeing.lam0)**(6./5.)
        dx = self.du
        
        x1 = (np.arange(0,n)-n/2.)*dx
        x,y = np.meshgrid(x1,x1)
        r = np.sqrt(x**2+y**2)
        dphi = 6.88*(r/r0)**(5./3.)
        if hasattr(self,'telescope'):
            f_ap = img.ft(self.ap)
            f_tau = f_ap*f_ap.conj()
            tau = img.ftinv(f_tau).real
            tau /= tau[n//2,n//2]
        else:
            tau = np.ones((n,n))
        if (short_exposure):
            d = self.telescope.D
            uu = (r/d).decompose().value
            du = (dx/d).decompose().value
            mask = img.circle((n,n),c=(n/2,n/2),r=1./du)
            arg = (-0.5 * dphi *(1-near*uu**(1./3.)))*mask
        else:
            arg = -0.5*dphi
            mask = np.ones((n,n))
        taua = np.exp(arg)*mask
        
        self.mtf = u.Quantity(taua)
        self.mtf.name = 'atmos MTF'
        self.mtf.longName = 'Atmosphere Modulation Transfer Function'
        self.mtf.comment = 'Normalized to equal 1 at zero frequency'
        self.mtf.dx = [(dx/self.seeing.r0).decompose().value]*2
        self.mtf.dx_doc = 'spatial frequency delta of one pixel in the fine grid, in units of r0'
        self.mtf.x0 = [-n/2*dx for dx in self.mtf.dx]
        self.mtf.axis_labels = ['%s, cycles per r0'%f for f in ['fx','fy']]
        self.mtf.wavelength = lam
        self.mtf.wavelength_doc = 'mavelength of the light'

class BlowingScreen(Screen):
    """The BlowingScreen is an Atmosphere that has a wind velocity associated with it
    It is long and thin and wraps around smoothly.
    """
    options = DotDict({
        'rotate_method':'direct',
        'rotate_order':0,
        'depiston':True,
    #     'rotate_method' : 'direct',
    #     'rotate_order' : 0,
    #     'depiston' : True,      
    })
    
    def __init__(self,dt=1*u.ms, tmax=4.*u.s, name='blowing screen', **kwargs):
        """
        :param float dt: sample time, seconds
        :param float v: wind velocity, meters per second
        :param str name: user-supplied name for the screen
        :param dict kwargs: parameters forwarded to :class:`Atmosphere <atmos.Atmosphere>` creator
        
        """
        super(BlowingScreen,self).__init__(**kwargs)
        self.dt = dt
        self.dt.doc = 'sample time'
        self.tmax = tmax
        self.tmax.doc = 'length of time before screen repeats (at least)'
        n = self.n
        v = self.seeing.wind_magnitude
        du = self.du
        m = ceil(v*tmax/du)
        m = max(n,m)
        m = img.nextpow2(m)
        self.gen_screen(shape=(n,m))
        self.t = 0.*u.s
        self.t.doc = 'elapsed time on the blowing screen'
        self.pointer = 0
        self.pointer_doc = 'mark on the screen of wind x elapsed time'
        a = self.screen.copy()
        av = a.value
        a = u.Quantity(np.block([av,av[:,0:n]]),a.unit)
        self.screen = a
        #self.screen = InfoArray(a,**self.screen.__dict__)
        self.screen.name = name
    
    def __repr__(self):
        if options.pretty_print == 'short':
            return super(BlowingScreen,self).__repr__()
        return oprint.pprint(self,out='string')
        
    @property
    def next(self):
        """get the next screen in the blowing screen sequence.
        
        :param bool crop: crop the screen with a circular aperture. (gets rid of rotation artifacts)
        
        """
        n,m = self.screen.shape
        p0 = self.pointer
        p1 = p0 + n
        if not hasattr(self,'fast_t'): # avoid unit arithmetic in pointer calculation
            self.t = self.t.to(u.s)
            self.dt = self.dt.to(u.s)
            self.du = self.du.to(u.m)
            self.seeing.wind_magnitude = self.seeing.wind_magnitude.to(u.m/u.s)
            self.fast_t = True
        t = self.t.value + self.dt.value
        self.t = u.Quantity(t,u.s)
        #self.t += self.dt
        #t = self.t
        du = self.du.value
        v = self.seeing.wind_magnitude.value
        vaz = self.seeing.wind_direction
        self.pointer = int(np.rint(v*t/du))
        #self.pointer = int((v*t/du).decompose().round())
        self.pointer = self.pointer % (m-n)
        s = self.screen[:,p0:p1] #.value
        if self.options.rotate_method == 'indirect':
            if not hasattr(self,'rotator'):
                self._make_rotator()
            s = s.ravel()[self.rotator].reshape((n,n))
        elif self.options.rotate_method == 'direct':
            s = rotate(s,vaz.to(u.degree).value,reshape=False,order=self.options.rotate_order)
        if self.options.depiston:
            s = img.depiston(s,self.ap)
        s = u.Quantity(s,self.screen.unit)
        s.t = self.t
        s.name = 'slice of blowing screen'
        
        return s
    
    def _make_rotator(self):
        n,m = self.screen.shape
        vaz = self.seeing.wind_direction
        rotator = np.arange(0,n*n).reshape((n,n))
        rotator = rotate(rotator,vaz.to(u.degree).value,reshape=False,order=0).astype(int)
        self.rotator = rotator.reshape((n*n,))
        
    def reset(self):
        """restart the screen sequence to the beginning
        """
        self.pointer = 0
        self.t *= 0.
    
    def sequence(self,count=None,tmax=None,verbose=False):
        """get a data cube of screens from the blowing screen sequence
        
        :param int count: number of screens to get
        :param Quantity tmax: amount of time to get
        :param boolean verbose: if True, prints out as it grabs screens
        
        Either **count** or **tmax**, but not both, must be specified
        
        :return: (*list*) -- A set of :class:`InfoArrays <info_array.InfoArray>` for **count**
            time instances, or from t=0 to t= **tmax** in steps of **dt**
        """
        if count is not None:
            n = count
        elif tmax is not None:
            n = int(round(tmax/self.dt))
        else:
            raise Exception('either count or tmax must be specified')
        if verbose:
            print('<BlowingScreen:sequence> getting',n,'screens')
        s = []
        krange = range(n)
        if verbose:
            krange = tqdm.tqdm(krange)
        for k in krange:
            s.append(self.next.copy())
    
        return s

class Atmosphere(object):
    '''A full atmosphere is a list of screens or blowing screens at various altitudes
    '''
    
    def __init__(self,dt=None,tmax=None,du=1.*u.cm, name='multi-screen atmosphere',seeing=seeing_layers,telescope=telescope):
        '''
        :param table: can be a dictionary or :class:`pandas.DataFrame` with the altitude-dependent parameters
        :param list r0: the atmospheric seeing paramter for each altitude, meters
        :param list v: the wind at each altitude, meters
        :param list vaz: the wind direction at each altitude, degrees CCW from +X
        :param list h: the altitudes, meters
        :param list L0: the outer scale at each altitude, meters
        :param int n: the number of fine pixels across the screens
        :param object telescope: (optional) telescope object
        :param float du: the fine pixel sample spacing, meters
        :param float lam0: the wavelength at which r0 is defined, microns
        :param float dt: the time sampling, seconds
        :param float tmax: the maximum time, which determines the length of the screen, seconds
        
        length of screen, in pixels > v*tmax/du (:math:`> v t_{max} / du`)
        
        :obj:`table` is an alternative way of entering the altitude-dependent parameters,
        `h`, `r0`, `v`, `vaz`, and `L0`. For example, the default parameters can be expressed
        as a dictionary:
        
        .. code-block:: python
        
            {'L0': [inf, inf, inf],
             'h': [0.0, 1000.0, 5000.0],
             'r0': [0.1, 0.07, 0.1],
             'v': [5.0, 10.0, 20.0],
             'vaz': [0.0, 90.0, 45.0] }
        
        or as a :class:`pandas.DataFrame`:
        
        .. code-block:: python
        
                        h    r0     v   vaz   L0
                0     0.0  0.10   5.0   0.0  inf
                1  1000.0  0.07  10.0  90.0  inf
                2  5000.0  0.10  20.0  45.0  inf


        '''
        
        self.seeing = seeing
        self.telescope = telescope
        self.du = du
        self.name = name
        self.n_layers = len(seeing.altitude)
        if (dt is not None) and (tmax is not None):
            self.dt = dt
            self.tmax = tmax
        
        k = 2*np.pi/seeing.lam0
        Cn2_total = seeing.r0**(-5./3.)/(0.423*k**2)
        Cn2 = (Cn2_total*u.Quantity(seeing.Cn2).normedL1()).decompose()
        self.Cn2 = Cn2
        r0 = ( ( 0.423*k**2 * Cn2 )**(-3./5.) ).to(u.cm)
        self.seeing.seeing = (seeing.lam0/seeing.r0).to(u.arcsec,equivalencies=u.dimensionless_angles())
        v = seeing.wind_magnitude
        tau0 = (( 2.91*k**2*(Cn2*v**(5./3.)).sum())**(-3./5.)).to(u.ms)
        self.seeing.tau0 = tau0
        fG = (0.135/tau0).to(u.Hz)
        self.seeing.fG = fG
        h = seeing.altitude
        theta0 = ((2.905*k**2*(Cn2*h**(5./3.)).sum())**(-3./5.)).to(u.arcsec,equivalencies=u.dimensionless_angles())
        self.seeing.theta0 = theta0
        layers = []
        for i in range(self.n_layers):
            seeing_this_layer = DotDict({
                'r0':r0[i],
                'lam0':seeing.lam0,
                'L0':seeing.L0,
                'altitude':seeing.altitude[i],
                'wind_magnitude':seeing.wind_magnitude[i],
                'wind_direction':seeing.wind_direction[i],
            })
            kwargs = {
                'du':du,
                'seeing':seeing_this_layer,
                'telescope':telescope,
            }
            if (dt is not None) and (tmax is not None):
                a = BlowingScreen(dt=dt,tmax=tmax,**kwargs)
            else:
                a = Screen(**kwargs)
            layers.append(a)
        self.layers = layers
        self.table = self._tabulate()
        
    def __repr__(self):
        if options.pretty_print == 'short':
            return super(Atmosphere,self).__repr__()
        return oprint.pprint(self,out='string')
                
    def pprint(self,**kwargs):
        """:py:func:`Pretty-print <oprint.pprint>` the object, displaying the
        names and values of all the attributes.
        """
        pprint(self,**kwargs)

    def _tabulate(self):
        qq = []
        for i,layer in enumerate(self.layers):
            q = {
                'h (km)':layer.seeing.altitude.to(u.km).value,
                'r0 (cm)':layer.seeing.r0.to(u.cm).value,
                'seeing (asec)':layer.seeing.seeing.to(u.arcsec).value,
                'v (m/s)':layer.seeing.wind_magnitude.to(u.m/u.s).value,
                'vaz (deg)':layer.seeing.wind_direction.to(u.degree).value,
            }
            qq.append(q)
        df = pd.DataFrame(qq)
        return df
    
    @property
    def next(self):
        '''Get the next integrated screen in the time sequence
        
        :rtype: :class:`InfoArray`
        '''
        r = 0
        for s in self.layers:
            r += s.next
        return r

    def sequence(self,count=None,tmax=None,verbose=False):
        '''Create a time sequence of integrated screens.
        
        :param int count: the total number of screens
        :param float tmax: the final time value of the sequence, seconds
        :param bool verbose: talk about it
        
        Only one of `count` and `tmax` should be sepcified. The other will be computed accordingly.
        '''
        if count is not None:
            n = count
        elif tmax is not None:
            n = int(round(tmax/self.dt))
        else:
            raise Exception('either count or tmax must be specified')
        if verbose:
            print('<Atmosphere:sequence> getting',n,'screens')
        s = []
        krange = range(n)
        if verbose:
            krange = tqdm.tqdm(krange)
        for k in krange:
            s.append(self.next.copy())
        
        return s

#-------------------- tests -------------------

def pe():
    """deferred enabling of matplotlib
    """
    global plt,dimg
    #import dimg
    plt = dimg.plt
    plt.ion()

def test1(plot=False):
    """test1
    Calculate PSF and MTFs of an atmospheric screen
    """
    a = Screen()
    a.calc_psf()
    if plot:
        pe()
        dimg.show(a.psf)
        dimg.show(a.mtf)

# def _fwhm(psf):
#     q = (psf > 0.5*psf.max()).astype(int)
#     r = 2*np.sqrt(q.sum()/np.pi)*psf.dx[0]
#     return r

def fwhm(psf):
    n,m = psf.shape
    dx,dy = psf.dx
    x0,y0 = psf.x0
    assert n==m
    assert np.isclose(dx,dy)
    iy0,ix0 = [int(k) for k in np.where(psf==psf.max())]
    x = np.arange(ix0,n)*dx + x0
    y = psf[ix0:n,iy0]/psf.max()
    f = interp1d(x,y,kind='cubic')
    x_init = np.where(y>.5)[0].max()*dx
    r = least_squares(lambda x: f(x)-0.5,x_init.value)
    return 2*r.x[0]*x.unit

def test_fwhm_2(r0=10.*u.cm,plot=False):
    """test_fwhm
    Measure the full-width-half-max of generated PSFs
    """
    try:
        local_seeing = seeing.copy()
        local_seeing.r0 = r0
        if not plot:
            a = Screen(seeing=local_seeing)
            a.calc_psf()
            fw_hm = fwhm(a.psf)
            print(f'r0 = {a.seeing.r0}, FWHM = {fw_hm}')
        else:
            pe()
            from matplotlib.ticker import ScalarFormatter
            plt.figure()
            lam0 = local_seeing.lam0
            r0s = u.Quantity(np.linspace(2,20,30),u.cm)
            lams = u.Quantity([.5,.7,.9],u.um)
            colors = ['green','red','black']
            for lam,color in zip(lams,colors):
                fwhms = []
                for r0 in tqdm.tqdm(r0s):
                    local_seeing.r0 = r0
                    a = Screen(seeing = local_seeing)
                    a.calc_psf(lam=lam)
                    fw_hm = fwhm(a.psf)
                    fwhms.append(fw_hm)
                fwhms = u.Quantity(fwhms)
                plt.plot(r0s,fwhms,label=r'$\lambda$=%0.2f $\mu$'%lam.to('micron').value,color=color)
                r0_lam = r0s*(lam/lam0)**(6./5.)
                # the factor 0.976 is detremined from test_fwhm()
                fwhm_est = 0.976*(lam/r0_lam).to_da(u.arcsec)
                plt.plot(r0s, fwhm_est,'--',color=color)
            plt.xlabel(r'$r_{00}$, cm')
            plt.ylabel(r'FWHM, arcsec')
            plt.ylim(.3,3.)
            plt.yscale('log')
            plt.grid(True,which='both')
            ax = plt.gca()
            ax.yaxis.set_major_formatter(ScalarFormatter())
            ax.yaxis.set_minor_formatter(ScalarFormatter())
            ax.set_yticks([.3,.4,.5,.6,.8,.9,1.,1.2,1.5,2.0,2.2,3.])
            plt.legend(loc='upper right')
            plt.title(r'Full-Width-Half-Max $vs$ Seeing ($r_0$ @ $\lambda = 0.5 \mu$)'+
                      '\n'+r'solid: PSF measured, dashed: 0.976 $\lambda/r_0$')
            e = np.array(fwhms) - np.array(fwhm_est)
            pc = e / np.array(fwhms)
            print('maximum percent difference: %0.2f%%'% (np.abs(pc)*100).max())
    finally:
        globals().update(locals())

def test_fwhm():
    '''measure the relationship between r0 and fwhm of the psf
    '''
    r = []
    local_seeing = seeing.copy()
    for r0 in [5,7,10,12,15,18,20]*u.cm:
        local_seeing.r0 = r0
        a = Screen(seeing=local_seeing)
        a.calc_psf()
        fw_hm = fwhm(a.psf)
        lam = a.psf.wavelength
        lam_over_r0 = (lam/r0).to_da(u.arcsec)
        rat = ( fw_hm / lam_over_r0 ).to('').value
        r.append({'r0 (cm)':r0.value,'fwhm (asec)':fw_hm.value, 'lam/r0 (asec)':lam_over_r0.value, 'ratio':rat})
    r = pd.DataFrame(r).set_index('r0 (cm)')
    return r
    
def test_bs(show=False):
    """test_bs
    Create a data cube of phase screens
    sequenced from a blowing screen
    """
    a = Atmosphere(dt=30.*u.ms,tmax=4.*u.s)
    q = a.sequence(count=300,verbose=True)
    if show:
        pe()
        dimg.play(q)
    
def dont_tes_bswfs(count=100,comp='CPU',play=False):
    """test_bswfs
    Generate a blowing screen then run it
    through the wavefront sensor simulator,
    generating a data cube of CCD images.
    
    For now, the simulation is limited to the 16x
    mode on the wavefront sensor, a phase
    fine grid of 512x512 pixels, wind velocity of
    20 m/sec and dt = 0.02 seconds (50 Hz) wavefront
    sensing step.
    
    """
    import wfs
    if play: pe()
    w = wfs.Wfs()
    if comp == 'GPU':
        w.ccd_image_gpu_prep()
        ttgpu = np.array([-1.,-1])*w.pixel_scale/2.
        ttphgpu = wfs.tilt(w.ap,ttgpu,w.lam)
    print('generating blowing screen'); sys.stdout.flush()
    b = BlowingScreen(n=w.N,dt=.02,v=20.)
    print('done'); sys.stdout.flush()
    s = b.sequence(count,verbose=True)
    k = 0
    q = []
    print('generating %d CCD images using the %s'%(count,comp))
    timer = Timer(count)
    timer.start()
    for ph in tqdm.tqdm(s):
        ph *= 2*np.pi*nm/(w.wavelength*microns)
        ph.units = 'radians'
        if comp == 'CPU':
            w.ccd_image(ph)
        else:
            ph += ttphgpu
            ph = ph.astype(np.float32)
            w.ccd_image_gpu(ph)
        q.append(w.ccd.copy())
        timer.tick()
    timer.done()
    if play:
        dimg.play(q)

def test_amplitude(nsh=0,ncurves=10,ntrials=10,show=False):
    """test_amplitude
    Check the amplitude of the random phase screens
    against theoretically predicted amplitudes
    """
    try:
        if show: pe()
        s = Screen()
        
        D = s.telescope.D
        r0 = s.seeing.r0
        s0 = np.sqrt(.134)*(D/r0)**(5./6.)*(s.seeing.lam0/(2*np.pi)) # Noll coefficient tip/tilt removed
        s0 = s0.to('nm')
        print('theory rms: %s' % s0)
    
        n = s.n
        ap = s.ap
        if show:
            plt.ion()
            plt.plot([1,ntrials],u.Quantity([s0,s0]))
            plt.title('<atmos.test_amplitude> r0 = %s'%r0)
            plt.ylabel('wavefront rms, nanometers')
            plt.xlabel('trial #')
        ph_set = []
        rms_set = []
        for j in tqdm.tqdm(range(ncurves)):
            rmsi_set = []
            for k in range(ntrials):
                s.gen_screen(nsh=nsh)
                ph = s.screen.copy()
                ph = ph[0:n,0:n]
                ph = img.depiston(ph,ap)
                ph = img.detilt(ph,ap)
                ph_rms = img.rms(ph,ap)
                rmsi_set.append(ph_rms)
                ph_set.append(ph)
            rmsi_set = u.Quantity(rmsi_set)
            rms_set.append(rmsi_set)
            if show:
                plt.plot(range(1,ntrials+1),u.Quantity(rmsi_set),'.')
                plt.pause(0.05)
    
        rms_set = u.Quantity(rms_set)
        rms_mean = rms_set.mean()
        rms_sd = rms_set.std()
        if show:
            # qm,qsd = rms_mean, rms_sd
            # plt.plot([1,ntrials],u.Quantity([qm,qm]),'k:')
            # plt.plot([1,ntrials],u.Quantity([qm+qsd,qm+qsd]),'k--')
            # plt.plot([1,ntrials],u.Quantity([qm-qsd,qm-qsd]),'k--')
            # plt.show()
            x = range(1,ntrials+1)
            y = rms_set.mean(axis=0).value
            yerr = rms_set.std(axis=0).value
            plt.errorbar(x,y,yerr=yerr,capsize=5)
    finally:
        globals().update(locals())
    
    ne = (rms_mean - s0) / (rms_sd/np.sqrt(ntrials*ncurves))
    print('theory rms = %s, mean rms = %s, diff = %s, std dev = %s'%(s0,rms_mean,abs(rms_mean-s0),rms_sd))
    assert ne < 5 , 'mean rms exceeds theory by more than %0.1fx>5x std dev'%ne

def test_powerSpectra(ntrials = 1,fs=500*u.Hz,r0=10.*u.cm, v=10.*u.m/u.s, N=256,plot = False, oplot=False):
    """test_powerSpectrum
    Check that the temporal power spectrum is as expected
    """

    try:
        if plot: pe()
        
        if ntrials > 1:
            oplot = False
            for trial in range(ntrials):
                test_powerSpectra(fs=fs,r0=r0,v=v,N=N,plot=plot,oplot=oplot)
                oplot = True
                plt.pause(.01)
            return
        
        bseeing = seeing.copy()
        bseeing.r0 = r0
        bseeing.wind_magnitude = v
        dt = (1./fs).to(u.ms)
        T = N*dt
        b = BlowingScreen(dt=dt,tmax=T,seeing=bseeing)
        D = b.telescope.D #3.0
        df = (1./T).to(u.Hz)
        d = 21.*u.cm # subaperture diameter
        
        t0 = 0.*u.ms
        t = Qrange(t0,T,dt)
        f0 = 0.*u.Hz
        f = Qrange(f0,N*df,df)
        
        n = b.n
        ap = b.ap
        r = u.Quantity([telescope.D,telescope.Ds]).mean()/2
        xy = [r/np.sqrt(2.)]*2
        x,y = [int((xx/b.du).decompose().round())+n//2 for xx in xy]
        di = int((d/b.du).decompose().round())
        r1 = np.sqrt(float(x-n//2)**2 + float(y-n//2)**2)*b.du # radial position of single point on aperture
        phaset = []
        sphaset = []
        for tk in tqdm.tqdm(t):
            ph = b.next
            ph = img.depiston(ph,ap)
            sph = np.mean(ph[x-di//2:x+di//2,y-di//2:y+di//2])
            phaset.append(ph[x,y])
            sphaset.append(sph)
        phaset = u.Quantity(phaset)
        phaset.name = 'single point phase at [%d, %d] in aperture'%(x,y)
        phaset.dt = dt
        phaset.seeing = b.seeing
        phaset.v = b.seeing.wind_magnitude
        phaset.t = t
        
        sphaset = u.Quantity(sphaset)
        sphaset.name = 'subap ave phase at [%d, %d] in aperture'%(x,y)
        sphaset.dt = dt
        sphaset.seeing = b.seeing
        sphaset.xy = [x*b.du,y*b.du]
        sphaset.v = b.seeing.wind_magnitude
        sphaset.t = t    
        
        #PSD = (1./T)*np.abs(np.fft.fft(phaset)*dt)**2 # astropy v3
        PSD = (1./T)*np.abs(np.fft.fft(phaset.value)*dt)**2 # astropy v4
        PSD = PSD.to(1./u.Hz)*(phaset.unit)**2
        PSD.name = 'temporal PSD single point phase'
        PSD.df = df
        PSD.seeing = phaset.seeing
        PSD.v = phaset.v
        PSD.f = f
        # 
        #sPSD = (1./T)*np.abs(np.fft.fft(sphaset)*dt)**2 # astropy v3
        sPSD = (1./T)*np.abs(np.fft.fft(sphaset.value)*dt)**2 # astropy v4
        sPSD = sPSD.to(1./u.Hz)*(sphaset.unit)**2
        sPSD.name = 'temporal PSD subap phase'
        sPSD.df = df
        sPSD.seeing = sphaset.seeing
        sPSD.v = sphaset.v
        sPSD.f = f
        
        if plot:
            if not oplot:
                fig = plt.figure(figsize=(8,12))
            ax = plt.subplot(211)
            plt.plot(phaset.t,phaset)
            plt.plot(sphaset.t,sphaset,'--')
            if not oplot:
                plt.xlabel('time, sec')
                plt.ylabel('phase, nanometers')
                plt.title(phaset.name)
                plt.grid(True)
            
            ax = plt.subplot(212)
            plt.plot(PSD.f[1:N//2],PSD[1:N//2],label='single point')
            plt.plot(sPSD.f[1:N//2],sPSD[1:N//2],'--',label='subap ave')
            if not oplot:
                plt.xscale('log')
                plt.yscale('log')
                plt.xlabel('frequency, Hz')
                plt.ylabel(r'nm$^2$ / Hz')
                plt.grid(True,which='both')
                plt.title('Phase Power Spectrum')
        
                #model from Greenwood and Fried
                '''Greenwood, D. P., & Fried, D. L. (1976). Power spectra
                requirements for wave-front-compensative systems. Optical
                Society of America, 66(3), 193.
                https://doi.org/10.1364/JOSA.66.000193'''
                # mid freq
                f_phi = (0.07713*(v/r0)**(5./3.)*f**(-8./3.)).to(1./u.Hz)
                # low freq
                f0 = (v/(np.pi*D)).to(u.Hz)
                f0k = int(round(f0/df))
                if f0k in range(len(f)):
                    f_phi[1:f0k] = (3.045*(v/r0)**(5./3.)*(r1/v)**2*f[1:f0k]**(-2./3.)).to(1./u.Hz)
                # high freq
                f1 = (0.366/(np.pi*(d/v))).to(u.Hz)
                f1k = int(round(f1/df))
                # g_phi: subap window filter
                g_phi = np.ones(len(f))
                g_phi[f1k:] = 1.26-0.712*np.pi*(d/v)*f[f1k:]
                g_phi = np.clip(g_phi,0.,np.inf)
                k = 2*np.pi / (b.seeing.lam0)
                f_phi = (1./k)**2*f_phi
                f_phi = f_phi.to(u.nm**2/u.Hz)
                f2_phi = f_phi*g_phi
                plt.plot(f[1:],f_phi[1:],linestyle='dotted',label='model')
                plt.plot(f[1:],f2_phi[1:],linestyle='dotted',label='model with subap')
                
                plt.legend()
        
    finally:
        globals().update(locals())

