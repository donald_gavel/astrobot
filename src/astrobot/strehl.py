"""Strehl.py
   does a "best-fit" calculation of Strehl ratio
"""
import numpy as np
from astrobot import img
from astrobot import atmos
from astrobot import dimg
from astrobot.info_array import InfoArray
import matplotlib.pyplot as plt
from astropy import units as u

version = 0.1

class PSF(InfoArray):
    special_attributes = ['ap','wavelength']
    def __new__(cls,input_array,**kwargs):
        u.set_enabled_equivalencies(u.dimensionless_angles())
        
        obj = InfoArray.__new__(InfoArray,input_array,**kwargs)
        dx = obj.dx
        n,m = input_array.shape
        assert n == m
        n = n
        if 'wavelength' not in kwargs:
            wavelength = 2.2*u.micron
        else:
            wavelength = kwargs['wavelength']
        if 'ap' not in kwargs:
            du = (wavelength/(n*dx)).to('m')
            r = 3.0*u.meter/2./du
            ap = InfoArray(img.circle((n,n),r=r),
                           dx=du,
                           name=obj.name+' aperture')
        else:
            ap = kwargs['ap']
        obj.ap = ap
        obj.wavelength = wavelength
        return obj.view(cls)
    
    def __repr__(self):
        return self.hdr
    
    def strehl(self):
        print '<PSF.strehl> 1.0 of course!'
    

def strehl(data,wavelength=1.6*u.micron, center=None, anulus=[], camera='ShARCS', pixel=None,
           dp=3.0*u.meter, ds=0.8*u.meter, verbose=True, plot=True):
    """
    Evaluate the Strehl of a PSF image
    
    :param InfoArray image: image data array
    :param Quantity wavelength: wavelength of imaging
    :param ndarray center: 2-vector approximate center of PSF, in pixels (this is refined during calculations)
    :param ndarray anunlus: 3-vector [r1,r2,r3] where r1 = photometry radius, in pixels and r2,r3 = inner and outer radius of sky background annular region, in pixels
    :param str camera: 'ShRACS' - ShaneAO infrared science camera: 'ShaneAO_TTS' - ShaneAO's visible light tip/tilt sensor CCD
    :param Quantity pixel: pixel scale on sky.  Required if camera not specified.
    :param Quantity dp: primary mirror diameter. Default 3 meters
    :param Quantity ds: secondary obscuration diameter. Default 0.8 meters
    
    :returns (strehl,datasn,dmask,bmask,psfn)
    
    where::
    
        strehl = strehl ratio
        datasn = data normalized to unit integral
        dmask = data mask
        bmask = background mask
        psfn = model diffraction-limited psf, normalized to unit integral
    """
    (n,m) = data.shape
    assert (n==m)
    if (center == None):
        center = np.array([n/2,m/2])
    if (pixel == None):
        pixel = 0.033*arcsec;
    if (anulus == []):
        anulus = [100.,100.,150.] # units of pixels
    r1,r2,r3 = anulus
    if (camera == 'ShARCS'):
        pixel = 0.033*arcsec;
        dp = 3.0
        ds = 0.8
    if (camera == 'TTS' or camera == 'ShaneAO_TTS'):
        pixel = 0.33*arcsec;
        dp = 3.0
        ds = 0.8
        
    # set up photometry aperture and background masks
    dmask = img.circle((n,m),center,r1)
    bmask = img.circle((n,m),center,r3) - img.circle((n,m),center,r2)
    
    # pixel-shift the data so the peak is at the center of the photometry aperture
    d = data*dmask
    c = np.array(np.unravel_index(d.argmax(),d.shape))
    if (verbose): print '<strehl> data peak is at '+str(c)
    datas = img.shift(data,center-c)
    
    # remove the background, as defined by the anulus
    dataB = img.depiston(datas,bmask)
    
    # find the sub-pixel centroid of the central core
    c = img.centroid(dataB,dmask)
    datasn = dataB/np.sum(dataB*dmask) # data normalized to integrate to one in the photometry aperture
    
    # define a model diffraction-limited PSF
    psf = img.psf(n,dp,ds,lam,pixel,center=c)
    psfn = psf/np.sum(psf*dmask) # model PSF is also normalized to integrate to one in the photometry aperture
    
    strehl = datasn.max()/psfn.max()
    if (verbose): print '<strehl> Strehl = '+str(strehl)
    
    if (plot): dimg.tv(datasn/datasn.max()+dmask*2+bmask,origin='center',dx=pixel/arcsec)
    return (strehl,datasn,dmask,bmask,psfn)

def dummy_image(r0=0.2,verbose=True,mH=10.,noise=13.,sky=12.,Texp=5.):
    """
    Create dummy test image for the strehl routine
    
    keyword arguments:
        * r0: Fried's parameter, given in meters at 0.55 micron wavelength
        * mH: magnitude of the object (star) in H band
        * noise: the number of noise electrons per pixel
        * sky: the sky background, in magnitudes per square arcsec
        * Texp: exposure time, in seconds.
        * verbose: whether or not to print out the predicted Strehl information
    
    returns:
        * p: the data image
        * strehlma: predicted Strehl based on Marechal's rms phase approximation
        * strehln: predicted Strehl based on Noll's model based on aperture and r0
        * ap: the aperture (1 inside, 0 outside the aperture)
        * wf: the complex aberrated wavefront
    """
    n = 256
    pixel = 0.033*arcsec
    lam = 1.6*micron
    telescope = 'Shane'
    dp = 3.0
    ds = 0.8
    
    # photometry
    lam0 = 0.55*micron
    r0_H = r0*(lam/lam0)**(6./5.)
    seeing = 2.*(lam/r0_H)/arcsec
    well_depth = 60000 # well depth of the HiII-RG detector
    flux = 2.82e9 # photons / m^2 / s for V=0 star; see photometry.xls
    qe = 0.4 # system throughput
    A = np.pi*((dp/2)**2 - (ds/2)**2)  # collecting area of the telescope
    nph = flux*10**(-mH/2.5)*A*Texp*qe  # total number of collected photons
    n_pixels_on_star = seeing**2 / (pixel/arcsec)**2
    ave_ph_per_pixel = nph/n_pixels_on_star
    if (verbose):
        print '<strehl.dummy_data> average signal/pixel = '+str(ave_ph_per_pixel)+ \
               ' compared to well depth of '+str(well_depth)
    
    background = flux*10**(-sky/2.5)*A*Texp*qe*(pixel/arcsec)**2
    if (verbose):
        print '<strehl.dummy_data> background flux = '+str(background)+' photons / pixel'
    
    # create a pupil
    dx = pixel
    du = lam/(n*dx)
    ap = img.circle((n,n),(n/2,n/2),dp/du/2.)
    if (ds > 0):
        ap -= img.circle((n,n),(n/2,n/2),ds/du/2.)
    
    mag = ap/np.sqrt(np.sum(ap*ap))
    
    # phase aberration
    lam0 = 0.55e-6
    #r0 = r0*(lam/lam0)**(6./5.) # scale r0 to wavelength
    s = atmos.Screen(telescope=telescope,n=n,r0=r0,du=du).gen_screen()
    ph = s.screen.to_units('radians',wavelength=(lam/micron)*u.microns)
    
    # analyze the wavefront
    e = img.rms(ph,ap)
    if (verbose): print '<strehl.dummy_data> rms wavefront error = '+str(e*lam/(2*np.pi)/nm)+' nm rms'
    strehlma = np.exp(-e) # Marechal approximation Strehl
    strehln = np.exp(-0.134*(dp/r0)**(5./3.)) # Noll's Strehl
    if (verbose): print '<strehl.dummy_data> Marechal approximation says Strehl = '+str(strehlma)
    if (verbose): print '<strehl.dummy_data> Noll approximation says Strehl = '+str(strehln)
    
    # create a complex near-field wavefront
    wf = mag*np.exp(1j*ph)
    
    # transform to image plane
    f = img.ft(wf)/float(n)
    p = np.abs(f)**2
    p *= nph
    
    # add photon noise from star
    star_noise = np.sqrt(p)*np.random.normal(size=(n,n))
    p += star_noise
    
    # add background and background noise
    sky_noise = np.sqrt(background)*np.random.normal(size=(n,n))
    p += background*np.ones((n,n)) + sky_noise
    
    # add read noise
    read_noise = noise*np.random.normal(size=(n,n))
    p += read_noise
    
    return (p,strehlma,strehln,ap,wf)

def test(n=20):
    """
    Run a statistical test of the Strehl calculator by generating
    a number of simulated images and evaluating Strehl. The
    Kolmogorov screen generator is used as a source of the aberrations.
    
    r0 determines the average phase screen statistics and the Noll
    estimate of Strehl is based on D/r0. The Marechal estimate is based
    on the rms phase aberration. Finally, the 'Sim' estimate is the
    one calculated from the image, as provided by the Strehl routine acting
    on the simulated image.
    
    The routine plots the test results in a scatter plot, and also
    displays results in histogram form. The Noll estimate is a single black
    line since it is determined by D/r0. The Marechal and Sim results are
    statistical points. Generally Sim Strehl is higher than the predicted
    Marechal Strehl because the single-screen realizations tend to have
    bright speckles.
    
    Arguments:
        * n: number of independent test realizations. Default is 20
        
    Module variable:
        * test_results is the table of test results.
        
    """
    global test_result
    r0 = 0.2
    print '===================================================='
    print '      Noll    |      Marechal       |     Sim      |'
    print '----------------------------------------------------'
    r = []
    for k in range(n):
        p,sma,sn,ap,wf = dummy_image(r0=r0,verbose=False)
        s,datasn,dmask,bmask,psfn = strehl(p,verbose=False,plot=False)
        print str(sn)+'  |  '+str(sma)+'  |  '+str(s)+'   |'
        r.append([sn,sma,s])
    print '===================================================='
    r = np.array(r)
    
    plt.figure(1)
    plt.plot(r[:,0],color='k',label='Noll')
    plt.plot(r[:,2],'g^',label='Sim')
    plt.plot(r[:,1],'rs',label='Marechal')
    plt.grid('on')
    plt.legend()
    plt.show()
    
    plt.figure(2)
    plt.hist(r[:,0],color='k',rwidth=.1,bins=25,range=(0,1),label='Noll')
    plt.hist(r[:,2],color=(0,1.0,0,.7),bins=25,range=(0,1),label='Sim')
    plt.hist(r[:,1],histtype='step',color=(1.0,0,0,.7),bins=25,range=(0,1),label='Marechal')
    plt.legend()
    plt.grid('on')
    plt.show()
    
    test_result = r
    
    