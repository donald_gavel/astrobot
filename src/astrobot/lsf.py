#!/usr/bin/env python
"""List all the fits files by keyword

lsf keyword

"""
import os
import sys
from astropy.io import fits

def listFits(keyword=None,in_comments=True):
    file_list = os.listdir('.')
    for fileName in file_list:
        if ('.fits' in fileName):
#        if (fileName.endswith('.fits')):
            if (keyword == None):
                print(fileName)
            else:
                hdu = fits.open(fileName)
                hdr = hdu[0].header
                if (keyword in hdr):
                    u = hdr.cards
                    lin = u[keyword]
                    print((fileName+': '+str(lin)).strip())
                if in_comments:
                    if ('COMMENT' in hdr):
                        c = str(hdr['COMMENT'])
                        if (keyword in c):
                            print((fileName+': '+'COMMENT '+c).strip())
                hdu.close()

def db(kwlist,dlist=None,ofile=None):
    if ofile:
        f = open(ofile,'w')
    else:
        f = None
    if not dlist:
        dlist = os.listdir('.')
    lin = 'dir, file'
    for kw in kwlist:
        lin += ', '+kw
    if f:
        f.write(lin+'\n')
    else:
        print(lin)
    for d in dlist:
        if os.path.isdir(d):
            flist = os.listdir(d)
            for fileName in flist:
                if ('.fits' in fileName):
                #if (fileName.endswith('.fits')):
                    hdu = fits.open(os.path.join(d,fileName))
                    hdr = hdu[0].header
                    lin = d+', '+fileName
                    for keyword in kwlist:
                        lin += ', '+str(hdr[keyword])
                    if f:
                        f.write(lin+'\n')
                    else:
                        print(lin)
                    hdu.close()

if __name__ == '__main__':
    doit = True
    in_comments = True
    keyword = None
    if len(sys.argv) > 1:
        for arg in sys.argv[1:]:
            if arg in ['--only','-o']:
                in_comments = False
            elif arg in ['--help','-h']:
                print('lsf ; lists all fits files in the current directory')
                print('lsf keyword ; lists all fits with the given keyword')
                print('     it also lists all fits files having that word as part of a comment')
                print('-o turns off printing comments')
                doit = False
            else:
                keyword = arg
    if doit:
        if keyword is None:
            listFits()
        else:
            listFits(keyword,in_comments=in_comments)

