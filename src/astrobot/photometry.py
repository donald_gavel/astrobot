#!/usr/bin/env python -i
'''photometry.py
Perform photometry calculations for astronomy.
Star types and magnitudes, fluxes, pass band filters

This file is in
_fn = '/Users/donaldgavel/astrobot/astrobot/photometry.py'
execfile(_fn)

from astrobot import photometry as photom

from astrobot.photometry import *
'''
import os
import sys
import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
import dateutil.parser as dparser
import datetime
import pytz
import re
from tqdm import tqdm as progbar
from pkg_resources import resource_stream
import warnings

from astropy import units as u
from astropy import constants as const
from astropy.table import Table, QTable, MaskedColumn, Column
from astropy.io import fits
from astroquery.simbad import Simbad
from astropy.time import Time
from astropy.coordinates import SkyCoord, EarthLocation, AltAz
from astropy.utils.exceptions import AstropyWarning

from astrobot import finder
from astrobot import oprint
from astrobot import q_helper
from astrobot.dotdict import DotDict
from astrobot.labellines.core import labelLine
from astrobot.tcolor import tcolor

import matplotlib.pyplot as plt

try:
    utf_enabled = 'utf' in os.environ['LANG'].lower()
except:
    utf_enabled = False
    
plt.ion()

h = const.h
c = const.c
kB = const.k_B

#=======================================
# Read in tables that support photometry
#=======================================

spectral_types_table = Table.read(resource_stream('astrobot','data/spectral_types_table.ecsv'),format='ascii.ecsv')

# 0 magnitude reference star spectrum
with warnings.catch_warnings(): # ignore the ANGSTROM and FLAM UnitsWarning
    warnings.simplefilter('ignore',AstropyWarning)
    vega_st = Table.read(resource_stream('astrobot','data/alpha_lyr_005.fits'))
vega_st.meta['comment'] = 'from https://ssb.stsci.edu/cdbs_open/cdbs/calspec, 0.1 to 2.6 micron'

vega_hr = QTable.read(resource_stream('astrobot','data/vega_hr.ecsv'),format='ascii.ecsv')
vega_hr.meta['comment'] = 'HST high-resolution, 0.38 to 1 micron'

# standard Johnson and IR band filters
filter_bands_table_text = '''
filter      descrip                 plot_color
U       "Johnson U - ultraviolet"   blueviolet
B       "Johnson B - blue"          blue
V       "Johnson V - mid visible"   green
R       "Johnson R - red"           red
I       "Infrared 800"              darkred
J       "Near IR 1.2"               blue
H       "Near IR 1.6"               green
Ks      "K short"                   red
K       "K long"                    darkred
D6-9    "SAO Dichroic 6-9um pass"   red
'''
filter_bands_table = Table.read(filter_bands_table_text,format='ascii')
filter_bands = list(filter_bands_table['filter'])

arnica_stars = pd.read_csv(resource_stream('astrobot','data/arnica.csv'),delimiter='\t')

#=============================
# spectro=photometry functions
#=============================

cached_data = {} # for storing spectra and other stuff repeatedly needed when creating black body spectra

def blackbody(lam,T):
    '''Planks' law
    inputs:
        lam = wavelength or list of wavelengths
        T = Temperature
    output:
        B = spectral radiance, per unit wavelength per unit area per unit solid angle
    '''
    h = const.h
    c = const.c
    kB = const.k_B
    B_unit = 'kW/(m**2*sr*nm)'
    if lam.isscalar:
        B = (2*h*c**2/lam**5)*(1/(np.exp(h*c/(lam*kB*T))-1))/u.sr
        return B.to(B_unit)
    B = u.Quantity([blackbody(x,T).value for x in lam],B_unit)
    r = QTable([lam,B],names=['wavelength','flux'])
    return r

def precalc_colors():
    '''calculate B-V and J-H color indices for each spectral
    type in the spectral types table
    (NOTE: this took 3 min 25 sec on a MacBook Pro)
    '''
    types = list(spectral_types_table['Spectral Type'])
    spectral_types_table['B-V'] = float('nan')
    spectral_types_table['J-H'] = float('nan')
    for row,star_type in tqdm.tqdm(enumerate(types),total=len(types),ascii=not utf_enabled):
        s = Star(type=star_type)
        B = s.mag('B')
        V = s.mag('V')
        H = s.mag('H')
        J = s.mag('J')
        spectral_types_table['B-V'][row] = B-V
        spectral_types_table['J-H'][row] = J-H
    spectral_types_table['B-V'].info.format = '{:15.4g}'
    spectral_types_table['J-H'].info.format = '{:15.4g}'

def save_precalc_colors():
    spectral_types_table.write('spectral_types_table.ecsv',format='ascii.ecsv')

def restore_precalc_colors():
    global spectral_types_table
    t = Table.read(resource_stream('astrobot','data/spectral_types_table.ecsv'),format = 'ascii.ecsv')
    spectral_types_table = QTable(t)
    spectral_types_table.add_index('Spectral Type')

def plot_precalc_colors():
    t = spectral_types_table
    plt.figure()
    plt.plot(t['Temperature'],t['B-V'],'.-',label='B-V')
    plt.plot(t['Temperature'],t['J-H'],'.-',label='J-H')
    plt.gca().set_xscale('log')
    plt.grid(1,which='both')
    plt.legend()
    plt.xlabel('Temperature, K')
    plt.ylabel('Color Index')
    plt.title('Color indices, from the spectral types table, based on blackbody spectra')

def demo_blackbody_curves():
    # see https://en.wikipedia.org/wiki/Black-body_radiation
    plt.figure()
    Ts = u.Quantity([5000,4000,3000])*u.Kelvin
    lams = np.linspace(0.2,3,100)*u.micron
    for T in Ts:
        BB = blackbody(lams,T)
        y = BB['flux']
        plt.plot(lams.value,y.value,label=str(T))
    
    plt.grid(1)
    plt.xlabel(r'Wavelength, $\mu$m')
    plt.ylabel(r'Spectral radiance, kW sr$^{-1}$ m$^{-2}$ nm$^{-1}$')
    #plt.legend()
    labelLines(plt.gca().get_lines(),zorder=2.5)
    plt.title('Black-body radiation')

def demo_solar_spectrum():
    # solar spectrum from
    # https://www.nrel.gov/grid/solar-resource/spectra-astm-e490.html
    # see: https://en.wikipedia.org/wiki/Planck%27s_law
    sun_spectrum_file = resource_stream('astrobot','data/e490_00a_amo.xls')
    s = pd.read_excel(sun_spectrum_file)
    lams = s['Wavelength, microns']
    ysol = s['E-490 W/m2/micron']
    
    plt.figure()
    plt.fill_between(lams,ysol,color='orange',label='Solar')
    plt.ylabel(r'Spectral irradiance, W/(m$^2$ $\mu$m)')
    plt.xlabel(r'Wavelength, $\mu$m')
    plt.gca().set_xlim(0,3.)
    plt.grid(1)
    
    # normalize integrated blackbody curve to 1367 W/m2 at T=5777K
    T = 5777*u.Kelvin
    lams = np.linspace(0.2,3,100)*u.micron
    dlam = lams[1]-lams[0]
    BB = blackbody(lams,T)
    y = BB['flux'].value
    yI = y.sum()*dlam
    yN = 1367*(y/yI)
    plt.fill_between(lams,yN,color='grey',alpha=0.5,label='black-body')
    
    plt.legend()
    plt.title('Solar spectrum compared to black-body at T = {}'.format(str(T)))

def vega_spectrum(data='hr',T = 9300*u.Kelvin, plot=False):
    '''Produce an astropy table of the Vega flux and
    another table of the close-fit blackbody curve
    
    Arguments:
        data - either 'hr' high-resolution, visible wavelengths, from Oke
               or 'st' space-telescope, vis and IR, from the space-telescope calibrations database
        T - the blackbody temperature
        plot - True means produce a plot as well as return the tables
    
    Returns:
        An Astropy Table with the Vega spectrum and
        an Astropy Table with a blackbody spectrum normalized to match Vega's at 0.548 microns
    '''
    if data == 'hr':
        lams = u.Quantity(vega_hr['Wavelength, A'],'Angstrom').to('micron')
        F = u.Quantity(vega_hr['Absolute flux Vega'],'W/(m**3)')
        lamsB = np.linspace(0.2,1.,100)*u.micron
        database = 'high resolution'
    elif data == 'st':
        # unrecognized units in the fits table: ANGSTROM, FLAM
        lams = u.Quantity(list(vega_st['WAVELENGTH']),'Angstrom').to('micron')
        F = u.Quantity(list(vega_st['FLUX']),'erg/s*cm**2*Angstrom').to('W/m**3')
        lamsB = np.linspace(0.2,2.6,250)*u.micron
        database = 'space telescope (alpha_lyr_005)'
    #T = 9300*u.Kelvin
    B = blackbody(lamsB,T)['flux']
    # make the fluxes match at 5480 Angstrom by scaling the black-body to the vega flux
    B0 = blackbody(5480*u.Angstrom,T)
    F0 = u.Quantity(0.0364,'W/(m**2*m)')
    B *= F0/B0
    if plot:
        plt.plot(lams,F,label='Vega')
        plt.plot(lamsB,B,label='black-body {}'.format(str(T)))
        plt.grid(1)
        plt.xlabel(r'Wavelength, $\mu$m')
        plt.ylabel(r'Flux, {}'.format(str(F.unit)))
        plt.legend()
    t = QTable([lams,F],names=('wavelength','flux'),
        meta={'name':'Vega',
              'data':data,
              'description':'Vega spectrum from the %s database'%database},)
    tb = QTable([lamsB,B],names=('wavelength','flux'),
        meta={'name':'blackbody',
              'T':T,
              'description':'normalized to match Vega flux at 5480 A'},)
    return [t,tb]

def starBB(mag = ('V',u.Magnitude(0)), color=9300*u.Kelvin,):
    '''create a star spectrum assuming it is blackbody.
    
    Arguments:
        magn - a tuple in the form of (band,value) where band
            is one of 'U','B','V','R','I','J', or 'K' and value is
            an Astropy Magnitude.
        color - either a tuple in the form of ('B-V',float) or ('J-H',float)
            or an Astropy Quantity blackbody temperature, T.
    
    Returns:
        An Astropy QTable with columns 'wavelength' and 'flux'
    '''
    if 'vega_spectrum_st' not in cached_data:
        vega,_ = vega_spectrum('st')
        cached_data['vega_spectrum_st'] = vega
    else:
        vega = cached_data['vega_spectrum_st']
        
    if 'spectral_types_temp_vs_BV' not in cached_data:
        tbv = interp1d(spectral_types_table['B-V'],spectral_types_table['Temperature'])
        cached_data['spectral_types_temp_vs_BV'] = tbv
    else:
        tbv = cached_data['spectral_types_temp_vs_BV']
    if 'spectral_types_temp_vs_JH' not in cached_data:
        tjh = interp1d(spectral_types_table['J-H'],spectral_types_table['Temperature'])
        cached_data['spectral_types_temp_vs_JH'] = tjh
    else:
        tjh = cached_data['spectral_types_temp_vs_BV']
    
    band,m = mag
    if not isinstance(m,u.Magnitude):
        m = u.Magnitude(m)
    if isinstance(color,tuple):
        name,val = color
        T = {'B-V':tbv,'J-H':tjh}[name](val)*u.Kelvin
        #t = interp1d(spectral_types_table[name],spectral_types_table['Temperature'])
        #T = t(val)*u.Kelvin
    else:
        T = color
    lamsB = np.linspace(0.2,2.6,250)*u.micron
    B = blackbody(lamsB,T)
    F = B['flux'].to('W / m**3*sr')
    fv = spectrum_flux(vega,band)
    fb = spectrum_flux(B,band=band)
    F = (F*(fv/fb)*m.physical)
    q = QTable([lamsB,F],names=['wavelength','flux'],meta={'mag':mag,'color':color})
    return q

def vega_flux(band,spectdb = 'st',plot=False,in_photons=False):
    '''
    interpolate the filter on the spectrum
    and integrate the flux
    
    Arguments:
        band - the filter band (one of U,V,B,R,I,J,H,Ks)
        spectdb - the spectrum databse, either 'hr' (hi-res vis) or 'st' (space-telescope).
                Note that only 'st' can be used for the IR filters
        plot - if True, produce a plot of the spectrum and the filter x spectrum
    
    Returns:
        bandpass flux, an Astropy Quantity with units of photons per unit time per unit area
    '''
    if ',' in band:
        band = band.split(',')
    if isinstance(band,list):
        r = []
        for bnd in tqdm.tqdm(band,ascii=not utf_enabled):
            f = vega_flux(bnd,spectdb=spectdb,plot=plot,in_photons=in_photons)
            r.append(f.value)
        if plot:
            try:
                labelLines(plt.gca().get_lines(),zorder=2.5,align=False)
            except:
                pass
            plt.title('Vega and bandpass filters')
        r = QTable([band,r*f.unit],names=['band','flux'])
        r['flux'].info.format = '{:15.4g}'
        r.add_index('band')
        return r
    
    vega,vegaBB = vega_spectrum(spectdb)
    if in_photons:
        vega = sed_to_spd(vega)
    
    return spectrum_flux(vega,band=band,plot=plot)

def blackbody_flux(band,T = 9300*u.Kelvin,plot=False,in_photons=False):
    '''
    interpolate the filter on the blackbody spectrum,
    which is scaled to match Vega at lambda = 5480 A, and
    integrate the flux
    
    Arguments:
        band - the filter band (one of U,V,B,R,I,J,H,Ks)
        T - the blackbody temperture
        plot - if True, produce a plot of the spectrum and the filter x spectrum
    
    Returns:
        bandpass flux, Astropy Quantity with units of photons per unit time per unit area
    '''
    if ',' in band:
        band = band.split(',')
    if isinstance(band,list):
        r = []
        for bnd in band:
            f = blackbody_flux(bnd,T,plot=plot,in_photons=in_photons)
            r.append(f.value)
        if plot:
            try:
                labelLines(plt.gca().get_lines(),zorder=2.5,align=False)
            except:
                pass
            plt.title('Blackbody {} and bandpass filters'.format(T))
        r = QTable([band,r*f.unit],names=['band','flux'])
        r['flux'].info.format = '{:15.4g}'
        r.add_index('band')
        return r
    lamsB = np.linspace(0.2,2.6,250)*u.micron
    B = blackbody(lamsB,T)
    B0 = blackbody(5480*u.Angstrom,T)
    F0 = u.Quantity(0.0364,'W/(m**2*m)')
    B['flux'] *= F0/B0
    if in_photons:
        B = sed_to_spd(B)
    return spectrum_flux(B,band=band,plot=plot)
    
def spectrum_flux(s, band, plot=False):
    '''
    interpolate the filter on the spectrum and integrate the flux
    
    Arguments:
        s - the spectrum. A QTable with columns 'wavelength' and 'flux'
        band - the filter band (one of U,V,B,R,I,J,H,Ks)
        plot - if True, produce a plot of the spectrum and the filter x spectrum
    
    Returns:
        bandpass flux, Astropy Quantity with units of flux x units of wavelength
    '''
    if ',' in band:
        band = band.split(',')
    if isinstance(band,list):
        r = []
        for bnd in tqdm.tqdm(band,ascii=not utf_enabled):
            f = spectrum_flux(s,bnd,plot=plot)
            r.append(f.value)
        if plot:
            try:
                labelLines(plt.gca().get_lines(),zorder=2.5,align=False)
            except:
                pass
            plt.title('Spectrum and bandpass filters')
        r = QTable([band,r*f.unit],names=['band','flux'])
        r['flux'].info.format = '{:15.4g}'
        r.add_index('band')
        return r
    if isinstance(s,QTable):
        wavelength_unit = s['wavelength'].unit
        flux_unit = s['flux'].unit
        s = s.to_pandas()
    else: # s is a pandas table, which doesn't carry units, so assume:
        print('WARNING s is a pandas table. Making assumptions on units...')
        wavelength_unit = 'micron'
        flux_unit = 'J/s*m**3'
    fdata = get_filter(band)
    fdata['wavelength'] = fdata['wavelength'].to(wavelength_unit)
    fdata['transmission'] = fdata['transmission'].to(u.Unit(''))
    fdata = fdata.to_pandas()
    w0,w1 = (lambda x: (x.min(),x.max()) )(fdata.wavelength)
    s = s[s['wavelength'].between(w0,w1)]
    f = interp1d(fdata['wavelength'],fdata['transmission'])
    t = f(s['wavelength'])
    t = u.Quantity(t[1:])
    dx = u.Quantity(s['wavelength'].diff(),wavelength_unit)[1:]
    s = s.iloc[1:]
    F = u.Quantity(s['flux'],flux_unit)
    lam = u.Quantity(s['wavelength'],wavelength_unit)
    r = (F*t*dx).sum()
    if plot:
        plt.plot(lam.value,F.value,color='black')
        plt.plot(lam.value,(F*t).value,label=band,color=filter_bands_table.loc[band]['plot_color'])
        plt.grid(1)
        plt.title('Spectrum and {} band filter'.format(band))
        plt.xlabel(r'Wavelength, {0:latex}'.format(lam.unit))
        plt.ylabel(r'Flux, {0:latex}'.format(F.unit))
    # try:
    r = r.decompose(bases=['ph','W','s','m','sr'])
    # except:
    #     r = r.decompose()
    # DEBUG globals().update(locals())
    return r

def get_filter(f):
    '''get the transmission curve for a given filter
    '''
    assert f in filter_bands, 'arg must be one of {}'.format(filter_bands)
    if f == 'D6-9':
        lams = (np.linspace(550,950,50)*u.nm).to('Angstrom') # from http://www.ifa.hawaii.edu/~tokunaga/filterSpecs.html
        t = u.Quantity([1. if x>600*u.nm and x<900*u.nm else 0. for x in lams])
    elif f == 'K':
        lams = (np.linspace(1.9,2.5,50)*u.micron).to('Angstrom') # from http://www.ifa.hawaii.edu/~tokunaga/filterSpecs.html
        t = u.Quantity([1. if x>2.03*u.um and x<2.37*u.um else 0. for x in lams])
    else:
        filter_data_file = resource_stream('astrobot','data/Photometry.xls')
        r = pd.read_excel(filter_data_file,sheet_name = f,header=1,usecols='B:C')
        if f in ['J','H','Ks']:
            lams = u.Quantity(r['wavelength'],'micron')
            t = u.Quantity(r['transmission'],u.percent)
        else:
            lams = u.Quantity(r['wavelength'],'Angstrom')
            t = u.Quantity(r['transmission'])
    q = QTable([lams,t],names=['wavelength','transmission'],meta={'filter':f})
    return q

def sed_to_spd(s):
    '''convert a spectrum from spectral energy distribution
    to spectral photon distribution
    
    Argument:
        s - a QTable spectrum, with columns 'wavelength' and 'flux' in energy units
    
    Returns:
        a QTable spectrum with flux converted to photons
    '''
    lam = s['wavelength']
    F = s['flux']
    h = const.h.to('W s**2')/u.photon
    c = const.c
    nu = (c/lam).to('1/s')
    nph = (F/(h*nu)).decompose(bases=['ph','W','s','m','sr'])
    r = QTable([lam,nph],names=['wavelength','flux'])
    return r

def spd_to_sed(s):
    '''convert a spectrum from spectral photon distribution
    to spectral energy distribution
    
    Argument:
        s - a QTable spectrum, with columns 'wavelength' and 'flux' in photons
    
    Returns:
        a QTable spectrum with flux converted to energy units
    '''
    lam = s['wavelength']
    nph = s['flux']
    h = const.h.to('W s**2')/u.photon
    c = const.c
    nu = (c/lam).to('1/s')
    F = (nph*h*nu).decompose(bases=['ph','W','s','m','sr'])
    r = QTable([lam,F],names=['wavelength','flux'])
    return r

def tinterp1d(tbl,x,y):
    '''form a 1d interpolating function for a pair of columns in an astropy Table
    
    Parameters:
    -----------
    tbl: astropy.Table
        the table to interpolate on
    x: str
        the column name of the x (independent) variable
    y: str
        the column name of the y (dependent) variable
    '''
    return qinterp1d(tbl[x],tbl[y])

def lookup_star(star,bail=True):
    '''look up a star by identifier in the simbad database and return the relevant
    photometry information.
    
    Argument:
    ---------
    star : str
        The star name
        
    bail : boolean
        Usually, if the star is not found either in the local cache or on
        simbad, a python Exception is raised. bail=False circumvents this
        and instead returns an incomplete star information dictionary, with
        'name' and 'error' entries. Default is True.
    '''
    assert isinstance(star,str)
    if star.startswith('AS'):
        for qstar in [star,star.replace('AS-','AS '),star.replace('AS','AS ')]:
            r = arnica_stars[arnica_stars['STARNAME'] == qstar]
            if len(r)>0:
                alt_name = r.iloc[0]['OTHERDESIGNATION']
                if isinstance(alt_name,str) and alt_name != '':
                    star = alt_name
                else:
                    msg = f'{tcolor.warning}Star {star} found in ARNICA\
                             standards list, but there is no alternate\
                             designator. Attempting to look it up in Simbad but this\
                             will probably fail.{tcolor.end}'
                    print(' '.join(msg.split()),flush=True)
                break
            
    if star in star_cache['TYPED_ID']:
        r = star_cache[star_cache['TYPED_ID']==star]
        if not r[0]['MAIN_ID']:
            msg = f'{tcolor.fail}Simbad could not find {star}{tcolor.end}'
            if bail: raise Exception(msg)
            else: return {'name': star,'error':msg}
    else:
        print(f'{tcolor.green}doing a Simbad query on {star}...{tcolor.end}',end='',flush=True); sys.stdout.flush()
        customSimbad = Simbad()
        customSimbad.add_votable_fields('typed_id','sptype')
        customSimbad.add_votable_fields(*[f'flux({x})' for x in ['B','V','R','J','H','K']])
        r = customSimbad.query_object(star)
        if not isinstance(r,Table):
            row = {'TYPED_ID':star}
            star_cache.add_row(row)
            msg = f'{tcolor.fail}Simbad cannot find {star}{tcolor.end}'
            if bail: raise Exception(msg)
            else: return {'name':star,'error':msg}
        elif isinstance(r,Table) and hasattr(r,'errors') and r.errors:
            row = {'TYPED_ID':star}
            star_cache.add_row(row)
            msg = f'{tcolor.fail}{r.errors}{tcolor.end}'
            if bail: raise Exception(msg)
            else: return {'name':star,'error':msg}
        row = dict(r[0])
        row = {key:val for key,val in row.items() if key in star_cache.columns}
        star_cache.add_row(row)
        print(f'{tcolor.green}found it.{tcolor.end}')
    r = r[0]
    RA = r['RA']
    Dec = r['DEC']
    coord = SkyCoord(RA,Dec,unit=(u.hourangle,u.deg))
    band,m = 'V',r['FLUX_V']
    b = r['FLUX_B']
    sp_type = r['SP_TYPE']
    if isinstance(b,np.ma.core.MaskedConstant):
        j, h = r['FLUX_J'],r['FLUX_H']
        if any([isinstance(x,np.ma.core.MaskedConstant) for x in [j,h]]):
            msg = f'{tcolor.fail}cannot establish color for star - no B or J-H magnitude data{tcolor.end}'
            if bail: raise Exception(msg)
            else: color = msg
        color = ('J-H',j-h)
    else:
        color = ('B-V',b-m)
    
    stard = {'name':star,
             'RA':RA,
             'Dec':Dec,
             'mV':m,
             'mag':(band,m),
             'color':color,
             'type': sp_type,
             }
    return stard
    
# ==================
# Lick specific data
# ==================
lick = EarthLocation.of_site('lick') # lick.geodetic to view
sharcs_filters_table = Table.read(resource_stream('astrobot','data/Sharcs_filters.ecsv'),format='ascii.ecsv')
sky_table_text = '''
#sky background magnitudes per square arcsecond; Ellie Gates email on 6/11/2020 (BVRI) and https://mthamilton.ucolick.org/techdocs/instruments/sharcs/summary/ for the IR bands'
band    mag
B       20.85
V       20.12
R       19.86
I       19.82
D6-9    19.86
J       15.84
H       13.85
K       13.87
Ks      14.60
'''
sky_table = Table.read(sky_table_text,format='ascii')
sky_trans_table = QTable.read(resource_stream('astrobot','data/LickSkyTrans.ecsv'),format='ascii.ecsv')
ccd_qe_table = QTable.read(resource_stream('astrobot','data/CCID66_QE.ecsv'),format='ascii.ecsv')
shaneao = DotDict(
    modes = {'16xLGS':DotDict({'d':21.4*u.cm,'ns':144}),
             '8xLGS': DotDict({'d':42.8*u.cm,'ns':40 }),
            },
    cam_gain = 8.*u.ct/u.ph, # camera DAC shifts the data left by 3 bits
    rate_table = { # The WFS CCD's "actual" frame rates
        50: 35,
        100: 94,
        250: 273,
        500: 475,
        700: 659,
        1000: 1089,
        1300: 1345,
        1500: 1524
    }, # see shaneao.py / def rate in the real-time code
    )

# ===================================================
# process logsheets from WFS engineering during 2021A
# ===================================================

# ------------- read the logsheet, if any -----------

logsheet_dirs = [
    os.path.expanduser('~'),
    os.path.expanduser('~/ShaneAO/data'),
]

def read_logsheet(path=None):
    '''Read the observation logsheet(s). This is one csv file, usually
    downloaded as such from the Google drive. Log sheet for AO engineering runs
    in early 2021 is here
    https://docs.google.com/spreadsheets/d/1DORP93zXfiPCly3tAgAQgMXo5vcWKF_mQ0AZHbdASo0/edit?usp=sharing
    and stored in the home directory or subdirctory ShaneAO/data
    
    Argument:
    ---------
    path : str
        The full file path to the csv file (user ~ will be expanded)
    
    Returns:
    --------
    pandas.DataFrame
        The logsheet as a database, one row per line of the logsheet
    '''
    if path is not None:
        logsheet_name = os.path.split(path)[1]
        fn = os.path.expanduser(path)
        if not os.path.isfile(fn):
            fn = None
    else:
        logsheet = None
        logsheet_name = '2020-2021 AO Engineering Logsheet.csv'
        
        for logdir in logsheet_dirs:
            fn = os.path.join(logdir,logsheet_name)
            if os.path.isfile(fn):
                break
    
    if fn is None:
        print(f'{tcolor.warning}WARNING logsheet {logsheet_name} not found{tcolor.end}')
        return None
    else:
        print(f'{tcolor.green}reading logsheet {fn}{tcolor.end}'); sys.stdout.flush()
        logsheet = pd.read_csv(fn)
    
    memo = {'fileName':logsheet_name,
            'path':fn,
            }
    finder.set_metadata(logsheet,memo)
    return logsheet

def make_stars_table(write=True):
    '''
    Create a star information table (flux and color) on stars used during the AO
    engineering runs in 2021A. These observations were intended to measure WFS
    and Sharcs photometric throughputs. Star info is gathered by queries to the
    Simbad server.
    
    Previously generated results are stored in the table 'PhotomStars.ecsv'
    which can quickly read in with a astropy.Table.read command. You do not need
    to run this routine unless there is a change to the star list, or to info
    you want extracted from the Simbad database.
    '''
    # stars' names gleaned from the log sheet
    # 2020-2021 AO Engineering Logsheet.xlsx
    stars = ['HD29122', 'HD44612', 'SA 100-280', 'SAO013747', 'AS14-1', 'AS15-0',
    'SAO042804', 'SAO062058', 'SAO119183', 'SA101-333', 'SA102-381', 'SA 109-71',
    'BD+2 2957', 'HD 161903', 'SAO 119183', 'SAO 015832', 'SAO 064525', 'FS 25',
    'SAO 017946', 'HD 149382', "Barnard's Star", 'HD 160233', 'SA 106-1024',
    'Gl748', 'HD 201941', 'HD 129653', 'HD 136754', 'HD 162208', 'HD 201941',
    'SA 114-750', 'HD 121968', 'HD 200340', 'SA 114 755']
    # The following gets rid of parenthesized comments in the star name
    stars = [re.sub("[\(\[].*?[\)\]]", "",x).strip() for x in stars]
    # These following two stars were from the ARNICA near infrad standard star list
    # (http://mthamilton.ucolick.org//techdocs/standards/ARNICA.tb1.html)
    # but whose names are not in Simbad. So we change their names to
    # alternative designations that are available in Simbad.
    stars[stars.index('AS14-1')] = 'TYC 4095-1863-1'
    stars[stars.index('AS15-0')] = 'NGC 2264 65'
    customSimbad = Simbad()
    customSimbad.add_votable_fields('typed_id','sptype')
    customSimbad.add_votable_fields(*[f'flux({x})' for x in ['B','V','R','J','H','K']])
    r = customSimbad.query_objects(stars)
    r.meta['comment'] = 'stars observed during 2021A engineering nights for new WFS commissioning'
    r.meta['logsheet'] = '2020-2021 AO Engineering Logsheet.xlsx'
    if write:
        r.write('PhotomStars.ecsv',format='ascii.ecsv')
    return r

stars = Table.read(resource_stream('astrobot','data/PhotomStars.ecsv'),format='ascii.ecsv')
AS_map = {'AS14-1': 'TYC 4095-1863-1',
          'AS15-0': 'NGC 2264 65'}
AS_rmap = dict(zip(AS_map.values(),AS_map.keys()))
stars['LOG_ID'] = [AS_rmap.get(x,x) for x in stars['TYPED_ID']]

def ccd_count(star_spectrum,airmass=1.,d=None,t=None,dichroic=False):
    '''compute the photocount from the star that ~should~ be detected by the CCD
    if the optics throughput were 100%. The star brightness vs wavelength is provided
    in the star_spectrum argument. The sky transmission and QE of the detector
    are applied, and the spectrum integrated from 350 nm to 900 nm, which is
    essentially the bandpass of the optical path and the dichroic splitter at
    the WFS pickoff after the MEMS DM.
    
    The next refinements are
        1) factor in additional loss due to zenith angle (airmass) of the observation,
        2) optionally, account for expected loss from upstream AR and aluminium mirror coatings.
    
    Arguments:
    ----------
    Star: astropy.QTable
        An SED or SPD spectrum of the star flux (per unit area, per unit wavelength)
    airmass: Quantity, unitless
        Airmass (secant zenith angle)
    d: Quantity, length-like
        The dimension of the subaperture (if provided, the result is multiplied by subaperture area d^2)
    t: Quantity, time-like
        The exposure time (if provided, the result is multiplied by the exposure time, t)
    
    Requires:
    ---------
    Global tables sky_trans_table and ccd_qe_table
    
    Returns:
    --------
    Quantity:
        The number of photoelectrons / unit area aperture / unit time detectable by the CCD
        If d is provided: photoelectrons / unit time detected in the subaperture
        If t is provided: photoelectrons / unit area dtected in exposure time t
        If both d and t are provided: photoelectrons
    '''
    unit = (star_spectrum['flux'].unit*u.Unit('m3')).decompose()
    if unit == u.Watt: # SED
        star_ph = sed_to_spd(star_spectrum)
    elif unit == u.ph/u.s: # SPD
        star_ph = star_spectrum
    else:
        raise Exception('star must be an SED or SPD spectrum')
    if dichroic: lam = [0.355,0.6]*u.micron
    else: lam = [0.355,0.9]*u.micron # wavelenth range
    lams = np.linspace(lam[0],lam[1],101)
    dlam = lams[1]-lams[0]
    f_star = tinterp1d( star_ph,'wavelength','flux')
    T_sky = tinterp1d( sky_trans_table,'Wavelength','Trans')
    qe = tinterp1d( ccd_qe_table,'Wavelength','QE')
    f = (f_star(lams)*T_sky(lams)**airmass*qe(lams)).sum()*dlam

    if d is not None:
        f = f*d**2
    if t is not None:
        f = f*t
        
    return f.decompose()

# example:
#from pwfs.pwfs_report import starBB
#star = starBB(mag=('V',u.Magnitude(10.909)), color=('J-H',0.237))
def qinterp1d(x,y):
    '''wraps interp1d to handle x and y being Quantities with units
    '''
    x = u.Quantity(x)
    y = u.Quantity(y)
    xunit = x.unit
    yunit = y.unit
    f1 = interp1d(x.value,y.value)
    f = lambda xx: f1(xx.to(x.unit).value)*y.unit
    return f

def exdate(s):
    '''parse the string and extract the date, or return None.
    This basically wraps dateutil.dparser.parse and returns None if it fails,
    rather than throw an exception.
    
    Parameters:
    -----------
    s: string
        an arbitrary string which may or may not have a date within it
        
    Returns:
    --------
    datetime.datetime
        The date, or None if no date could be parsed.
    '''
    try:
        r = dparser.parse(s,fuzzy=True,default=datetime.datetime(999,1,1,0,0,0))
    except:
        return None
    if r.year in [2020,2021]:
        return r
    else:
        return None

def get_log_date(line_no,dates):
    '''get the date associated with line number line_no of the log sheet
    '''
    ls = [d[0] for d in dates] + [np.inf]
    k = [line_no > l1 and line_no < l2 for l1,l2 in zip(ls,np.roll(ls,-1))].index(True)
    return dates[k][2]

def clean(starname):
    '''gets rid of the parenthisized comment after a star name
    '''
    return re.sub("[\(\[].*?[\)\]]", "",starname).strip()
    
def db_init(logsheet=None):
    '''initialize the database of observations given the log sheet.
    '''
    if logsheet is None:
        logsheet = read_logsheet()
        globals().update({'logsheet':logsheet})
        
    dates = [x for x in [(k,d,exdate(d)) for k,d in enumerate(list(logsheet['Obs#']))] if x[2] is not None]
    pattern = re.compile('data|splitter|mirror|closed|open')
    y = []
    notastar = ('WDS','M3','M13','Dome','dome','ditto','seeing')
    for k,e in logsheet.iterrows():
        if str(e['Object']).startswith(notastar): continue
        x = str(e['Unnamed: 6']) + ' ' + str(e['Unnamed: 7'])
        if pattern.search(str(x)): y.append((k,x))
    
    pattern = re.compile(' \d+[\s,](?!Hz)') # glean data file numbers only (reject frequenies, star magnitudes, wfs mode etc)
    z = [(k,pattern.findall(x+' '), re.findall('splitter|mirror',x)) for k,x in y]
    z = [x for x in z if x[1] != []] # ignore lines with no data file numbers
    
    # form a database of data files recorded in the logsheet
    entries = []
    for line_no,data_list,spl_mir_list in progbar(z):
        date = get_log_date(line_no,dates)
        spl_mir = spl_mir_list[0] if spl_mir_list else 'unknown' # assumes one log line correspond to only one setting of the splitter/mirror
        for data_num_str in data_list:
            num = int(re.findall('\d+',data_num_str)[0])
            filename = f'Data_{num:04d}'+'.fits'
            path = os.path.join(date.strftime('%Y-%m-%d'),'AOtelemetry',filename)
            entries.append((line_no,date,filename,path,spl_mir))
    db = pd.DataFrame(entries,columns = ['log#','date','file','path','spl_mir'])
    db['object'] = [logsheet.iloc[k]['Object'] for k in db['log#']]
    
    dbo = db[['object']].copy()

    dbo['clean'] = [clean(x) for x in dbo['object']]
    db = db[dbo['clean'].isin(stars['LOG_ID'])]
    
    # look up the stars' RA and Dec
    RA = [stars[stars['LOG_ID']==clean(x)]['RA'][0] for x in db['object']]
    Dec = [stars[stars['LOG_ID']==clean(x)]['DEC'][0] for x in db['object']]
    db['RA'] = RA
    db['Dec'] = Dec
    
    return(db)

def db_read_inten(db):
    '''
    Open each data file in the database, get the recorded intensity values,
    and enter them into the database.
    
    Follow up by computing the normalized flux (photons/(cm2*ms)) and
    enter these as additional columns in the database
    
    Parameters:
    -----------
    db: Pandas.DataFrame
        The WFS data observations database, as crated by db_init(logsheet)
    
    Returns:
    --------
    Pandas.DataFrame
        Contains all the original database columns plus additional columns
        with recorded intensity data (these are counts per subaperture per frame
        averaged over subapertures) and a computed normalized flux
    '''
    # do a pattern match to the telemetry data file structure
    # Don's is <main>/<date>/AOTelemetry/Data_xxxx.fits
    # I'm not sure what real.ucolick.org uses, so assume it is generically
    # <pre>/<date>/<post>/Data_xxxx.fits
    # and let finder get a pattern from the first entry in the database
    date0 =  db.iloc[0].date.strftime(format='%Y-%m-%d')
    path = finder.findVol('telemetry',date0)[1]
    if isinstance(path,list): path = path[0]
    pre,post = path.split(date0)
    post = post.strip('/')
    
    dates = [x.strftime(format='%Y-%m-%d') for x in db['date']]
    paths = [os.path.join(pre,x,post,y) for x,y in zip(dates,db['file'])]
    
    #db['isfile'] =  [os.path.isfile(os.path.join(telem_dir,fn)) for fn in db['path']]
    db['isfile'] = [os.path.isfile(x) for x in paths]
    assert db['isfile'].all()
    # patterns matched, paths are all good
    
    db['path'] = paths
    q = []
    ind = []
    pdt_change = datetime.datetime.strptime('2021-03-14','%Y-%m-%d')
    for i,r in progbar(db.iterrows(),total=len(db)):
        #with fits.open( os.path.join( telem_dir, r['path'])) as hdu:
        with fits.open( r['path'] ) as hdu:
            hdr = hdu[0].header
        inten_start = hdr.get('INTENSTA',np.nan)
        inten_end = hdr.get('INTENEND',np.nan)
        rate = hdr.get('RATE',np.nan)
        cent = hdr.get('CENT',np.nan)
        loop = hdr.get('LOOP','unknown')
        mode = hdr.get('MODE','unknown')
        date = hdr.get('DATE','')
        time = hdr.get('TIME','')
        obstime = datetime.datetime.strptime(date+' '+time,'%Y-%m-%d %H%M%S')
        hrs = 7 if obstime > pdt_change else 8 # PDT or PST vs UT
        time_ut = obstime + datetime.timedelta(hours = hrs)
        RA = r.RA
        Dec = r.Dec
        coord = SkyCoord(RA,Dec,unit=(u.hourangle,u.deg))
        airmass = coord.transform_to(AltAz(obstime=Time(time_ut),location=lick)).secz
        q.append([time_ut,airmass.value,rate,inten_start,inten_end,cent,loop,mode])
        ind.append(i)
    dba = pd.DataFrame(q,index=ind,columns=['time_ut','airmass','rate','inten_start','inten_end','cent','loop','mode'])
    dbm = db.join(dba)
    dbm.attrs['units'] = {'rate':u.Hz,'inten_start':u.ct,'inten_end':u.ct}
    
    q = []
    for i,r in dbm.iterrows():
        mode,rate = r[['mode','rate']]
        inten_start,inten_end = r[['inten_start','inten_end']].to_list()*u.ct/shaneao.cam_gain
        d,ns = [shaneao.modes[mode][x] for x in ['d','ns']]
        rate = shaneao.rate_table[rate]*u.Hz
        t = (1./rate).to(u.ms)
        flux_s = inten_start/(t*d**2)
        flux_e = inten_end/(t*d**2)
        q.append((flux_s.value,flux_e.value))
    dbm[['flux_s','flux_e']] = q
    dbm.attrs['units'].update({'flux_s':flux_s.unit,'flux_e':flux_e.unit})

    return dbm

def plt_obj(obj):
    '''plot summary WFS photometry data for a star observation.
    
    Parameter:
    ----------
    Obj: str
        The name of the star (in its original form in the logsheet)
    
    Global:
    -------
    dbm: Pandas.DataFrame
        The database of observation data with columns 'object', 'file','rate',
        'spl_mir','inten_start','inten_end','loop','mode','flux_s','flux_e'
    '''
    for spl_mir in ['splitter','mirror']:
        plt.figure()
        plt.title(obj+' - '+spl_mir)
        maxflux = 0.
        for loop in ['open','closed']:
            q = dbm[(dbm['spl_mir']==spl_mir) & (dbm['loop']==loop) & (dbm['object']==obj)].copy()
            if len(q) > 0:
                symb = 'x' if loop=='closed' else 'o'
                plt.plot(q['rate'],q['flux_s'],symb,mfc='None',label=loop+' (start)')
                plt.plot(q['rate'],q['flux_e'],symb,mfc='None',label=loop+' (end)')
                ax = plt.gca()
                ave = 0.5*(q['flux_s'].mean()+q['flux_e'].mean())
                ls = '--' if loop=='open' else '-'
                ax.axhline(ave,color='black',ls=ls)
                ax.set_xlim(0,1500)
                maxflux = max(maxflux,max(q['flux_s'].max(),q['flux_e'].max()))
        ax.set_ylim(0,1.3*maxflux)
        plt.xlabel('frame rate '+'$[Hz]$')
        plt.ylabel('Flux '+'$\\left[\\frac{ph}{(cm^2 ms)}\\right]$')
        plt.legend()
        plt.grid('on')

def db_crunch(dbm):
    '''crunch the data and determine the throughput for each
    observation, accounting for sky transmission, dichroic or mirror splitter,
    and QE of the wfs camera
    '''
    rows = []
    for n in progbar(dbm.index):
        obs = dbm.loc[n]
        star = stars[stars['LOG_ID'] == clean(obs.object)][0]
        mag = ('V',star['FLUX_V'])
        color = ('B-V',star['FLUX_B']-star['FLUX_V'])
        starsp = starBB(mag=mag,color=color)
        T = cached_data['spectral_types_temp_vs_BV'](color[1])*u.Kelvin
        d = 21.4*u.cm if '16x' in obs['mode'] else 42.8*u.cm
        t = (1/(obs['rate']*u.Hz)).to(u.ms)
        pred_count = ccd_count(starsp,airmass=obs['airmass'],d=d,t=t)*shaneao.cam_gain
        throughput = obs[['inten_start','inten_end']].mean()*u.ct/pred_count
        throughput = u.Quantity(throughput).to(u.percent)
        row = [mag[1],color[1],T.value,pred_count.value,throughput.value]
        rows.append(row)
        #print(n,obs['spl_mir'],obs['rate'],cnt, (obs['inten_start'],obs['inten_end']),throughput)
    cols = ['mV','B-V','T','pred_count','Trans']
    r = pd.DataFrame(rows,index=dbm.index,columns=cols)
    
    dbmr = dbm.copy().join(r)
    dbmr.attrs = dbm.attrs
    dbmr.attrs['units'].update({'T':T.unit,'pred_count':pred_count.unit,'Trans':throughput.unit})
    return dbmr

def utc_from_local(time,ambiguous='DST'):
    '''convert local Pacific time to utc time.
    
    Note:
    -----
    There is an ambiguous one hour of local time in the fall at the
    end of daylight savings time - local time 1-2 am is repeated, once
    as DST and again as PST.
    '''
    local = pytz.timezone("America/Los_Angeles")
    is_dst = {'DST':True,'PDT':True,'PST':False,'ST':False}.get(ambiguous) # defaults to None
    try:
        local_dt = local.localize(time, is_dst=is_dst)
    except pytz.AmbiguousTimeError:
        print(f'{tcolor.warning}time {time} is ambigous with respect to standard or daylight savings time, assuming DST {tcolor.end}')
        local_dt = local.localize(time, is_dst=True)
    return local_dt.astimezone(pytz.utc)

def save_star_cache():
    if star_cache is not None:
        star_cache.write(star_cache_filename,format = 'ascii.ecsv',overwrite=True)

def get_star_cache():
    if os.path.isfile(star_cache_filename):
        t = Table.read(star_cache_filename,format='ascii.ecsv')
    else:
        t = stars.copy()
    return t
    
star_cache_filename = os.path.join(finder.cache_path,'star_cache.ecsv')
star_cache = get_star_cache() #stars.copy()

def ismasked(x):
    return isinstance(x,np.ma.core.MaskedConstant)

def trans(star,data,airmass=None):
    '''predict the photo count from the star expected on the wfs and
    compare to a dataset's measured intensity reading to
    calculate a tranmissivity percentage.
    
    Arguments:
    ----------
    star : dict or str
        A dictionary of items that describes the star, -or- a
        string name of the star (triggers a simbad search).
        A dictionary argument has:
            'name': string - name of the star
            'RA': string - Right ascension (in a format astropy SkyCoord can parse, in hour angle)
            'Dec': string - Declination (in degrees)
            'mag': star magnitude: float - (it will assume mV) or tuple - (band,m) where
                    band is one of 'U,B,V,R,I,J,H,Ks'
            'color': float - B-V color, or Quantity - temperature (degrees Kelvin)
    data : str or Quantity
        If a string, the path to the telemetry file -or-
        a data (enhanced Quantity) component from telemetry_analysis.Set
    airmass : str or None
        If float, the airmass (secant zenith angle)
        Otherwise,calculate the airmass. To do this, argument star must be the name
        of the star so the RA/Dec will be looked up using Simbad
        or the star dictionary needs to have RA and Dec entries.
    '''
    if isinstance(star,dict):
        star = DotDict(star)
        coord = SkyCoord(star.RA,star.Dec,unit=(u.hourangle,u.deg))
        if isinstance(star.mag,tuple):
            band,m = star.mag
        else:
            band,m = 'V',star.mag
        if isinstance(star.color,(u.Quantity,tuple)): # its a temperature or band-band delta magnitude
            color = star.color
        else: # otherwise its a float B-V magnitude
            color = ('B-V',star.color)
    elif isinstance(star,str):
        star = lookup_star(star)
        mag,color,RA,Dec = [star[x] for x in ['mag','color','RA','Dec']]
        band,m = mag
        coord = SkyCoord(RA,Dec,unit=(u.hourangle,u.deg))
    else:
        raise TypeError(f'{tcolor.fail}{star} not a valid type for a star{tcolor.end}')
    
    if isinstance(data,str): # path to file
        with fits.open(data) as hdu:
            hdr = hdu[0].header
    elif isinstance(data,u.Quantity): # a datum
        hdr = data.header
    else: # assume it came from a telemtry_analysis dataset. use the open loop one or first one
        ds = data
        if not hasattr(ds,'data'):
            ds.read()
        datum = list(ds.data.values())[0]
        for d in ds.data.values():
            if d.loop == 'open':
                datum = d
                break
        hdr = datum.header
    
    if airmass is None:
        date = hdr.get('DATE','')
        time = hdr.get('TIME','')
        obstime = datetime.datetime.strptime(date+' '+time,'%Y-%m-%d %H%M%S')
        utc = utc_from_local(obstime)
        airmass = coord.transform_to(AltAz(obstime=Time(utc),location=lick)).secz
        if airmass > 2.5:
            print(f'{tcolor.warning}Airmass = {airmass} seems a little high for AO. are you sure this is the right star for this dataset?{tcolor.end}')
    
    inten_start = hdr.get('INTENSTA')
    inten_end = hdr.get('INTENEND')
    if inten_start is None or inten_end is None:
        print(f'{tcolor.warning}Intensity data was not recorded in telemetry before 2016{tcolor.end}')
        return None,star
    mode = hdr.get('MODE')
    rate = hdr.get('RATE')
    dichroic = hdr.get('TTTDICHR','none')=='Splitter'
    starsp = starBB(mag=(band,m),color=color)
    d = 21.4*u.cm if '16x' in mode else 42.8*u.cm
    t = (1/(rate*u.Hz)).to(u.ms)
    pred_count = ccd_count(starsp,airmass=airmass,d=d,t=t,dichroic=dichroic)*shaneao.cam_gain
    throughput = u.Quantity([inten_start,inten_end]).mean()*u.ct/pred_count
    throughput = u.Quantity(throughput).to(u.percent)
    return throughput,star
    
def init(from_saved=True,**kwargs):
    global dbLogsheet,dbObs,db,dbT
    if not from_saved:
        print('reading logsheet');sys.stdout.flush()
        dbLogsheet = db_init()
        print('reading intensity data from telemetry files');sys.stdout.flush()
        dbObs = db_read_inten(dbLogsheet)
        print('crunching the numbers for throughput');sys.stdout.flush()
        db = db_crunch(dbObs)
    else:
        dbT,db = restore_database(**kwargs)

def save_database(dbmr,name=None):
    '''saves the completed (intensities read and crunched) database to disk
    file in 'ascii.ecsv' format
    
    Arguments:
    ----------
    dbmr: pandas.DataFrame
        The database
    name: str
        The file name; defaults to 'ObsDB_2021A.ecsv'
    '''
    dbmrT = QTable.from_pandas(dbmr,units=dbmr.attrs['units'])
    if name is None:
        name = 'ObsDB_2021A.ecsv'
    if not name.endswith('.ecsv'):
        name = name + '.ecsv'
    dbmrT.write(name,format='ascii.ecsv')

def restore_database(name=None):
    '''restore the database from disk file.
    
    Argument:
    ---------
    name: str
        the name of the file. defaults to 'ObsDB_2021A.ecsv'
    
    Returns:
    --------
    The database is returned as a tuple of two objects (dbmrQT,dbmrP)
    
    dbmrQT: astropy.QTable
    dbmrP: pandas.DataFrame
    
    The pandas.DataFrame has units in its 'attrs' attribute.
    '''
    if name is None:
        name = 'ObsDB_2021A.ecsv'
    if not name.endswith('.ecsv'):
        name = name + '.ecsv'
    name = os.path.expanduser(name)
    if not os.path.isfile(name):
        dirname,filename = os.path.split(name)
        if dirname == '': dirname = os.getcwd()
        fn = os.path.join(dirname,filename)
        raise Exception(f'{tcolor.fail}File {fn} not found{tcolor.end}')
    dbmrT = QTable.read(name,format='ascii.ecsv')
    dbmr = dbmrT.to_pandas()
    dbmr.attrs['units'] = {x:dbmrT[x].info.unit for x in dbmrT.columns if dbmrT[x].info.unit is not None}
    return dbmrT,dbmr

def plot(q,x,y,symb,**kwargs):
    '''plot a curve on one column of a DataFrame vs another
    
    Arguments:
    ----------
    q: pandas DataFrame
        the relevant DataFrame
    x: str
        the name of the column whose data are the 'x' values
    y: str
        the name of the column whose data are the 'y' values
    symb: str
        the marker symbol

    Keywords are passed to the matplotlib.pyplot.plot routine, except that
    symbols default to open rather than filled (markerfacecolor='none')
    '''
    mfc = kwargs.pop('mfc','none')
    kwargs['markerfacecolor'] = kwargs.pop('markerfacecolor',mfc)
    plt.plot([xx for xx in q[x]],[yy for yy in q[y]],symb,**kwargs)

def do_obs_trans_vs_color_plot(dbmr,x='B-V'):
    '''plot Transmission of each observation vs x, where x is B-V color, or temperature.
    
    Arguments:
    ----------
    dbmr: DataFrame
        database of observations, already crunched for transmission
    x: str
        'B-V' or 'T'
    '''
    plt.figure()
    q =  dbmr[(dbmr['object']!="Barnard's Star") & (dbmr['spl_mir']=='mirror')]
    plot(q,x,'Trans','x',label='mirror')
    q =  dbmr[dbmr['spl_mir']=='splitter']
    plot(q,x,'Trans','o',label='splitter')
    q = dbmr[(dbmr['object']=="Barnard's Star") & (dbmr['spl_mir']=='mirror')]
    plot(q,x,'Trans','x',color='red',label="Barnard's Star")
    plt.legend()
    ax = plt.gca()
    ax.set_xlabel({'T':'Temperature, K','B-V':'B - V'}[x])
    ax.set_ylabel('Transmission, %')
    if x == 'B-V': ax.invert_xaxis()
    plt.grid('on')

# rainbow colors https://www.webnots.com/vibgyor-rainbow-color-codes/
roygbiv = [[148, 0, 211], # violet
        [75, 0, 130],
        [0, 0, 255],
        [0, 255, 0],
        [255, 255, 0],
        [255, 127, 0],
        [255, 0, 0]]  # red
# in this order because larger B-V is redder
roygbiv_colors = ['violet',
                  'indigo',
                  'blue',
                  'green',
                  'yellow',
                  'orange',
                  'red']

def do_inten_vs_mV_plots(dbm):
    '''plot the four intensity vs mV plots, a summary of the
    collected data. intensity is ccd counts / subap / ms
    
    Argument:
    ---------
    dbm: DataFrame
        the database of observations (output of read_inten)
    
    '''
    fig,axs = plt.subplots(2,2,figsize=(6.4,6.8))
    dbm['ncount'] = dbm['inten_start']*dbm['rate']/1000. # count/subap/ms
    bv = list(dbm['B-V'])
    bin = (max(bv)-min(bv))*1.01/len(roygbiv)
    for mode,axcol in zip(['16xLGS','8xLGS'],[0,1]):
        for sm,axrow in zip(['mirror','splitter'],[0,1]):
            q = dbm[(dbm['mode']==mode) & (dbm['spl_mir']==sm)].copy()
            ncount = list(q['ncount'])
            mag = list(q['mV'])
            ind = [ np.floor( (x-min(bv))/bin ).astype(int) for x in q['B-V']]
            q['color'] = [ roygbiv_colors[k] for k in ind]
            colors = np.array([ roygbiv[k] for k in ind ])/255
            ax = axs[axrow,axcol]
            ax.scatter(mag,ncount,c=colors,marker='x')
            ax.set_yscale('log')
            ax.set_ylim(10,10**4)
            ax.set_xlabel('mV')
            ax.set_ylabel('count / subap / ms')
            ax.grid('on',which='both')
            ax.set_title(mode+' '+sm)
    fig.tight_layout(pad=1)

def do_pred_vs_act_plots(dbmr):
    '''
    Plot predicted vs actual counts on a log-log scale
    along with contour lines for various optical throughputs
    '''
    plt.figure()
    scale = 'log'
    for spl_mir in ['mirror','splitter']:
        q = dbmr[dbmr['spl_mir']==spl_mir]
        act_count = q[['inten_start','inten_end']].mean(axis=1)
        plt.plot(q['pred_count'],act_count,'x',label=spl_mir)
    ax = plt.gca()
    ax.set_xscale(scale)
    ax.set_yscale(scale)
    ax.set_xlim(auto=False)
    ax.set_ylim(auto=False)
    ax.set_xlim(left=2000)
    ax.set_ylim(bottom=2000)
    xlim = np.array(ax.get_xlim())
    lxlim = np.log(xlim)
    ylim = np.array(ax.get_ylim())
    lylim = np.log(ylim)
    ax.plot(xlim,xlim,'-',color='gray')
    x = np.exp(lylim.mean())
    labelLine(ax.lines[-1],x,'100%')
    for t in [.5,.3,.2,.1,.05]:
        ax.plot(xlim,xlim*t,'--',color='gray')
        x = np.exp((lylim-np.log(t)).mean())
        labelLine(ax.lines[-1],x,f'{int(t*100)}%')
    ax.set_ylabel('average [inten_start, inten_end]')
    ax.set_xlabel('$T_{optics}=1$ predicted count')
    plt.legend()
    plt.grid('on',which='both')

# aluminum coating
al_coating_table = QTable.read(resource_stream('astrobot','data/al_coating.ecsv'),format='ascii.ecsv')
def do_trans_vs_wavelength_plot(refl=True):
    plt.figure()
    plt.plot(sky_trans_table['Wavelength'].value,sky_trans_table['Trans']*100,label='Sky')
    plt.plot(ccd_qe_table['Wavelength'].value,ccd_qe_table['QE'].value,label='WFS CCD')
    if refl:
        plt.plot(al_coating_table['Wavelength'].to(u.nm).value,al_coating_table['Refl'].value,'-.',color='gray',label='Al reflectivity')
        almir3 = (al_coating_table['Refl'].to('')**3).to(u.percent)
        plt.plot(al_coating_table['Wavelength'].to(u.nm).value,almir3.value,':',color='gray',label='3 Al reflections')
    ax = plt.gca()
    ax.set_xlim(300,1000.)
    ax.set_ylim(0,100.)
    ax.set_xlabel('Wavelength, nm')
    ax.set_ylabel('Percent')
    plt.legend()
    plt.grid('on')

def do_inten_start_vs_end_plot(dbmr):
    plt.figure()
    plot(dbmr,'inten_start','inten_end','x')
    ax = plt.gca()
    ax.set_aspect(1)
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim((1,2e5),auto=False)
    ax.set_ylim((1,2e5),auto=False)
    xmin = dbmr[['inten_start','inten_end']].min().min()
    xmax = dbmr[['inten_start','inten_end']].max().max()
    plt.plot([xmin,xmax],[xmin,xmax])
    ax.set_xlabel('inten_start')
    ax.set_ylabel('inten_end')
    plt.grid('on',which='both')
    
def do_all_plots(db=None):
    if db is None: db = dbmr
    do_inten_start_vs_end_plot(db)
    do_obs_trans_vs_color_plot(db,x='B-V')
    do_trans_vs_wavelength_plot()
    do_pred_vs_act_plots(db)
    do_inten_vs_mV_plots(db)

# =============== Tests =================
def test0():
    '''photometry calculation'''
    import astrobot.telemetry_analysis as ta
    
    print(f'{tcolor.bold}{test0.__doc__}{tcolor.end}')
    name = 'HD 104624 (SAO 119183, AS 23-0)'
    star = stars[stars['MAIN_ID']==clean(name)][0] # there's a duplicate entry, use the first one
    mV = star['FLUX_V']
    mB = star['FLUX_B']
    BV = mB - mV
    RA = star['RA']
    Dec = star['DEC']
    star = {'name':name,
             'RA': RA,
             'Dec': Dec,
             'mag': mV,
             'color':BV,}
    date = '2021-04-28'
    dsn = [11,12]
    ds = ta.Set(date,dsn)
    t,s = trans(star,ds)
    print(f'\n{tcolor.green}Transmission is {t}{tcolor.end}')

def test1():
    '''test is we can read from the saved database'''
    print(f'{tcolor.bold}{test1.__doc__}{tcolor.end}')
    init(from_saved=True)

def test2():
    '''test if we can read from the logsheet'''
    print(f'{tcolor.bold}{test2.__doc__}{tcolor.end}')
    db = db_init()

def test3():
    '''test if we can find the telemetry files'''
    print(f'{tcolor.bold}{test3.__doc__}{tcolor.end}')
    dbm = db_read_inten(db_init())
    
    