# config files
import os
import ConfigParser
import io

class FlexConfigParser (ConfigParser.ConfigParser,object):
    """A ConfigPaser that allows config files with no sections
    and has a __getitem__ method
    """
    def read(self,fileName,verbose=False,defaultSectionName = None):
        """Read from a config file given the file name.
        If the config file has no sections, put all the items
        in a section with the default name, or the basename of the file (file name stripped of '.cfg').
        If the file does not exist, create a dummy section with the default name, or the basename
        of the file unless that section exists already.
        """
        if defaultSectionName is None:
            sectionName = os.path.splitext(fileName)[0]
        else:
            sectionName = defaultSectionName
        if os.path.exists(fileName):
            try:
                super(FlexConfigParser,self).read(fileName)
                if verbose:
                    print 'read file %s, with sections:'%fileName
                    print self.sections()
            except ConfigParser.MissingSectionHeaderError:
                if verbose: print 'read file %s but no section headers. creating section named "%s"'%(fileName,sectionName)
                fp = open(fileName,'r')
                u = fp.read()
                fp.close()
                u = '['+sectionName+']\n' + u
                self.readfp(io.BytesIO(u))
        else:
            if sectionName not in self.sections():
                if verbose: print 'no file named %s, creating section named "%s"'%(fileName,sectionName)
                self.add_section(sectionName)
            else:
                if verbose: print 'no file named %s, but section named "%s" already exists'%(fileName,sectionName)
        return self
    
    def sread(self,s):
        """Read from a string
        """
        self.readfp(io.BytesIO(s))
        return self
    
    def __getitem__(self,key):
        """Get an item using a dictionary-like indexing format
        self[section], self[section,item], or self[section][item].
        Returns a dictionary of items if the key is a section name.
        """
        if isinstance(key,tuple):
            return dict(self.items(key[0]))[key[1]]
        return dict(self.items(key))
    
    def as_dict(self,section,convert=True):
        """Returns a dictionary of the section, with values converted to types (optional)
        """
        d = dict(self[section])
        if convert:
            for key in d:
                d[key] = str_to_type(d[key])
        return d
    
def str_to_type(s):
    """ Get possible cast type for a string
    s : string
    Returns
    float,int,str,bool : type
        Depending on what it can be cast to
    """
    try:
        f = float(s)
        if "." not in s:
            return int(s)
        return float(s)
    except ValueError:
        value = s.upper()
        if value in ['TRUE','YES']: return True
        if value in ['FALSE','NO']: return False
        return s

def test():
    cfg_list = [f for f in os.listdir('.') if f.endswith('.cfg')]
    
    p = FlexConfigParser()
    for f in cfg_list:
        p.read(f)
