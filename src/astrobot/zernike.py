import numpy as np
import astropy.units as u

from astrobot import q_helper
from astrobot import img
from astrobot import oprint

class Zernike(object):
    """A class to hold information and calculations of Zernike sets
    """
    def __init__(self, n=256, R=None, kind='Noll'):
        """initialize the Zernike object, create an aperture
        
        Parameters
        ----------
        n : int
            the size of the image (a scalar, the image shape will be (n,n))
        R : float
            the aperture radius, in pixels
        kind : str
            Zernike numbering convention. Currently, only 'Noll'[1]_
            convention is supported
        
        References
        ----------
        [1] Noll, R. J. (1976). "Zernike polynomials and atmospheric turbulence."
        Journal of the Optical Society of America, 66(3), 207-211.
        """
        self.kind = kind
        self.n = n
        if R is None:
            R = n/2
        self.R = R
        self.dx = 1./float(R)
        self.ap = img.circle((n,n),r=R)
        x = (np.arange(n) - n/2)/float(R)
        x,y = np.meshgrid(x,x)
        self.r = u.Quantity(np.sqrt(x**2+y**2)).set_attributes(dx=self.dx,name='r')
        self.th = u.Quantity(np.arctan2(y,x),u.rad).set_attributes(dx=self.dx,name='theta')
    
    def pprint(self):
        oprint.pprint(self)
    
    @property
    def pp(self):
        self.pprint()
    
    def ztable(self, kmax=None, nmax=None):
        """create a list of the valid Zernike n,m numbers.
        Result is stored in member ztab
        
        Parameters
        ----------
        kmax : int
            maximum number of entries in the table
        nmax : int
            maximum radial order (if not None, supercedes kmax)
        
        Returns
        -------
        list
            the list of (n,m) Zernike number pairs
        """
        k = 0
        n = 0
        ztab = []
        if nmax is None and kmax is None:
            raise Exception('one of kmax or nmax must be specified')
        if nmax is not None:
            kmax = (nmax+1)*(nmax+2)//2
            
        while k < kmax:
            m = -n
            while m <= n:
                ztab.append((n,m))
                m += 2
                k += 1
            n += 1
        self.ztab = ztab
        
        return ztab
        
    def calc(self, n,m):
        """calculate zernikes n,m and n,-m.
        
        Parameters
        ----------
        n : int
            radial order (must be positive)
        m : int
            azimuthal order (must be postive)
        fast : bool
            fast lookup from a pre-computed table (see `calc_zet`)
        
        Returns
        -------
        list
            The calculated Zernike pair or singlet
            Result is stored as a list [z(n,m),z(n,-m)]
            unless m=0, in which case the list is [z(n,0)].

        """
        zrules(n,m)
            
        r,th = self.r,self.th
        p = 0*r
        for s in range((n-m)//2+1):
            p = p + (-1)**s * (np.math.factorial(n-s) / np.math.factorial(s) ) / \
                              (np.math.factorial((n+m)//2-s) * np.math.factorial((n-m)//2-s)) \
                            * r**(n-2*s)
        p *= np.sqrt(n+1)
        if m != 0:
            p *= np.sqrt(2.)
        if m == 0:
            self.z = [p]
        else:
            self.z = [p*np.cos(m*th), p*np.sin(m*th)]
        for z in self.z:
            z.name = 'Zernike n=%d, m=%d'%(n,m)
            z.dx = [1,1]*u.Quantity(self.dx)
            z.x0 = -(z.shapexy/2)*z.dx
            z.ap = self.ap
            z.axis_names = ['x','y']
            z.n = n
            z.n_doc = 'radial order'
            z.m = m
            z.m_doc = 'azimuthal order'
            m *= -1
        
        return self.z
    
    def calc_set(self, kmax=None, nmax=5):
        '''Pre-calculate a set of Zernikes and store them
        in member zset
        
        Parameters
        ----------
        kmax : int
            the maximum number of Zernikes to compute
        nmax : int
            the maximum radial order of Zernikes to compute (supercedes kmax)
        '''
        self.ztable(kmax=kmax,nmax=nmax)
        nmax = max([z[0] for z in self.ztab])
        zset = []
        for n,m in self.ztab:
            if m<0:
                continue
            self.calc(n,m)
            zset.append(self.z[0].copy())
            if m != 0:
                zset.append(self.z[1].copy())
        self.zset = zset
        self.nmax = nmax
    
    def get(self,n,m):
        """return the n,m Zernike
        Get it from the precalculated set if available,
        otherwise calculate it.
        
        Parameters
        ----------
        n,m : int
            The n and m number of the Zernike
        
        Returns
        -------
        Quantity
            Zernike(n,m) 
        """
        zrules(n,abs(m)) # m may be negative

        try:
            k = self.ztab.index((n,m))
            r = self.zset[k].copy()
        except:
            r = self.calc(n,abs(m))
            r = (r[0] if m>=0 else r[1]).copy()
            
        return r
    
    def __getitem__(self,nm):
        '''implements the [n,m] syntax to get the n,m'th zernike
        from the pre-computed set
        '''
        n,m = nm
        r = self.get(n,m)
        return r
        
    def graphic(self,nmax=None,plot = True):
        """Draw a Zernike "Pascal triangle" graphic
        
        Parameters
        ----------
        nmax : int
            go up to this order. If None, use the pre-computed Zernike set
        
        """
        if nmax is not None:
            if hasattr(self,'zset') and (self.nmax < nmax):
                self.calc_set(nmax=nmax)
        else: # nmax is None
            if hasattr(self,'zset'):
                nmax = self.nmax
            else:
                raise Exception('need to specify nmax or pre-compute the Zernikes using calc_set')
        npix = self.n
        nbuf = npix//5
        nn = (nmax+1)*(npix+nbuf)
        nn = np.rint(nn).astype(int)
        g = np.zeros((nn,nn))
        dx = (npix + nbuf)//2
        dy = npix+nbuf
        kmax = ((nmax+1)*(nmax+2))//2
        self.ztable(kmax=kmax)
        ztab = self.ztab
        ap = img.circle((npix,npix),c=(npix/2,npix/2),r=self.R)
        for k in range(kmax):
            n,m = ztab[k]
            x = m*dx + nn//2
            y = nn - (n*dy + npix//2)
            s = {0:0, 1:0, -1:1}[np.sign(m)]
            #self.calc(n,abs(m))
            z = ap*self.z[s]
            g[y-npix//2:y+npix//2,x-npix//2:x+npix//2] = z
        r = u.Quantity(g)
        r.dx = u.Quantity([2,-1])/(npix+nbuf)
        p0 = [nn//2,nn-npix//2] # pixel address of origin
        r.x0 = -u.Quantity(p0)*r.dx
        r.axis_labels = ['m','n']
        if plot:
            import matplotlib.pyplot as plt
            import matplotlib.ticker as ticker
            r.show()
            ax = plt.gca()
            ax.xaxis.set_major_locator(ticker.FixedLocator(np.arange(-nmax,nmax+1)))
        return r
    
    def fit(self, p):
        """compute the Zernike fit to an image. return the coefficient set:
        p = sum(c_i*z_i)
        
        Parameters
        ----------
        p : Quantity
            the image to fit to
        
        Returns
        -------
        list
            The list of fitted coefficients (floats)
        """
        assert p.shape == (self.n,self.n)
        cset = []
        den = self.ap.sum()
        for zz in self.zset:
            c = float((zz*self.ap*p).sum()/den)
            cset.append(c)
        return cset
    
    def series(self, cset):
        """calculate an image from a Zernike coefficient set
        p = sum(c_i*z_i)
        
        Parameters
        ----------
        cset : list
            the list of coefficients (floats)
        
        Returns
        -------
        Quantity
            The image created by summing the coeffients times
            the zernikes.
        """
        assert len(cset) == len(self.zset)
        p = 0.
        for c,zz in zip(cset,self.zset):
            p += c*zz*self.ap
        return p
    
def is_odd(n):
    return (n%2)

def is_even(n):
    return 1 - (n%2)

def zrules(n,m):
    """check if n,m is valid Zernike numbering
    """
    assert n>=0,'invalid zernike number {},{}'.format(n,m)
    
    if is_even(n):
        assert is_even(m) and m<=n,'invalid zernike number {},{}'.format(n,m)
    else:
        assert is_odd(m) and m<=n,'invalid zernike number {},{}'.format(n,m)

def zernike(n,m,N=256,R=100,ap=True):
    """helper routine to compute the n,m and n,-m zernikes
    
    Parameters
    ----------
    n : int
        radial order (must be positive)
    m : int
        azimuthal order (must be positive)
    N : int
        number of fine pixels in the generated image
    R : float
        radius of circle in pixels
    ap : bool
        whether or not the returned images are multiplied by the aperture
    """
    global z
    
    zrules(n,m)
    z = Zernike(n=N,R=R)
    z.calc(n,m)
    if ap:
        r = [zz*z.ap for zz in z.z]
        return r
    return [zz.copy() for zz in z.z ]
    
def test(n=2,m=2,show=False):
    global z
    z = Zernike()
    z.calc(n,m)
    if show:
        (z.z[0]*z.ap).show()
        if m != 0:
            (z.z[1]*z.ap).show()

def test2():
    global z
    z = Zernike()
    z.calc_set()
    r = z.get(2,2)
    assert r.n == 2 and r.m == 2
    r = z.get(2,0)
    assert r.n == 2 and r.m == 0
    z.calc(2,2)
    r = z.z
    assert len(r) == 2
    assert r[0].n == 2 and r[0].m == 2
    assert r[1].n == 2 and r[1].m == -2
    z.calc(2,0)
    r = z.z
    assert len(r) == 1
    assert r[0].n == 2 and r[0].m == 0

def testc(verbose=False):
    """check the fit and series methods
    """
    global z,test_result
    z = Zernike()
    z.calc_set()
    n = len(z.zset)
    c = np.random.normal(size=(n))
    p = z.series(c)
    cfit = z.fit(p)
    cfit = np.array(cfit)
    e = c - cfit
    if verbose:
        print(e)
    test_result = [u.Quantity(p),c,cfit]
    assert np.isclose(c,cfit,rtol=1.e-1,atol=5.e-2).all()

def testcor():
    """check the cross corelation of zernike modes
    """
    global z,test_result
    z = Zernike()
    z.calc_set()
    n = len(z.zset)
    R = []
    for zz in z.zset:
        c = z.fit(zz)
        R.append(c)
    R = u.Quantity(R)
    E = R - np.identity(n)
    test_result = R,E
    assert np.linalg.norm(E.flatten(),np.inf) < .01
