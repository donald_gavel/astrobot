"""
Create the matrix equivalent of a Fourier Transform

The matrix F is useful for illustrating the linear-algebraic
properties of the Fourier transform.

"""

import numpy as np
import scipy

from astrobot import img
from astrobot import dimg

precision = 2
np.set_printoptions(precision,linewidth=200)

def fmatrix(n=4,inverse=False):
    """Create the equivalent matrix representation
    of the Fourier transform in 1 dimension
    """
    i = 1j
    
    x = np.matrix(np.arange(n))
    fx = np.matrix(np.arange(n))
    if (inverse):
        sign = +1
        norm = 1./float(n)
    else:
        sign = -1
        norm = 1.
    F = norm*np.exp(sign*2*np.pi*i*x.T*fx/float(n))
    F = np.matrix(F)
    if (inverse):
        F = F.T
    return F

def fmatrix2d(n=(4,4),inverse=False):
    """Create the equivalent matrix representation
    of the Fourier transform in 2 dimensions.

    Conventions:
        Data in the spatial domain are assumed stored
        in raster-scan order, x (column) index increasing
        fastest.

        Data in the Fourier domain are stored in raster
        scan order, kx (column) index increasing
        fastests. fx,fy = 0,0 is the first
        element, i.e. the Fourier domain data is -not-
        centered.

    """
    nx,ny = list(map(float,n))
    i = 1j
    x = np.arange(n[0])
    y = np.arange(n[1])
    x,y = np.meshgrid(x,y)
    x = np.matrix(x.flatten())
    y = np.matrix(y.flatten())

    fx = np.arange(n[0])
    fy = np.arange(n[1])
    fx,fy = np.meshgrid(fx,fy)
    fx = np.matrix(fx.flatten())
    fy = np.matrix(fy.flatten())

    if (inverse):
        sign = +1
        norm = 1./(nx*ny)
    else:
        sign = -1
        norm = 1.
    F = norm*np.exp(sign*2*np.pi*i*(x.T*fx/nx + y.T*fy/ny))
    F = np.matrix(F)
    if (inverse):
        F = F.T
    return F

def grad(n=16,d=14,dp=0,circulant=False):
    """create a phase to slopes matrix using finite-differencing
    """
    x = np.arange(n) - n/2
    x,y = np.meshgrid(x,x)
    r = np.sqrt(x**2+y**2)
    ap_slopes = np.where(np.logical_and(r<(d/2),r>=(dp/2)),1.,0.)
    ap_phases = np.zeros((n+1,n+1))
    ap_phases[0:n,0:n] += ap_slopes
    ap_phases[0:n,1:n+1] += ap_slopes
    ap_phases[1:n+1,0:n] += ap_slopes
    ap_phases[1:n+1,1:n+1] += ap_slopes
    ap_phases = np.clip(ap_phases,0,1)
    ns = int(np.sum(ap_slopes))
    na = int(np.sum(ap_phases))
    Hx = np.zeros((n+1,n+1,n,n))
    Hy = np.zeros((n+1,n+1,n,n))
    for i in range(n):
        for j in range(n):
            Hx[i:i+2,j:j+2,i,j] = np.array([[+1,-1],[+1,-1]])/2.
            Hy[i:i+2,j:j+2,i,j] = np.array([[+1,+1],[-1,-1]])/2.
    if circulant:
        Hx[0,0:n,:,:] += Hx[n,0:n,:,:]
        Hx[0:n,0,:,:] += Hx[0:n,n,:,:]
        Hx[0,0,:,:] += Hx[n,n,:,:]
        Hx = Hx[0:n,0:n,:,:]
        Hy[0,0:n,:,:] += Hy[n,0:n,:,:]
        Hy[0:n,0,:,:] += Hy[0:n,n,:,:]
        Hy[0,0,:,:] += Hy[n,n,:,:]
        Hy = Hy[0:n,0:n,:,:]    
        Hx = np.matrix(Hx.reshape(n*n,n*n))
        Hy = np.matrix(Hy.reshape(n*n,n*n))
    else:
        Hx = np.matrix(Hx.reshape((n+1)*(n+1),n*n))
        Hy = np.matrix(Hy.reshape((n+1)*(n+1),n*n))
    H = np.vstack([Hx,Hy])
    return H

def grad_Fourier(n=16,return_Fourier=False):
    """create a phase to slopes matrix using finite-differencing
    with Fourier Transforms
    """
    F = fmatrix2d((n,n))
    Fi = fmatrix2d((n,n),inverse=True)
    
    Gradx = np.zeros((n,n))
    Gradx[0:2,0:2] = np.array([[+1,-1],[+1,-1]])/2.
    Gradx_ft = F*np.matrix(Gradx.flatten()).T
    Gradx_ft = np.array(Gradx_ft).flatten()
    Gradx_ft = np.matrix(np.diag(Gradx_ft))
    
    Grady = np.zeros((n,n))
    Grady[0:2,0:2] = np.array([[+1,+1],[-1,-1]])/2.
    Grady_ft = F*np.matrix(Grady.flatten()).T
    Grady_ft = np.array(Grady_ft).flatten()
    Grady_ft = np.matrix(np.diag(Grady_ft))
    
    if (return_Fourier):
        Grad = np.vstack([Gradx_ft,Grady_ft])
    else:
        Grad = np.vstack([Fi*Gradx_ft*F,Fi*Grady_ft*F])
        Grad = np.real(Grad)
    return Grad

def recon_Fourier(n=16):
    """create the reconstructor matrix using Fourier domain
    Poisson solution: div/grad^2
    """
    F = fmatrix2d((n,n))
    Fi = fmatrix2d((n,n),inverse=True)
    
    Gradx = np.zeros((n,n))
    Gradx[0:2,0:2] = np.array([[+1,-1],[+1,-1]])/2.
    Gradx_ft = F*np.matrix(Gradx.flatten()).T
    Gradx_ft = np.array(Gradx_ft).flatten()
    
    Grady = np.zeros((n,n))
    Grady[0:2,0:2] = np.array([[+1,+1],[-1,-1]])/2.
    Grady_ft = F*np.matrix(Grady.flatten()).T
    Grady_ft = np.array(Grady_ft).flatten()
    
    k2_ft = Gradx_ft**2 + Grady_ft**2
    zind = np.where(np.isclose(k2_ft,0))
    k2_ft[zind] = 1.0 # prevent division by zero
    k2i_ft = 1./k2_ft
    #k2i_ft[zind] = 0.0 # no need, Gradx_ft and Grady_ft are already zero at these frequencies
    R = np.hstack( [ Fi*np.matrix(np.diag(Gradx_ft*k2i_ft)*F), Fi*np.matrix(np.diag(Grady_ft*k2i_ft)*F) ] )
    return R

def waffleQ(n=16):
    """generate the ~Q matrix for de-wafflizing a Fourier
    reconstructor using the penalty function method.
    See "Anomalous Local Waffle Suppression in Fourier Domain Wavefront Reconstruction"
    by D. Gavel, 2011.
    """
    i = 1j
    
    kx = np.arange(n)*2*np.pi/float(n)
    kx,ky = np.meshgrid(kx,kx)
    q = 1 - np.exp(i*kx) - np.exp(i*ky) + np.exp(i*(kx+ky))
    q = q.flatten()
    Q = np.matrix(np.diag(q))
    Q = Q.H*Q
    return Q

def recon_dewaffle(G=None, alpha=1.,alphap=1,n=16,return_Fourier=True):
    """Create a de-wafflized Fourier reconstructor
    G is the matrix representing the gradient, in the Fourier domain
    alpha is the waffle penalty
    """
    if (G is None):
        G = grad_Fourier(n=n,return_Fourier=True)
    Q = waffleQ(n)
    p = np.zeros((n,n))
    p[0,0] = 1.0
    P = np.matrix(np.diag(p.flatten()))
    R = (G.H*G + alpha*Q + alphap*P).I*G.H
    if return_Fourier:
        return R
    else:
        F = fmatrix2d(n=(n,n))
        Fi = fmatrix2d(n=(n,n),inverse=True)
        R = Fi*R*scipy.linalg.block_diag(F,F)
        R = np.real(R)
        return R

def gen_powerlaw(n,p):
    """generate a 2d array of random data with the given spectral power law
    n: dimension of square array (n,n)
    p: spectral power
    return:
        ph = Finv { k^p } ^(1/2) * w
        where w is white noise
    """
    a = np.random.normal(size=(n,n))
    fx = np.arange(n) - n/2
    fx,fy = np.meshgrid(fx,fx)
    P = np.sqrt(fx**2 + fy**2)**p
    P[n/2,n/2] = 0
    P = np.fft.fftshift(P)
    a_f = np.fft.fft2(a)
    r_f = np.sqrt(P)*a_f
    r = np.fft.ifft2(r_f)
    return r


def test_waffle(alpha=1.,alphap=1.,pattern='waffle',show=False):
    """Test the de-wafflizer
    """
    n = 16
    i = 1j
    
    if (pattern == 'random'):
        ph = np.random.normal(size=(n,n))
        ph = img.blur(ph,7)
    if (pattern == 'waffle'):
        ph = np.zeros((n,n))
        ph[n/2,n/2] = 1.0
        ph[n/2-1,n/2] = -1.0
        ph[n/2,n/2-1] = -1.0
        ph[n/2-1,n/2-1] = 1.0
    if (pattern == 'random_waffle'):
        ph_f = np.random.normal(size=(n,n)) + i*np.random.normal(size=(n,n))
        ph = np.fft.ifft2(ph_f)
    if (pattern == 'power_law'):
        ph = gen_powerlaw(n,-11./3.)
    phv = np.matrix(ph.flatten()).T
    G = grad_Fourier(n=n)
    s = G*phv
    R = recon_dewaffle(alpha=alpha,alphap=alphap,n=n,return_Fourier=False)
    a = R*s
    a = np.array(a).flatten().reshape((n,n))
    if show:
        dimg.show(np.real(np.hstack([ph,a])))
        dimg.plt.title(pattern+' [ original | reconstructed ]'+' alpha = '+str(alpha))
    return (ph,a)
    
def test1d(n=4):
    """check that 1d fmatrix is the same as a 1d Fourier transform
    """
    F = fmatrix(n)
    x = np.matrix(np.random.normal(size=n)).T
    y = F*x
    fx = np.fft.fft(x.T).T
    return np.isclose(y,fx).all()

def test2d(n=(4,4)):
    """check that 2d fmatrix is the same as a 2d Fourier transform
    """
    x = np.random.normal(size=n)
    fx = np.fft.fft2(x)
    fx = np.matrix(fx.flatten()).T
    F = fmatrix2d(n)
    x = np.matrix(x.flatten()).T
    y = F*x
    return np.isclose(fx,y).all()

def test_G(n=16):
    """check that the fmatrix-based Gradient matrix is the same as
    one created by finite-diferencing in the spatial domain
    """
    H = grad(n,circulant=True)
    G = np.real(grad_Fourier(n))
    return np.isclose(H,G).all()

def test_R(n=16):
    """check that the fmatrix-based Reconstrution matrix R
    is identical to the pseudo-inverse of G
    """
    G = grad(n,circulant=True)
    R = recon_Fourier(n)
    Rtest = np.linalg.pinv(G)
    return np.isclose(R,Rtest).all()

def _waffle(n=16):
    """create a normalized waffle pattern for testing against results
    """
    x = np.matrix((np.arange(n) % 2 - 0.5)*2)
    w = x.T*x
    w = w / np.sqrt(np.sum(np.array(w)**2))
    return w

def _piston(n=16):
    """create a normalized piston pattern for testing against results
    """
    p = np.matrix(np.ones((n,n)))
    p = p / np.sqrt(np.sum(np.array(p)**2))
    return p

def test_recons(n=16,ntests=5):
    """check that the fmatrix-based reconstructor reproduces
    original random phases run through the gradient operation
    but without piston and waffle
    """
    G = grad(n,circulant=True)
    R = recon_Fourier(n)
    check = []
    for k in range(ntests):
        a = np.matrix(np.random.normal(size=(n,n)).flatten()).T
        w = np.matrix(_waffle(n).flatten()).T
        p = np.matrix(_piston(n).flatten()).T
        s = G*a
        a_hat = R*s
        a = a - (a.T*w)[0,0]*w - (a.T*p)[0,0]*p # remove piston and waffle from the phase input to compare to the output
        check.append( np.isclose(a,a_hat).all() )
    print(check)
    return np.array(check).all()

def tests():
    """run all the internal tests
    """
    test_list = [test1d,test2d,test_G,test_R,test_recons]
    for a in test_list:
        print((a.__name__, ':', 'passed' if a() else 'failed'))

def picture(n=16):
    """generate a picture of the residual pattern in R*G - Identity
    """
    G = grad(n,circulant=True)
    R = np.real(recon_Fourier(n))
    eye = np.identity(n*n)
    return (R*G-eye)
