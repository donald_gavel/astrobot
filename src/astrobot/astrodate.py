'''astrodate.py
utilities for converting dates and loose date parsing
'''
import numpy as np
import datetime
import dateutil.parser as dparser
import pytz
from pandas import Timestamp,to_datetime
from astropy.time import Time

def p_date(date=None):
    ''' parse the string "date", which has some reasonable text
    representation of a date, and return the standard YYYY-mm-dd format
    representation for the observing night. Time is assumed local time.
    If a time after midnight but before noon is given, the result is
    the -previous- day, since that is the date of the observing night.
    
    Argument:
    ---------
    date: str, datetime.time, datetime.datetime, or pandas.Timestamp
        a "fuzzy" denotation of a date, either one of the date-like objects
        above or a string that is recognizable by the
        dparser.parse routine. Today/now year, month, day, hour, etc. are
        default, including the case where date is an empty string ''
    
    Returns:
    ========
    str: observing night, in the form YYYY-mm-dd
    
    '''
    if isinstance(date,np.datetime64): date = Timestamp(date)
    if isinstance(date,(Timestamp,datetime.date,datetime.datetime)): date = str(date)
    now = datetime.datetime.now()
    if date in ['','today','now',None]: date = str(now)
    r = dparser.parse(date,fuzzy=True)
    midnight = datetime.datetime(r.year,r.month,r.day)
    noon = datetime.datetime(r.year,r.month,r.day,12)
    if r > midnight and r < noon: #morning hours are previous night's observing run
        r = r - datetime.timedelta(days = 1)
    s = r.strftime('%Y-%m-%d')
    return s

def utc_from_local(time,ambiguous='DST',tz='America/Los_Angeles'):
    '''convert local time to utc time.
    
    Note:
    -----
    There is an ambiguous one hour of local time in the fall at the
    end of daylight savings time - local time 1-2 am is repeated, once
    as DST and again as PST.
    '''
    if isinstance(time,Time):
        time = time.to_datetime()
    local = pytz.timezone(tz)
    is_dst = {'DST':True,'PDT':True,'PST':False,'ST':False}.get(ambiguous) # defaults to None
    try:
        local_dt = local.localize(time, is_dst=is_dst)
    except pytz.AmbiguousTimeError:
        wprint(f'time {time} is ambigous with respect to standard or daylight savings time, assuming DST')
        local_dt = local.localize(time, is_dst=True)
    return Time(local_dt.astimezone(pytz.utc))
