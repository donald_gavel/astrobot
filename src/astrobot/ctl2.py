#!/usr/bin/env python -i

import numpy as np
import sympy
from sympy import Symbol,Eq,roots,E,I,pi,lambdify,Expr,symbols,exp,conjugate,residue
from sympy.printing import latex, pretty, pprint
from sympy.printing.pretty.pretty import PrettyPrinter
from sympy.printing.pretty.stringpict import prettyForm
from IPython.display import display
import types
from astrobot.tcolor import tcolor, gprint, wprint, eprint, cstr

import matplotlib.pyplot as plt
from matplotlib import rc

plt.ion()
rc('text',usetex=True)
rc('font',family='serif')

class TF():
    '''Transfer Function defined as a sympy Expr but keeping track of domain, e.g. s or z
    '''
    s,z = symbols('s z')
    valid_domains = {s,z}
    def __init__(self, e, *args, **kwargs):
        if isinstance(e, TF): e = e.e
        self.e = e
        self.domain = set(args).intersection(self.valid_domains)
        self.name = ''
        self.__dict__.update(kwargs)
        self.fun = lambdify(list(self.domain),self.e)

    def _repr_latex_(self):
        if hasattr(self,'name') and isinstance(self.name,str) and len(self.name)>0:
            s = f'{self.name}({",".join((x.name for x in self.domain))}) = '
            s += latex(self.e, mode='plain')
            s = f'$\\displaystyle {s}$'
        else:
            s = latex(self.e, mode='plain')
            s += '\\ ;\\quad ' + ','.join((x.name for x in self.domain))
            s = f'$\\displaystyle {s}$'
        return s

    def _pretty(self,*args,**kwargs):
        return PrettyPrinter()._print(self.e)

    def __str__(self):
        if hasattr(self,'name') and isinstance(self.name,str) and len(self.name)>0:
            s = f'{self.name}({",".join((x.name for x in self.domain))}) =\n'
            s += str(self.e)
        else:
            s = str(self.e)
            s += '  ;  ' + ','.join(x.name for x in self.domain)
        return s

    def __repr__(self):
        return self._pretty().s

    def __call__(self, *args):
        ''' substitute and return result. First arguments must correspond to domain.
        An optional last argument can be a substitution dictionary
        '''
        assert len(args) == len(self.domain),f'<TF.__call__> {self.name} takes {len(self.domain)} arguments but {len(args)} were given'
        res = self.fun(*args)
        return res

    def subs(self, d):
        '''d is a dictionary of variable substitutions'''
        domain = self.domain
        for x, xr in d.items():
            if x in domain:
                if isinstance(x, Symbol) and isinstance(xr, Symbol):  # substitute one symbol for another, like {s:z}
                    domain.remove(x)
                    domain.add(xr)
                elif isinstance(x, Symbol):  # substitute a symbol with an expression, like {z: e**(s*T)}
                    domain.remove(x)
                    domain = domain.union(xr.free_symbols)
        domain = tuple(domain)
        return TF(self.e.subs(d), *domain,name=getattr(self,'name',''))

    @property
    def free_symbols(self):
        return self.e.free_symbols

    @property
    def domain_str(self):
        return f'({",".join((str(x) for x in self.domain))})'

    def __mul__(self, b):
        if isinstance(b, TF):
            domain = tuple(self.domain.union(b.domain))
            return TF(self.e * b.e, *domain)
        elif isinstance(b, PS):
            return b.__mul__(self)
        elif b in self.domain:  # allows for multiplying by a domain symbol
            b = TF(b, b)
            return self * b
        else:
            return TF(self.e * b, *self.domain)

    def __rmul__(self, a):
        return self.__mul__(a)

    def __add__(self, b):
        if isinstance(b, TF):
            domain = tuple(set(self.domain + b.domain))
            return TF(self.e + b.e, *domain)
        elif b in self.domain:  # allows adding a domain symbol
            b = TF(b, b)
            return self + b
        else:
            return TF(self.e + b, *self.domain)

    def __radd__(self, b):
        return self.__add__(b)

    def __sub__(self, b):
        if isinstance(b, TF):
            domain = tuple(set(self.domain + b.domain))
            return TF(self.e - b.e, *domain)
        elif b in self.domain:  # allows adding a domain symbol
            b = TF(b, b)
            return self - b
        else:
            return TF(self.e - b, *self.domain)

    def __rsub__(self, b):
        if isinstance(b, TF):
            domain = tuple(set(self.domain + b.domain))
            return TF(b.e - self.e, *domain)
        elif b in self.domain:  # allows subtracting a domain symbol
            b = TF(b, b)
            return b - self
        else:
            return TF(b - self.e, *self.domain)

    def __truediv__(self, b):
        if isinstance(b, TF): # TF/TF -> TF
            domain = tuple(set(self.domain + b.domain))
            return TF(self.e / b.e, *domain)
        elif isinstance(b,PS): # TF/PS not allowed
            raise Exception(cstr('fail','<TF.__truediv__> cannot divide by a power spectrum'))
        elif b in self.domain:  # allows a domain symbol
            b = TF(b, b)
            return self / b
        else: # TF/Symbol -> TF
            return TF(self.e / b, *self.domain)

    def __rtruediv__(self, b):
        if isinstance(b, TF): # TF/TF -> TF
            domain = tuple(set(self.domain + b.domain))
            return TF(b.e / self.e, *domain)
        elif isinstance(b, PS): # PS/TF -> PS
            return b.__truediv__(self)
        elif b in self.domain:  # allows a domain symbol
            b = TF(b, b)
            return b / self
        else:
            return TF(b / self.e, *self.domain)

    def __pow__(self,p):
        return TF(self.e**p,*self.domain)

    def __abs__(self):
        return TF(abs(self.e),*self.domain)

    def as_expr(self):
        return self.e

#    def _sympy_(self):
#        return self.e

    def as_numer_denom(self):
        if not hasattr(self, '_numer'):
            self._numer, self._denom = self.e.as_numer_denom()
        return self._numer, self._denom

    @property
    def numer(self):
        if not hasattr(self, '_numer'):
            self.as_numer_denom()
        return self._numer

    @property
    def denom(self):
        if not hasattr(self, '_denom'):
            self.as_numer_denom()
        return self._denom

    def poles(self, **kwargs):
        '''compute the roots of the denominator
        kwargs are possible substitutions for free Symbols
        '''
        den = self.denom
        if kwargs:
            den = den.subs(kwargs)
            domain_variable = [x for x in self.domain if x in den.free_symbols][0]
        else:
            domain_variable = list(self.domain)[0]
        p = roots(den, domain_variable, multiple=True)
        return p

    def zeros(self, **kwargs):
        '''compute the roots of the numerator
        kwargs are possible substitutions for free Symbols
        '''
        num = self.numer
        if kwargs:
            num = num.subs(kwargs)
            domain_variable = [x for x in self.domain if x in num.free_symbols][0]
        else:
            domain_variable = list(self.domain)[0]
        p = roots(num, domain_variable, multiple=True)
        return p

    def residues(self):
        assert self.free_symbols == set(self.domain)
        F = simplify(self)
        s = list(F.domain)[0]
        p = F.poles()
        return [[pp,residue(F.e,s,pp)] for pp in p]

    def plot(self,kind=None,fig=None,f=None,color=None,unwrap=True,**kwargs):
        '''kind can be one of:
        'magnitude','bode','nyquist','nichols','pole','zero','pole_zero','root_locus'
        '''
        if 'subs' in kwargs:
            self = self.subs(kwargs['subs'])

        fig = plt.figure(fig)

        translators = {'poles':'pole',
                       'zeros':'zero',
                       'pole-zero':'pole_zero',
                       'root-locus':'root_locus'}
        if kind in translators: kind = translators[kind]
        pole_zero_kinds = ['pole','zero','pole_zero','root_locus']
        if hasattr(fig,'kind') and kind is None:
            kind = fig.kind
        elif not hasattr(fig,'kind') and kind is not None:
            fig.kind = kind
        elif hasattr(fig,'kind') and kind is not None:
            if kind != fig.kind:
                if kind in pole_zero_kinds and fig.kind in pole_zero_kinds:
                    fig.kind = kind
                else:
                    raise Exception(cstr('fail', f'<TF.plot> kind "{kind}" doesnt match figure kind "{fig.kind}"'))
        elif not hasattr(fig,'kind') and kind is None:
            kind = fig.kind = 'magnitude' # default kind

        F = self # could be in s or f domain.

        fsamples = f # protect f

        f, s, z, T = symbols('f s z T')
        fsyms = F.free_symbols
        if f in fsyms:
            F = F.subs({f:s/(2*pi*I)})
        if z in fsyms:
            F = F.subs({z:exp(I*s*T)})
        if len(F.domain) != 1:
            plt.close(fig)
            raise Exception(cstr('fail',f'<TF.plot> TF must be a function of s or f'))
        fsyms = F.free_symbols
        assert s in fsyms, cstr('fail',f'<TF.plot> TF must be a function of s or f')
        fsyms.remove(s)

        f = fsamples # restore f

        if len(fsyms)>0 and kind != 'root_locus':
            plt.close(fig)
            raise Exception(cstr('fail',f'<TF.plot> The transfer function must be purely a funcion of s. These are undefined symbols: {fsyms}'))

        label = kwargs.get('label',f'${F.name}{F.domain_str}$')

        if kind in ['pole','poles','zero','zeros','pole_zero','root_locus']:
            if isempty(fig.axes):
                fig.subplots()
                ax = fig.axes[0]
                ax.axvline(x=0,color='black')
                ax.axhline(y=0,color='black')
            ax = fig.axes[0]
            properties = {'aspect':1, 'xlabel':'Real','ylabel':'Imag','autoscale_on':True}
            plt.setp(ax,**properties)
            plt.grid(True)

            if kind == 'pole':
                if color is None: color = 'red'
                p = F.poles()
                p = [complex(x.n()) for x in p]
                for pp in p:
                    ax.plot(pp.real,pp.imag,marker='x',fillstyle='none',linestyle='none',color=color)
                plt.suptitle(f'Poles of ${F.name}{F.domain_str}$')

            elif kind == 'zero':
                if color is None: color = 'green'
                p = F.zeros()
                p = [complex(x.n()) for x in p]
                for pp in p:
                    ax.plot(pp.real,pp.imag,marker='o',fillstyle='none',linestyle='none',color=color)
                plt.suptitle(f'Poles of ${F.name}{F.domain_str}$')

            elif kind == 'pole_zero':
                F.plot(kind='zero',fig=fig,color=color)
                F.plot(kind='pole',fig=fig,color=color)
                plt.suptitle(f'Poles and zeros of ${F.name}{F.domain_str}$')

            elif kind == 'root_locus':
                g = Symbol('g')
                assert g in F.free_symbols,cstr('fail','<TF.plot> (root_locus) there must be a gain variable, g')
                if color is None: color = 'orange'
                gvals = kwargs.get('gvals',np.logspace(-1,1))
                marker = kwargs.get('marker','.')
                markersize = kwargs.get('markersize',2)

                for gain in gvals:
                    p = F.subs({g:gain}).poles()
                    p = [complex(x) for x in p]
                    for pp in p:
                        ax.plot(pp.real,pp.imag,color=color,marker=marker,markersize=markersize,linestyle='None')

                plt.suptitle(f'Root-Locus of ${F.name}{F.domain_str}$')

            xlim, ylim = (plt.getp(ax, s) for s in ['xlim', 'ylim'])
            r = max((abs(x) for x in xlim + ylim))
            properties = {'xlim':(-r,r),'ylim':(-r,r)}
            plt.setp(ax,**properties)

            return fig

        if f is None: f = np.logspace(-2,2,1000)
        fun = lambdify(s,F.e)
        res = fun( complex(2*pi*I)*f )

        if kind == 'magnitude':
            if isempty(fig.axes):
                fig.subplots()
            ax = fig.axes[0]
            ax.plot(f,np.abs(res),color=color,label=label)
            ax.set_yscale('log')
            ax.set_xscale('log')
            ax.grid(True,which='both')
            ax.set_ylabel(r'$\left|F(s) \right|\quad (s=2\pi i f)$')
            ax.set_xlabel(r'$f$ (Hz)')
            plt.suptitle(f'${self.name}{self.domain_str}$ magnitude plot')

        elif kind == 'bode':
            if isempty(fig.axes):
                axes = fig.subplots(nrows=2)

            ax = fig.axes[0]
            ax.autoscale(enable=True)
            ax.plot(f,np.abs(res),color=color,label=label)
            ax.set_xscale('log')
            ax.set_yscale('log')
            ax.grid(True,which='both')
            liminclude(ax,'y',1,eps=0.1)
            ax.axhline(y=1,color='black')
            ax.set_ylabel(r'$\left|F(s) \right|\quad (s=2\pi i f)$')

            ax = fig.axes[1]
            ax.autoscale(enable=True)
            ax.set_xscale('log')
            ax.grid(True,which='both')
            phase = np.angle(res)
            if unwrap: phase = np.unwrap(phase)
            ax.plot(f,phase)
            liminclude(ax,'y',0,eps=0.1)
            ax.axhline(y=-pi,color='red',linestyle='dashed',linewidth=1)
            ax.set_ylabel(r'$\angle F(s)$')

            plt.suptitle(f'Bode plot ${self.name}{self.domain_str}$')

        elif kind == 'nyquist':
            if isempty(fig.axes):
                fig.subplots()

            ax = fig.axes[0]
            ax.plot(res.real,res.imag,color=color,label=label)
            ax.grid(True)
            ax.set_aspect(1)
            ax.set_xlabel('Real')
            ax.set_ylabel('Imag')

            th = np.linspace(0,2*np.pi)
            cx,cy = np.cos(th),np.sin(th)
            ax.plot(cx,cy,color='black')
            ax.axvline(x=0,color='black')
            ax.axhline(y=0,color='black')

            plt.suptitle(f'Nyquist plot ${self.name}{self.domain_str}$')

        elif kind == 'nichols':
            if isempty(fig.axes):
                fig.subplots()

            ax = fig.axes[0]
            ax.autoscale(enable=True)
            mag,phase = np.abs(res),np.angle(res)
            if unwrap: phase = np.unwrap(phase)
            ax.plot(phase,mag,color=color,label=label)
            ax.plot(-pi,1,marker='o',color='red',linestyle='none')
            ax.grid(True)
            liminclude(ax,'y',1,eps=0.1)
            liminclude(ax,'x',0,eps=0.1)
            liminclude(ax,'x',-pi,eps=0.1)
            ax.axvline(x=-pi,color='black')
            ax.axvline(x = 0,color='black')
            ax.set_xlabel('phase')
            ax.set_ylabel('magnitude')

            plt.suptitle(f'Nichols chart ${self.name}{self.domain_str}$')

        else:
            plt.close(fig)
            raise Exception(cstr('fail',f'<TF.plot> plot type {type} not supported'))

def Abs(F):
    if isinstance(F,TF):
        return F.__abs__()
    else:
        return sympy.Abs(F)

def simplify(F):
    if isinstance(F,TF):
        return TF( sympy.simplify(F.e), *F.domain)
    else:
        return sympy.simplify(F)

def together(F):
    if isinstance(F,TF):
        return TF( sympy.together(F.e), *F.domain)
    else:
        return sympy.together(F)

def apart(F,*args):
    if isinstance(F,TF):
        return TF( sympy.apart(F.e,*args), *F.domain)
    else:
        return sympy.apart(F,*args)

class PS():
    '''Power spectrum defined over the frequency domain, f
    '''
    f = Symbol('f')
    valid_domains = {f}
    def __init__(self,e,**kwargs):
        if isinstance(e, PS): e = e.e
        self.e = e
        self.name = ''
        self.domain = f = Symbol('f')
        self.__dict__.update(kwargs)
        self.fun = lambdify(f,e)

    def _repr_latex_(self):
        s = latex(self.e, mode='plain')
        s = f'$\\displaystyle {s}$'
        return s

    def _pretty(self):
        return PrettyPrinter()._print(self.e)

    def __str__(self):
        s = str(self.e)
        return s

    def __repr__(self):
        return self._pretty().s

    @property
    def domain_str(self):
        return f'({self.domain.name})'

    def __call__(self,arg):
        res = self.fun(arg)
        return res

    def subs(self,d):
        domain = {self.domain}
        for x, xr in d.items():
            if x in domain:
                if isinstance(x, Symbol) and isinstance(xr, Symbol):  # substitute one symbol for another, like {s:z}
                    domain.remove(x)
                    domain.add(xr)
                elif isinstance(x, Symbol):  # substitute a symbol with an expression, like {z: e**(s*T)}
                    domain.remove(x)
                    domain = domain.union(xr.free_symbols)
        domain = domain.pop()
        return PS(self.e.subs(d), domain,name=getattr(self,'name',''))

    @property
    def free_symbols(self):
        return self.e.free_symbols

    def __add__(self,b):
        if isinstance(b,PS):
            assert self.domain == b.domain
            return PS(self.e + b.e)
        else:
            return PS(self.e + b)

    def __radd__(self,b):
        return self.__add__(b)

    def __sub__(self,b):
        if isinstance(b,PS):
            assert self.domain == b.domain
            return PS(self.e - b.e)
        else:
            return PS(self.e - b)

    def __rsub__(self,b):
        if isinstance(b,PS):
            assert self.domain == b.domain
            return PS(b.e - self.e)
        else:
            return PS(b - self.e)

    def __mul__(self,b):
        if isinstance(b,PS):
            raise Exception(cstr('fail','<PS.__mul__> cannot multiply two power spectra'))
        elif isinstance(b,TF): # PS*TF -> PS hopefully you've abs-squared the TF <future: check if TF is real and positive>
            f,s  = symbols('f s')
            assert s in b.domain
            bf = b.subs({s:2*pi*I*f})
            return PS(self.e*bf.e)
        else:
            return PS(self.e*b)

    def __rmul__(self,b):
        return self.__mul__(b)

    def __truediv__(self,b):
        if isinstance(b,PS): # return the ratio as a transfer function PS/PS -> TF(f)
            assert self.domain == b.domain
            r = TF(self.e/b.e,self.domain)
            f = Symbol('f')
            s = self.domain
            return r.subs({f:s/(2*pi*I)})
        elif isinstance(b,TF): # PS/TF -> PS
            f,s = symbols('f s')
            assert s in b.domain
            assert self.domain == f
            bf = b.subs({s:2*pi*I*f})
            return PS( self.e/bf.e)
        else: # PS/Symbol
            return PS(self.e/b)

    def __rtruediv__(self,b):
        if isinstance(b,PS): # PS/PS -> TF
            return b.__truediv__(self)
        else: # Symbol/PS not allowed
            raise Exception(cstr('fail', '<PS.__rtruediv__> cannot divide by a power spectrum '))

    def _sympy_(self):
        return self.e

    def plot(self,fig=None,f=None,color=None,**kwargs):
        fig = plt.figure(fig)
        label = kwargs.get('label',f'${self.name}{self.domain_str}$')

        if f is None: f = np.logspace(-2,2,1000)

        F = self
        fsamples = f
        f,s = symbols('f s')
        if s in self.free_symbols:
            F = F.subs({s:2*pi*I*f})

        f = fsamples

        res = F.fun(f)
        if isempty(fig.axes):
            fig.subplots()
        ax = fig.axes[0]
        ax.plot(f,res,color=color,label=label)
        ax.set_yscale('log')
        ax.set_xscale('log')
        ax.grid(True,which='both')
        ax.set_ylabel(r'Power spectral density, <<units>>')
        ax.set_xlabel(r'Frequency, $f$, Hz')
        plt.suptitle(f'${F.name}{F.domain_str}$')

def isempty(a):
    return len(a) == 0

def liminclude(ax,axis,val,eps=0):
    '''include a y value within the limits of the graph.
    axis must be 'x' or 'y'
    eps is the margin beyond the limit to keep in the graph
    '''
    getlim = getattr(ax,f'get_{axis}lim')
    setlim = getattr(ax,f'set_{axis}lim')
    lim = list(getlim())
    if val<lim[0]: lim[0] = val-eps
    if val>lim[1]: lim[1] = val+eps
    setlim(lim)

legacy_code = False
if legacy_code:
    def poles(F):
        return F.poles()

    def zeros(F):
        return F.zeros()

    def figure(fig=None,kind=None):
        if fig is None: # plot to existing latest figure, or, if no figures exist, create one
            if isempty(plt.get_fignums()):
                fig = plt.figure()
                ax = fig.add_subplot()
            else:
                fig = plt.gcf()
                ax = plt.gca()
        else: # fig is the number of the figure or the Figure object to use
            fig = plt.figure(fig)
            if isempty(fig.axes):
                ax = fig.add_subplot()
            else:
                ax = fig.axes[0]
        if hasattr(fig,'kind') and fig.kind != kind:
            wprint(f'<ctl2.figure> current figure kind is {fig.kind}, not {kind}')
        fig.kind = kind
        if kind == 'poles':
            properties = {'aspect': 1, 'xlabel': 'Real', 'ylabel': 'Imag','autoscale_on':True}
            ax.axvline(x=0)
            ax.axhline(y=0)
            plt.setp(ax, **properties)
        else:
            pass
        plt.grid(True)
        fig.finish = types.MethodType(fig_finish,fig)
        return fig

    def fig_finish(fig):
        ax = plt.gca()
        if fig.kind == 'poles':
            xlim, ylim = (plt.getp(ax, s) for s in ['xlim', 'ylim'])
            r = max((abs(x) for x in xlim + ylim))
            properties = {'xlim':(-r,r),'ylim':(-r,r)}
            plt.setp(ax,**properties)

    def zero_plot(F,fig=None,color='green'):
        z = F.zeros()
        z = [complex(x.n()) for x in z]
        fig = figure(fig,'poles')
        for zz in z:
            plt.plot(zz.real,zz.imag,color=color,marker='o',fillstyle='none',linestyle='None')
        plt.title(f'Zeros of {F.name}{F.domain_str}')
        fig.finish()
        return fig

    TF.zero_plot = zero_plot

    def pole_plot(F,fig=None,color='red'):
        p = F.poles()
        p = [complex(x.n()) for x in p]
        fig = figure(fig,kind='poles')
        for pp in p:
            plt.plot(pp.real,pp.imag,color=color,marker='x',linestyle='None')
        plt.title(f'Poles of {F.name}{F.domain_str}')
        fig.finish()
        return fig

    TF.pole_plot = pole_plot

    def pole_zero_plot(F,fig=None,pcolor='red',zcolor='green'):
        fig = pole_plot(F,fig=fig,color=pcolor)
        zero_plot(F,fig=fig,color=zcolor)
        plt.title(f'Pole-Zero plot of {F.name}{F.domain_str}')
        return fig

    TF.pole_zero_plot = pole_zero_plot

    def root_locus_plot(G,g,gvals=np.logspace(-1,1),fig=None,color='red',marker='.',markersize=2):
        '''G is the closed-loop transfer function
        g is the gain, a symbol within G
        '''
        fig = figure(fig,kind='poles')
        assert g in G.free_symbols
        for gain in gvals:
            p = G.subs({g:gain}).poles()
            p = [complex(x) for x in p]
            for pp in p:
                plt.plot(pp.real,pp.imag,color=color,marker=marker,markersize=markersize,linestyle='None')
        plt.title(f'Root-Locus of {G.name}{G.domain_str}')
        fig.finish()

    TF.root_locus_plot = root_locus_plot

def test():
    from IPython.display import display
    from sympy.abc import a,b,g,s,z
    from sympy import conjugate,I
    F = TF( (s+1)/((s+a)*(s+conjugate(a))), s, name='F')
    G = TF( 1/(1+g*F),s,name = 'G')
    display(F)
    F.plot(subs={a:0.1+I},kind='pole_zero'); plt.show()
    display(G)
    G.plot(subs={a:0.1+I},kind='root_locus'); plt.show()
    F.plot(subs={a:0.1+I},kind='bode'); plt.show()
    F.plot(subs={a:0.1+I},kind='nichols'); plt.show()
    F.plot(subs={a:0.1+I},kind='nyquist'); plt.show()
    globals().update(locals())

def test2():
    from sympy.abc import s,z,gamma
    zi = 1/z
    zoh = TF( (1-zi) / s, s,z)
    comp = TF( zi/(1-gamma*zi),s,z)
    zoh = zoh.subs({z:1+s})
    comp = comp.subs({gamma:0.9,z:1+s})
    F = zoh*zoh * comp
    globals().update(locals())