import astrobot.control as c
import numpy as np
import astropy.units as u
from astrobot import oprint
import copy
from collections import namedtuple
isscalar = c.isscalar
plt = c.plt
pi = np.pi
rint = lambda x: int(np.rint(x))
sentinel = 'here'

def exme():
    execfile('astrobot/astrobot/scratch2.py')

class SystemBase():
    @property
    def pp(self):
        oprint.pprint(self)
    
    @property
    def source(self):
        return inspect.getsource(self.f)

    def set(self,**kwargs):
        '''set the attributes of the system block
        '''
        for key,val in kwargs.items(): self.__dict__[key] = val
        return self
    
    def copy(self):
        return copy.deepcopy(self)
    
class SystemBlock(SystemBase):
    '''define a class of items that can be used
    in a time simuation
    '''
    record_buffer_size = 10000
    valid_kinds = ['standard-block','generator','multi-input']
    
    def __init__(self,**kwargs):
        self.x = 0.
        self.y = 0.
        self.record = np.array([0.]*self.record_buffer_size)
        self.record_pointer = 0
        self.kind = kwargs.pop('kind','standard-block')
        n = kwargs.pop('n',0)
        x = np.array([0.]*n)
        x = kwargs.pop('x',x) # specifying x overrides n
        n = len(x)
        self.x,self.n = x,n
        self.input = kwargs.pop('input',None)
        if self.input is None and self.kind == 'multi-input':
            self.input = []
        self.output = None
        self.triggered = False
        self.ptr = 0
        self.hold = False
        self.scale = 1.
        self.clock = None
        self.set(**kwargs)
    
    def reset(self):
        '''reset the state to x0 and clear all the history
        '''
        x,y,n = self.x,self.y,self.n
        x0 = getattr(self,'x0',0.)
        x = x*0+x0
        y = 0
        self.x,self.y = x,y
        self.record = 0*self.record
        self.record_pointer = 0
        self.triggered = False
        self.ptr = 0

    @property
    def connections(self):
        return connections(self)
    
    def connection_diagram(self):
        connection_diagram(self)
    
    def connect(self,*args):
        return connect(self,*args)
    
    def run(self,**kwargs):
        run(self,**kwargs)
        return self
    
    def record_entry(self):
        y,p = self.y,self.record_pointer
        self.record[p] = y
        p = (p+1)%self.record_buffer_size
        self.record_pointer = p
    
    def multiplot(self,**kwargs):
        multiplot(self,**kwargs)
        return self
    
    def summarize(self,plot=True):
        '''summarize results after a run: create
        Signals for the input disturbance and residuals
        from their data records and optionally plot them.
        '''
        nt,dt = self.clock.k, self.clock.dt
        t = np.arange(0,nt)*dt
        fmax = (1/(2*dt)).to(u.Hz) # need to fix this in the spectrum.plot routine
        ph =  c.Signal((t,system.record[0:nt]*u.rad),name='phi')
        phe = c.Signal((t,adder.record[0:nt]*u.rad),name='residual')
        if plot:
            ph.plot(label='atmos aber'); fig = plt.gcf()
            phe.plot(label='residual',fig=fig,title=', '.join([c.latex_prep(x) for x in [ph.name,phe.name]]))
        plt.gca().set_xlim(0,(nt*dt).value)
        phF = ph.spectrum()
        pheF = phe.spectrum()
        if plot:
            phF.abs().plot(fig=None,fmax=fmax); fig = plt.gcf()
            pheF.abs().plot(fig=fig,fmax=fmax,title=', '.join([c.latex_prep(x) for x in [phF.name,pheF.name]]))
        cltf = pheF/phF
        if plot:
            cltf.abs().plot(fmax=fmax)
        system.summary = {'ph':ph,
                          'phe':phe,
                          'phF':phF,
                          'pheF':pheF,
                          'cltf':cltf}
        return system.summary
        
    def trigger(self,val=True):
        self.triggered = val
        
    def __call__(self,*args,**kwargs):
        if kwargs: return self.set(**kwargs)
        r = self.f(self,*args)
        self.record_entry()
        self.triggered = False
        
        return r

Trigger = namedtuple('Trigger',['block','start','interval','kind'],
                     defaults = ('on'))

class SystemClock(SystemBase):
    '''The sytem clocking mechanism. Used to coordinate operations
    of discrete time SystemBlock components.
    '''
    def __init__(self,**kwargs):
        self.T = kwargs.pop('T',1*u.ms)
        self.dt =  kwargs.pop('dt',0.1*u.ms)
        self.K = int(self.T/self.dt)
        self.signals = {} # dict([(sysblock1,k1),(sysblock2,k2)..]) These can be updated later
        self.set(**kwargs)
        self.reset()
    
    def set(self,**kwargs):
        ''' capture the setting of signals
        '''
        signals = kwargs.pop('signals',None)
        if signals is not None:
            self.signals = []
            self.lastk = []
            for val in signals:
                self.signals.append(Trigger(*val))
                self.lastk.append(0)
                
        return super().set(**kwargs)
        
    def tick(self,tick_size=1):
        '''transition the clock one fine time sample. Trigger any
        blocks that are set up to trigger at this point.
        Calling with tick_size=0 causes just the triggering
        without advancing the clock.
        '''
        self.t += tick_size*self.dt
        self.k += tick_size
        k = self.k
        for signum,val in enumerate(self.signals):
            block,start,interval,kind = val
            lastk = self.lastk[signum]
            if (k >= start) and (k == start or (k - lastk) == interval):
                block.trigger(kind)
                self.lastk[signum] = k
    
    def reset(self, **kwargs):
        t0 = kwargs.pop('t0',0.*u.ms)
        #print(f'<reset> t0 = {t0}')
        self.t = t0
        self.k = rint(t0/self.dt)
    
def connect(*args):
    self = args[0]
    self.components = args
    for a in args:
        a.output = None
        a.input = [] if a.kind == 'multi-input' else None
    for a1,a2 in zip(args[:-1],args[1:]):
        a1.output = a2
        if a2.kind == 'multi-input':
            a2.input.append(a1)
        else:
            a2.input = a1
    return self

def connection_diagram(arg,seen=[],level=0):
    if level == 0: seen=[None]
    if arg in seen:
        if hasattr(arg,'name'): print(arg.name)
        return
    seen.append(arg)
    print(arg.name,end='->')
    if hasattr(arg,'output'):
        connection_diagram(arg.output,seen=seen,level=level+1)
    else:
        return

def connections(arg,seen=[None],level=0):
    if level == 0: seen=[None]
    r = []
    if arg in seen:
        if hasattr(arg,'name'): r.append(arg)
        return r
    seen.append(arg)
    r.append(arg)
    if hasattr(arg,'output'):
        r += connections(arg.output,seen=seen,level=level+1)
    return r

def run(s, clock=None, nt=1, t0 = 0*u.ms, reset=True):
    if clock is not None:
        s.clock = clock
    else:
        clock = s.clock
    clock.reset(t=t0)
    s0 = s
    if reset:
        for sub in s.connections:
            sub.reset()
    clock.tick(0)
    while clock.k < nt:
        y = 0
        s = s0
        seen = [None]
        while True:
            if s in seen: break
            seen.append(s)
            if s.kind == 'multi-input': y = s(*[sx.y for sx in s.input])
            elif s.kind == 'generator': y = s()
            else: y = s(y)
            if not hasattr(s,'output'): break
            s = s.output
        clock.tick()
    return

def multiplot(s,offset=2,text_offset=2,figsize=[6.5,6.5],title=''):
    if title == '': title = s.name
    clock = s.clock
    k,dt = clock.k,clock.dt
    t = np.arange(0,k)*dt
    seen = [None]
    chan = 0
    fig = None
    while True:
        if s in seen: break
        seen.append(s)
        c.Signal((t,(s.record[0:k]-offset*chan)*u.Unit(''))).plot(fig=fig,figsize=figsize)
        line_color = plt.gca().lines[-1]._color
        x,y = 0., -offset*chan
        plt.annotate(s.name.replace('_',' '),
                     xy=(x,y), xycoords='data',
                     xytext=(text_offset,text_offset), textcoords='offset points',
                     )
        plt.gca().axhline(y=y)
        fig = plt.gcf()
        chan += 1
        if not hasattr(s,'output'): break
        s = s.output
    plt.gcf().suptitle(c.latex_prep(title))

'''System Block function definitions -
    These generate the system output y and change of state x as a funtion of the input u.
Rules:
    Output before update: The output is first calculated as a function of the ~present~ state and ~present~ input,
        then the state is updated.
    Any throughput of input to output is explicit  (for example D != 0 in a state-space model).
    The resulting output (y) is stored in the object, so it can be read out at any subsequent
        time without triggering a state transistion.
    Box signals: includes left point, excludes right point. @------O
        i.e. "overlapping" time point at the transition belongs to the
        subsequent box signal
Clocking:
    t,k is the global clock time. Each block function may depend on k or t, and any internal clock-oriented
    attributes (such as T or K the interval at which it triggers a state transition).
    Internal attribute K is auto-set at the start of a simulation if the object has a T attribute so
    that K = int(T/dt) where dt is the simulation fine interval.
Triggering:
    sample_xxx functions are intended to do a state transition only on a trigger, so they
    act as discrete time functions. The trigger is usually set by a Clock. It is cleared
    automatically after the function call.
    
'''
def sample_stateSpacef(self,u):
    A,B,C,D,x = self.A, self.B, self.C, self.D, self.x
    y = C*x + D*u
    if self.triggered:
        x = A*x + B*u
    self.x,self.y = x,y
    return y
def sample_integratorf(self,u):
    gain, gamma, x = self.gain, self.gamma, self.x
    if self.triggered:
        y = x + gain*u
        x = gamma*(x+gain*u)
    elif self.hold:
        y = self.y
    else:
        y = 0.
    self.x,self.y = x,y
    return self.y
def integratorf(self,u):
    scale = getattr(self,'scale',1)
    state = getattr(self,'state','on')
    triggered = self.triggered
    #clock = self.clock
    x = self.x
    if triggered == 'on':
        y = x
        x = u
        state = 'on'
        #print(clock.k,state)
    elif triggered == 'off' or state == 'off':
        y = 0.
        x = 0.
        state = 'off'
        #if triggered == 'off': print(clock.k,state)
    else:
        y = x
        x = x + u
    y = scale*y
    self.x,self.y = x,y
    self.state = state
    return y
def delayf(self,u):
    x = self.x
    y = x[-1]
    x = np.roll(x,1)
    x[0] = u
    self.x,self.y = x,y
    return y        
def adderf(self,x1,x2):
    s = self.signs
    y = s[0]*x1 +s[1]*x2
    self.y = y
    return y
def boxcarf(self,u):
    scale = getattr(self,'scale',1.)
    x,n = self.x,self.n
    y = x.sum()/n
    x = np.roll(x,1)
    x[0] = scale*u
    self.x,self.y = x,y
    return y
def sample_holdf(self,u):
    scale = getattr(self,'scale',1)
    x,y = self.x, self.y
    if self.triggered:
        y = x = scale*u
        self.x,self.y = x,y
    return y
def samplef(self,u):
    scale = getattr(self,'scale',1)
    if self.triggered:
        y = scale*u
    else:
        y = 0
    self.y = y
    return y
def ccd_readoutf(self,u):
    x,y = self.x, self.y
    if self.triggered:
        y = x[-1]
    x = np.roll(x,1)
    x[0] = u
    self.x, self.y = x,y
    return y
    
def disturbf(self):
    y = np.random.normal()
    self.y = y
    return y
def impulse_trainf(self):
    scale = getattr(self,'scale',1)
    if self.triggered:
        y = scale
    else:
        y = 0.
    self.y = y
    return y
def impulsef(self):
    scale = getattr(self,'scale',1.)
    if not hasattr(self,'done'):
        self.done = False
    if not self.done and self.triggered:
        y = scale
        self.done = True
    else:
        y = 0.
    self.y = y
    return y
def constantf(self):
    scale = getattr(self,'scale',1.)
    self.y = y = scale
    return y
def sigf(self):
    ptr,data = self.ptr,self.data
    y = data[ptr]
    ptr += 1
    self.ptr,self.y = ptr,y
    return y

'''System Block instances -
'''

T = 1*u.ms
dt = 0.1*u.ms
tau = 0.3*u.ms

if 'ph' not in locals():
    ph = c.Sphi.instance(name='phi',dt=.1*u.ms,n=10240)

abcd_sys = SystemBlock(name = 'asys',f = sample_stateSpacef,descr='5 step delay represented in state space',
    A = np.matrix(np.diag(np.ones(4),1)),
    B = np.matrix([0,0,0,0,1.]).T,
    C = np.matrix([1.,0,0,0,0]),
    D = 0,
    x = np.matrix([0,0,0,0,0]).T,
    )
control_law = SystemBlock(name='control_law',f = sample_integratorf,descr='leaky integrator control law',
                   gamma=0.8,
                   T = T,
                   n = 1,
                   gain = .4,
                   )
WFSdelay = SystemBlock(name='WFSdelay',f=delayf,descr='WFS readout delay',
                       tau = T,
                       n = rint(T/dt),
                       )
compute_delay = SystemBlock(name='compute_delay',f = delayf,descr='compute delay',
                    tau = tau,
                    n = rint(tau/dt),
                    )
adder = SystemBlock(name='adder',f = adderf,descr='two-input +/- difference',
                    signs = (+1,-1),
                    kind = 'multi-input',
                    )
WFSexpose = SystemBlock(name='WFSexpose',f = integratorf, descr='continuous integration',
                        T = T,
                        n = 1,
                        scale = float(dt/T),
                        )
WFSframebuf = SystemBlock(name='WFSframebuf',f = sample_holdf,descr='WFSdata in the frame buffer',
                             T = T,
                             n = 1,
                             )
WFSreadout = SystemBlock(name = 'WFSreadout', f = ccd_readoutf, descr='Time it takes to read out the array',
                         n = rint(T/dt),
                         )
WFSoutputhold = SystemBlock(name='WFSoutputhold',f = sample_holdf,descr='WFS output held for one period',
                             T = T,
                             n = 1,
                             )
DM = SystemBlock(name='DM',f=sample_holdf,descr='Command held on the DM',
                            T = T,
                            n = 1,
                            )
boxcar = SystemBlock(name='boxcar',f=boxcarf,descr='boxcar average',
                     n = rint(T/dt),
                     )
sampler = SystemBlock(name='sampler',f=samplef,descr='sample at regular interval',
                      scale=1.)
sample_and_hold = SystemBlock(name='sample_and_hold',f=sample_holdf,descr='Sample and hold')
delay = SystemBlock(name='delay',f=delayf,descr='delay',
                    n = rint(T/dt))

disturb = SystemBlock(name='disturb',f = disturbf,
                      kind = 'generator',
                      )
impulse = SystemBlock(name='impulse',f = impulsef,
                      kind = 'generator',
                      scale = float(T/dt),
                      )
impulse_train = SystemBlock(name='impule train',f=impulse_trainf,
                            kind = 'generator',
                            scale=float(T/dt))
constant = SystemBlock(name='constant',f = constantf,
                       kind = 'generator',
                       scale = .1,
                       )
atmos = SystemBlock(name='atmos',f=sigf,descr='Atmospheric phase turbulence',
                    kind = 'generator',
                    data = ph.samples.x.value,
                    ptr = 0,
                    )

impulse.scale = 10.
constant.scale = 1.
control_law.gain = .4
control_law.gamma = .999

def test_impulse(nt=70):
    s = impulse.set(done=False)
    s.connect(adder, WFSexpose, WFSframebuf, WFSdelay, compute_delay, control_law, DM, adder)
    s.clock = SystemClock(T=T,dt=dt)
    n = rint(tau/dt)
    s.clock.signals = {impulse:0,
                       WFSexpose:0,
                       WFSframebuf:0,
                       control_law:n,
                       DM:n}
    control_law.hold = True
    
    s.run(nt=nt).multiplot(offset=2)
    return s

def test_atmos(signal=ph,nt=7000,summarize=False,plot=False):

    if signal is None:
        signal = c.Sphi.instance(name='phi',dt=.1*u.ms,n=10240)
    assert nt < len(signal.samples.x)
    disturb = SystemBlock(name='atmos',f=sigf,descr='Atmospheric phase turbulence',
                        kind = 'generator',
                        data = signal.samples.x.value,
                        ptr = 0,
                        )

    clock = SystemClock(T=T,dt=dt)
    n = rint(tau/dt)
    clock.signals = {WFSexpose:0,
                     WFSframebuf:0,
                     control_law:n,
                     DM:n}
    control_law.hold = True

    system = disturb.copy()
    x = system.data[0:nt]
    t = np.arange(0,nt)*dt
    #x = x * np.exp(-3*t/(nt*dt)) # window to enable a good FFT
    system.data = x
    system.connect(adder, WFSexpose, WFSframebuf, WFSdelay, compute_delay, control_law, DM, adder)
    system.set(clock=clock).run(nt=nt)
    if plot: system.multiplot(offset=10)

    if summarize:
        t = np.arange(0,nt)*dt
        fmax = (1/(2*dt)).to(u.Hz) # need to fix this in the spectrum.plot routine
        ph =  c.Signal((t,system.record[0:nt]*u.rad),name='phi')
        phe = c.Signal((t,adder.record[0:nt]*u.rad),name='residual')
        ph.plot(label='atmos aber'); fig = plt.gcf()
        phe.plot(label='residual',fig=fig,title=', '.join([c.latex_prep(x) for x in [ph.name,phe.name]]))
        plt.gca().set_xlim(0,(nt*dt).value)
        phF = ph.spectrum()
        pheF = phe.spectrum()
        phF.abs().plot(fig=None,fmax=fmax); fig = plt.gcf()
        pheF.abs().plot(fig=fig,fmax=fmax,title=', '.join([c.latex_prep(x) for x in [phF.name,pheF.name]]))
        (pheF/phF).abs().plot(fmax=fmax)
    
    return system

def test_amany(n=10):
    sa = None
    for k in range(n):
        print(k)
        s = test_atmos(signal=None,nt=10000).summarize(plot=False)['cltf'].abs()
        if sa:
            sa = sa + s
        else:
            sa = s
            name = '\\left<' + s.name + '\\right>_{%s}'%n
    sa = sa/n
    sa.name = name
    return sa
    
def test_constant(nt=250):
    system = constant
    system.connect(adder, WFSexpose, WFSframebuf, WFSdelay, compute_delay, control_law, DM, adder)
    system.set(clock = SystemClock(T=T,dt=dt))
    system.clock.signals = {WFSexpose:0,
                            WFSframebuf:0,
                            control_law:rint(tau/dt),
                            DM:rint(tau/dt)}
    system.run(nt=nt).multiplot(offset=2,title='test\_constant')
    return system

def test_sample(nt=70,offset=10):
    ph = c.Sphi.instance(name='phi',dt=.1*u.ms,n=10240)
    a = atmos.copy()
    a.data = ph.samples.x.value

    sampler2 = sampler.copy()
    system = a.connect(sampler,sample_and_hold,delay, sampler2)
    clock = SystemClock(T=T,dt=dt)
    clock.signals = {sampler:0,
                     sample_and_hold:0,
                     sampler2:0}
    system.set(clock=clock).run(nt=nt)
    title = c.latex_prep('p_T \\circ e^{-sT} \\circ \\frac{1-e^{-sT}}{s} \\circ p_T')
    system.multiplot(offset=offset,title=title)
    system1 = system.copy()
    
    system = a.connect(sampler,delay)
    system.run(clock=clock,nt=nt)
    title = c.latex_prep('e^{-sT} \\circ p_T')
    system.multiplot(offset=offset,title=title)
    return system1,system

def test_ol(nt=70,offset=10.):
    s = atmos
    delay = SystemBlock(name='delay',f=delayf,n=15)
    system = connect(s,WFSexpose,sampler,delay,sample_and_hold)
    clock = SystemClock(T=T,dt=dt)
    clock.signals = {WFSexpose:0,
                     sampler:0,
                     sample_and_hold:5}
    system.set(clock=clock).run(nt=nt)
    system.multiplot(offset=offset,title='Loop transfer')
    return system

def test_cl(nt=70,offset=10.):
    clock = SystemClock(T=T,dt=dt)
    s = atmos
    delay = SystemBlock(name='delay',f=delayf,n=15)
    control_law.hold = False
    boxcar2 = boxcar.copy().set(name='boxcar2',scale=rint(T/dt))
    system = connect(s,adder,boxcar,sampler,delay,control_law,boxcar2,adder)
    clock.signals = {sampler:0,
                     sample_and_hold:5,
                     control_law:5}
    control_law.gain = 0.4
    system.set(clock=clock).run(nt=nt).multiplot(offset=offset,title='Closed loop')
    return system

def test_sh(nt=70,offset=10):
    clock = SystemClock(T=T,dt=dt)    
    clock.signals = {sampler:0}
    sampler.scale = rint(T/dt)
    system = connect(atmos,sampler,boxcar)
    system.run(clock=clock,nt=nt)
    system.multiplot(offset=offset,title='test\_sh')
    return system
    
''' evaluation
system = test_xxx(...)
resid = system.connections[1]
q = c.Signal((t,system.record[0:nt]*u.rad),name='input')
e = c.Signal((t,resid.record[0:nt]*u.rad),name='residual')
qF = q.spectrum()
eF = e.spectrum()
qF.abs().plot()
eF.abs().plot()
(eF/qF).abs().plot()
'''