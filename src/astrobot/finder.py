'''finder v2
Finder supports searching disk directories for fits files associated with ShaneAO
and making pandas databases of the files and their fits headers information.

Files databases:
----------------
The most general routine is::

    from astrobot import finder
    df = dbFiles(top)

which makes a database of all the
fits files in a directory structure. It can take a directory name argument
(with, optionally, dots and tildes).

Headers databases:
------------------
From a files database, one can sort out sharcs or telemetry files and make
databases containing the fits headers information, ::

    df_sharcs = dbHdrs('sharcs',df)

makes a database from the headers in sharcs files.

::

    df_telem = dbHdrs('telemetry',df)

makes a database fromt the headers in telemetry files.

Dates:
------
Show all the dates where there is telemetry data, then generate a
headers database for a particular date ::

    dates('telemetry')
    dbtelem = dbHdrs('telemetry','2020-11-23') 

Caching:
--------
Search results are cached, so that subsequent searches are much faster. However,
if new data files are added, you will need to refresh the cache ::

    cache_state('stale') # this only applies to the next search, it then gets reset to 'good'
    dbtelem = dbHdrs('telemety','2020-11-23') # a new search

The current observing night's files cache is always stale, so as new files are added
subsequent calls to dbFiles and dbHdrs will automatically regenerate their databases.

To force re-doing a search, use the Cache context manager
to override cache lookups temporarily ::

    with Cache('off'):
        dbdates = finder.dates('telemetry')
        dbtelem = dbHdrs('telemetry',dbdates.iloc[-1].date)
        ...

The results of the searches will still be cached, so this is a way
to refresh the cache.
'''
import os
import pathlib
import pandas as pd
import numpy as np
import datetime
import dateutil.parser as dparser
import glob
import re
import json
import hashlib
from collections import deque
from tqdm import tqdm as progbar
import warnings
from pkg_resources import resource_filename
import astropy.units as u
from astropy.io import fits

from astrobot.astrodate import p_date
from astrobot.tcolor import tcolor,gprint,wprint,eprint
from astrobot import oprint

version = '2.1'
try:
    utf_enabled = 'utf' in os.environ['LANG'].lower()
except:
    utf_enabled = False
progbar_disabled = True
pd.options.display.min_rows = 30
pd.options.display.max_colwidth = 30
    
home = os.path.expanduser('~')
workingDir = os.path.abspath('.')
realAtUcolickDataDir = '/net/real/local/data'
cache_path = os.path.join(home,'.astrobot')
sample_dataset_path = resource_filename('astrobot','data')

dataVols = deque([
    '/Users/donaldgavel/data_lick/LittleWDdrive_backup_11-6-2015/ShaneAO_Data',
    '/Volumes/External 2TB Drive #1/LittleWDdrive_backup_11-6-2015/ShaneAO_Data',
    '/Volumes/External 2TB Drive #1/ShaneAO_Data',
    '/Volumes/External 2TB Drive #2/ShaneAO_Data',
    '/Volumes/LittleWDdrive/ShaneAO_Data',
    os.path.join(home,'telemetry'),
    os.path.join(home,'data'),
    os.path.join(home,'shaneao_data'),
    os.path.join(home,'ShaneAO/data'),
    '/Volumes/Donalds-Mac-Pro/donaldgavel/data',
    os.path.join(realAtUcolickDataDir),
    sample_dataset_path,
    ])

dataVols = deque([
    os.path.join(realAtUcolickDataDir),
    os.path.join(home,'ShaneAO','net_real/local/data'),
    os.path.join(home,'ShaneAO','data'),
    sample_dataset_path,
])

dataVols = deque([v for v in dataVols if os.path.exists(v)])

filename_template = {
    'sharcs'  : 's[0-9]{4}.fits',  # Sharcs data files
    'telemetry': 'Data_[0-9]{4}.fits', # Shaneao telemetry data files
    'TTimage' : 'TT_Data_[0-9]{4}.fits', # Shaneao Tip/Tilt sensor image files
    'WFSimage': 'WFS_Data_[0-9]{4}.fits', # Shaneao Wavefront sensor image files
}

# default header key sets:
hdr_keys = {
    'sharcs': ['date-beg', 'object', 'itime','coadds','ra', 'dec',
               'ha','airmass','tubangle','loopstat', 'filt1nam', 'filt2nam', 'loopsub',
               'opmode','twtrgain', 'woofgain', 'wfsrate', 'ttrate', 'lasonsky'],
    'telemetry': ['date','time','mode','substate','cent',
                  'loop','rate','gain','wgain','alpha','tweeter_','woofer_b'],
    'TTimage':[],
    'WFSimage':[],
}

sort_key = {
    'sharcs': 'date-beg',
    'telemetry': 'time',
    'TTimage':'',
    'WFSimage':'',
}

def dsn_from_filename(filename):
    dstype = fileType(filename)
    pat = filename_template[dstype]
    prefix = pat.split('[')[0]
    suffix = '.fits'
    n = filename.lstrip(prefix).rstrip(suffix)
    n = int(n)
    return n

def fileType(filename):
    for key,pat in filename_template.items():
        r = re.match(pat,filename)
        if r is not None:
            return key
    return 'unknown'

def dbFile(path_to_file):
    fileDir,fileName = os.path.split(path_to_file)
    dstype = fileType(fileName)
    datePattern = re.compile('\d{4}-\d{2}-\d{2}')
    r = datePattern.findall(path_to_file)
    if len(r) > 0: obs_date = r[-1]
    else: obs_date = ''
    time_stamp = os.path.getmtime(path_to_file)
    wtime = datetime.datetime.fromtimestamp(time_stamp)
    
    r = re.search('\d{4}',fileName)
    if r is None: dsn = pd.NA #np.nan
    else: dsn = int(r.group(0))
     
    columns = ['filename','type','dsn','obs_date','file_write_time','path']
    data=[fileName,dstype,dsn,obs_date,wtime,path_to_file]
    df = pd.DataFrame(columns=columns,data=[data])
    if 'obs_date' in df.columns:
        df['obs_date'] = df['obs_date'].apply(pd.Timestamp)
    if 'time' in df.columns:
        df['time'] = df['time'].apply(pd.Timestamp)

    return df

def dbHdr(path_to_file):
    assert os.path.isfile(path_to_file),f'{tcolor.fail}{path_to_file} is not a file'
    path_to_file = os.path.abspath(os.path.expanduser(path_to_file))
    path,filename = os.path.split(path_to_file)
    dstype = fileType(filename)
    df = dbFile(path_to_file)
    with fits.open(path_to_file) as hdu:
        hdr = hdu[0].header
        row = {}
        for key in hdr_keys[dstype]:
            row[key] = hdr.get(key,'unknown')
    dfr = pd.DataFrame([row])
    if dstype == 'telemetry':
        dfr['time'] = Timestamp( list(dfr['date']), list(dfr['time']) )
    dfr = pd.concat([df,dfr],axis=1)
    dsn = dsn_from_filename(filename)
    dfr.index = [dsn]
    return dfr
    
def findVol(dstype,date,dsn=None,quiet=False):
    '''find the volume that has this specific data set, or data date.
    Return the volume and the path to the data file(s).
        
    Parameters
    ----------
    dstype : str
        One of the data file types: 'sharcs', 'telemetry', 'TTimage', 'WFSimage' or 'all'
    date : str or Timestamp, or datetime.date
        The date associated with the dataset
    dsn : int
        The data set number.
    
    Returns
    -------
    (volume,path) : A tuple of strings
    '''
    types = list(filename_template.keys())
    if dstype == 'all':
        return {x:findVol(x,date,dsn) for x in types}
    if isinstance(dstype,list):
        return {x:findVol(x,date,dsn) for x in dstype}
    if dstype not in types:
        raise ValueError(f'{tcolor.fail}type must be one of {types}{tcolor.end}')
    prefix = filename_template[dstype].split('[')[0]
    
    if isinstance(date,(pd.Timestamp,datetime.datetime)): # Timestamp or datetime.date
        date = str(date.date())
    elif isinstance(date,datetime.date):
        date = str(date)
    elif isinstance(date,np.datetime64):
        date = str(date).split('T')[0]
    elif isinstance(date,str):
        pass
    else:
        raise TypeError(f'{tcolor.fail}date argument must be a string or one of the date types{tcolor.end}')
    
    if isinstance(dsn,str): # assume it is a string (such as a filename) with the data set number imbedded
        r = re.search('\d{4}',dsn)
        if r is None:
            raise ValueError(f'{tcolor.fail}dsn = {dsn} is not a valid dataset designation{tcolor.end}')
        dsn = int(r.group(0))
    
    if not quiet: print(f'<finder.findVol>looking for {date} {dstype} data...',flush=True)
    for vol in list(dataVols):
        #paths = glob.glob(os.path.join(vol,'**/%s'%date),recursive=True)
        ipaths = glob.iglob(os.path.join(vol,f'**/{date}'),recursive=True)
        paths = [x for x in progbar(ipaths,total=10,disable=progbar_disabled,ascii=not utf_enabled)]
        for path in paths:
            if dsn is None:
                files = glob.glob(os.path.join(path,'**/%s*.fits'%prefix),recursive=True)
                if len(files) != 0:
                    paths = [os.path.dirname(x) for x in files]
                    paths = sorted(list(set(paths)))
                    #print(f'{tcolor.green}<finder.findVol> found {dstype} data for {date}{tcolor.end}')
                    if not quiet: print(f'{tcolor.green}found in {vol}{tcolor.end}')
                    return vol,paths
            else:
                files = glob.glob(os.path.join(path,'**/%s%04d.fits'%(prefix,dsn)),recursive=True)
                if len(files) != 0:
                    files = sorted(list(set(files)))
                    if not quiet: print(f'{tcolor.green}found in {vol}{tcolor.end}')
                    return vol,files
        dataVols.rotate(-1) # spin the deque. This leaves latest success on top
    
    print(f'{tcolor.warning}<finder.findVol> WARNING did not find {dstype} data for {date} in any of the dataVols{tcolor.end}')
    
    return None,None

def dbFiles(top='all',date='all', dstype='all',verbose=False):
    ''' Make a database of FITS files in a directory structure
    
    Parameters
    ----------
    top : str or list
        str: the top level directory name - all FITS files in the directory
            and recursively all subdirectories will be included in the result.
        list: a list of directories or file names
    date: str
        a date string ('YYYY-mm-dd').
        Only the subdirectories from this date will be included in the result. 
    dstype: str
        (optional) type of the file: 'telemetry', 'sharcs', etc. Defaults to all types.
        
    Returns
    -------
    a pandas.DataFrame with directory, filename, type and date of each file
    '''
    valid_dstypes = list(filename_template.keys()) + ['all']
    assert dstype in valid_dstypes,f'{tcolor.fail}{dstype} not valid, must be one of {valid_dstypes}{tcolor.end}'

    if top == 'all':
        top = dataVols        
    if isinstance(top,(list,deque)):
        dbs = [dbFiles(x,date=date,dstype=dstype) for x in top]
        df = pd.concat(dbs).reset_index(drop=True)
        return df
    if isinstance(top,str):
        topdir = top
        topdir = os.path.abspath(os.path.expanduser(topdir))
        
        memo = {'function':'dbFiles','topdir':topdir}
        if date != p_date('today'): df = from_cache(memo)
        else: df = None
        if df is not None:
            if verbose: gprint(f'<dbFiles> fileList found in cache',flush=True)
            fileList = list(df.fileList)
        else:
            end = '' if progbar_disabled else '\n'
            if verbose: gprint(f'<dbFiles> recursively searching for all FITS files in {topdir}...',end=end,flush=True)
            fli = glob.iglob(os.path.join(topdir,'**/*.fits'),recursive=True)
            fileList = [x for x in progbar(fli,disable=progbar_disabled,ascii=not utf_enabled)]
            if verbose: gprint(f'done',flush=True)
            df = pd.DataFrame(columns=['fileList'])
            df['fileList'] = fileList
            df = set_metadata(df,memo)
            cache(df)
        
        fnList = [os.path.split(x)[-1] for x in fileList]
        keys = ['filename','type','dsn','obs_date','file_write_time','path']
        memo = {'function':'dbFiles','topdir':topdir,'keys':keys,'files':fnList,'type':dstype,'date':date}
        
        # try to restore from cache
        df = from_cache(memo)
        if df is not None:
            if verbose: gprint(f'<dbFiles> database found in cache')
            return df
    else:
        raise Exception(f'{tcolor.fail}first argument must be a string or list{tcolor.end}')
    
    datePattern = re.compile('\d{4}-\d{2}-\d{2}')  # date pattern
    filePatterns = {}
    for key,pat in filename_template.items():
        cpat = re.compile(pat)
        filePatterns[key] = cpat
    
    vals = []
    end = '' if progbar_disabled else '\n'
    if verbose: gprint(f'<finder.dbFiles> making a database of {dstype} files in {top}, date = {date}...',end=end,flush=True)
    for fullpath in progbar(fileList,disable=progbar_disabled,ascii=not utf_enabled):
        path,filename = os.path.split(fullpath)
        r = datePattern.findall(path)
        if len(r) > 0: fdate = r[-1]
        else: fdate = ''
        if date not in ['all',fdate]: continue
        time_stamp = os.path.getmtime(fullpath)
        time = datetime.datetime.fromtimestamp(time_stamp)
        for key,cpat in filePatterns.items():
            r = cpat.match(filename)
            if r is not None:
                ftype = key
                break
        else:
            ftype = ''
            
        r = re.search('\d{4}',filename)
        if r is None: dsn = pd.NA #np.nan
        else: dsn = int(r.group(0))
        
        if dstype in [ftype,'all']:
            vals.append( (filename,ftype,dsn,fdate,time,fullpath) )
    if progbar_disabled and verbose: gprint(f'done.')
    df = pd.DataFrame(columns=keys, data=vals)
    df = set_metadata(df,memo) # df.metadata = memo
    
    # store to cache
    if 'topdir' in memo:
        cache(df)
    
    # because the cache restore does this, we need to do it first time around
    if 'obs_date' in df.columns:
        df['obs_date'] = df['obs_date'].apply(pd.Timestamp)
    if 'time' in df.columns:
        df['time'] = df['time'].apply(pd.Timestamp)
    
    return df

def dbHdrs(dstype,df):
    '''
    Create a database of files of a certain type, with header information
    
    Parameters
    ----------
    dstype : str
        One of the data file types: 'sharcs', 'telemetry', 'TTimage', 'WFSimage'
        
    df : DataFrame or str or date
        DataFrame: The files database, like that produced by a call to dbFiles()
            (files that are not files of the given type will be ignored).
        str: The name of the top level directory or a path to a single file. Returns info
            on all the FITS files in this directory and its subdirectories
            (for files of the given type). In the case of a single file,
            the 'database' will have one row, pertaining to that file.
        date: The date (can also be a string with a YYYY-MM-DD format)
            In this case, the search is limited to directories with that date.
        
    Returns
    -------
    pandas.DataFrame database of files and file header information
    
    Examples
    --------
    ::
        db = dbHdrs('telemetry','2016-06-19')
        db = dbHdrs('telemetry',dataVols[0])
        db = dbHdrs('telemetry',dbFiles('~/telemetry'))
    '''
    date = None
    if isinstance(df,list):
        r = [dbHdrs(dstype,x) for x in df]
        return pd.concat(r).reset_index(drop=True)
    elif isinstance(df,str):
        r = re.match('^\d{4}-\d{2}-\d{2}$',df)
        if r is not None: # its a date
            date = df
        else: # it's a directory name or a file path
            if os.path.isdir(df):
                q = df = dbFiles(df)
            elif os.path.isfile(df):
                return dbHdr(df)
    elif isinstance(df,pd.DataFrame):
        q = df.copy()
    elif isinstance(df,(pd.Timestamp,datetime.datetime)):
        date = str(df.date())
    elif isinstance(df,datetime.date):
        date = str(df)
    elif isinstance(df,np.datetime64):
        date = str(df).split('T')[0]
    if date is not None:
        d = findVol(dstype,date)[1]
        if d is None: return pd.DataFrame()
        if date == obs_night(datetime.datetime.now()):
            cache_state('stale')
        q = df = dbFiles(d)
       
    if q is not None: q = q[q.type == dstype]
    unk = 'unknown'
    keys = hdr_keys[dstype]
    #dfr = pd.DataFrame(columns=keys)
    
    memo = {'function':'dbHdrs',
            'type': dstype,
            'keys': keys,
            'files': list(q.path),
            }
    if hasattr(df,'metadata'):
        memo['df.metadata'] = df.metadata

    # try restoring the database from cache
    dfr = from_cache(memo)
    if dfr is not None:
        return dfr
    
    # open every fits file and read the headers.
    # This may take a while, so show a progress bar
    dfr = pd.DataFrame([],columns=keys)
    for qr in progbar(q.itertuples(),disable=progbar_disabled,total=len(q),ascii=not utf_enabled):
        filepath = qr.path
        with fits.open(filepath) as hdu:
            hdr = hdu[0].header
            row = {}
            for key in keys:
                row[key] = hdr.get(key,unk)
                
        dfr = dfr.append(pd.DataFrame([row],index=[qr.Index]))
        
    if dstype == 'telemetry': # convert date/time to timestamp
        dfr['time'] = Timestamp( list(dfr['date']), list(dfr['time']) )
    
    dfr = pd.concat([q,dfr],axis=1)
    
    # make sure column names are unique
    f_cols = ['f_'+col if col in keys else col for col in df.columns]
    dfr.columns = f_cols + keys
    
    # sort and reset the index
    if sort_key[dstype] != '':
        dfr = dfr.sort_values(by=sort_key[dstype]).reset_index(drop=True)
        
    set_metadata(dfr,memo) # dfr.metadata = memo
    
    # cache the result
    cache(dfr)
    
    # because the cache restore does this, we need to do it first time around
    if 'obs_date' in dfr.columns:
        dfr['obs_date'] = dfr['obs_date'].apply(pd.Timestamp)
    if 'time' in df.columns:
        dfr['time'] = dfr['time'].apply(pd.Timestamp)
        
    return dfr

def dates(dstype,vol=None):
    '''return a list of dates when data of a given type were taken.
    
    Parameters
    ----------
    dstype : str
        One of the valid data set types: 'sharcs', 'telemetry', 'TTimage', 'WFSimage'
    vol: str or list
        The volume (or volume list) to check. The default is to check all the dataVols.

    Returns
    -------
    DataFrame : A database of "unique" observation dates. A particular date entry may be
        duplicated if is available in multiple volumes.
    '''
    types = list(filename_template.keys())
    if dstype not in types:
        raise ValueError(f'{tcolor.fail}type must be one of {types}{tcolor.end}')
    if vol is None:
        dataVolsList = dataVols
    elif isinstance(vol,list):
        dataVolsList = vol
    else:
        dataVolsList = [vol]
    r = []
    for vol in dataVolsList:
        dbf = dbFiles(vol)
        dbf = dbf[dbf.type==dstype]
        if dbf.empty:
            continue
        the_dates = list(dbf.obs_date.dropna().unique())
        counts = []
        paths = []
        for date in the_dates:
            q = dbf[dbf.obs_date==date]
            path = os.path.split(q.iloc[0].path)[0].replace(vol,'')
            paths.append(path)
            count = len(q)
            counts.append(count)
        the_dates = [str(x).split('T')[0] for x in the_dates]
        q = pd.DataFrame(list(zip(the_dates,counts,paths)))
        q.columns = ['date','n_files','path']
        q['vol'] = vol
        q = q.sort_values(by='date')[['date','n_files','vol','path']]
        r.append(q)
    r = pd.concat(r).sort_values(by='date').reset_index(drop=True)
    return r

# ----------- pandas DataFrame extensions ---------
def set_metadata(df,memo):
    '''insert metadata into the attributes of df without
    triggering the attribute access warning
    '''
    with warnings.catch_warnings():
        warnings.filterwarnings('ignore',category=UserWarning)
        df.metadata = memo
    return df

# ----------- cache ------------
class Cache(object):  # context manager
    '''context manager to temporarily disable cache lookups ::
    
        with Cache('off'):
            finder.dbFiles(dir)
            
    Results will still be cached, so this is a way to refresh cache.
    (see: https://book.pythontips.com/en/latest/context_managers.html)
    '''
    def __init__(self,state='good'):
        self.cstate = state
    def __enter__(self):
        self.ostate = cache_state()
        cache_state(self.cstate)
        return
    def __exit__(self,t,v,tb):
        cache_state(self.ostate)
        
_cache_state = 'good'
_valid_cache_states = ['good','stale','off']
def cache_state(v=None):
    '''Query or set the cache state
    
    Argument:
    ---------
    v : str or None
        'good' - caching is enabled and in a fresh state
        'stale' - cache is declared stale for just the next from_cache operation, then returns to 'good'
        'off' - cache is turned off. like 'stale' for all further from_cache operations, until restored to 'good'
        
    '''
    global _cache_state
    valid_states = _valid_cache_states
    if v is None:
        return _cache_state
    assert v in valid_states,f'{tcolor.fail}cache_state {v} not a valid state. must be one of {valid_states}{tcolor.end}'
    _cache_state = v

def cache(df=None):
    '''cache a data base.
    
    Parameters
    ----------
    df : pandas.DataFrame
        the database

    df must have an attribute 'metadata' which is a
    dictionary that uniquiely identifies the database. This will be used to
    generate the cache file name. It must be json-encodable
    as it will be stored in the cache file along with the database
    
    If df is None or a string, this will be equivalent to a call to cache_state
    (either query or set the caching mode to 'off', 'stale', or 'good')
    '''
    global _cache_state
    if isinstance(df,str) or df is None:
        return cache_state(v=df)
    if not os.path.isdir(cache_path):
        os.mkdir(cache_path)
    memo = df.metadata
    s = str(memo) + f'version = {version}' # hash changes if finder.version updated
    cache_filename = hashlib.sha224(s.encode()).hexdigest()
    cache_filepath = os.path.join(cache_path,cache_filename)
    dbSave(df,cache_filepath)
    if _cache_state != 'off':
        _cache_state = 'good'

def from_cache(memo):
    '''try to get a cached database given the metadata
    
    Parameters
    ----------
    memo : dict
        The metadata dictionary. The cache filename is generated from the metadata
    '''
    if _cache_state in ('off','stale'):
        return None
    s = str(memo) + f'version = {version}' # hash changes if finder.version updated    
    cache_filename = hashlib.sha224(s.encode()).hexdigest()
    cache_filepath = os.path.join(cache_path,cache_filename)
    if os.path.isfile(cache_filepath):
        df = dbRestore(cache_filepath)
        return df
    else :
        return None

def cached():
    '''get metadata from each cached file'''
    files = os.listdir(path=cache_path)
    df = []
    for fn in files:
        filename = os.path.join(cache_path,fn)
        if not os.path.isfile(filename):
            wprint(f'{filename} is not a file')
            continue
        try:
            with open(filename) as f:
                memo = json.loads(f.readline())
        except:
            wprint(f'{filename} is not a finder cache')
            continue
        memo['cached_file_name'] = fn
        df.append(memo)
    df = pd.DataFrame(df)
    return df
    
def dbSave(db,filename):
    '''save a database (pandas.DataFrame) to disk, with a memo
    
    Parameters
    ----------
    db : DataFrame
        the database to save
    memo : str or dict
        a descriptor string or dictionary with discriptor items for the
        saved database
    filename : str
        the file name
    '''
    s = db.to_json()
    memo = db.metadata
    
    with open(filename,'w') as f:
        f.write(json.dumps(memo) + '\n')
        f.write(s)

def dbRestore(filename):
    '''restore a database from disk file.
    
    Parameters
    ----------
    filename : str
        the filename
    
    Returns
    -------
    (db,memo) as a tuple
    '''
    with open(filename) as f:
        memo = json.loads(f.readline())
        s = f.readline()
    db = pd.read_json(s,convert_dates=['obs_date','time'])
    set_metadata(db,memo) # db.metadata = memo
    return (db)

# ---- support for dates and times interpretation ----
def Timestamp(date,time, date_pat='%Y-%m-%d', time_pat='%H%M%S'):
    '''convert date and time strings to pandas Timestamps
    
    Notice the unusal time pattern, which is the one used in telemetry
    data FITS file headers. A more standard time pattern is '%H:%M:%S'
    '''
    if isinstance(date,list) and isinstance(time,list):
        r = [Timestamp(d,t) for d,t in zip(date,time)]
        return r
    if isinstance(date,str):
        date = datetime.datetime.strptime(date,date_pat).date()
    elif isinstance(date,datetime.date):
        pass
    else:
        raise TypeError
    if isinstance(time,str):
        time = datetime.datetime.strptime(time,time_pat).time()
    elif isinstance(date,datetime.time):
        pass
    else:
        raise TypeError
    time = pd.Timestamp(datetime.datetime.combine(date,time))
    return time

def obs_night(time='now'):
    '''
    parse the string "date", which has some reasonable text
    representation of a date, and return the standard YYYY-mm-dd format
    representation for the observing night. Time is assumed local time.
    If a time after midnight but before noon is given, the result is
    the -previous- day, since that is the date of the observing night.
    
    Argument:
    ---------
    date: str, datetime.time, datetime.datetime, or pandas.Timestamp
        a "fuzzy" denotation of a date, either one of the date-like objects
        above or a string that is recognizable by the
        dparser.parse routine. Today/now year, month, day, hour, etc. are
        default, including the case where date is an empty string ''
    
    Returns:
    ========
    str: observing night, in the form YYYY-mm-dd
    '''
    time = parse_time(time)
    r = datetime.datetime.fromisoformat(time)
    midnight = datetime.datetime(r.year,r.month,r.day)
    noon = datetime.datetime(r.year,r.month,r.day,12)
    if r > midnight and r < noon: #morning hours are previous night's observing run
        r = r - datetime.timedelta(days = 1)
    s = r.strftime('%Y-%m-%d')
    return s
    
def parse_time(time='now'):
    ''' parse the "time", which is one of the many python time
    objects or a reasonable text
    representation of a date and time, and return the standard
    YYYY-mm-ddThh:mm:ss format representation for the time.
    Time is assumed local time.
    
    Argument:
    ---------
    time: str, datetime.time, datetime.datetime, or pandas.Timestamp
        a "fuzzy" denotation of a date, either one of the date-like objects
        above or a string that is recognizable by the
        dparser.parse routine. Today/now year, month, day, hour, etc. are
        default, including the case where date is an empty string ''
    
    Returns:
    ========
    str: time in the ISO 8601 format YYYY-mm-ddThh:mm:ss.xxxx
    
    '''
    if isinstance(time,(pd.Timestamp,datetime.date,datetime.datetime)): time = str(time)
    now = datetime.datetime.now()
    if time in ['','today','now',None]: time = str(now)
    r = dparser.parse(time,fuzzy=True)
    s = r.isoformat()
    return s

def test(with_cache='off'):
    prev_state = cache_state()
    cache_state( with_cache )
    date = '2021-06-15'
    findVol('telemetry',date)
    _q = dates('telemetry')
    print(f"{tcolor.cyan}{'2021-06-15' in list(_q['date'])}{tcolor.end}")
    dbHdrs('telemetry','2021-06-15')
    dbHdrs('sharcs','2021-06-15')
    _q = dates('sharcs')
    date = _q.iloc[-1].date
    print(f'{tcolor.cyan}date with sharcs data: {date}{tcolor.end}')
    dbHdrs('sharcs',date)
    cache_state(prev_state)

