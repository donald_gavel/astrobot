"""Template code:
Handles argument parsing, configuration file, and default config parameters
"""
import sys
import argparse
import FlexConfigParser

# Set some defaults
config_defaults = {'has': 'one','verbose': '1'}

# Read a config file (overriding defaults)
cp = FlexConfigParser.FlexConfigParser(config_defaults)
cp.read('plate.cfg')
config = cp.as_dict('plate')

# Parse the arguments (overriding config file and defaults)
parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('-v','--verbose',help='increase output verbosity',action='count')
parser.add_argument('-q','--query',help='query status',action='store_true')
parser.add_argument('-s','--has',help='set the value of "has"')
parser.add_argument('-t','--two',help='set the value of "two"')
args = parser.parse_args()
# override mechanism
for key in args.__dict__:
    if args.__dict__[key] is not None:
        config[key] = args.__dict__[key]

# report on what got through

print config
