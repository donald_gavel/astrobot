# coding=utf-8
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np
from astropy import units as u
from astropy.coordinates import SkyCoord
from matplotlib import cm, colors
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.basemap import Basemap

ra_random = np.random.rand(100)*360.0 * u.degree
dec_random = (np.random.rand(100)*180.0-90.0) * u.degree
c = SkyCoord(ra=ra_random, dec=dec_random, frame='icrs')
"""
Because matplotlib needs the coordinates in radians and between 
π and π, not 0 and 2π, we have to convert them. For this purpose
the astropy.coordinates.Angle object provides a special method,
which we use here to wrap at 180:
"""
def test1():
    ra_rad = c.ra.wrap_at(180 * u.deg).radian
    dec_rad = c.dec.radian
    
    plt.figure(figsize=(8,4.2))
    plt.subplot(111, projection="aitoff")
    plt.title("Aitoff projection of our random data")
    plt.grid(True)
    plt.plot(ra_rad, dec_rad, 'o', markersize=5, alpha=1)
    plt.subplots_adjust(top=0.95,bottom=0.0)
    plt.show(block=True)

def test2():

    # Create a sphere
    r = 1.
    pi = np.pi
    cos = np.cos
    sin = np.sin
    phi, theta = np.mgrid[0.0:pi:10j, 0.0:2.0*pi:10j]
    x = r*sin(phi)*cos(theta)
    y = r*sin(phi)*sin(theta)
    z = r*cos(phi)
    
    #Import data
    n = 40
    theta = np.random.uniform(size=n,low=-np.pi,high=np.pi)
    phi = np.random.uniform(size=n,low=-np.pi/2.,high=np.pi/2.)
    rr = 1.01
    xx, yy, zz = (rr*cos(phi)*cos(theta),rr*cos(phi)*sin(theta),rr*sin(phi))
    
    #Set colours and render
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    ax.plot_surface(
        x, y, z,  rstride=1, cstride=1, color='w', alpha=1, linewidth=0)
    
    ax.scatter(xx,yy,zz,color="k",s=20)
    ax.set_axis_off()
    
    ax.set_xlim([-1,1])
    ax.set_ylim([-1,1])
    ax.set_zlim([-1,1])
    ax.set_aspect("equal")
    plt.tight_layout()
    plt.show(block=True)

def test3():
    global map, pl,ml
    fig = plt.figure()
    #map = Basemap(projection='ortho',lat_0=45,lon_0=-100,resolution='l')
    #map = Basemap(projection='ortho',lat_0=90,lon_0=-100,resolution='l')
    map = Basemap(projection='laea',rsphere=1,width=2,height=2,lat_0 = 45,lon_0=-100,resolution='l')
    pl = np.arange(-90,90,10)
    map.drawparallels(pl)
    ml = np.arange(0,360,15)
    map.drawmeridians(ml)
    map.scatter([-100,-100],[10,20],latlon=True)
    map.scatter([-110,-110],[10,20],latlon=True,color='g')
    for lo in ml:
        x,y = map(lo,0)
        ha = lo/15.
        plt.annotate('%dh'%ha,xy=(x,y),xycoords='data',fontsize=10)
    for la in pl:
        if la != 0:
            x,y = map(270,la)
            plt.annotate('%d'%la,xy=(x,y),xycoords='data',fontsize=10)
    plt.draw()

