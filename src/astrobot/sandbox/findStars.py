# -*- coding: utf-8 -*-
""" findStars.py
Analyze a star field image and select the brightest stars
This is a python coding of the ideas described in
Diolaiti, E., Bendinelli, O., Bonaccini, D., Close, L., Currie, D., &
Parmeggiani, G. (2000). Analysis of isoplanatic high resolution stellar fields
by the StarFinder code. Astronomy and Astrophysics Supplement Series, 147(2),
335–346. http://doi.org/10.1051/aas:2000305
"""
import os
import numpy as np
import scipy
#from astrobot import common_units as units
#from astrobot.graphics import plt
from sauce2 import img
#from astrobot.info_array import InfoArray
from astropy.units import Quantity
import astropy.units as u
import types
import json
#from astrobot import FlexConfigParser
import configparser
import pandas as pd
from sauce2.oprint import pprint

#plt.ion()
config_defaults = {'star_list_dir': '.'}
config = configparser.ConfigParser(config_defaults)
config.read('findStars.cfg')
config_d = dict(config['DEFAULT'])
starListDir = config_d['star_list_dir']

class SimCamera(object):
    '''simulated camera, with photo-sensitivity, bias, read noise,
    bad pixels and cosmic rays
    '''
    def __init__(self, nm, dx, bias=0, noise=0, badpix=None):
        '''
        :param (int,int) n: size of the image
        :param float dx: pixel scale, in arcsec per pixel
        :param float bias: camera bias
        :param float noise: camera read noise, in electrons/read
        :param array_or_float badpiz: the bad-pixel array (0=bad), or a float designating the percent (0...1) of bad pixels.
        '''
        if badpix is None:
            badpix = InfoArray(np.ones(nm),name='bad pixels') # 0 = bad pixel, 1 = good pixel
        if isinstance(badpix,float):
            assert badpix > 0 and badpix < 1
            badPixelPercent = badpix
            badpix = InfoArray(np.random.choice([0,1],nm,p=[badpix,1-badpix]))
        else:
            assert nm == badpix.shape
        self.ccd = InfoArray(np.zeros(nm), name = 'CCD array',
                             bias = bias, noise = noise,
                             badpix = badpix)
        if 'badPixelPercent' in locals():
            self.ccd.badPixelPercent = badPixelPercent
        self.dx = dx
        self.dx_units = 'arcsec'
        self.fieldSize = [dx*nm[0],dx*nm[1]]
        self.fieldSize_units = 'arcsec'

    def pprint(self):
        '''pretty-print the object's contents
        '''
        pprint(self)
    
    def readout(self):
        '''read out the ccd, adding bias and noise
        
        :return: the ccd array
        :rtype: ndarray
        '''
        noise = np.random.normal(size=self.ccd.shape) * self.ccd.noise
        ccd = self.ccd + self.ccd.bias + noise
        return ccd
    
    def expose(self, starlist,psf):
        '''expose the ccd to the starlist
        
        :param list starlist:
        :param ndarray psf: the point-spread function
        '''
        dtheta = self.dx # arcsec
        n,m = int(self.fieldSize[0]/dtheta), int(self.fieldSize[1]/dtheta)
        
        for x,y,m in starlist:
            nx = np.round(x/dtheta).astype(int) + n/2
            ny = np.round(y/dtheta).astype(int) + n/2
            try:
                img.insert(psf*m,self.ccd,[nx,ny])
            except:
                print ('error ',x,y,nx,ny)
        
        self.ccd *= self.ccd.badpix
    
    def erase(self):
        '''clear the ccd
        '''
        self.ccd *= 0.

class Camera(object):
    '''an object to hold the characteristics of the actual camera,
    even a slot for the data if desired
    '''
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
    
    def read_info(self, configFile):
        '''read camera information from a configuration file.
        
        :param str configFile: the name of the config file
        '''
        config_defaults = self.__dict__
        config = FlexConfigParser.FlexConfigParser(config_defaults).read(configFile,defaultSectionName='camera')
        d = dict(config['camera'])
        for key,val in d.iteritems():
            try:
                d[key] = eval(val)
            except:
                pass                
        
        self.__dict__.update(d)
    
    def pprint(self):
        '''pretty-print the object's contents
        '''
        pprint(self)
    
def readStarList():
    """Read a star list from a file and return the list.
    
    The star list is a text file containing a list of
    [locx, locy, magnitude] triplets
    locx and locy are in arcsec. magnitude is "brightness units"
    that multiply the PSF. If the brightness units are photons,
    the resulting image is linear in photocounts.
    If magnitude is assumed as stellar magnitude
    but then the image of the star field will be in log-stretched
    in photo-counts
    """
    global star_db
    filename = os.path.expanduser(os.path.join(starListDir,'starlist.txt'))
    with open(filename,'r') as f:
        starlist = json.loads(f.read())

    star_db = pd.DataFrame(starlist,columns=['X','Y','photocounts'])    
    return starlist
    
def simStars(starlist,psf,field_size=20):
    """Simulate a field of random stars with various positions and magnitudes
    
    :param list starlist:    List of stars, where entries are [x,y,b]: x,y = field position (arcsec). b = brightness (photons).
    :param float field_size: = size of the field (arcsecons)
    
    :return: the image of the star field
    :rtype: ndarray
    """
    dtheta = psf.dx # arcsec
    n = int(field_size/dtheta)
    field = np.zeros((n,n))
    for x,y,m in starlist:
        nx = np.round(x/dtheta).astype(int) + n/2
        ny = np.round(y/dtheta).astype(int) + n/2
        try:
            field = img.insert(psf*m,field,[nx,ny])
        except:
            print ('error ',x,y,nx,ny)
    
    field = InfoArray(field, name = 'star field',
                       units = '(brightness units)',
                       dx = dtheta,
                       dx_units = 'arcsec',
                       x0y0 = np.array([-1,-1])*(n/2)*dtheta,
                      )
    return field

def seeing_psf(n,dtheta,r0=0.1*u.m,D=3.0*u.m,lam=0.5*u.micron,verbose=False,check=False):
    """Create an average seeing PSF
    FRIED, D. L. (1966). Optical Resolution Through a Randomly Inhomogeneous
    Medium for Very Long and Very Short Exposures. Journal of the Optical
    Society of America, 56(10), 1372. http://doi.org/10.1364/JOSA.56.001372
    
    parameters:
        int n: number of pixels across
        Quantity dtheta: angular size of each pixel
        Quantity r0: seeing parameter
        Quantity D: telescope aperture diameter
        float alpha: 0 if long-exposure, 1 if short-exposure
        
    returns:
        Quantity psf = the image of the psf
    
    Formula:
    
    .. math::
        PSF(\\theta) = {\cal F}_{u \\rightarrow \\theta} \left\{e^{\,-\,{\\frac 1 2}\, {\cal D} (u)}\\right\}
        
    
    where
    
    .. math::
    
        {\cal D}(r) = 6.88 (r/r_0)^{5/3}
    """
        
    # assure adequate sampling
    dtheta = dtheta.to(u.arcsec)
    difflim = (lam/D).to(u.arcsec,equivalencies=u.dimensionless_angles())
    seeinglim = (lam/r0).to(u.arcsec,equivalencies=u.dimensionless_angles())
    fwhm = min([difflim,seeinglim])
    spread = max([difflim,seeinglim])
    dth = dtheta
    nn = n
    du = (lam/(n*dth)).to(u.m,equivalencies=u.dimensionless_angles())
    s = np.ceil(2*dth/fwhm) # subsample the psf fwhm: require 2*dth < fwhm
    dth /= s
    s = np.ceil(4*spread/(nn*dth)) # cover the psf fwhm: require nn*dtheta > 4*spread
    nn *= int(s)
    du = (lam/(nn*dth)).to(u.m,equivalencies=u.dimensionless_angles())
    s = np.ceil(2*du/r0) # subsample r0: require 2*du < r0
    nn *= int(s)
    du /= s
    s = np.ceil(2*D/(nn*du)) # cover the aperture: require nn*du > 2*D
    nn *= int(s)
    if verbose:
        print('\tnn=%d'%nn, 'dth=%r'%dth, 'du=%r'%du,sep='\n\t')
    
    tests = {}
    tests['psf subsample'] = 2*dth < fwhm
    tests['psf cover'] = nn*dth > 4*spread
    tests['r0 subsample'] = 2*du < r0
    tests['aperture cover'] = nn*du > 2*D
    limits = {}
    n_max = 1024
    limits['nn < %d'%n_max] = nn <= n_max
    checks = {'nn':nn,'dth':dth,'du':du,'tests':tests,'limits':limits}
    if check:
        return checks
    
    assert all(tests.values())
    assert all(limits.values())
    
    # calculate structure function, MTF, and PSF
    x = np.arange(-nn/2,nn/2)*du
    x,y = np.meshgrid(x,x)
    r = np.sqrt(x**2+y**2)
    sf = 6.88*(r/r0)**(5./3.)
    tau = np.exp(-0.5*sf)
    psfo = np.real(np.fft.fftshift(np.fft.fft2(np.fft.fftshift(tau))))
    
    # subsample the PSF
    s = int(dtheta/dth)
    ns = n*s
    s2 = int(np.round(float(s)/2.)) # shift center to center of resampled pixel
    psfc = img.crop(psfo,(nn//2-s2,nn//2-s2),(ns,ns))
    psf = img.rebin(psfc,(n,n))
    
    psf /= psf.sum() # normalize
    x0y0 = np.array([1,1])*(-n//2 - [1,0][n%2]*0.5)*dtheta
    psf = Quantity(psf)
    psf.name = 'PSF'
    psf.dx = dtheta
    psf.x0y0 = x0y0
    # psf = InfoArray(psf,name='PSF',
    #                 units='',
    #                 dx = dtheta/units.arcsec,
    #                 dx_units = 'arcsec',
    #                 x0y0 = x0y0/units.arcsec,
    #                 )
    psf.fullsamp = Quantity(psfo)
    psf.fullsamp.name = 'Fully Sampled PSF'
    psf.fullsamp.dx = dth
    psf.fullsamp.checks = checks
    # psf.fullsamp = InfoArray(psfo,name='Fully Sampled PSF',
    #                          units='',
    #                          dx = dth/units.arcsec,
    #                          dx_units='arcsec',
    #                          checks = checks,
    #                          )
    
    # helper functions
    def lineout(self,normalization='integral'):
        import matplotlib.pyplot as plt
        plt.ion()
        nn = self.shape[0]
        th = np.arange(-nn//2,nn//2)*self.dx
        v = self[nn//2,:].copy()
        if normalization == 'peak':
            v /= self.max()
        plt.plot(th,v)
        plt.grid('on')
        plt.xlabel(r'$\theta$, %s'%self.dx.unit)
        plt.ylabel('amplitude, normalized to PSF %s'%normalization)
        plt.title(self.name)        
    
    psf.lineout = types.MethodType(lineout,psf)
    psf.fullsamp.lineout = types.MethodType(lineout,psf.fullsamp)

    return psf

def kolmog(n,dtheta,r0):
    """Creates a random phase screen realization with the Kolmogorov spectrum
    
    :param int n: = the number of pixels across the phase screen
    :param float dtheta: = the size of the pixel in the phase screen (radians)
    :param float r0: = seeing parameter (meters)
    :return: (ph,ap,du,s)
    :rtype: tuple
    
    where
    
    * ph = the random phase screen
    * ap = the aperture
    * du = the size of a pixel in the aperture plane
    * s = approximate number of diffraction-limits per pixel in the image plane
    """
    dtheta_DL = 0.5*(lam/D)
    if dtheta_DL < dtheta:
        s = np.round(dtheta/dtheta_DL).astype(int)
    else:
        s = 1
    du = s*lam/(n*dtheta)
    noll_rms = np.sqrt(0.111*(D/r0)**(5./3.))
    ap = img.circle((n,n),r=D/2./du)
    kx = np.linspace(-4,4,n)
    kx,ky = np.meshgrid(kx,kx)
    k = np.sqrt(kx**2+ky**2)
    Phisr = k**(-11./6.)
    u = np.random.normal(size=(n,n))
    fu = np.fft.fft2(u)
    fph = Phisr*fu
    fph = np.fft.fftshift(fph)
    ph = np.real(np.fft.ifft2(fph))
    ph = img.detilt(img.depiston(ph,ap),ap)
    ph = noll_rms*ph/img.rms(ph,ap)
    return ph,ap,du,s

def image(ph,ap):
    """This takes a phase screen and an aperture (alternatively, amplitude)
    and returns a far-field PSF
    
    :param ndarray ph: phase screen
    :param ndarray ap: aperture
    :return: far-field PSF
    :rtype: ndarray
    """
    i = 1j
    wf = ap*np.exp(i*ph)
    psf = np.abs( np.fft.fftshift(np.fft.fft2(wf)))
    return psf

def corrCoef(a,b):
    """Computes the correlation coefficient between two images
    
    :param ndarray a:  like-sized images
    :param ndarray b:
    :return: correlation coefficient
    :rtype: float
    
    The formula is:
    
    $$ C_{ab} = {\overline {(a-\bar a)(b-\bar b)} \, / \, \sigma_a \sigma_b} $$
    """
    assert a.shape == b.shape
    n = float(a.size)
    cc = ((a-a.mean())*(b-b.mean())).sum()/n / (a.std()*b.std())
    return cc

def findStars(field,psf,verbose=False,return_groups=False):
    """Find the brighest stars by cross-correlating to the PSF
    
    Inputs:
    
    :param ndarray field: = the star field, a 2-d image
    :param ndarray psf: = the point-spread, a 2-d image
    
    Outputs:
    
    locs = list of 3-element entries, [x,y, correlation coef] where x and y are pixel locations of the star

    groups (optional) = groups of pixels identified by the blob finder
    """
    n,m = field.shape
    n1,m1 = psf.shape
    # change to pixels coordinates
    field.dx = 1.0
    field.dx_units = 'pixels'
    field.x0y0 = [-.5,-.5]
    #
    thresh = field.max()*0.5
    z = field >= thresh
    # find groups of connected pixels
    u,cnt = scipy.ndimage.label(z)
    groups = []
    for k in range(1,cnt+1):
        xs,ys = np.where(u==k)
        groups.append((k,zip(xs,ys)))

    locs = []
    for index,group in groups:
        c_max = 0.
        loc = None
        for xy in group:
            x,y = (xy[0],xy[1])
            region = img.crop(field,(x,y),psf.shape)
            c = corrCoef(region,psf)
            if c > c_max:
                loc = x,y
                c_max = c
        locs.append((index,loc,c_max))
        if verbose: print('group %d - %.2f'%(index,c_max))
    
    if return_groups:
        return locs,groups
    else:
        return locs

def removeStars(field,psf,locs):
    """Remove stars on a field assuming a PSF and given a list of star locations.
    This uses a simple method: for each star on the list estimate a scale factor m,
    then subtract m x PSF from the field
    
    :param ndarray field: = the field of stars, 2-d image
    :param ndarray psf: = PSF, a 2-d image
    :param list locs: = list of stars, each entry with 3 elements: x,y,c. x,y are star locations, in pixels. c is unused
    
    :return: w = field residual, a 2-d image
    :rtype: ndarray
    
    proc_starlist = processed list of stars, each with 4 elements: x,y,c,m. x,y are star locations, in pixels.
    c is unchanged.
    m is the estimated brightness, normalized to the PSF brighness
    """
    denom = (psf*psf).sum()
    w = field.copy()
    proc_starlist = []
    for index,loc,c in locs:
        if loc is not None:
            x,y = loc
            region = img.crop(field,(x,y),psf.shape)
            a = (psf*region).sum() / denom
            b = a*psf
            w = img.insert(-b,w,loc)
            proc_starlist.append((index,loc,c,a))

    return w,proc_starlist

#====================

def test(plot=False):
    global psf,field,locs,starlist
    
    starlist = readStarList()
    #starlist = [[0.0, 0.0, 7.0]] # Test
    n_psf = 14
    dtheta = 0.3 # arcsec
    psf = seeing_psf(n_psf,dtheta)
    field = simStars(starlist,psf)
    n,m = field.shape
    n1,m1 = psf.shape
    psf_size = psf.shape
    
    # look for the brighest pixels
    thresh = field.max()*0.5
    z = field >= thresh
    # find groups of connected pixels
    u,cnt = scipy.ndimage.label(z)
    groups = []
    for k in range(1,cnt+1):
        xs,ys = np.where(u==k)
        groups.append((k,zip(xs,ys)))
    
    # show the data with identified groups
    if plot:
        plt.figure()
        plt.imshow(field,cmap='Greys')
    for index,group in groups:
        xy = (group[0][1],group[0][0])
        s = '%d'%index
        if plot: plt.text(xy[0],xy[1],s,color='red',
                 verticalalignment='center',horizontalalignment='center')
    if plot:
        plt.xlim(0,n)
        plt.ylim(0,n)
    
    # for each group, find the one with the highest correlation
    locs = []
    for index,group in groups:
        c_max = 0.
        loc = None
        for xy in group:
            x,y = (xy[0],xy[1])
            region = img.crop(field,(x,y),psf_size)
            c = corrCoef(region,psf)
            if c > c_max:
                loc = x,y
                c_max = c
        locs.append((index,loc,c_max))
        print ('group %d - %.2f'%(index,c_max))
    
    # show the data with the identified center of correlation
    if plot:
        plt.figure()
        plt.imshow(field,cmap='Greys')
        for index,loc,c in locs:
            if loc is not None:
                plt.text(loc[1],loc[0],'x',color='red',
                         verticalalignment='center',horizontalalignment='center')
                s = '%2d'%int(np.round(100*c))
                plt.annotate(s,(loc[1]+5,loc[0]+5))
        
        plt.xlim(0,n)
        plt.ylim(0,n)
    
    # remove the discovered stars from the original image
    denom = (psf*psf).sum()
    w = field.copy()
    proc_starlist = []
    for index,loc,c in locs:
        if loc is not None:
            x,y = loc
            region = img.crop(field,(x,y),psf_size)
            a = (psf*region).sum() / denom
            b = a*psf
            w = img.insert(-b,w,loc)
            proc_starlist.append((index,loc,c,a))

    if plot:
        plt.figure()
        plt.imshow(w,cmap='Greys')
        plt.xlim(0,n)
        plt.ylim(0,n)

def test2(plot=False):
    '''test the classes
    '''
    n = 66
    dtheta = 0.3
    cam = SimCamera((n,n),dtheta,noise=0.05)
    n_psf = 14
    psf = seeing_psf(n_psf,dtheta)
    starlist = readStarList()
    cam.expose(starlist,psf)
    im = cam.readout()
    if plot:
        im.show()
    cam.pprint()
    im.pprint()
    camr = Camera()
    camr.read_info('ShARCS.cfg')
    camr.pprint()

