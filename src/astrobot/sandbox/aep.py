"""aep.py
Azimuth Equalarea Projection
stolen from:
http://www.samuelbosch.com/2014/02/azimuthal-equidistant-projection.html

To run from jupyter notebook:
>>> jupyter notebook
select the file: aep.ipynb
"""

from math import cos, sin, acos, radians
import numpy as np
import matplotlib
if matplotlib.get_backend() != 'nbAgg':
    matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from astropy import units as u
from astropy.table import Table
from astropy.coordinates import EarthLocation
from astropy.time import Time
import FlexConfigParser

cp = FlexConfigParser.FlexConfigParser()
cp.read('aep.cfg')
config = cp.as_dict('aep')

plt.ion()

Lick_coords = {'latitude':37.3414, 'longitude':-121.6429, 'altitude':1283.*u.meter}
Lick_location = EarthLocation.from_geodetic(
    Lick_coords['longitude'],
    Lick_coords['latitude'],
    Lick_coords['altitude'])

class AzimuthalEquidistantProjection(object):
    """ 
        http://mathworld.wolfram.com/AzimuthalEquidistantProjection.html
        http://www.radicalcartography.net/?projectionref
    """
    def __init__(self, location=None, time='now'):
        """Initialize an AzimuthalEquidistantProjection object. The graph is centered
        at DEC = the observatory latitude and RA = the current sidereal time at the observatory.
        
        Keyword Parameters
        ------------------
        location : an astropy.coordinates.EarthLocation object. Default is Lick Observatory
        time : an astropy.time.Time object. Default is the current time
        """
        
        if location is None or location == 'Lick':
            self.obs_name = 'Lick'
            location = Lick_location
        else:
            self.obs_name = 'lat: %s, lon: %s'%(location.latitude.to_string(u.deg),location.longitude.to_string(u.deg))
        self.location = location
        self.ut = Time(Time.now(),location=location)
        self.sidereal_time = self.ut.sidereal_time('mean')
        self.t1 = location.latitude.rad ## latitude center of projection
        self.l0 = self.sidereal_time.rad ## longitude center of projection
        self.cost1 = cos(self.t1)
        self.sint1 = sin(self.t1)
        self.res = 5. # degree resolution on axis lines
        self.grid_step_ra = 1. # hours
        self.grid_step_dec = 10. # degrees
        self.grid_step_alt = 10. # degrees
        self.grid_step_az = 15. # degrees
        self.xylim = np.pi/2.
        self.fig = None
        self.radec_lines = []
        self.show_radec_lines = False
        self.altaz_lines = []
        self.show_altaz_lines = False
        self.radec_texts = []
        self.show_radec_texts = False
        self.altaz_texts = []
        self.show_altaz_texts = False
        self.bounding_circle = []
        self.antipod_texts = []
    
    def __repr__(self):
        r = ('%s'%(self.__class__)).split('.')[-1]
        r = r+'\n'+'skys from %s Observatory'%self.obs_name
        return r
        
    def stars(self,atab,mvlim=4, s=(1,50),magstr='Vmag'):
        """Put stars on the chart
        
        Parameters
        ----------
        atab : an astropy Table object, having at least the columns '_RAJ2000', '_DEJ2000', and 'Vmag'
            The RA and Dec need to be in decimal degrees
        
        Keyword arguments
        -----------------
        mvlim : upper magnitude limit
        s : size range of the dots, in points
        magstr : alternative name of the column containing the magnitude
            For example, use this to select magnitudes for other bands (like 'Kmag')
        """
        plt.figure(self.fig.number)
        ax = plt.gca()
        ptmi,ptma = s
        xs = []
        ys = []
        mvs = []
        for ind in range(len(atab)):
            lonlat = (atab[ind]['_RAJ2000'],atab[ind]['_DEJ2000'])
            mv = atab[ind][magstr]
            if mv <= mvlim:
                xy = p.project(lonlat)
                if np.sqrt(xy[0]**2+xy[1]**2) < p.xylim:
                    xs.append(xy[0])
                    ys.append(xy[1])
                    mvs.append(mv)
        mvs = np.array(mvs)
        mi,ma = (mvs.min(), mvs.max())
        ptmi,ptma = s
        mvs = (mvs - mi)*(-(ptma-ptmi)/(ma-mi))+ptma
        ax.collections = [] # remove old stars
        plt.scatter(xs,ys,s=mvs,c='b',marker='o',linewidths=0.)
        self.stardots = ax.collections
    
    def show_stars(self,val=True):
        """Show or hide the stars.
        
        Parameters
        ----------
        val : True or False or 'on' or 'off'
        """
        assert val in [True, False, 'on','off']
        plt.figure(self.fig.number)
        ax = plt.gca()
        if val in [True,'on']:
            ax.collections = self.stardots
            plt.draw()
        elif val in [False,'off']:
            ax.collections = []
            plt.draw()
        
    def project(self, longlat_deg):
        """Project the longitude and latitude to a point x,y in chart coordinates
        
        Parameters
        ----------
        longlat_deg : a tuple of (longitude,latitude) in degrees
        
        Returns
        -------
        (x,y) : position in the chart data coordinates
        """
        lat = radians(longlat_deg[1])
        lon = radians(longlat_deg[0])
        costcosll0 = cos(lat) * cos(lon-self.l0)
        sint = sin(lat)
        
        c = acos ((self.sint1) * (sint) + (self.cost1) * costcosll0)
        k = c / (sin(c)+1.e-12)
        x = k * cos(lat) * sin(lon-self.l0)
        y = k * (self.cost1 * sint - self.sint1 * costcosll0)
        x = -x # mirror image: star charts have East on the left
        return (x, y)

    def show(self,opt='redo'):
        """Draw or redraw the chart.
        
        Keywords
        --------
        opt : 'redo' or 'move'
        """
        fig = self.fig
        if fig is None:
            fig = plt.figure()
            self.fig = fig
        fig = plt.figure(fig.number)
        ax = plt.gca()
        if fig != self.fig:
            # figure was closed, so reset the custom
            # axis parameters
            if opt == 'redo':
                lim = self.xylim
                ax.set_aspect('equal')
                ax.set_xlim(-lim,lim)
                ax.set_ylim(-lim,lim)
                fig.patch.set_visible(False)
                #ax.axis('off')
            elif opt == 'move':
                manager = fig.canvas.manager
                manager.canvas.figure = self.fig
                self.fig.set_canvas(manager.canvas)
            self.fig = fig
        ax.axis('off')
        ax.lines = []
        if len(self.bounding_circle) == 0:
            self._create_view()
        ax.lines += self.bounding_circle
        if self.show_radec_lines:
            ax.lines += self.radec_lines
        if self.show_altaz_lines:
            ax.lines += self.altaz_lines
        ax.texts = []
        ax.texts += self.antipod_texts
        if self.show_radec_texts:
            ax.texts += self.radec_texts
        if self.show_altaz_texts:
            ax.texts += self.altaz_texts
        plt.draw()
    
    def radec_grid(self,val=True):
        """Turn on or off display of the ra-dec grid
        
        Keyword
        -------
        val : True or False
        """
        assert val in [True,False,'on','off']
        if val in [True,'on']:
            if len(self.radec_lines) == 0:
                self._create_radec_grid()
            self.show_radec_lines = True
            self.show_radec_texts = True
        else:
            self.show_radec_lines = False
            self.show_radec_texts = False
        self.show()
        
    def altaz_grid(self,val=True):
        """Turn on or off display of the ra-dec grid
        
        Keyword
        -------
        val : True or False        
        """
        assert val in [True,False,'on','off']
        if val in [True,'on']:
            if len(self.altaz_lines) == 0:
                self._create_altaz_grid()
            self.show_altaz_lines = True
        else:
            self.show_altaz_lines = False
        self.show()
    
    def radec_labels(self,val=True):
        """Turn on or off display of the ra-dec labels
        
        Keyword
        -------
        val : True or False        
        """
        assert val in [True,False,'on','off']
        if val in [True,'on']:
            if len(self.radec_texts) == 0:
                self._create_radec_labels()
            self.show_radec_texts = True
        else:
            self.show_radec_texts = False
        self.show()
        
    def altaz_labels(self,val=True):
        """Turn on or off display of the alt-az labels
        
        Keyword
        -------
        val : True or False        
        """
        assert val in [True,False,'on','off']
        if val in [True,'on']:
            if len(self.altaz_texts) == 0:
                self._create_altaz_labels()
            self.show_altaz_texts = True
        else:
            self.show_altaz_texts = False
        self.show()

    def _create_view(self):
        """create the bounding circle and antipods
        """
        lim = self.xylim
        plt.clf()
        ax = plt.gca()
        ax.set_aspect('equal')
        ax.set_xlim(-lim,lim)
        ax.set_ylim(-lim,lim)
        # hide background and axes
        self.fig.patch.set_visible(False)
        ax.axis('off')
        # bounding circle
        th = np.linspace(0,2*np.pi,100) # angle samples on a circle
        x,y = (lim*np.cos(th),lim*np.sin(th))
        self.bounding_circle = plt.plot(x,y,'b')
        # antipods
        buf = 1.02
        tn=plt.text(0,lim*buf,'N',horizontalalignment='center',verticalalignment='bottom')
        te=plt.text(-lim*buf,0,'E',horizontalalignment='right',verticalalignment='center')
        ts=plt.text(0,-lim*buf,'S',horizontalalignment='center',verticalalignment='top')
        tw=plt.text(lim*buf,0,'W',horizontalalignment='left',verticalalignment='center')
        self.antipod_texts = [tn,te,ts,tw]

    def _create_radec_grid(self):
        """create a ra-dec grid on the sky hemisphere above the center
        """
        if self.fig is None:
            self.fig = plt.figure()
            fig = self.fig
        else:
            fig = plt.figure(self.fig.number)
        print 'creating RA Dec grid'
        p = self
        res = self.res
        lim = self.xylim # limits of the x and y projection
        #
        step_dec = self.grid_step_dec # degrees between lines
        step_ra = self.grid_step_ra # hours
        th = np.linspace(0,2*np.pi,100) # angle samples on a circle
        lon_center_deg = self.sidereal_time.deg
        # draw Dec lines
        dec_lines = []
        for dec in np.arange(-80,81,step_dec):
            u0 = np.arange(lon_center_deg,lon_center_deg+180.1,res)
            u1 = np.arange(lon_center_deg,lon_center_deg-180.1,-res)
            if np.abs(dec) > 70:
                u0 = u0[0::2]
                u1 = u1[0::2]
            u2 = [u0,u1]
            for u in u2:
                x0,y0 = None,None
                for ra in np.array(u):
                    x,y = p.project((ra,dec))
                    if x**2 + y**2 < lim**2:
                        if x0 is not None:
                            dec_lines += plt.plot([x0,x],[y0,y],'k:')
                        x0,y0 = x, y
        
        # draw RA lines
        ra_lines = []
        for ra in np.arange(0,360,15.*step_ra):
            x0,y0 = None,None
            for dec in np.arange(-80,81,res):
                x,y = p.project((ra,dec))
                if x**2 + y**2 < lim**2:
                    if x0 is not None:
                        ra_lines += plt.plot([x0,x],[y0,y],'k:')
                    x0,y0 = x, y
        
        self.radec_lines = dec_lines + ra_lines
        
    def _create_radec_labels(self):
        """create the text labels that go on RA and Dec axes
        """
        print 'creating RA Dec labels'
        lon_center_deg = self.sidereal_time.deg
        lat_center_deg = self.location.latitude.deg
        lim = self.xylim
        step_dec = self.grid_step_dec # degrees between lines
        step_ra = self.grid_step_ra # hours

        # label the RA lines
        ra_texts = []
        dec = 0.
        ras = np.arange(0,360.,15.*step_ra)
        hrs = ras/15.
        hr_labels = [r'$%d\,{\rm h}$'%int(ha) for ha in hrs]
        vis2 = (lim)**2
        for ra,hr_label in zip(ras,hr_labels):
            x,y = p.project((ra,dec))
            if x**2 + y**2 < vis2:
                ra_texts.append(plt.text(x,y,hr_label,
                         horizontalalignment='center',
                         verticalalignment='center',
                         fontsize=10,
                         bbox={'facecolor':'white','alpha':.5,'pad':0,'linewidth':0}))
        
        # label the Dec lines
        dec_texts = []
        ra = np.round(lon_center_deg/15.)*15.
        decs = np.arange(-80,81,step_dec)
        dec_labels = [r'$%+d^\circ$'%int(dec) for dec in decs]
        for dec,dec_label in zip(decs,dec_labels):
            x,y = p.project((ra,dec))
            if x**2 + y**2 < vis2 and dec != 0.:
                dec_texts.append(plt.text(x,y,dec_label,
                         horizontalalignment='center',
                         verticalalignment='center',
                         fontsize=10,
                         bbox={'facecolor':'white','alpha':.5,'pad':0,'linewidth':0}))
        
        self.radec_texts = ra_texts + dec_texts
    
    def _create_altaz_grid(self):
        """create an altitude-aximuth grid
        """
        print 'creating ALT-AZ grid'
        if self.fig is None:
            self.fig = plt.figure()
            fig = self.fig
        else:
            fig = plt.figure(self.fig.number)
        ax = plt.gca()
        # draw alt-az grid
        lim = self.xylim
        step_alt = self.grid_step_alt
        step_az = self.grid_step_az
        th = np.linspace(0,2*np.pi,100) # angle samples on a circle
        self.altaz_lines = []
        for alt in np.arange(10,91,step_alt):
            r = alt*(lim/90.)
            x,y = (r*np.cos(th),r*np.sin(th))
            self.altaz_lines += plt.plot(x,y,'b:')
        for az in np.arange(0,361,step_az):
            r1,r2 = lim*(10./90.),lim
            x1,y1 = r1*np.cos(radians(az)),r1*np.sin(radians(az))
            x2,y2 = r2*np.cos(radians(az)),r2*np.sin(radians(az))
            self.altaz_lines += plt.plot([x1,x2],[y1,y2],'b:')

    def _create_altaz_labels(self):
        """create labels for the ALT-AX grid
        """
        print 'creating ALT-AZ labels'
        print '... not implemented yet'

import unittest
class Test_AzimuthalEquidistantProjection(unittest.TestCase):
    def test_project(self):
        p = AzimuthalEquidistantProjection((0.0,0.0))
        rx,ry = p.project((1.0,1.0))
        self.assertAlmostEqual(0.01745152022, rx,)
        self.assertAlmostEqual(0.01745417858, ry)

        p = AzimuthalEquidistantProjection((1.0,2.0))
        rx,ry = p.project((3.0,4.0))
        self.assertAlmostEqual(0.03482860733, rx)
        #self.assertAlmostEqual(0.03494898734, ry)

        p = AzimuthalEquidistantProjection((-10.0001, 80.0001))
        rx,ry = p.project((7.935, 63.302))
        self.assertAlmostEqual(0.1405127567, rx)
        #self.assertAlmostEqual(-0.263406547, ry)

def prep():
    global p, star_table
    lon = Lick_coords['longitude']
    lat = Lick_coords['latitude']
    p = AzimuthalEquidistantProjection(location='Lick')
    star_table = Table.read(config['starlist'])
    return p
    

if __name__ == '__main__':
    unittest.main()
