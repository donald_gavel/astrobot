# astroplan
#   Plan an observation set

from datetime import datetime, timedelta
from astropy import time
from astropy.time import Time
import time as _time
import pytz
from astropy import coordinates
from astropy.coordinates import EarthLocation, SkyCoord, AltAz
import astropy.units as u
from astroquery.simbad import Simbad
import sys
import numpy as np
import tqdm

from labellines import labelLines,labelLine
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

from astrobot.oprint import pprint
from astrobot.dotdict import DotDict

plt.ion()

# register matplotlib's time/date converters with pandas
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

lo = EarthLocation.of_site('Lick Observatory')
pacific = pytz.timezone('US/Pacific')
utc = pytz.timezone('UTC')
lon = lo.to_geodetic().lon
lat = lo.to_geodetic().lat

class Basic():
    def pprint(self):
        pprint(self)
        
    @property
    def pp(self):
        self.pprint()
    
class Place(Basic):
    def __init__(self,**kwargs):
        self.__dict__.update(kwargs)
    
    def now(self):
        '''Print a summary of the current astronomical
        situation - current time utc, current sidereal time,
        and local time
        '''
        print('-'*len(self.name))
        print(self.name)
        print('-'*len(self.name))
        t = Time.now() # astropy time is utc
        print('Time now: %s UTC'%t)
        st = t.sidereal_time('apparent',longitude=self.lon)
        print('Local sidereal time: %s'%st)
        lt = utc.localize(t.datetime).astimezone(self.timezone) # astimezone converts to timezone's time
        fmt = '%a, %d %B %Y %H:%M:%S'
        print('local time: %s %s'%(lt.strftime(fmt),self.timezone))
    
Lick = Place(name = 'Lick Observatory',
             loc = lo,
             timezone = pacific,
             lat = lat,
             lon = lon,
             )

# observing run
class Run(Basic):
    def __init__(self,place,times,objects,**kwargs):
        '''set up an observing run
        
        :param Place place: the observatory
        :param tuple times: local start and end times: two strings in iso format
        '''
        self.__dict__.update(kwargs)
        # place
        self.place = place
        # observing times
        t0,tf = times
        t0 = datetime.fromisoformat(t0)
        t0 = place.timezone.localize(t0)
        tf = datetime.fromisoformat(tf)
        tf = place.timezone.localize(tf)
        self.t0 = t0
        self.tf = tf
        self.midnight = t0 + timedelta(hours = (24-t0.hour))
        # objects
        self.objects = objects
    
    def lookup_objects(self):
        data = []
        for id in tqdm.tqdm(self.objects):
            tbl = Simbad.query_object(id)
            ra = tbl[0]['RA']
            dec = tbl[0]['DEC']
            data.append([id,ra,dec])

        db = pd.DataFrame(data,columns = ['ID','RA','Dec'])
        
        ra = list(db.RA)
        dec = list(db.Dec)
        cl = SkyCoord(list(zip(ra,dec)), unit=(u.hourangle, u.deg))
        db['SkyCoord'] = cl
        self.object_db = db
    
    @property
    def info(self):
        '''print a summary of this run'''
        print('Place = ',self.place.name)
        print('start time: ',self.t0)
        print('finish time: ',self.tf)
        print('      (times are %s)'%self.place.timezone)
        dt = (Time(run.tf)-Time(run.t0)).to(u.hr)
        print('length of run: %s'%dt)
    
    def sunchart(self):
        sunchart(self.t0,self.tf)
    
    def airmass_plot(self,index=None):
        '''make an airmass plot of all the objects, or,
        optionally, a subset of them.
        
        :param index list: a list of the objects to be plotted,
            containing integer indices into the object database
        '''
        if not hasattr(self,'object_db'):
            self.lookup_objects()
            
        db = self.object_db
        if index is None:
            index = list(db.index)
        if not isinstance(index,list):
            index = [index]
            
        db = db.loc[index]
        names = list(db.ID)
        c = list(db.SkyCoord)
        t0 = self.t0
        tf = self.tf
        airmass_plot(c,names,t0=t0,tf=tf)
        
run = Run(place=Lick,
          times=('2020-10-26 19:00:00',
                 '2020-10-27 06:00:00'),
          objects = ['M92',
                     'M39',
                     'NGC 6871',
                     'M15',
                     'M33',
                     'NGC 884',
                     'M45',
                     ],
         )

def now(t='now',p=Lick):
    '''Print a summary of the current astronomical
    situation - current time utc, current sidereal time,
    and local time
    
    :param Time t: the time
    :param Place p: the DotDict describing the observing location
    '''
    if t == 'now':
        t = Time.now()
    print('Time now: %s UTC'%t)
    st = t.sidereal_time('apparent',longitude=lon)
    print('Local sidereal time: %s'%str(st))
    th = utc.localize(t.datetime).astimezone(pacific)
    print('local time: %s PDT'%th.strftime('%a, %d %B %Y %H:%M:%S'))

def sidereal(t='now'):
    '''Return the sidereal time at Lick Observatory
    
    :param Time t: the time (utc)
    '''
    if t == 'now':
        t = Time.now()
    return t.sidereal_time('apparent',longitude=lon)

def altaz(c,t='now'):
    '''Return the altitude, azimuth of the coordinate c
    as seen from Lick Observatory at time t
    
    :param coordinates c: coordinate
    :param Time t: the time (utc)
    '''
    if t=='now':
        t = Time.now()
    if c=='sun':
        c = coordinates.get_sun(t)
    lf = AltAz(location=lo,obstime=t)
    return c.transform_to(lf)

def local_time(arg='now'):
    '''Return the pacific-localized datetime given
    the string argument
    
    :param str arg: acceptable time string
    '''
    if arg == 'now':
        arg = Time.now()
    t = Time(arg,location=lo)
    return pacific.localize(t.datetime)

# t_obs_start = local_time('2018-09-24 18:00')
# t_obs_end   = local_time('2018-09-25 08:00')
# midnight = t_obs_start + timedelta(hours = (24-t_obs_start.hour))

targets = [
    ['00h42m30s','+41d12m00s'],
    ['21h14m','+5d0m']
]

cl = SkyCoord(targets)

def sunchart(t0,tf):
    '''plot the sun angle throughout the
    observing period
    '''
    midnight = t0 + timedelta(hours = (24-t0.hour))
    t = Time(t0)
    tf = Time(tf)
    alts = []
    ts = []
    while t <= tf + 0.1*u.hour:
        #print pacific.localize(t.datetime).astimezone(pacific)
        twi = altaz('sun',t).alt
        alts.append(twi.value)
        ts.append(utc.localize(t.datetime).astimezone(pacific))
        #print 'sun: %r'%twi
        t = t + 1.*u.hour
    
    fig = plt.figure()
    ax = plt.gca()
    ax.xaxis_date()
    formatter = mdates.DateFormatter('%d %H:%M',tz=pacific)
    ax.xaxis.set_major_formatter(formatter)
    ax.xaxis.set_major_locator(mdates.HourLocator(interval=2))
    ax.xaxis.set_minor_locator(mdates.HourLocator(interval=1))
    plt.plot(ts,alts)
    plt.gcf().autofmt_xdate()
    plt.grid(True,which='both')
    plt.xlabel('Local Time',fontsize=12)
    plt.ylabel('Sun Altitude, degrees',fontsize=12)
    #plt.plot(ts,0*np.ones((len(ts),)))
    ax.axhline(0.,color='gray')
    ax.axvline(midnight,color='white',linestyle='-',linewidth=1)
    ax.axvline(midnight,color='gray',linestyle='--',linewidth=1)
    plt.title('Sun Altitude %s'%t0.strftime('%b %d, %Y'))

def airmass_plot(c,names=None,clip=3.,t0=None,tf=None):
    '''plot the air mass of a coordinate
    throughout the observing period
    
    :param coordinates c: object coordinate
    :param list names: list of names (optional)
    :param float clip: clip at this number of airmasses
    '''
    try:
        n = len(c)
    except:
        n = 1
        c = [c]
    
    midnight = t0 + timedelta(hours=(24-t0.hour))
    fig = plt.figure()
    ax = plt.gca()
    ax.xaxis_date()
    formatter = mdates.DateFormatter('%d %H:%M',tz=pacific)
    ax.xaxis.set_major_formatter(formatter)
    ax.xaxis.set_major_locator(mdates.HourLocator(interval=2))
    ax.xaxis.set_minor_locator(mdates.HourLocator(interval=1))

    for index,cc in enumerate(c):
    
        t = Time(t0)
        ams = []
        ts = []
        while t <= Time(tf)+0.1*u.hour:
            #print pacific.localize(t.datetime).astimezone(pacific)
            z = 90.*u.deg - altaz(cc,t).alt
            if z < 90.*u.deg:
                am = (1./np.cos(z)).value
                if am < clip:
                    ams.append(am)
                    ts.append(utc.localize(t.datetime).astimezone(pacific))
            else:
                pass
                #ams.append(clip)
            #ts.append(utc.localize(t.datetime).astimezone(pacific))
            #print 'sun: %r'%twi
            t = t + 1.*u.hour
        
        if names:
            label = names[index]
        else:
            label = '%s %s'%(cc.ra.to_string(u.hour), cc.dec.to_string())
        plt.plot(ts,ams,label=label)

    labelLines(plt.gca().get_lines(),align=True,fontsize=10)
    plt.gcf().autofmt_xdate()
    plt.grid(True,which='both')
    plt.xlabel('Local Time',fontsize=12)
    plt.ylabel('Air Mass',fontsize=12)
    ax.set_ylim(1.,clip)
    ax.set_xlim(t0,tf)
    ax.axvline(midnight,color='white',linestyle='-',linewidth=1)
    ax.axvline(midnight,color='gray',linestyle='--',linewidth=1)
    plt.title('Airmass %s'%t0.strftime('%b %d, %Y'))

import pandas as pd
import sqlite3
from termcolor import colored
try:
    path = os.path.dirname(sys.modules[__name__].__file__)
    dataHome = os.path.abspath(os.path.join(path,'../../data'))
except:
    dataHome = os.path.expanduser('~/astrobot/data')
default_planner_file = os.path.join(dataHome,'example_plan.sql3')
if not os.path.isfile(default_planner_file):
    print(colored('WARNING default AstroPlanner file %s was not found'%default_planner_file,'red'))

def from_AstroPlanner(filename=default_planner_file):
    conn = sqlite3.connect(filename)
    df = pd.read_sql_query('select * from objects',conn)
    conn.close()
    return df

def to_AstroPlanner(df,filename):
    conn = sqlite3.connect(filename)
    cur = conn.cursor()
    cur.execute('drop table objects')
    df.to_sql('objects',conn)
    conn.close()

def tables(filename):
    conn = sqlite3.connect(filename)
    df = pd.read_sql_query("select name from sqlite_master where type = 'table'",conn)
    conn.close()
    return df

def airmass_plot_db(df,t0=None,tf=None):
    names = list(df['ID'])
    c = []
    for index,row in df.iterrows():
        cc = SkyCoord(row.RA,row.Dec,unit=u.deg)
        c.append(cc)
    airmass_plot(c,names,t0=t0,tf=tf)

def test():
    sunchart()
    df = from_AstroPlanner()
    airmass_plot_db(df.loc[0:4])

def test2():
    '''observations for the WFS upgrade commissioning run
    '''
    t_obs_start = local_time('2020-10-26 18:00')
    t_obs_end   = local_time('2020-10-27 08:00')
    midnight = t_obs_start + timedelta(hours = (24-t_obs_start.hour))

    ids = ['M92','M39']
    id = ids[0]
    tbl = Simbad.query_object(id)
    ra = tbl[0]['RA']
    dec = tbl[0]['DEC']
    data = [[id,ra,dec]]
    db = pd.DataFrame(data,columns = ['ID','RA','Dec'])
    
    #sunchart(t_obs_start,t_obs_end)
    airmass_plot_db(db,t0=t_obs_start,tf=t_obs_end)
    
    