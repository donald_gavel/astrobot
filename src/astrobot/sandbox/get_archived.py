'''
scratch file for testing ideas
'''
import requests
from bs4 import BeautifulSoup
import os
import sys
import pandas as pd

# wget --user=d.gavel --password=dgavel --auth-no-challenge https://mthamilton.ucolick.org/data/2014-08/11/AO/Don.Gavel
directory_url = 'https://mthamilton.ucolick.org/data/2014-08/11/AO/Don.Gavel'
filename = 's0002.fits'
file_url = os.path.join(directory_url,filename)
auth = ('d.gavel','dgavel')

def excepthook(type,value,traceback):
    print(value)

sys.excepthook = excepthook

r = requests.get(directory_url,auth=auth)
if r.status_code != 200:
    print directory_url
    raise Exception, 'response code = %s'%r.status_code

soup = BeautifulSoup(r.text,'lxml')

# file = os.path.expanduser('~/archive-2014-08-11-AO-Gavel.html')
# with open(file) as fp:
#     soup = BeautifulSoup(fp,'lxml')

rows = soup.find_all('table')[1].find_all('tr')
dfr = []
for row in rows[1:]:
    cols = row.find_all('td')
    name = cols[1].text
    objtime = cols[2].text.strip()
    time = objtime.split()[-1]
    object = objtime[:-len(time)-1]
    dfr.append((name,time,object))

df = pd.DataFrame(dfr,columns=['name','time','object'])

r = requests.get(file_url,auth=auth,stream=True)

if r.status_code != 200:
    print file_url
    raise Exception, 'response code = %s'%r.status_code

with open(filename,'wb') as f:
    f.write(r.content)
