import matplotlib
matplotlib.use('TkAgg')
import starlink.Grf as Grf
import starlink.Ast as Ast
import starlink.Atl as Atl
import matplotlib.pyplot as plt
import pyfits

plt.ion
fileName = 'gc_2mass_k.fits'
try:
   hdu_list = pyfits.open(fileName)
except:
   stars = '*'*80
   print stars
   print '### ERROR ###'
   print "can't open file %s"%fileName
   print 'get this file from https://astropy.stsci.edu/data/galactic_center/gc_2mass_k.fits'
   print stars
   raise Exception
fitschan = Ast.FitsChan(Atl.PyFITSAdapter(hdu_list[0]))
wcsinfo = fitschan.read()
#  Create a matplotlib figure, 12x9 inches in size.
dx=12.0
dy=9.0
fig = plt.figure( figsize=(dx,dy) )
fig_aspect_ratio = dy/dx

#  Get the bounds of the pixel grid from the FitsChan. If the NAXIS1/2
#  keywords are not in the FitsChan, use defaults of 500.
if "NAXIS1" in fitschan:
   naxis1 = fitschan["NAXIS1"]
else:
   naxis1 = 500

if "NAXIS2" in fitschan:
   naxis2 = fitschan["NAXIS2"]
else:
   naxis2 = 500

#  Set up the bounding box of the image in pixel coordinates, and get
#  the aspect ratio of th eimage.
bbox = (0.5, 0.5, naxis1 + 0.5, naxis2 + 0.5)
fits_aspect_ratio = ( bbox[3] - bbox[1] )/( bbox[2] - bbox[0] )

#  Set up the bounding box of the image as fractional offsets within the
#  figure. The hx and hy variables hold the horizontal and vertical half
#  widths of the image, as fractions of the width and height of the figure.
#  Shrink the image area by a factor of 0.7 to leave room for annotated axes.
if fig_aspect_ratio > fits_aspect_ratio :
   hx = 0.5
   hy = 0.5*fits_aspect_ratio/fig_aspect_ratio
else:
   hx = 0.5*fig_aspect_ratio/fits_aspect_ratio
   hy = 0.5

hx *= 0.7
hy *= 0.7
gbox = ( 0.5 - hx, 0.5 - hy, 0.5 + hx, 0.5 + hy )

#  Add an Axes structure to the figure and display the image within it,
#  scaled between data values zero and 100. Suppress the axes as we will
#  be using AST to create axes.
ax_image = fig.add_axes( [ gbox[0], gbox[1], gbox[2] - gbox[0],
                           gbox[3] - gbox[1] ], zorder=1 )
ax_image.xaxis.set_visible( False )
ax_image.yaxis.set_visible( False )
ax_image.imshow( hdu_list[0].data,vmin=400,vmax=1000, cmap=plt.cm.gist_heat, origin='lower', aspect='auto')

#  Add another Axes structure to the figure to hold the annotated axes
#  produced by AST. It is displayed on top of the previous Axes
#  structure. Make it transparent so that the image will show through.
ax_plot = fig.add_axes( [ 0, 0, 1, 1 ], zorder=2 )
ax_plot.xaxis.set_visible(False)
ax_plot.yaxis.set_visible(False)
ax_plot.patch.set_alpha(0.0)

#  Create a drawing object that knows how to draw primitives (lines,
#  marks and strings) into this second Axes structure.
grf = Grf.grf_matplotlib( ax_plot )

#  Create the AST Plot, using the above object to draw the primitives. The
#  Plot is based on the FrameSet that describes the WCS read from the FITS
#  headers, so the plot knows how to convert from WCS coords to pixel
#  coords, and then to matplotlib data coords.
plot = Ast.Plot( wcsinfo, gbox, bbox, grf )

#  Set some nice attributes for the plot.
plot.Colour_Border = "lime"
plot.Colour_Ticks = "lime"
plot.MajTickLen = 0.02
plot.Width_TextLab = 2
plot.Width_NumLab = 2
plot.Width_Title = 2

#  And finally, draw the annotated WCS axes.
plot.grid()

#  Make the matplotlib plotting area visible
plt.show()
