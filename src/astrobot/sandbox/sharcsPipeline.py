"""ShARCS data post-processor
with particular emphasis on the m93 image and
distortion mapping.

It is also has a simulator of the ShARCS array with bad pixels
and some random stars, having an AO core-halo PSF
"""
import os
import astropy.io.fits as pyfits
import astropy.units as u
import numpy as np
import scipy.optimize
import pandas as pd
import time

from astrobot import sharcs
from astrobot import bad_median
from astrobot import img
from astrobot import q_helper
from astrobot import fits

import matplotlib.pylab as plt

plt.ion()

a = sharcs.Sharcs()

n = a.n
dtheta = 0.033*u.arcsec
lam = 1.6*u.micron
du = (lam/(n*dtheta)).to_da(u.cm)
d = 3.0*u.m
r0 = 15.*u.cm
strehl = 0.7

#pd.set_option('display.max_columns',13)
#pd.set_option('display.width',0) #200
#pd.reset_option('all')

def init():
    """top level initialization. read in the databases and read in the pipeline files
    """
    global m92, hst_m92, bad_pix, flat, dark
    
    db_init()
    m92 = db_getFile('data')
    hst_m92 = db_getFile('hst')
    bad_pix = a.bad_pixel_map
    flat = None
    dark = None

def db_init():
    """initialize three databases
    db - the overall database of files for the pipeline
    darks_db - the database of files in the directory which has the darks
    flats_db - the database of files in the directory which has the flats
    """
    global db, darks_db, flats_db
    
    home = os.path.expanduser('~/Desktop/m92')
    #external_drive = '/Volumes/LittleWDdrive/ShaneAO_Data'
    external_drive = os.path.expanduser('~/data_lick/LittleWDdrive_backup_11-6-2015/ShaneAO_Data/sharcs')

    db = pd.DataFrame(columns =
                        [ 'directory',                          'filename',                       'purpose'                         ])
    
    db.loc['data'] = [ os.path.join(home,'data'),  'm92_s0275.fits',                    'ShARCS image of m92 star cluster']
    db.loc['hst']  = [ os.path.join(home,'data'),  'HST_10775_58_ACS_WFC_F814W.fits',   'HST image of m92 star cluster'   ]
    db.loc['bad']  = [ os.path.join(home,'data'),  'badpix_prelim_g9_140409_full.fits', 'bad pixel file for ShARCS'       ]
    db.loc['flat'] = [ 'None',                     'None',                              'flat field file'                 ]
    db.loc['dark'] = [ 'None',                     'None',                              'dark file'                       ]    
    db.loc['dset'] = [ os.path.join(home,'data'),  'hlsp_acsggct_hst_acs-wfc_ngc6341_r.rdviq.cal.adj.zpt.txt', 'reference HST data file for astrometry/photometry']
    
    # Darks
    darks_date = '2014-09-13'
    darks_db = pd.read_csv(os.path.join(external_drive,darks_date,'Sharcs','log.csv'))
    db['directory']['dark'] = os.path.join(external_drive,darks_date,'Sharcs')
    
    # Flats
    flats_date = '2014-08-12'
    flats_db = pd.read_csv(os.path.join(external_drive,flats_date,'Sharcs','log.csv'))    
    db['directory']['flat'] = os.path.join(external_drive,flats_date,'Sharcs')

def flat_db_init(band=None):
    """select the files that are flat files, based on the word 'flat' in the OBJECT column
    
    band can be a filter selector, one of 'J', 'H', 'Ks', or 'Open'
    """
    global flats_selected_db
    
    flats_selected_db = flats_db[['flat' in str(x).lower() for x in flats_db.OBJECT]]
    if band:
        flats_selected_db = flats_selected_db[flats_selected_db.FILT1NAM == band]

def get_flats(thresh = 0.01,satval = 35000.):
    """Read the flat raw data files from disk
    Evaluate them for saturation quality.
    If saturated pixel fraction within the aperture exceeds the threshhold, mark as saturated
    in the flats_selected_db database
    """
    flat_set = []
    d = db['directory']['flat']
    npix = float(np.sum(a.ap)) # number of pixels in the field aperture
    flats_selected_db['quality'] = 'good'
    for k in flats_selected_db.index:
        fn = os.path.join(d,flats_selected_db['FILE NAME'][k])
        f = fits.readfits(fn)
        flat_set.append(f)
        # evaluate the possibility of saturated exposure
        sat_frac = np.sum(f*a.ap > satval)/npix
        if (sat_frac > thresh):
            flats_selected_db.loc[k,'quality'] = 'saturated'
    return flat_set

def debad_flat(flat):
    """grab a section of the flat-field (the illuminated area) and
    clear it of bad pixels.
    
    Input: the original flat-field data
    Output: flat-field cleared of bad pixels
    """
    ap = a.ap
    bad = a.bad
    api = np.where(ap)
    b = 4 #boundary margin
    x1,x2 = api[1].min()-b, api[1].max()+b
    y1,y2 = api[0].min()-b, api[0].max()+b
    n,m = y2-y1, x2-x1
    flat_sub = flat[y1:y2,x1:x2]
    ap = ap[y1:y2,x1:x2]
    bad = bad[y1:y2,x1:x2]
    flat_sub = np.fliplr(np.rot90(flat_sub,k=1))
    ap = np.fliplr(np.rot90(ap,k=1))
    bad = np.fliplr(np.rot90(bad,k=1))
    
    flat_unbad = bad_median.bad_med(flat_sub,bad,ap)*ap
    fp = np.ones((5,5))
    flat_unbad_med = scipy.ndimage.filters.median_filter(flat_unbad,footprint=fp)
    u = flat_unbad_med[n/2-50:n/2+50,m/2-50:m/2+50].flatten()
    r = np.histogram(u,bins=200)
    xh = (r[1][:-1]+r[1][1:])/2.
    yh = r[0]
    popt,pcov=scipy.optimize.curve_fit(gau,xh,yh,p0=(np.mean(u),np.std(u),yh.max()))
    plt.plot(xh,gau(xh,popt[0],popt[1],popt[2]))
    ave = popt[0]
    sd = popt[1]
    t = (flat_unbad_med - flat_unbad)/sd
    nsd = (4,4)
    ba = (t>nsd[0]) | (t<-nsd[1]) | np.isnan(t)
    flat_unbad_2 = bad_median.bad_med(flat_unbad,ba,ap)

    return flat_unbad_2,ap,(y1,y2,x1,x2)

def process_flats(band='Ks',pair = (252,254), clean = False):
    """Process the flat-field data.
    Read in the twilight sky flats and determine the
    throughput part and the emission part (in the case of Ks band)
    
    Input:
        band is a wavelength band selector, one of 'J', 'H', 'Ks', or 'Open'
        pair is the selected pair of raw files, (brighter, dimmer) during twilight, given
            as the index on the selected_flats_db database. These files will
            be used to compute the emission and flat-field images
        clean is whether or not to wipe the databases clean and star from the beginning
    Results:
        ff is the unit-normalized flat-field multiplier
        es is the smoothed emission field
    """
    global flats_selected_db, flat_set, flat_debad_set, ap, box, ff, es, ff_explanation
    
    if clean:
        if ('flats_selected_db' in globals()): del flats_selected_db
        if ('flat_set' in globals()): del flat_set
        if ('flat_debad_set' in globals()): del flat_debad_set
    
    if ('flats_selected_db' not in globals()):
        print('<process_flats> initializing the flats_selected_db database')
        flat_db_init(band)
    if ('flat_set' not in globals()):
        print('<process_flats> reading in the flats raw data')
        flat_set = get_flats()
    
    if ('flat_debad_set' not in globals()):
        print('<process_flats> cleaning bad pixels from flats')
        flat_debad_set = []
        flats_selected_db['tag'] = ''
        flats_selected_db['procdata'] = None
        n = len(flat_set)
        for i,flat,k in zip(flats_selected_db.index,flat_set,list(range(n))):
            print('#',k,'/',n, end=' ')
            if (flats_selected_db.loc[i,'quality'] == 'good'):
                u = debad_flat(flat)
                flat_debad_set.append(u[0])
                flats_selected_db.loc[i,'procdata'] = {'contents':u}
                if (i == pair[0]):
                    flats_selected_db.loc[i,'tag'] = 'bright'
                if (i == pair[1]):
                    flats_selected_db.loc[i,'tag'] = 'dim'
                print('good')
            else:
                print('saturated')
        ap = u[1]
        box = u[2]
        print('<process_flats> done with bad pixel removal')

    ff_explanation = """
     Determine the emission component and figure a flat-field
    
     Let b = sky brightness, e = emission within instrument
     The two exposures differ because of differing sky brightness (degrees twilight)
     but emission is the same because the exposure time is the same in both images.
       v1 = b1 + e
       v2 = b2 + e
       b1 > b2, b2 = n*b1 where n < 1
      Determine n by taking v2/v1 in a region of the image where e = 0
      which is presumably the center of the field, which is clear of vignetting.
       e = (n*v1 - v2)/(n-1) = (n*b1 + n*e - b2 - e) / (n-1) = (n*e - e) / (n-1) = e
       f = (n*(v1-e) + (v2-e)) = n*b1 + b2 = 2*b2
      Blur both results by a spatial smoothing function g, which integrates to one.
       e is the absolute emission in counts / exposure
      Normalize f by the mean of the central region, then take the reciprocal.
      The result is the flat-field multiplier, which hovers around one near the
      center of the field but gets larger than one in the vignetted regions
    
      In processing images, subtract the emission, scaled by the exposure time,
      then multiply by the flat-field:
        p = (r = e*(t_exp/t_exp_flat))*ff
      where r = raw imaage, p = processed image
    """
    
    bright = flats_selected_db.query('tag == "bright"')['procdata'].iloc[0]['contents'][0]
    dim = flats_selected_db.query('tag == "dim"')['procdata'].iloc[0]['contents'][0]
    print('<process_flats> figuring emission component')
    g = img.gauss2((100,100),10) # smoothing kernel
    g = g / np.sum(g) # normalize to intergral 1
    n,m = bright.shape
    norm_factor = np.mean(dim[n/2-50:n/2+50,m/2-50:m/2+50].flatten()) / \
                  np.mean(bright[n/2-50:n/2+50,m/2-50:m/2+50].flatten())
    e = (norm_factor*bright - dim ) / ( norm_factor - 1)
    es = img.blur(e,g) # smoothed emission map, absolute in counts / minute (sky exposure was 60 sec)
    
    print('<process_flats> figuring the throughput/flat-field component')
    f = ((dim-e)+(bright-e))/2.
    f_norm = np.mean(f[n/2-50:n/2+50,m/2-50:m/2+50].flatten())
    f = img.blur(f/f_norm,g)*ap + (1-ap)
    ff = 1./f # flat-field multiplier map (mostly near 1)

def ref_init(m_limit=13):
    """read in the reference dataset of stars for m93 and select some of the brightest ones
    that will be useful for ShARCS distortion correction
    
    m_limit is the magnitude limit for selecting stars to go in to the starlist_selected dataset
    """
    global starlist_db, starlist_selected_db
    
    if ('db' not in globals()):
        db_init()
    # open the HST m92 star data set file
    fn = db.filename.dset
    d = db.directory.dset
    fn = os.path.join(d,fn)
    f = open(fn,'r')
    u = f.read()
    f.close()
    uu = u.split('\n')
    hdr = uu[0].strip().split()
    n = len(uu)
    u2 = np.array([x.strip().split() for x in uu[3:n-1]])
    u3 = [list(map(float,x)) for x in u2]
    starlist_db = pd.DataFrame(data=u3,columns = hdr)    
    starlist_selected_db = starlist_db[starlist_db['Vground'] < m_limit][['Vground','RA','Dec']]
    
def m92_simulation(instrument='HST'):
    """create a simulated image of the m92 cluster given the star list
    
    instrument can be 'HST' or 'ShARCS'
    """
    degrees = np.pi/180.
    if ('starlist_selected_db' not in globals()):
        print('<m92_simulation> initializing starlist ...')
        ref_init()
        print('<92_simulation> done')
    
    # read the WCS information in the HST image
    f = os.path.join(db.directory.hst,db.filename.hst)
    hdulist = pyfits.open(f)
    hdr = hdulist['SCI'].header
    hdulist.close()
    n,m = (int(hdr['NAXIS1']),int(hdr['NAXIS2']))
    crpix1, crpix2 = (float(hdr['CRPIX1']),float(hdr['CRPIX2']))
    crval1, crval2 = (float(hdr['CRVAL1']),float(hdr['CRVAL2']))
    cos_dec = np.cos(crval2*degrees)
    cd1_1, cd2_2 = (float(hdr['CD1_1']),float(hdr['CD2_2']))
    
    # populate the simulated image with stars
    print('<m92_simulation> putting in stars')
    field = np.zeros((m,n))
    for k in starlist_selected_db.index:
        x = starlist_selected_db.loc[k]['RA']
        y = starlist_selected_db.loc[k]['Dec']
        x = crpix1 + ((x - crval1)/cd1_1)*cos_dec
        y = crpix2 + (y - crval2)/cd2_2
        mv = starlist_selected_db.loc[k]['Vground']
        flux = 10**(-mv/2.5)
        if (x >= 0 and x < n) and (y >=0 and y < m):
            field[y,x] = flux
    
    # blur by the point-spread function
    print('m92_simulation> blurring by PSF')
    pixelscale = np.abs(cd1_1)*degrees
    if (instrument == 'HST'):
        lam = 814.e-9
        d = 2.4
        ds = 0.66
    if (instrument == 'ShARCS'):
        lam = 2.2e-6
        d = 3.0
        ds = 0.8
    n_psf = 256
    psf = img.airy(n_psf,n_psf,1.22*(lam/d)/pixelscale,s2=1.22*(lam/ds)/pixelscale)
    field = img.blur(field,psf)
    # add simulated sky noise and photon noise
    
    print('<m92_simulation> done')

    return field

def db_getFile(tag):
    """get a file from the processing set given the tag
    """
    e = db.loc[tag]
    f = os.path.join(e.directory,e.filename)
    if tag == 'hst':
        d = fits.readfits(f,isHST = True)
    else:
        d = fits.readfits(f)
    return d
    
def process_darks(dfilter = None, load=False, save=False):
    """read and process the dark files, using the darks database
    
    arguments:
        dfilter - a 2-tuple indicating a file selection filter (keyword,value)
        load - boolean indicating whether or not to load the partial results database
        save - boolean indicating whether or not to save the partial results database
    
    returns:
        pdarkset - processed dark images, stored in a list
        
    output:
        pdarkdb is an database of dark processing results, which is useful for summary
        analysis and for holding previous results when only a few darks are processed at a time.
    """
    global pdarkdb, pdarkdb_filename
    
    pdarkdb_filename = 'pdarkdb.csv'
    ap = a.ap
    n = ap.shape
    d = db.directory.dark
    if load:
        pdarkdb = pd.from_csv(pdarkdb_filename)
    if ('pdarkdb' not in globals()):
        pdarkdb = darks_db[darks_db['OBJECT']=='dark']
    pdarkdb = pdarkdb.sort('ITIME')
    if (dfilter is not None):
        pdarkdbf = pdarkdb[pdarkdb[dfilter[0]] == dfilter[1]]
    else:
        pdarkdbf = pdarkdb
    index = pdarkdbf.index
    nfiles = len(index)
    pdarkset = []
    for i in index:
        texp = pdarkdb.loc[i].ITIME
        f = pdarkdb.loc[i]['FILE NAME']
        f = os.path.join(d,f)
        dark = fits.readfits(f)
        s = dark.shape
        pdarkdb.loc[i,'NX'] = s[0]
        pdarkdb.loc[i,'NY'] = s[1]
        if (dark.shape != n):
            print(i, dark.shape)
            continue
        ms,dud = process_one_dark(dark)
        pdarkset.append(dud)
        namps = len(ms)
        for amp in range(namps):
            col_name = 'SD_Amp'+str(amp)
            pdarkdb.loc[i,col_name] = ms[amp]
        m = ms.mean()
        s = ms.std()
        pdarkdb.loc[i,'AVE_NoiseAmp'] = m
        pdarkdb.loc[i,'SD_NoiseAmp'] = s
        print('<sharcsStars.process_darks> '+str(i)+': texp = '+str(texp)+' mean = '+str(m)+' stddev = '+str(s))
    if save:
        pdarkdb.to_csv(pdarkdb_filename)
    return pdarkset

def process_one_dark(dark):
    n,n = a.bad.shape
    ampind = list(range(0,n,a.amps_delta))
    xy = np.where(a.ap)
    x = xy[1]
    y = xy[0]
    xmin = x.min()-a.amps_delta
    xmax = x.max()
    ymin = y.min()
    ymax = y.max()
    ap = np.zeros((n,n))
    ap[ymin:ymax, xmin:xmax] = 1
    dark_unbad = bad_median.bad_med(dark,a.bad,ap)*ap
    ampind2 = []
    for k in ampind:
        if (k >= xmin and k < xmax):
            ampind2.append(k)
    
    dark_unbad_debiased = np.zeros(dark_unbad.shape)
    stds = []
    for xx in ampind2:
        amp_strip = dark_unbad[ymin:ymax,xx:xx+a.amps_delta]
        #ap_strip = a.ap[ymin:ymax,xx:xx+a.amps_delta]
        #d = amp_strip[np.where(ap_strip)]
        bias = amp_strip.mean()
        std = amp_strip.std()
        stds.append(std)
        dark_unbad_debiased[ymin:ymax,xx:xx+a.amps_delta] = dark_unbad[ymin:ymax,xx:xx+a.amps_delta] - bias
    stds = np.array(stds)
    return stds, dark_unbad_debiased

def plot_dark_summary(ra):
    plt.figure()
    plt.plot(np.sqrt(ra[:,0]),ra[:,2],'x'); plt.xlabel('sqrt(texp)'); plt.ylabel('noise std dev'); plt.grid()
    plt.title('Darks summary')
    plt.figure()
    plt.plot(ra[:,0],ra[:,1],'x'); plt.xlabel('texp');plt.ylabel('mean');plt.grid()
    plt.title('Darks means')
    plt.figure()
    plt.plot(ra[:,1],ra[:,2],'x'); plt.xlabel('mean');plt.ylabel('std dev');plt.grid()
    plt.title('std dev vs means')
    
def star_sim():
    """simulate a cluster of stars, with AO psfs
    then corrupt with 'bad' pixels according to ShARCS'
    bad pixel map
    
    returns the tupple: (corrupted image, clean image)

    to clean the image:
    tgpa = bad_median.bad_med(gpa,a.bad,a.ap)*a.ap
    """
    p0 = atmos.psf0(n,du,d)
    p = atmos.psf(n,du,r0)
    p = strehl*p0 + (1-strehl)*p
    fp = np.fft.fft2(p)
    
    posx = np.random.uniform(low=0,high=n-1,size=100)
    posy = np.random.uniform(low=0,high=n-1,size=100)
    bri = np.random.uniform(low=0,high=1,size=100)
    
    g = np.zeros((n,n))
    for x,y,b in zip(posx,posy,bri):
        g[x,y] = b
    
    fg = np.fft.fft2(g)
    fgp = fg*fp
    gp = np.real(np.fft.ifft2(fgp))
    gp = (gp/gp.max())*35000. # reasonable DN range

    gpa = gp*(1-a.bad)*a.ap+65000.*a.bad
    return (gpa,gp)
    
def blur(a,n):
    """blur an image by an nxn kernel block
    n must be an odd integer
    """
    assert (n % 2)==1
    b = np.zeros(a.shape)
    b[0:n,0:n] = 1./float(n)**2
    b = img.shift(b,(-(n//2),-(n//2)))
    ab = np.real(np.fft.ifft2(np.fft.fft2(a)*np.fft.fft2(b)))
    return ab

def gau(x,m,s,a):
    """gaussian model
    m = mean
    s = std dev
    a = amplitude
    """
    return a*np.exp(-((x-m)/s)**2)

def debad(n1=293,m1=645,dn=1000,dm=1000,nsd = (1.0, 0.2),show_images=False):
    """grab a section of the original m92 image and
    clear it of bad pixels
    
    Inputs:
        n1,m1 - lower left corner of section
        dn,dm - size of section
        nsd - a tuple of bad pixel detection thresholds (positive, negative), in sigmas
    
    Returns:
        qf - clear image
        q - original image (section)
        sigma - standard deviation of the noise
    """
    print('<test1> first pass at bad pixel removal')
    m92db = bad_median.bad_med(m92,a.bad_pixel_map,a.ap)*a.ap
    n2 = n1+dn
    m2 = m1+dm
    q = m92db[n1:n2,m1:m2] #[659:675,1165:1191]
    q = np.fliplr(np.rot90(q,k=1)) # rotate to sky-right (north up, east left)
    if (show_images): dimg.show(q,origin='lower',title='q')
    print('<test1> determining noise std dev')
    u = q-blur(q,3)
    if (show_images): dimg.show(u,origin='lower',title='q-blur(q)')
    plt.figure('HIST')
    r = np.histogram(u[1:-1,1:-1].flatten(),bins=50)
    xh = (r[1][:-1]+r[1][1:])/2.
    yh = r[0]
    popt,pcov=scipy.optimize.curve_fit(gau,xh,yh)
    sigma = popt[1]
    print('<test1> std dev = '+'str(sigma)')
    plt.plot(xh,gau(xh,popt[0],popt[1],popt[2]))
    print('<test1> running second pass at bad pixel removal (median filter)')
    fp = np.zeros((3,3))
    fp[:,1] = 1
    fp[1,:] = 1
    qm = scipy.ndimage.filters.median_filter(q,footprint=fp)
    t = (q-qm)/np.sqrt(qm)
    ba = (t>nsd[0]) | (t<-nsd[1]) | np.isnan(t)
    print('<test1> running third pass at bad pixel removal')
    if (show_images): dimg.show(ba,origin='lower',title='bad pix')
    ap = np.zeros((dn,dm))
    ap[1:-1,1:-1]=1
    qf = bad_median.bad_med(q,ba,ap,w=1)
    ba = np.isnan(qf)
    qf[np.isnan(qf)] = 0.
    print('<test1> running fourth pass at bad pixel removal (cleaning NaNs)')
    qf = bad_median.bad_med(qf,ba,ap,w=1)
    qf[np.isnan(qf)] = 0.
    if (show_images): dimg.show(qf,origin='lower',title='q fixed')
    return qf,q,sigma

def sbs(qf):
    """display the HST image of the m92 region
    side-by-side with the processed ShARCS image
    """
    dimg.ds9(qf)
    time.sleep(5) # wait a bit for ds9 to read the file
    dimg.ds9(hst_m92)
    
def find_stars(qf):
    """find the stars in the image, label peaks and find centroids
    """
    qcl = np.clip(qf-900,0,1e6)
    lbl = scipy.ndimage.label(qcl)[0]
    n = lbl.max()
    com = scipy.ndimage.measurements.center_of_mass(qcl,lbl,list(range(1,n)))
    return com,qcl,lbl

def plot_stars(qf,com,scale='linear'):
    """plot the results of peak finding and centroiding
    """
    plt.figure()
    ax = plt.subplot(111)
    if (scale=='linear'): qd = qf
    if (scale=='log'):
        f = qf[qf>0].min()
        qf[qf<=0] = f
        qd = np.log10(qf)
    if (scale=='sqrt'): qd = np.sqrt(qf)
    plt.imshow(qd,origin='lower',cmap='bone')
    for peak in com:  #Draw some rectangles around the objects
        xy  = (peak[1]-5,peak[0]-5)
        width  = 11
        height = 11
        rect = matplotlib.patches.Rectangle(xy,width,height,fc='none',ec='red')
        ax.add_patch(rect,)

def test():
    global qf,q,sigma,com,qcl,lbl
    
    init()
    qf,q,sigma = debad()
    com,qcl,lbl = find_stars(qf)
    plot_stars(qf,com,scale='log')
    