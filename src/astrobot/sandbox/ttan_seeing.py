'''read and analyze TTdata files to evaluate seeing.
Determine FWHM and r00 (r0 at 550nm)

Usage (python):
---------------
    from astrobot.sandbox import ttan_seeing as ttan
    tt = ttan.Ttan('2021-08-25')
    tt.fit().plot()

Unix command line
-----------------
    python3 -i -m astrobot.sandbox.ttan_seeing -- [--help] [-d <date>] [--plot]

Options
-------
    -d, --date <date>
        The observation date. 'today' refers to the observation night,
        which in the wee hours of the morning is yesterday. Default is 'today'
        
    -h, --help
        Print this help and exit
        
    -p, --plot
        After gathering and analyzing the date, produce a plot. The
        program suspends until the plot is closed.

Install
-------
    git clone git@bitbucket.org:donald_gavel/astrobot.git
    pip install -e .

'''
import sys
if any([x == s for x in ['-h','--help'] for s in sys.argv]):
    import pydoc
    pydoc.pager(__doc__)
    sys.exit()

import os
import datetime
import numpy as np
import pandas as pd
from pandas import Timestamp
import re
import pydoc
from collections import deque
from tqdm import tqdm as progbar
import subprocess
from astropy.io import fits
import astropy.units as u
from astropy.modeling import models,fitting

import astrobot.q_helper
from astrobot import img
from astrobot import oprint
from astrobot.astrodate import p_date, utc_from_local
from astrobot.tcolor import tcolor,gprint,cstr

import matplotlib.pyplot as plt

plt.ion()

data_paths = {'home':'/Users/donaldgavel/ShaneAO/data', #/2021-08-25/AOsample'
              'Lick':'/net/real/local/data',
             }

# search for the data directory, depending on machine
for key,path in data_paths.items():
    if os.path.isdir(path):
        place = key
        break
else:
    raise Exception(cstr('fail',f'neither {dgDataDir} nor {realAtUcolickDataDir} is a valid path to data'))

def get_path(date):
    '''get the full path to the TT_Data given the date. The
    path depends on the host file system, either home pc or Lick.
    '''
    top = data_paths[place]
    if place == 'Lick':
        datadir = os.path.join(top,'sample',date)
    else:
        datadir = os.path.join(top,date,'AOsample')
    if not os.path.isdir(datadir):
        raise Exception(cstr('fail',f'no data directory {datadir}'))
    return datadir
    
class Ttan:
    def __init__(self,date='2021-08-25'):
        date = p_date(date)
        self.n = 80
        self.pixel_scale = 0.33*u.arcsec
        self.wavelength = 750*u.nm
        self.name = 'ShaneAO Tip Tilt sensor'
        self.descrip = 'Marconi CCD39 with SciMeasure controller'
        self.date = date
        self.read(date)
        self.bg_ave()
    
    def read(self,date):
        datadir = get_path(date)
        files_list = os.listdir(datadir)
        if not files_list:
            raise Exception(cstr('fail',f'no TT_Data in dirctory {datadir}'))
        files_list = sorted([x for x in files_list if x.startswith('TT_Data_')])
        pat = 'TT_Data_(\d{4})'
        nums_list = [re.match(pat,f) for f in files_list]
        nums_list = [int(x.group(1)) for x in nums_list if x is not None]
        db = []
        data = {}
        for num in nums_list:
            d = TTQ(date,num)
            data[num] = d
            h = {'date':d.date,
                 'name':os.path.splitext(d.filename)[0],
                 'num':num,
                 'frozen':d.frozen,
                 'ttrate':d.header['ttrate'],
                 }
            db.append(h)
        db = pd.DataFrame(db)
        self.db = db
        self.data = data
    
    @property
    def pp(self):
        oprint.pprint(self)
        
    def __getitem__(self,key):
        return self.data[key]
    
    def bg_ave(self):
        '''do background averages in groups
        (2 backgrounds: one for 'open' one for 'frozen')
        '''
        bg_frozen = 0
        bg_open = 0
        for key,data in self.data.items():
            z = np.array(data)
            n,m = z.shape
            ap = img.circle((n,n),r=.9*n)
            ctr =  np.unravel_index(np.argmax(z),z.shape)
            dx = self.pixel_scale
            rblob = (2*u.arcsec/dx).to('').value
            mask = img.circle((n,n),r=rblob).astype(bool)
            n2 = n//2
            a,b,c,d = [np.mean(z[x:x+n2,y:y+n2],where=~mask[x:x+n2,y:y+n2]) for x,y in zip([0,n2,0,n2],[0,0,n2,n2])]
            q = np.ones((n2,n2))
            bg = np.block([[a*q,b*q],[c*q,d*q]])
            if data.frozen:
                bg_frozen += bg
            else:
                bg_open += bg
        n_frozen = len([key for key,val in self.data.items() if val.frozen])
        n_open = len([key for key,val in self.data.items() if not val.frozen])
        self.bg_frozen = bg_frozen/n_frozen
        self.bg_open = bg_open/n_open
        for key,data in self.data.items():
            if data.frozen: bg = self.bg_frozen
            else: bg = self.bg_open
            data.bgr = data - bg
    
    def fit(self):
        '''fit a psf to each of the data frames and determine
        the FWHM. calculate an average FWHM for open and frozen
        frames separately
        '''
        for data in self.data.values():
            data.fit()
        fwhm = u.Quantity( [self[num].fwhm for num in self.db.num] )
        lam = self.wavelength
        lam0 = 550*u.nm
        r00 = 0.985*((lam/fwhm)*(lam0/lam)**(6/5)).to_da(u.cm)
        self.db['fwhm'] = fwhm.value
        self.db['r00'] = r00.value
        db = self.db
        a = db[~db.frozen]['fwhm'].mean()*u.arcsec
        b = db[db.frozen]['fwhm'].mean()*u.arcsec
        self.avg_fwhm = {'open':a,'frozen':b}
        a = db[~db.frozen]['r00'].mean()*u.cm
        b = db[db.frozen]['r00'].mean()*u.cm
        self.avg_r00 = {'open':a,'frozen':b}
        
        return self
        
    def plot(self,title=None):
        '''plot the measured FWHM and r00 vs data frame, and
        the averages
        '''
        db = self.db
        dbo = db[~db.frozen]
        dbf = db[db.frozen]
        fig,axs = plt.subplots(2)
        
        ax = axs[0]
        ax.plot(dbo['num'],dbo['fwhm'],'x',color='blue',label='open')
        ax.plot(dbf['num'],dbf['fwhm'],'x',color='orange',label='frozen')
        
        v = self.avg_fwhm['open'].value
        x0,x1 = dbo['num'].min(), dbo['num'].max()
        ax.plot([x0,x1],[v,v],'-',color='blue')
        
        v = self.avg_fwhm['frozen'].value
        x0,x1 = dbf['num'].min(), dbf['num'].max()
        ax.plot([x0,x1],[v,v],'-',color='orange')
        
        ax.set_ylabel('FWHM, arcsec')
        ax.grid('on')
        ax.legend()
        
        
        ax = axs[1]
        ax.plot(dbo['num'],dbo['r00'],'x',color='blue')
        ax.plot(dbf['num'],dbf['r00'],'x',color='orange')
        
        v = self.avg_r00['open'].value
        x0,x1 = dbo['num'].min(), dbo['num'].max()
        ax.plot([x0,x1],[v,v],'-',color='blue')
        
        v = self.avg_r00['frozen'].value
        x0,x1 = dbf['num'].min(), dbf['num'].max()
        ax.plot([x0,x1],[v,v],'-',color='orange')
        
        ax.set_ylabel('$r_{00}$, cm')
        ax.grid('on')
        
        if title is None:
            title = f'TTcam seeing data {self.date}'
        plt.suptitle(title)
        
        return self
        
class TTQ(u.Quantity):
    def __new__(cls,date,filenum):
        datadir = get_path(date)
        filename = f'TT_Data_{filenum:04d}.fits'
        path = os.path.join(datadir,filename)
        with fits.open(path) as hdu:
            data = hdu[0].data
            hdr = hdu[0].header
        
        self = super().__new__(cls,data)
        n = 80
        self.wavelength = 750*u.nm # with splitter 600-900, this is average wavelength
        self.pixel_scale = 0.33*u.arcsec
        self.dx = [self.pixel_scale]*2
        self.x0 = [-n//2*du for du in self.dx]
        self.header = hdr
        self.filename = filename
        self.number = filenum
        self.path = path
        self.date = date
        time = datetime.datetime.strptime(hdr['TIME'],'%H%M%S').time().strftime('%H:%M:%S')
        self.obs_time = Timestamp(date+'T'+time)
        frozen = {True:'frozen',False:'open'}[hdr['frozen']]
        self.frozen = hdr['frozen']
        self.name = f'{date} {os.path.splitext(filename)[0]} {frozen}'
        return self

    def __array_finalize__(self,obj):
        if obj is None: return
        if not hasattr(obj,'__dict__'): return
        d = obj.__dict__
        for k,v in d.items():
            setattr(self,k,v)
    
    def bg_remove(self,bg):
        self.bgr = self - bg
        return self.bgr
    
    def fit(self,model='Moffat'):
        z = self.bgr
        n,m = z.shape
        dx = self.dx[0]
        n2 = n//2
        fit = fitting.LevMarLSQFitter()
        y,x = np.mgrid[:n,:n]
        if model in ['Gaussian','gaussian']:
            g = models.Gaussian2D(amplitude=z.max(), x_mean=n2, y_mean=n2, x_stddev=10, y_stddev=10)
            p = fit(g,x,y,z)
            self.fwhm_xy = u.Quantity([2*np.sqrt(2*np.log(2))*sig*dx for sig in [p.x_stddev,p.y_stddev]])
            self.fwhm = np.mean(self.fwhm_xy)
        elif model in ['Moffat','moffat']:
            mof = models.Moffat2D(amplitude=z.max(), x_0=n2, y_0=n2, gamma=10, alpha=2,fixed={'alpha':True})
            mof.amplitude.min = 0
            mof.gamma.min = 0.1
            p = fit(mof,x,y,z)
            gamma = (p.gamma.value*dx).abs()
            alpha = p.alpha.value
            self.fwhm = 2*gamma*np.sqrt(2**(1/alpha)-1)
        self.r0 = 0.985*(self.wavelength/self.fwhm).to_da(u.cm)
        self.pfit = p
        return self

def ttan(date='2021-08-25',plot=False):
    '''Read all the data files, determine FWHM for each and averages
    over groups of 10, and return results in a database
    
    Arguments:
    ----------
    date : str
        date of the data taking
    plot : bool
        whether to produce a plot
    
    Returns:
    --------
    pandas.DataFrame
        One line per file of the FWHM determined by various methods
    '''
    datadir = os.path.join(top,date,'AOsample')
    files_list = os.listdir(datadir)
    files_list = sorted([x for x in files_list if x.startswith('TT_Data_')])
    
    data = []
    db = []
    for f in files_list:
        with fits.open(os.path.join(datadir,f)) as hdu:
            d = hdu[0].data
            h = hdu[0].header
        data.append(d)
        db.append(dict(h))
    db = pd.DataFrame(db)
    db.columns = [x.lower() for x in db.columns]
    db['file'] = files_list
    
    # evaluate background
    # it is a 2x2 grid of 40x40 pixel regions. The star is roughly in the middle
    # within an nxn box
    k = 30
    gfitx = []
    gfity = []
    gfitt = []
    mfit = []
    gfitx_ave = []
    gfity_ave = []
    mfit_ave = []
    verbose = False
    for k in progbar(range(0,len(files_list),1)):
        s = db.iloc[k]
        if verbose: print(f'{s.file} rate={s.rate} frozen={s.frozen} {s.comment}')
        z = data[k]
        n,m = z.shape
        ap = img.circle((n,n),r=.9*n)
        ctr =  np.unravel_index(np.argmax(z),z.shape)
        dx = 0.33*u.arcsec
        rblob = (2*u.arcsec/dx).to('').value
        mask = img.circle((n,n),r=rblob).astype(bool)
        
        if k%10 == 0:
            kmax = min(k+10,len(data)-1)
            bg = np.mean(data[k:kmax],axis=0)
        n2 = n//2
        a,b,c,d = [np.mean(bg[x:x+n2,y:y+n2],where=~mask[x:x+n2,y:y+n2]) for x,y in zip([0,n2,0,n2],[0,0,n2,n2])]
        #a,b,c,d = [img.planeFit(bg[x:x+n2,y:y+n2],ap=~mask[x:x+n2,y:y+n2])[0] for x,y in zip([0,n2,0,n2],[0,0,n2,n2])]
        q = np.ones((n2,n2))
        bg = np.block([[a*q,b*q],[c*q,d*q]])
        z = z - bg
        
        g = models.Gaussian2D(amplitude=z.max(), x_mean=n2, y_mean=n2, x_stddev=10, y_stddev=10)
        fit = fitting.LevMarLSQFitter()
        y,x = np.mgrid[:n,:n]
        p = fit(g,x,y,z)
        fwhm_g = u.Quantity([2*np.sqrt(2*np.log(2))*sig*dx for sig in [p.x_stddev,p.y_stddev]])
        if verbose: print(f'FWHM (Gaussian2D fit) = {fwhm_g}')
        
        mof = models.Moffat2D(amplitude=z.max(), x_0=n2, y_0=n2, gamma=10, alpha=2,fixed={'alpha':True})
        mof.amplitude.min = 0
        mof.gamma.min = 0.1
        mofp = fit(mof,x,y,z)
        gamma = (mofp.gamma.value*dx).abs()
        alpha = mofp.alpha.value
        fwhm_mof = 2*gamma*np.sqrt(2**(1/alpha)-1)
        if verbose: print(f'FWHM (Moffat2D fit) = {fwhm_mof}')
        
        gfitx.append(fwhm_g[0].value)
        gfity.append(fwhm_g[1].value)
        gfitt.append(p.theta.value)
        mfit.append(fwhm_mof.value)
        
        if k%10 == 9:
            kmax = min(k+1,len(data)-1)
            z = np.mean(data[k-9:kmax],axis=0)
            z = z-bg
            p = fit(g,x,y,z)
            fwhm_g = u.Quantity([2*np.sqrt(2*np.log(2))*sig*dx for sig in [p.x_stddev,p.y_stddev]])
            mofp = fit(mof,x,y,z)
            fwhm_mof = 2*gamma*np.sqrt(2**(1/alpha)-1)
            gfitx_ave += [fwhm_g[0].value]*10
            gfity_ave += [fwhm_g[1].value]*10
            mfit_ave += [fwhm_mof.value]*10
    
    db['g_fwhm_x'] = gfitx
    db['g_fwhm_y'] = gfity
    db['g_theta'] = gfitt
    db['m_fwhm'] = mfit
    if len(mfit_ave) < len(data):
        r = len(data) - len(mfit_ave)
        gfitx_ave += [np.nan]*r
        gfity_ave += [np.nan]*r
        mfit_ave += [np.nan]*r
    db['g_fwhm_x_ave'] = gfitx_ave
    db['g_fwhm_y_ave'] = gfity_ave
    db['m_fwhm_ave'] = mfit_ave
    cols = ['file','rate','frozen','comment','g_fwhm_x','g_fwhm_y','g_theta','m_fwhm','g_fwhm_x_ave','g_fwhm_y_ave','m_fwhm_ave']

    if plot: plotdb(db)
    
    globals().update(locals())
    return db,db[cols]

def ttread(date='2021-08-25'):
    '''Read all the data files, return headers in a database.
    
    Arguments:
    ----------
    date : str
        date of the data-taking
    
    Returns:
    --------
    pandas.DataFrame
        One line per file of the file header info
    
    Global:
    -------
    data : list of 2D arrays
        The raw data
    '''
    global data
    
    datadir = os.path.join(top,date,'AOsample')
    files_list = os.listdir(datadir)
    files_list = sorted([x for x in files_list if x.startswith('TT_Data_')])
    
    data = []
    db = []
    n = 80
    dx = [0.33*u.arcsec]*2
    x0 = [-n//2*du for du in dx]
    for f in files_list:
        with fits.open(os.path.join(datadir,f)) as hdu:
            d = hdu[0].data
            h = hdu[0].header
        d = u.Quantity(d)
        d.dx = dx
        d.x0 = x0
        d.name = f
        d.number = int( re.match('TT_Data_(\d{4})',f).group(1) )
        data.append(d)
        db.append(dict(h))
    db = pd.DataFrame(db)
    db.columns = [x.lower() for x in db.columns]
    db['file'] = files_list
    return db

def fit(k,model='Gaussian',plot=False):
    '''fit the raw data k to the model
    
    Arguments:
    ----------
    k : int or str
        index or data file name. Note it is assumed that TT_Data files
        are numbered sequentially and in groups of 10, as is standard
        with the tt_cam_run seeing measurement procedure.
    model : str
        'Gaussian' or 'Moffat' for now. Later upgrade: 'Atmos', which will
        be the PSF of atmospheric turbulence with r0 being a parameter
    
    Returns:
    --------
    The data and model fit, as a tuple
    '''
    z = bg_remove(k)
    n,m = z.shape
    n2 = n//2
    if model in ['Gaussian','gaussian']:
        g = models.Gaussian2D(amplitude=z.max(), x_mean=n2, y_mean=n2, x_stddev=10, y_stddev=10)
        fit = fitting.LevMarLSQFitter()
        y,x = np.mgrid[:n,:n]
        p = fit(g,x,y,z)
    elif model in ['Moffat','moffat']:
        mof = models.Moffat2D(amplitude=z.max(), x_0=n2, y_0=n2, gamma=10, alpha=2,fixed={'alpha':True})
        mof.amplitude.min = 0
        mof.gamma.min = 0.1
        p = fit(mof,x,y,z)
    if plot:
        plot_fit(z,p)
    return (z,p)

def bg_remove(k):
    '''background removal on a per quadrant basis
    
    Arguments:
    ----------
    k : int
        index or data file name. Note it is assumed that TT_Data files
        are numbered sequentially and in groups of 10, as is standard
        with the tt_cam_run seeing measurement procedure.
    
    Returns:
    --------
    Quantity, 2D
        The background-removed image
    '''
    if isinstance(k,str):
        r =  re.match('TT_Data_(\d{4})',k)
        if not r:
            raise Exception(f'filename {k} fails to match pattern for a TT file')
        k = int(r.group(1))
    z = data[k]
    n,m = z.shape
    ap = img.circle((n,n),r=.9*n)
    ctr =  np.unravel_index(np.argmax(z),z.shape)
    dx = z.dx[0]
    rblob = (2*u.arcsec/dx).to('').value
    mask = img.circle((n,n),r=rblob).astype(bool)
    k = k - k%10
    bg = np.mean(data[k:k+10],axis=0)
    n2 = n//2
    a,b,c,d = [np.mean(bg[x:x+n2,y:y+n2],where=~mask[x:x+n2,y:y+n2]) for x,y in zip([0,n2,0,n2],[0,0,n2,n2])]
    #a,b,c,d = [img.planeFit(bg[x:x+n2,y:y+n2],ap=~mask[x:x+n2,y:y+n2])[0] for x,y in zip([0,n2,0,n2],[0,0,n2,n2])]
    q = np.ones((n2,n2))
    bg = np.block([[a*q,b*q],[c*q,d*q]])
    z = z - bg
    return z
    
def plotdb(db):
    date = db.iloc[0]['date']
    title = f'{date} FWHM from TT data'
    dbf = db[db.frozen]
    dbo = db[~db.frozen]
    plt.figure()
    plt.title(title)
    ax = plt.gca()
    ax.set_aspect(1)
    cir = [(np.cos(t),np.sin(t)) for t in np.linspace(0,2*np.pi,100)]
    x,y = [[x for x,y in cir],[y for x,y in cir]]
    plt.plot(x,y,'r')
    xy = [(r*np.cos(t),r*np.sin(t)) for r,t in zip(dbo['g_fwhm_x'],dbo['g_theta'])]
    x = [x for x,y in xy]
    y = [y for x,y in xy]
    plt.plot(x,y,'x',color='blue',label='Gaussian, open')
    xy = [(r*np.cos(t),r*np.sin(t)) for r,t in zip(dbf['g_fwhm_x'],dbf['g_theta'])]
    x,y = [x for x,y in xy], [y for x,y in xy]
    plt.plot(x,y,'x',color='orange',label='Gaussian, frozen')
    rs = db['m_fwhm']
    xy = [(r*np.cos(t),r*np.sin(t)) for r,t in zip(rs,np.linspace(0,2*np.pi,len(rs)))]
    x,y = [x for x,y in xy], [y for x,y in xy]
    x = list(rs)
    y = [0 for x in rs]
    plt.plot(x,y,'^',mfc='none',mec='green',label='Moffat')
    ax.set_xlabel('Gaussian fit FWHM x, arcsec')
    ax.set_ylabel('Gaussian fit FWHM y, arcsec')
    plt.legend()
    plt.grid('on')
    
    plt.figure()
    plt.title(title)
    plt.plot(dbo['m_fwhm'],dbo['g_fwhm_x'],'x',color='blue',label='open x')
    plt.plot(dbf['m_fwhm'],dbf['g_fwhm_x'],'x',color='orange',label='frozen x')
    plt.plot(dbo['m_fwhm'],dbo['g_fwhm_y'],'+',color='blue',label='open y')
    plt.plot(dbf['m_fwhm'],dbf['g_fwhm_y'],'+',color='orange',label='frozen y')
    ax = plt.gca()
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    plt.plot([0,2],[0,2],'--',color='Gray')
    ax.set_xlim(ylim)
    ax.set_ylim(ylim)
    plt.legend()
    plt.grid('on')
    ax.set_xlabel('Moffat fit FWHM, arcsec')
    ax.set_ylabel('Gaussian fit FWHM (x and y), arcsec')

def plot_fit(z,f,title=None):
    '''Show 4 plots:
    Original data, Model, Residual, and a 1d lineout of data and model.
    
    Arguments:
    ----------
    z : Quantity, 2D
        A TT image (background removed)
    f : astropy.modeling.Model
        The model - with parameters fit to data
    title : str
        Title for the figure
    '''
    # plots
    fitter_name = f.__class__.__name__
    n = z.shape[0]
    y,x = np.mgrid[:n,:n]
    dx = 0.33*u.arcsec
    n2 = n//2
    extent = tuple(b*n2*dx.value for b in [-1,1,-1,1])
    plt.figure(figsize=(8,8))
    plt.subplot(2,2,1)
    plt.imshow(z, origin='lower', interpolation='nearest',extent=extent)
    plt.xlabel('arcsec')
    plt.title("Data")
    plt.subplot(2,2,2)
    plt.imshow(f(x,y), origin='lower', interpolation='nearest',extent=extent)
    plt.xlabel('arcsec')
    plt.title("Model")
    plt.subplot(2,2,3)
    v = z.max()
    plt.imshow(z - f(x,y), origin='lower', interpolation='nearest',extent=extent,
               vmin=-v,vmax=v)
    plt.xlabel('arcsec')
    plt.title("Residual")
    plt.subplot(2,2,4)
    x = np.mgrid[:n]
    xx = (x-n2)*dx
    if fitter_name == 'Moffat2D':
        y = f.y_0.value
    elif fitter_name == 'Gaussian2D':
        y = f.y_mean.value
    plt.plot(xx,z[int(y),x])
    x = np.linspace(0,n,2000)
    xx = (x-n2)*dx
    plt.plot(xx,f(x,y))
    plt.grid('on')
    plt.xlabel('arcsec')
    
    if title is not None:
        plt.suptitle(title)
    else:
        plt.suptitle(f'{z.name} {fitter_name} fit')
    plt.tight_layout()

# ------------ tests --------------
def test():
    tt = Ttan()
    tt.fit()
    tt.plot()

# ------------- Executable ------------
class parser(object):
    def __init__(self):
        params = dict(
            date = dict(pat='\d{4}-\d{2}-\d{2}|today',kind=1,key=['-d','--date']),
            plot = dict(pat='',kind=0,key=['-p','--plot']),
            help = dict(pat='',kind=0,key=['-h','--help']),
        )
        params = pd.DataFrame(params).transpose()
        params['value'] = ''
        self.params = params
        return
    
    def parse(self,args):
        df= self.params
        df['value'] = '' # reset to default
        # if isinstance(args,str):
        #     args = shlex.split(args) # preserve spaces in quoted strings
        q = deque(args)
        if '--' in q:
            p = ''
            while p != '--': p = popleft(q)
        r = {}
        mode = None
        while len(q) > 0:
            arg = q.popleft()
            param = df[[arg in x for x in df['key']]]
            if not param.empty: # flag or key value pair
                param = param.iloc[0]
                if param.kind == 1: # has a succeding value
                    val = q.popleft()
                    df.at[param.name,'value'] = val
                else:
                    df.at[param.name,'value'] = 'True'
            else: # stand alone. Try to decypher based on format
                for ind,e in df.iterrows():
                    if re.match(e.pat,arg):
                        break
                else:
                    raise Exception(f'{tcolor.fail}not a valid argument: [{arg}]{tcolor.end}')
                df.at[e.name,'value'] = arg
        return dict(df['value'])

def run():
    global tt,fwhm,r0,date
    
    args = sys.argv[1:]
    if '--' in args: args.remove('--')
    p = parser()
    r = p.parse(args)
    if r['help']:
        pydoc.pager(__doc__)
        return
    date,plot = (r[x] for x in ['date','plot'])
    if not date: date = 'today'
    plot = plot not in ['',None,0,'no','No',False,'False']
    
    tt = Ttan(date).fit()
    
    print('')
    topline = f'-------- Seeing {date} --------'
    gprint(topline)
    fwhm = tt.avg_fwhm['frozen']
    r0 = tt.avg_r00['frozen']
    print(f'average FWHM: {fwhm.round(2)}')
    print(f'average r0: {r0.round(2)}')
    gprint('-'*len(topline))
    
    # FITS card dump of seeing analysis results
    fits_cards = False
    keywords = True
    if fits_cards:
        try:
            print('')
            c = []
            first_file_index = list(tt.data.keys())[0]
            if 'UTC' in tt.data[first_file_index].header:
                time = tt.data[first_file_index].header['UTC']
            else:
                time = utc_from_local(tt.data[first_file_index].obs_time).to_datetime().isoformat()
            card = fits.Card('AOS_TIME',time,'UTC time of the seeing data')
            c.append(card)
            filename = tt.data[first_file_index].filename
            card = fits.Card('AOSTFILE',filename,'name of first seeing datafile (AO TTsensor)')
            c.append(card)
            card = fits.Card('AOSTFWHM',fwhm.round(2).value,'arcsec AO FWHM on TT sensor')
            c.append(card)
            card = fits.Card('AOSTR0',r0.round(2).value,'cm AO R0 from FWHM on TT sensor')
            c.append(card)
            for x in c: print(x)
        except:
            print(f'{tcolor.fail}failed to do a FITS card dump{tcolor.end}')
    
    if keywords:
        first_file_index = list(tt.data.keys())[0]
        filename = tt.data[first_file_index].filename
        cmd = ['modify','-s','saoseeing',f'TTFILE={filename}']
        subprocess.run(cmd); print(' '.join(cmd))
        cmd = ['modify','-s','saoseeing',f'TT_R0={r0.round(2).value}']
        subprocess.run(cmd); print(' '.join(cmd))
        cmd = ['modify','-s','saoseeing',f'TT_FWHM={fwhm.round(2).value}']
        subprocess.run(cmd); print(' '.join(cmd))
        if 'UTC' in tt.data[first_file_index].header:
            time_str = tt.data[first_file_index].header['UTC']
        else:
            time_str = utc_from_local(tt.data[first_file_index].obs_time).to_datetime().isoformat()
        time_str += '+00:00' # make datetime aware this is UTC
        time = datetime.datetime.fromisoformat(time_str).timestamp()
        cmd = ['modify','-s','saoseeing',f'SEEINGTIME={time}']
        subprocess.run(cmd); print(' '.join(cmd))
        
    if plot:
        if not sys.flags.interactive:
            plt.ioff()
            tt.plot()
            plt.show()
            plt.ion()
        else:
            tt.plot()

if __name__ == '__main__':
    run()
    
