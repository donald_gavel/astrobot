'''distortion.py
model camera distortion
'''
import numpy as np
from scipy import ndimage
from scipy.interpolate import interp2d
from skimage import data
import matplotlib.pyplot as plt

def undistort(img,K=[0,0],P=[0,0],c=None,show=False):
    radial_order = len(K)
    tan_order = len(P)
    ny,nx = img.shape
    if c is not None:
        xc,yc = c
    else:
        xc = nx//2
        yc = ny//2
    x = np.arange(nx)-xc
    y = np.arange(ny)-yc
    x,y = np.meshgrid(x,y)
    r2 = x**2 + y**2
    scale = np.sqrt(r2.max())
    x = x/scale
    y = y/scale
    r2 = r2/scale**2
    r4 = r2**2
    Kr = 0
    Pr = 1
    for i in range(radial_order):
        Kr += K[i]*r2**(i+1)
    for i in np.arange(2,tan_order):
        Pr += P[i]*r2**(i-1)
    P1,P2 = P[0],P[1]
    xu = (x + x*Kr + (P1*(r2 + 2*x**2) + 2*P2*x*y)*Pr)*scale + xc
    yu = (y + y*Kr + (2*P1*x*y + P2*(r2 + 2*y**2))*Pr)*scale + yc
    a = ndimage.map_coordinates(img,[yu,xu])
    if show:
        plt.clf()
        plt.imshow(a)
    return a,xu,yu

def map(n,K=[0,0],P=[0,0],c=None):
    radial_order = len(K)
    tan_order = len(P)
    ny,nx = n
    if c is not None:
        xc,yc = c
    else:
        xc = nx//2
        yc = ny//2
    x = ( np.arange(nx)-xc )/nx
    y = ( np.arange(ny)-yc )/ny
    x,y = np.meshgrid(x,y)
    r2 = x**2 + y**2
    Kr = 0
    Pr = 1
    for i in range(radial_order):
        Kr += K[i]*r2**(i+1)
    for i in np.arange(2,tan_order):
        Pr += P[i]*r2**(i-1)
    P1,P2 = P[0],P[1]
    xu = x + x*Kr + (P1*(r2 + 2*x**2) + 2*P2*x*y)*Pr
    yu = y + y*Kr + (2*P1*x*y + P2*(r2 + 2*y**2))*Pr
    return xu,yu

def f(p,K=[0,0],P=[0,0]):
    x,y = p
    r2 = x**2 + y**2
    radial_order = len(K)
    tan_order = len(P)
    Kr = 0
    Pr = 1
    for i in range(radial_order):
        Kr += K[i]*r2**(i+1)
    for i in np.arange(2,tan_order):
        Pr += P[i]*r2**(i-1)
    P1,P2 = P[0],P[1]
    xu = x + x*Kr + (P1*(r2 + 2*x**2) + 2*P2*x*y)*Pr
    yu = y + y*Kr + (2*P1*x*y + P2*(r2 + 2*y**2))*Pr
    return xu,yu
    
x = np.array([.5,.5])
x0 = x.copy()
K1,K2 = 0.1,0.0

for k in range(5):
    r2 = x[0]**2 + x[1]**2
    m = K1*r2 + K2*r2**2
    x = x0 / (1+m)
    
    r2 = x[0]**2 + x[1]**2
    resid = x0 - (x + x*(K1*r2 + K2*r2**2))
    print(k,resid)

def m(x,K):
    x,y = x
    radial_order = len(K)
    Kr = 0
    r2 = x**2 + y**2
    for i in range(radial_order):
        Kr += K[i]*r2**(i+1)
    return Kr

def s(x,P):
    x,y = x
    tan_order = len(P)
    Pr = 1
    for i in np.arange(2,tan_order):
        Pr += P[i]*r2**(i-1)
    P1,P2 = P[0],P[1]    
    s = np.array([P1*(r2+2*x**2) + 2*P2*x*y,
                  2*P1*x*y + P2*(r2 + 2*y**2)]) * Pr
    return s

K = [.1,0]
P = [0.,0.]

x0 = np.array([.5,.5])
def solve(x0,K,P):
    x = x0.copy()
    niter = 5
    for i in range(niter):
        x = (x0 - s(x,P)) / (1 + m(x,K))
    return x

#z = [solve(x,K,P) for x in pts]

def test1():
    img = data.checkerboard()
    imgd,xu,yu = distort(img,K=[1.,0])
    nr,nc = img.shape
    r = np.arange(nr)
    c = np.arange(nc)
    c,r = np.meshgrid(c,r)
    rd,xurd,yurd = distort(r,K=[1.,0])
    cd,xucd,yucd = distort(c,K=[1.,0])
    q = np.zeros((nr,nc))+128
    for i in range(nr):
        for j in range(nc):
            q[rd[i,j],cd[i,j]] = imgd[i,j]
    globals().update(locals())    

def test2(K=[1.,0],P=[0,0],n=10):
    '''draw nxn grid lines and distort them
    '''
    radial_order = len(K)
    tan_order = len(P)
    def f(x,y,K=[1.,0.],P=[0,0]):
        Kr = 0
        Pr = 1
        r2 = x**2 + y**2
        for i in range(radial_order):
            Kr += K[i]*r2**(i+1)
        for i in np.arange(2,tan_order):
            Pr += P[i]*r2**(i-1)
        P1,P2 = P[0],P[1]
        xu = (x + x*Kr + (P1*(r2 + 2*x**2) + 2*P2*x*y)*Pr)
        yu = (y + y*Kr + (2*P1*x*y + P2*(r2 + 2*y**2))*Pr)
        return xu,yu
    s = np.linspace(-1,1,n)
    # horizontal lines
    for t in np.linspace(-1,1,n):
        xc,yc = f(s,t,K=K,P=P)
        plt.plot(xc,yc)
    # vertical lines
    for t in np.linspace(-1,1,n):
        xc,yc = f(t,s,K=K,P=P)
        plt.plot(xc,yc)
    globals().update(locals())

x,y = map((200,200),K=[.4,0])
ix = np.round(x*n+c).astype(int)
iy = np.round(y*n+c).astype(int)
ixc = np.clip(ix,0,199)
iyc = np.clip(iy,0,199)
img = data.checkerboard()
imgd = img[ixc,iyc]
imgu = np.ones_like(imgd)*128
imgu[ixc,iyc] = imgd
plt.figure();plt.imshow(imgd)
plt.figure();plt.imshow(imgu)
imgd2 = ndimage.map_coordinates(img,[ixc,iyc])
