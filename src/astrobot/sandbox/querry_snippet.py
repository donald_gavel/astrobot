# https://www.google.com/search?q=snubber+circuit&client=safari&hl=en&tbm=isch&tbo=u&source=univ&sa=X&ei=u5VeVbTHGYPosAXhy4DoAw&ved=0CDUQ7Ak&biw=768&bih=905

try:
    urllib2.urlopen(full_url)
except urllib2.HTTPError as e:
    print e.code
    print e.read()

url = 'http://www.google.com/search?q=goof'
headers = {'User-agent':'Mozilla/11.0'}
req = urllib2.Request(url,None,headers)
site = urllib2.urlopen(req)
data = site.read()
site.close()

# astroquery
from astroquery.simbad import Simbad
theta1c = Simbad.query_object("tet01 Ori C")
theta1c.pprint()
# theta1c is an astropy.table.table object
theta1c.keys()
len(theta1c)
result_table = Simbad.query_object("m [1-9]", wildcard=True)
len(result_table)
result_table[2][result_table.index_column('MAIN_ID')]
id = result_table.index_column('MAIN_ID')
ra = result_table.index_column('RA')
dec = result_table.index_column('DEC')
for k in range(len(result_table)):
    print result_table[k][id] + '; ' + result_table[k][ra] + '; ' + result_table[k][dec]

t2 = result_table.copy()
# note: this fails!! you must extract the data and rebuild a Table. Maybe something
# to do with the ?masked? array and nans in the original table?
t2.sort('RA')
for k in range(len(t2)):
    print t2[k][id] + '; ' + t2[k][ra] + '; ' + t2[k][dec]

# http://www.aanda.org/articles/aa/full_html/2009/26/aa11411-08/aa11411-08.html
