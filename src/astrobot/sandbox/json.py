import json
from astropy.utils.misc import JsonCustomEncoder
import numpy as np
import astropy.units as u

class QEncoder(json.JSONEncoder):
    def default(self,obj):
        if isinstance(obj,u.Quantity):
            if np.iscomplexobj(obj):
                d = {'__tag__':'Quantity.complex'}
                d['real'] = obj.real.value
                d['imag'] = obj.imag.value
            else:
                d = {'__tag__':'Quantity.real'}
                d['real'] = obj.value
            d.update(obj.__dict__)
            d['_unit'] = obj.unit.to_string()
            return d
        return JsonCustomEncoder().default(obj)
        
def QDecoder(dct):
    tag = dct.pop('__tag__',None)
    if tag is None:
        return dct
    if tag == 'Quantity.real':
        unit = dct.pop('_unit')
        q = u.Quantity(dct.pop('real'),unit)
        return q
    elif tag == 'Quantity.complex':
        unit = u.Unit(dct.pop('_unit'))
        qr = u.Quantity(dct.pop('real'))
        qi = u.Quantity(dct.pop('imag'))
        q = (qr + 1j*qi)*unit
    q.__dict__.update(dct)
    return q

def test():
    try:
        a = np.random.normal(size=(2,2))
        q = u.Quantity(a)*u.cm
        s = json.dumps(q,cls=QEncoder)
        qd = json.loads(s,object_hook=QDecoder)
        if np.isclose(q,qd).all():
            print('pass')
        qr = q
        qi = np.random.normal(size=(2,2))*qr.unit
        q = qr + 1j*qi
        s = json.dumps(q,cls=QEncoder)
        qd = json.loads(s,object_hook=QDecoder)
        if np.isclose(q,qd).all():
            print('pass')
    finally:
        globals().update(locals())

df = pd.DataFrame(
    {'A': [1, 2, 3]*u.cm,
     'B': ['a', 'b', 'c'],
     'C': pd.date_range('2016-01-01', freq='d', periods=3),
    }, index=pd.Index(range(3), name='idx'))

s = pd.io.json.build_table_schema(df)

