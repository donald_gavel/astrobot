"""AO Workflow step 1
Process observer science list to add calibrators and guide stars to the plan

Input:
    Text file of format: (alternatively, xml from web form)
        Object RA Dec Epoch filter exposure [calibrator flags]
    Object: required, name of object
    RA, Dec, Epoch: optional (can look up object in VO database, or prioritize position)
    Exposure: either seconds or SNR. (both will be calculated and user asked to approve)
    Calibrator flags: (all default to yes)
        PSF (yes/no)
        Darks
        Sky flats
    
Output:
    xml and csv of observer groups - each group with all associated observations
    Tags include observer SNR objectives.
    (This feeds to step 2, which will assign nights, calculate Strehl, and adjust
    SNR prediction)

----------------------------------
The idea is:
        user_star_list --read--> stage1plan ---write---> [ xml or json version ]
                                      |                          |
                                      or<------read--------------/
                externalData-------\  |
                                 stage2plan ---write---> [ xml or json version ]
                                      |                          |
                                      or<------read--------------/
                                      |
                                 stage3plan
                                      etc.
    Features:
        Option of I/O streams instead of file.
        ZMQ links to other machines for parallel processing.
    
    Requires:
        Translators from and to object structure to xml or json or md5 ...
        These need encoders for objects:
            string, number
            pandas dataFrame, astropy Table(redundant?)
            lists of above
            dicts, OrderedDicts with above as attributes
            recursive into structures
"""
import os
import sys
from collections import OrderedDict
import csv
import json
import dicttoxml
from xml.dom.minidom import parseString
from xml.etree import ElementTree as ET
from astrobot import oprint
import numpy as np
import pytz
import datetime

#replace this with a web check of local time zone
tzone = pytz.timezone('US/Pacific')

print 'importing astropy VO client and helpers'; sys.stdout.flush()
import astropy
from astropy.vo.client import conesearch
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.coordinates import ICRS, FK5
import pandas
catalog = 'Guide Star Catalog v2 1'
print 'okay'; sys.stdout.flush()

class basePlan(object):
    """basePlan is inherited by planner tasks. It contains the standardized
    I/O and display functions
    """
    def __init__(self, aPlan=None, name='tempPlan', stage=0):
        """Extract the plan output from an earlier
        and prepare it for the next stage
        
        All plans have
            1. meta data (a dictionary)
            2. an object list (a list of objects, each dictionaries)
            3. a tasklog (a list of 2-tuples of strings: (time stamp, operation description))
        """
        if aPlan is not None:
            self.meta = aPlan.meta
            self.object_list = aPlan.object_list
            self.tasklog = aPlan.tasklog
        else:
            self.meta = {'name': name,
                         'fileFormat': 'json',
                         'stage': stage,
                        }
            if stage > 0:
                fileName = '%s_%04d.%s'%(self.meta['name'],self.meta['stage'],self.meta['fileFormat'])
                self.load(fileName=fileName)
    
    def pprint(self):
        """Pretty-print the plan contents
        """
        oprint.pprint(self,expand=2)

    def load(self,fileName=None):
        """restore a plan at its stage from a file or i/o stream, for subsequent continuation of planning
        """
        if fileName is None:
            fileName = '%s_%04d.%s'%(self.meta['name'],self.meta['stage'],self.meta['fileFormat'])
        head,format = os.path.splitext(fileName)
        format = format.lstrip('.')
        if format != 'json':
            raise Exception,'%s format is not yet supported'%format
        
        if format == 'json':
            with open(fileName) as fp:
                s = fp.read()
            d = json.loads(s,cls=JSONDecoder)
            self.meta = d['meta']
            self.object_list = d['object_list']
            self.tasklog = map(tuple,d['tasklog'])
        
    def dump(self,fileName=None,format=None,xmlOption=1,indent=True,print_=False):
        """write the staged observer plan to a file or i/o stream
        """
        formats = ['csv','xml','json']
        if fileName is None: # glean file name from the original star list's name
            fileName = os.path.basename(self.meta['original_starlist_file'])
            head,ext = os.path.splitext(fileName)
            ext = ext.lstrip('.')
            if format is None:
                format = 'json'
            fileName = head + '.' + format
        if format is None: # glean the format from the file's extension
            head,ext = os.path.splitext(fileName)
            ext = ext.lstrip('.')
            format = ext
        assert format in formats
            
        if format == 'csv':
            with open(fileName,'w') as fp:
                w = csv.writer(fp)
                for key in self.meta:
                    w.writerow([key,self.meta[key]])
                w.writerow([])
                w.writerow(['start'])
                w.writerow(self.required_keys + self.option_keys + ['Comment'])
                for obj in self.object_list:
                    w.writerow(obj.values())
                w.writerow( ['end'] )
                
        elif format == 'json':
            d = OrderedDict({'meta':self.meta,
                             'object_list':self.object_list,
                             'tasklog':self.tasklog})
            if indent:
                ss = json.dumps(d,indent=4,cls=JSONEncoder)
            else:
                ss = json.dumps(d,cls=JSONEncoder)
            if print_:
                sys.stdout.write(ss)
            else:
                with open(fileName,'w') as fp:
                    fp.write(ss)
                    
        elif format == 'xml':
            if xmlOption == 0:
                if self.stage > 1:
                    raise Exception,'Guide star tables not supported yet with xmlOption 0'
                d = OrderedDict({'meta':self.meta,
                                 'object_list':self.object_list,
                                 'tasklog':tasklog})
                item_func = lambda x: 'object'
                xml = dicttoxml.dicttoxml(d,item_func=item_func)
                dom = parseString(xml)
                if print_:
                    if indent:
                        sys.stdout.write(dom.toprettyxml(encoding='utf-8',indent=' '*4))
                    else:
                        sys.stdout.write(dom.toxml(encoding='utf-8'))
                else:
                    with open(fileName,'w') as fp:
                        if indent:
                            fp.write(dom.toprettyxml(encoding='utf-8',indent=' '*4))
                        else:
                            fp.write(dom.toxml(encoding='utf-8'))
            elif xmlOption == 1:
                root = ET.Element('root')
                meta = ET.SubElement(root,'meta')
                ol = ET.SubElement(root,'object_list')
                tl = ET.SubElement(root,'tasklog')

                for key in self.meta:
                    se = ET.SubElement(meta,key)
                    se.text = self.meta[key]

                for obj in self.object_list:
                    se = ET.SubElement(ol,'object')
                    for key in obj:
                        val = obj[key]
                        if isinstance(val,str):
                            se2 = ET.SubElement(se,key)
                            se2.text = obj[key]
                        elif isinstance(val,pandas.DataFrame):
                            s = '<%s>%s</%s>'%(key,val.to_xml(),key)
                            q = ET.XML(s)
                            q.set('type','Pandas.DataFrame')
                            se.append(q)

                for e in tasklog:
                    se = ET.SubElement(tl,'log_item')
                    se.text = str(e)
                
                if indent: xml_indent(root)
                if print_:
                    fileName = sys.stdout
                q = ET.ElementTree(root)
                q.write(fileName,encoding='utf-8',xml_declaration=True)

    def log(self,msg):
        """put a time-stamped entry into the log
        """
        if not hasattr(self,'tasklog'):
            self.tasklog = []
        t = datetime.datetime.now()
        t = tzone.localize(t)
        fmt = '%Y-%m-%d %H:%M:%S %Z%z'
        timeStamp = t.strftime(fmt)
        self.tasklog.append((timeStamp,msg))

    def read(self,fileName,verbose=False):
        """Read in the observer's file and convert it to a standard set of targets
        
        This takes a certain formatted text input file, an example of which
        is in testPlan.txt
        """
        assert self.meta['stage'] == 0
        assert os.path.isfile(fileName)

        required_keys = ['ObjectName','RA','Dec','Epoch','Filter','Exposure']
        option_keys = ['PSF','SkyFlat','Dark','Standard']
    
        fileName = os.path.basename(fileName)
        directory = os.path.dirname(fileName)
        if directory == '':
            directory = os.getcwd()
        assert fileName in os.listdir(directory) # case sensitive file name
        self.object_list = [] # this will become a list of dictionaries, one per object
        nr_keys = len(required_keys)
        nr_options = len(option_keys)
        comment = ''
        with open(fileName,'r') as fp:
            for line in fp:
                if verbose: print line,
                if line[0] == '#': # comment line
                    continue
                if line.strip().startswith('['):
                    comment = line.strip().strip('[]')
                    continue
                tokens = tokenize(line)
                if len(tokens) == 0:  # blank line
                    continue
                key = tokens[0]
                if key.endswith(':'): # metadata line
                    self.meta[key[:-1]] = tokens[1]
                    continue
                obj = OrderedDict() # target line
                for key,val in zip(required_keys,tokens[:nr_keys]):
                    obj[key] = val
                # now process the calibrator flags
                for option in option_keys:
                    obj[option] = 'no'
                for option in tokens[nr_keys:]:
                    obj[option] = 'yes'
                obj['Comment'] = comment
                self.object_list.append(obj)
        # additional meta data
        self.meta['original_starlist_file'] = os.path.join(directory,fileName)
        self.meta['stage'] = 1
        self.log('read object list %s'%self.meta['original_starlist_file'])
        return

    def advance(self):
        """Advance to the next stage of the pipeline
        """
        stage = self.meta['stage']
        
        advance_function = 'step%d'%stage
        getattr(self,advance_function)()
        
        stage += 1
        self.meta['stage'] = stage
        self.log('stage %d completed'%self.meta['stage'])
        fileName = '%s_%04d.%s'%(self.meta['name'],self.meta['stage'],self.meta['fileFormat'])
        self.dump(fileName = fileName)
    
    def step0(self):
        filename = '%s.txt'%self.meta['name']
        self.read(filename)
        
    def step1(self):
        """Select guide stars for each target
        """
        assert self.meta['stage'] == 1
        radius = 2*u.arcmin
        for o in self.object_list:
            ra = o['RA']
            dec = o['Dec']
            epoch = o['Epoch'].upper()
            if epoch == 'ICRS':
                epoch = ICRS()
            elif epoch == 'FK5':
                epoch = FK5()
            elif epoch.startswith('J'):
                epoch = FK5(equinox=epoch)
            c = SkyCoord(ra,dec,frame=epoch)
            print 'search around object %s'%o['ObjectName']
            try:
                result = conesearch.conesearch(c, radius, catalog_db=catalog,verbose=False)
            except:
                print 'FAILED'
                return None
            tab = result.to_table()
            df = tab[['hstID','ra','dec','epoch','VMag']].to_pandas()
            #df = df[np.isfinite(df['Mag'])].sort('Mag')
            df = df.sort('VMag')
            df['sep'] = 0
            for ind in df.index:
                star = df.loc[ind]
                ra = star.ra*u.degree
                dec = star.dec*u.degree
                epoch = 'J%r'%(star.epoch)
                epoch = FK5(equinox=epoch)
                epoch = FK5() # hmm... 
                cgs = SkyCoord(ra,dec,frame=epoch)
                df.loc[ind,'sep'] = c.separation(cgs).arcsec
            o['gstable'] = df

def tokenize(str):
    return str.split()
                    
# json helpers ===========

class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, pandas.DataFrame):
            return dict(__DataFrame__=obj.to_json())
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)

def as_DataFrame(dct):
    if '__DataFrame__' in dct:
        return pandas.read_json(dct['__DataFrame__'])
    return dct

class JSONDecoder(json.JSONDecoder):
    def decode(self,s):
        obj = json.loads(s,object_hook=as_DataFrame)
        return obj

# xml helpers =============

def xml_indent(elem, level=0, indentSize=4):
    i = "\n" + level*indentSize*" "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + " "*indentSize
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            xml_indent(elem, level+1,indentSize)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

def to_xml(df, filename=None, mode='w'):
    def row_to_xml(row):
        xml = ['<item>']
        for i, col_name in enumerate(row.index):
            xml.append('  <field name="{0}">{1}</field>'.format(col_name, row.iloc[i]))
        xml.append('</item>')
        return '\n'.join(xml)
    res = '\n'.join(df.apply(row_to_xml, axis=1))
    
    if filename is None:
        return res
    with open(filename, mode) as f:
        f.write(res)

pandas.DataFrame.to_xml = to_xml

#===========================

def test():
    global p
    p = basePlan(name='testPlan')
    p.advance()
    p.advance()
    p.advance()
