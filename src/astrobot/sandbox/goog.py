"""goog.py - process Google search requests

Usage:
links = getgooglelinks('python','http://www.stackoverflow.com/')
for link in links:
       print link
"""

import urllib2

def getgoogleurl(search,siteurl=False):
    """form a google search url string
    """
    if siteurl==False:
        return 'http://www.google.com/search?q='+urllib2.quote(search)
    else:
        return 'http://www.google.com/search?q=site:'+urllib2.quote(siteurl)+'%20'+urllib2.quote(search)

def getgooglelinks(search,siteurl=False):
   """return a list of links given a search string
   """
   #google returns 403 without user agent
   headers = {'User-agent':'Mozilla/11.0'}
   req = urllib2.Request(getgoogleurl(search,siteurl),None,headers)
   site = urllib2.urlopen(req)
   data = site.read()
   site.close()

   #no beatifulsoup because google html is generated with javascript
   start = data.find('<div id="res">')
   end = data.find('<div id="foot">')
   if data[start:end]=='':
      #error, no links to find
      return False
   else:
      links =[]
      data = data[start:end]
      start = 0
      end = 0        
      while start>-1 and end>-1:
          #get only results of the provided site
          if siteurl==False:
            start = data.find('<a href="/url?q=')
          else:
            start = data.find('<a href="/url?q='+str(siteurl))
          data = data[start+len('<a href="/url?q='):]
          end = data.find('&amp;sa=U&amp;ei=')
          if start>-1 and end>-1: 
              link =  urllib2.unquote(data[0:end])
              data = data[end:len(data)]
              if link.find('http')==0:
                  links.append(link)
      return links

# more code snippets for html

import urllib2
from bs4 import BeautifulSoup
import os
import webbrowser

home = os.environ['HOME']

# ==================================================================================
# open Google with a search, write the result to a file, and display in a web browser

search_term = 'cobra'
url = 'http://www.google.com/search?q='+search_term
headers = {'User-agent':'Mozilla/11.0'}
req = urllib2.Request(url,None,headers)
site = urllib2.urlopen(req)
data = site.read()
site.close()
d2 = BeautifulSoup(data)
s = d2.prettify()
filename = os.path.join(home,'text.html')
f = open(filename,'w')
f.write(s.encode('utf-8'))
f.close()
webbrowser.open('file://'+filename)
u = []
for link in d2.find_all('a'):
    u.append(link.get('href'))

u = []
for link in d2.find_all('a'):
    ll = link.get('href')
    if (ll.startswith('/url?q=')):
        ind1 = ll.find('http:')
        ind2 = ll.find('&')
        print ll[ind1:ind2]
        u.append(ll[ind1:ind2])

# =================
# parse an xml file

url = 'http://library.blackboard.com/ref/df5b20ed-ce8d-4428-a595-a0091b23dda3/Content/_admin_server_samples/sample.xml'
headers = {'User-agent':'Mozilla/11.0'}
req = urllib2.Request(url,None,headers)
site = urllib2.urlopen(req)
data = site.read()
site.close()
soup = BeautifulSoup(data,'xml')
print soup.prettify()

