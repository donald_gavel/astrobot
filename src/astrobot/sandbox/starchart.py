"""Query star catalogs for star chart near object of interest
"""
import os
import sys
import argparse
from astrobot import FlexConfigParser

if __name__ == '__main__':
    # Set some defaults
    config_defaults = {}
    
    # Read a config file (overriding defaults)
    cp = FlexConfigParser.FlexConfigParser(config_defaults)
    cp.read('starchart.cfg')
    config = cp.as_dict('starchart')
    
    # Parse the arguments (overriding config file and defaults)
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-v','--verbose',help='increase output verbosity',default=0,action='count')
    parser.add_argument('--object',help='name of object',type=str,default='M31')
    parser.add_argument('--radius',help='cone search radius, in degrees',type=float,default= 0.1)
    parser.add_argument('--cat', help='catalog to search',type=str,default='ask')
    parser.add_argument('--out', help='file to write results to',type=str,default='NONE')
    parser.add_argument('-d','--demo',help='do the plot demo - galactic center @ 21 microns',action='store_true')
    
    if 'running_jupyter' in globals():
        args = parser.parse_args(j_args)
    else:
        args = parser.parse_args()
    
    # override mechanism
    #print args.__dict__
    for key in args.__dict__:
        if args.__dict__[key] is not None:
            config[key] = args.__dict__[key]
    #print config
    verbose = config['verbose'] > 0

############################################
import warnings
import astropy
from astropy.vo.client import conesearch
from astropy.coordinates import SkyCoord
from astropy import units as u

def get_database_info():
    """Retrieve and load in the most recent 'consearch_good.json' file
    with a dictionary of available database information
    """
    global db
    db = astropy.vo.client.vos_catalog.get_remote_catalog_db('conesearch_good')
    cat_names = db.list_catalogs()
    cat_urls = db.list_catalogs_by_url()
    db_info = {}
    for name,url in zip(cat_names,cat_urls):
        db_info[name] = url
    return db_info

def get_object_cone(object_name,catalog='ask',save_file=False):
    """Simple interface for doing a cone search on a given catalog
    """
    global c, my_catname, result, ra,dec,j,h,k
    
    radius = config['radius']
    radius_limit = 0.15
    if radius > radius_limit:
        raise Exception, 'specified radius %r larger than upper limit %r'%(radius,radius_limit)

    c = SkyCoord.from_name(object_name)
    if verbose:
        print 'object: %s'%object_name
        print 'RA, Dec: %r %r'%(c.ra, c.dec)
    
    db_info = get_database_info()
    cats = db_info.keys()
    
    if catalog=='default':
        cats = ['Two Micron All Sky Survey (2MASS) 1']
    elif catalog=='ask':
        #cats = conesearch.list_catalogs()
        for num,cat in zip(range(1,len(cats)+1),cats):
            print '%d) %s'%(num,cat)
        ans = raw_input('pick one #: ')
        assert int(ans) in range(1,len(cats)+1)
        cats = [cats[int(ans)-1]]
    else: # find catalog by partial string match
        cats = conesearch.list_catalogs(pattern = catalog)
        if len(cats) != 0:
            if verbose:
                print 'using catalogs %r'%cats
        else:
            raise Exception,'catalog %s not found in avaiable catalogs'%catalog
    
    if verbose:
        print 'looking up in catalogs:'
        for base in cats:
            print '%r'%base
        sys.stdout.flush()
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        result = conesearch.conesearch(c, radius * u.degree, catalog_db=cats,verbose=verbose,cache=True)
        
    print 'matching url is %s'%result.url

    if verbose:
        print 'number of objects found: %r'%result.array.size
    
    # ra = result.array['ra']
    # dec = result.array['dec']
    # j = result.array['j_m']
    # h = result.array['h_m']
    # k = result.array['k_m']
    
    if config['verbose'] > 1:
        print 'headers:'
        print result.array.dtype.names
        print 'data:'
        print result
    
    if save_file:
        if isinstance(save_file,str):
            fileName = save_file
        else:
            fileName = 'conesearch_results.dat'
        result_table = result.to_table()
        base,ext = os.path.splitext(fileName)
        fmt_d = {'.dat':'ascii',
                 '.csv':'ascii.csv',
                 '.fits':'fits',
                 '.tex':'ascii.latex',
                 '.html':'ascii.html',
                 '.hdf5':'hdf5'}
        assert ext in fmt_d.keys()
        if ext == '.hdf5':
            result_table.write(fileName,format='hdf5',path='data')
        else:
            result_table.write(fileName,format=fmt_d[ext])
        if verbose:
            print 'wrote results to file %s'%fileName

# from http://wcsaxes.readthedocs.io/en/latest/getting_started.html
from astropy.wcs import WCS
# from wcsaxes import datasets <------------ ***fails*** needs: from astropy.coordinates.matrix_utilities import rotation_matrix -- problem with wcsaxes
from astropy.visualization.wcsaxes.tests import datasets
from astropy.io import fits
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np

def do_plot(figsize=(10,10)):
    global hdu,wcs,fig,ax
    hdu = datasets.fetch_msx_hdu()
    wcs = WCS(hdu.header)
    fig = plt.figure(figsize=figsize)
    ax = fig.add_axes([0.15,0.1,0.75,0.75],projection=wcs)
    ax.set_xlim(-0.5, hdu.data.shape[1] - 0.5)
    ax.set_ylim(-0.5, hdu.data.shape[0] - 0.5)
    ax.set_autoscale_on(False)

def decorate_plot(bolo=False,interpolation='bilinear'):
    """see
    http://wcsaxes.readthedocs.io/en/latest/index.html
    http://matplotlib.org/examples/images_contours_and_fields/interpolation_methods.html
    http://data.astropy.org/
    """
    global lon,lat,hdub
    ax.imshow(hdu.data, vmin=-2.e-5, vmax=2.e-4, cmap=plt.cm.gist_heat,origin='lower',interpolation=interpolation)
    if bolo:
        hdub = datasets.fetch_bolocam_hdu()
        ax.contour(hdub.data,transform=ax.get_transform(WCS(hdub.header)),levels=[1,2,3,4,5,6],colors='white')
    else:
        ax.contour(hdu.data, levels=np.logspace(-4.7, -3., 10), colors='white', alpha=0.5)
    ax.set_autoscale_on(False)
    r = Rectangle((30., 50.), 60., 50., edgecolor='white', facecolor='none')
    ax.add_patch(r)
    ax.scatter([40, 100, 130], [30, 130, 60], s=100, edgecolor='white', facecolor=(1, 1, 1, 0.5))
    ax.coords.grid(color='yellow')
    lon = ax.coords['glon']
    lat = ax.coords['glat']
    lat.set_axislabel(r'${\rm G}_{LAT}$',minpad=1.,fontsize=24)
    lon.set_axislabel(r'${\rm G}_{LON}$',minpad=1.,fontsize=24)
    lon.set_ticks(color='white')
    lat.set_ticks(color='white')
    lat.display_minor_ticks(True)
    lon.display_minor_ticks(True)
    
    overlay = ax.get_coords_overlay('fk5')
    overlay['ra'].set_ticks(color='white')
    overlay['dec'].set_ticks(color='white')
    overlay['ra'].set_axislabel(r'$\alpha$ Right Ascension',fontsize=16)
    overlay['dec'].set_axislabel(r'$\delta$ Declination',fontsize=16)
    overlay.grid(color='white', linestyle='solid',alpha=0.5)
    
    if bolo:
        plt.title(r'Galactic Center at $\lambda=21\mu m$, with Bolocam contours',y=1.1,fontsize=22)
    else:
        plt.title(r'Galactic Center at $\lambda=21\mu m$',fontsize=22,y=1.1)

if __name__ == '__main__':
    if config['demo']:
        if sys.flags.interactive:
            print 'interactive mode, type exit() to quit'
            plt.ion()
        else:
            print 'non interactive mode, close window to quit'
        do_plot()
        decorate_plot()
        plt.show()
    else:
        save_file = config['out']
        if save_file == 'NONE':
            save_file = False
        get_object_cone(config['object'],catalog=config['cat'],save_file=save_file)
