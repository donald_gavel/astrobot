"""This code does an example of plotting with the use of world-coordinate-system
Taken from http://docs.astropy.org/en/stable/wcs/#matplotlib-plots-with-correct-wcs-projection
"""
import os
import sys
import argparse
from matplotlib import pyplot as plt
import astropy
from astropy.io import fits
from astropy.wcs import WCS
from astropy.utils.data import download_file
from astropy import units as u
import numpy as np
import warnings

if sys.flags.interactive:
	plt.ion()

valid_images = ['sharcs','neb']
help_str = 'image to display. one of %r'%valid_images

if 'running_jupyter' in globals():
    class Args:
        pass
    args = Args()
    args.image = valid_images
else:
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('image',type=str,help=help_str,nargs='+')

    args = parser.parse_args()

for image_k in args.image:
	if image_k == 'sharcs':
		image_file = 's0362.fits'
	else:
		image_file = 'HorseHead.fits'
		if not os.path.exists(image_file):
			d_file = 'http://data.astropy.org/tutorials/FITS-images/HorseHead.fits'
			image_file = download_file(d_file, cache=True)
	
	hdu = fits.open(image_file)[0]
	with warnings.catch_warnings():
		warnings.simplefilter('ignore',category=astropy.wcs.FITSFixedWarning)
		wcs = WCS(hdu.header)
	
	fig = plt.figure()
	ax = fig.add_subplot(111, projection=wcs)
	n,m = hdu.data.shape
	ra = ax.coords['ra']
	dec = ax.coords['dec']
	ra.set_major_formatter('hh:mm:ss.ss')
	dec.set_major_formatter('dd:mm:ss')
	ra.set_axislabel('RA')
	dec.set_axislabel('Dec')
	R = wcs.wcs.get_pc()
	a = np.abs(np.arctan2(R[0,1],R[0,0]))
	if a > np.pi/4 and a < 3*np.pi/4.:  # RA is vertical axis, Dec horizontal
		ra.set_ticklabel_position('l')
		ra.set_axislabel_position('l')
		dec.set_ticklabel_position('b')
		dec.set_axislabel_position('b')
	else:
		pass
		
	if image_file == 's0362.fits':
		dec.set_ticks(spacing=10*u.arcsec,color='white')
		ra.set_ticks(spacing=15*u.arcsec,color='white')
	
	ra.display_minor_ticks(True)
	ra.set_minor_frequency(5)
	dec.display_minor_ticks(True)
	dec.set_minor_frequency(5)
	dec.grid(color='white',linestyle='dashed')
	ra.grid(color='white',linestyle='dashed')
	data = np.clip(hdu.data,1.e-6,np.inf)
	plt.imshow(np.log10(data), origin='lower', cmap='gray')
	plt.draw()

plt.show()

