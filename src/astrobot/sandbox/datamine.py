'''
Data-mine the Observatory database archives
1. Create summary database(s) of both image and telemetry files
2. Determine when a specific observer observed
3. Auto-analyze data
'''

import os
import sys
from astrobot.oprint import pprint
import pandas as pd
import numpy as np
import h5py
import requests
from bs4 import BeautifulSoup
from urlparse import urlsplit, urljoin
import re
import pyfits
from collections import OrderedDict
from cStringIO import StringIO
import time
import sqlalchemy as sa
import tqdm

pd.options.display.width = 200
pd.options.display.max_colwidth = 80

def excepthook(type,value,traceback):
    print(value)

class DataBase(object):
    '''A container for a pandas DataFrame and metadata
    '''
    def __init__(self,df,col_expl,meta=None):
        '''create a database with a pandas DataFram,
        columns explanation, and metadata
        
        :param str name: the name of the database
        :param DataFrame df: the DataFrame with the bulk of the database data
        :param Series col_expl: the Series with column explanations (each a string)
        :param Series meta: an optional additional Series with string entires
        '''
        assert isinstance(df,pd.DataFrame)
        assert isinstance(col_expl,pd.Series)
        assert isinstance(meta,(pd.Series,type(None)))
        self.df = df
        self.col_expl = col_expl
        self.meta = meta

    def pprint(self):
        '''pretty print
        '''
        pprint(self)
        
    def HDFSave(self,hdfFile,name):
        '''store the DataBase in an HDF5 file
        
        :param str hdfFile: the HDF filename
        :param str name: the group name
        '''
        store = pd.HDFStore(hdfFile)
        store[name] = self.df
        store['%s/col_expl'%name] = self.col_expl
        if self.meta is not None:
            store['%s/meta'%name] = self.meta
        store.close()
    
    @classmethod
    def HDFRead(cls,fileName,name):
        '''read the hd5 file and return a DataBase
        
        :param str fileName: the name of the HDF file
        :param str name: the name of the database group within the HDF file
        '''
        store = pd.HDFStore(fileName)
        df = store[name]
        col_expl = store['%s/col_expl'%name]
        try:
            meta = store['%s/meta'%name]
        except:
            meta = None
        store.close()
        return DataBase(df,col_expl,meta)
    
    @classmethod
    def HDFMeta(cls,fileName,name):
        '''read the hd5 file and return the header info
        
        :param str fileName: the name of the HDF file
        :param str name: the name of the database group within the HDF file
        '''
        store = pd.HDFStore(fileName)
        try:
            meta = store['%s/meta'%name]
        except:
            meta = None
        store.close()
        return meta
        
class WebDB(object):
    '''A class that assists in getting data from a web database of fits files
    '''
    def __init__(self,auth=None):
        if auth is None:
            self.auth = {'d.gavel':('d.gavel','dgavel'),
                         'Don.Gavel':('d.gavel','dgavel'),
                         'public':'',}
        else:
            self.auth = auth

    def pprint(self):
        pprint(self)
    
    def _authorizer(self,auth):
        '''returns an authorization tuple given a user name
        (if the input is a tuple, it is simply returned without change)
        '''
        if isinstance(auth,(str,unicode)):
            if auth != '':
                auth = self.auth[auth]
        return auth
    
    def download(self,url,auth=''):
        '''retrieve the entire fits file stored at url.
        
        :param str url: web address of the fits file, either full ('https://...') or tail without the base name ('yyyy-mm/dd/...')
        :param tuple auth: authorization tuple or just a user name
        :rtype: pyfits.HDUlist
        
        There is an assumption that it is a fits file.
        Return the data as a pyfits HDUlist.
        '''
        if not url.startswith('https:'):
            url = os.path.join(self.baseUrl,url)
        auth = self._authorizer(auth)
        filename = os.path.split(url)[1]
        ext = os.path.splitext(filename)[1]
        r = requests.get(url,auth=auth)
        f = StringIO(r.content)
        r.connection.close()
        hdulist = pyfits.open(f)
        return hdulist
    
    def get_hdr(self,url,auth='',verbose=False):
        '''retrieve the header only from a fits file stored at url

        :param str url: web address of the fits file, either full ('https://...') or tail without the base name ('yyyy-mm/dd/...')
        :param tuple auth: authorization 2-tuple, or just a user name
        :rtype: pyfits.Header
        '''
        if not url.startswith('https:'):
            url = os.path.join(self.baseUrl,url)
        auth = self._authorizer(auth)
        filename = os.path.split(url)[1]
        ext = os.path.splitext(filename)[1]
        endcard = 'END'+' '*(80-3)
        n_blocks_max = 10
        fits_block_size = 2880
        n_blocks_per_chunk = 1 # experiment with this for download speed
        chunk_size = fits_block_size*n_blocks_per_chunk

        go = True
        n_blocks = 0
        cards = []

        r = requests.get(url,auth=auth,stream=True)

        if r.status_code != 200:
            err_str = '<get_hdr> failure to open url %s, status code %d'%(url,r.status_code)
            r.connection.close()
            print err_str
            return False
        while go and n_blocks < n_blocks_max:
            if verbose:
                print '<get_hdr> chunk %d'%(n_blocks/n_blocks_per_chunk)
            b_chunk = r.raw.read(chunk_size)
            n_blocks += n_blocks_per_chunk
            bcc = [b_chunk[i:i+80] for i in range(0,len(b_chunk),80)]
            if len([x for x in bcc if x == endcard]) > 0: go = False
            cards += bcc    
        
        r.connection.close()
        
        hdr = pyfits.Header()
        for card in cards:
            hdr.append(pyfits.Card.fromstring(card),end=True)

        return hdr

    def hdr_db(self,urllist,auth=''):
        '''get the header from each file in the urllist
        and return a DataBase of header info
        
        :param list urllist: a list of the fits file urls
        :return:  a :class:`DataBase` object containing: 'df' a pandas DataFrame with one row per file and an entry for each keyword in the header,
        and 'col_expl' a dictionary with the keyword explanations (gleaned from the comment sections of the fits cards)
        '''
        auth = self._authorizer(auth)
        remkeys = ['','COMMENT','END']
        dset = []
        urlbase_list = []
        col_expl = OrderedDict({})
        for url in urllist:
            urlbase,filename = os.path.split(url)
            hdr = self.get_hdr(url,auth=auth)
            if hdr == False:
                continue
            for key in remkeys:
                if key in hdr:
                    del hdr[key]
            d = OrderedDict(hdr)
            col_expl.update([(c.keyword,c.comment) for c in hdr.cards])
            d['fileName'] = filename
            d['downloadURL'] = url
            dset.append(d)
            urlbase_list.append(urlbase)
        
        if len(dset) == 0:
            raise Exception,'no headers were read'
            
        df = pd.DataFrame(dset).set_index('fileName')
        dfe = pd.Series(col_expl)
        if np.array([x == urlbase_list[0] for x in urlbase_list]).all():
            meta = pd.Series({'urlbase':urlbase_list[0]})
        else:
            meta = None
        return DataBase(df,dfe,meta)
    
class LickWebDB(WebDB):
    '''Tools to assist in mining the Lick Observatory web data archive.
    Creates databases of available archive data files and their header
    information.
    '''
    def __init__(self,auth=None):
        super(self.__class__,self).__init__(auth=auth)
        self.baseUrl='https://mthamilton.ucolick.org/data'
        self.nodes = 'https://site/subsite/year-month/day/instrument/user/file'
        self.nodes = self.nodes.split('/')
        self.user_node_index = self.nodes.index('user') - len(self.nodes)
        self.site = 'mthamilton.ucolick.org'
        self.subsite = 'data'
        self.years = range(2013,time.localtime().tm_year+1)
        self.instruments = ['AO','AOtelemetry','AOsample']
        self.users = ['d.gavel','Don.Gavel','public']
        self.fits_suffix = 'fits'
    
    def crawl(self,url,**kwargs):
        '''crawl the data archive recursively, returning a list of nodes visited.
        Assumes the Lick archive structure:
        https://mthamilton.ucolick.org/data/yyyy-mm/dd/instrument/user/datafile
        
        :param str url: root url. This can start at any level
        :param bool crawl_files: include all the filenames at the leaves
        
        url is a string with format:
           urlhead/yyyy-mm[/dd[/instrument[/user]]] -or-
           yyyy-mm[/dd[/instrument[/user]]] (urlhead will default) -or-
           yyyy (the whole year will be searched)
           '' (empty string - whole data archive will be searched)
        kwargs can be:
            year (e.g. 2017), month (e.g. 11), instrument ('AO'), user ('public')
            or lists of such. This will limit the crawl accordingly.
            crawl_files: if True, the data file names at the file level will be gathered
        
        Returns a list of tuples: (URL visited, number of files at URL)
        '''
        if bool(re.match('^\d{4}$',url)): #  the whole year
            links = ['%s-%02d'%(url,k) for k in range(1,13)] # forms yyyy-mm url list
            r_list = []
            for link in links:
                r_list += self.crawl(link,**kwargs) # crawl through every month
            return r_list
            
        if not url.startswith('https:'):
            url = os.path.join(self.baseUrl,url)
        kkeys = ['year','month','instrument','user']
        filt = {}
        if 'crawl_files' in kwargs:
            crawl_files = filt['crawl_files'] = kwargs['crawl_files']
        else:
            crawl_files = False
        for kkey in kkeys:
            if kkey in kwargs:
                if isinstance(kwargs[kkey],list):
                    filt[kkey] = kwargs[kkey]
                else:
                    filt[kkey] = [kwargs[kkey]]
        
        nodes = url.rstrip('/').split('/')
        node_types = self.nodes
        node_type = node_types[len(nodes)-1]
        page = nodes[-1]
        r_list = [(url,)]
        if node_type == 'year-month':
            if not bool(re.match('^\d{4}-\d{2}$',page)): # dddd-mm
                print 'year-month %s doesnt match yyyy-mm format'%page
                print '    url = %s'%url
                return r_list
            year,month = page.split('-')
            if 'year' in filt:
                if year not in filt['year']:
                    return r_list
            if 'month' in filt:
                if month not in filt['month']:
                    return r_list
        elif node_type == 'day':
            if not bool(re.match('^\d{2}$',page)): # dd
                print 'day %s doesnt match dd format'%page
                return r_list
            if 'day' in filt:
                if page not in filt['day']:
                    return r_list
        elif node_type == 'instrument':
            if 'instrument' in filt:
                if page not in filt['instrument']:
                    return r_list
        elif node_type == 'user':
            if 'user' in filt:
                if page not in filt['user']:
                    return r_list
        
        # if target is an instrument, limit to a defined set
        if node_type == 'instrument':
            if page not in self.instruments:
                return r_list
        # if target is a user, use the auth
        if node_type == 'user':
            user = page
            if user not in self.auth:
                print 'user %s not in authorizations'%user
                return r_list
            else:
                auth = self.auth[user]
            
        # if target is a user, limit to a subset
        #   and drill no further (or trigger the header download)
        
        if node_type == 'user':
            r = requests.get(url,auth=auth)
        else:
            r = requests.get(url)

        if r.status_code != 200:
            print 'cannot enter %s, status code %d'%(url,r.status_code)
            r.connection.close()
            return r_list
        else:
            print 'visiting %s page %s'%(node_type,url)
        soup = BeautifulSoup(r.text,'lxml')
        r.connection.close()
        links = soup.find_all('a')
        # go down only relative links
        links = [x for x in links if x.get('href').startswith('./')]
        links = [x.get('href').strip('./') for x in links]
        r_list = [(url,len(links))]
        if node_type != 'user':
            for link in links:
                r_list += self.crawl(os.path.join(url,link),**filt)
        if node_type == 'user' and crawl_files:
            flist, infodict = self.hdr_db(url,filenames_only=True)
            flist = [(fn,0) for fn in flist]
            r_list += flist

        return r_list
    
    def db_from_urllist(self, urllist):
        '''create a pandas database from a list of (url,file_count) tuples
        
        :param list urllist: list of (url,file_count) tuples (typically the output of :method:`crawl`)
        
        Returns a pandas DataFrame
        '''
        cols = ['level','year','month','day','instrument','user','n_files','filename','url']
        rows = []
        for i,item in enumerate(urllist):
            if isinstance(item,(str,unicode)):
                url = item
                count = 0
                print '%d string'%i
            elif isinstance(item,tuple) and len(item) != 2:
                url = item[0]
                count = 0
                print '%d 1-tuple'%i
            elif isinstance(item,tuple) and len(item)==2:
                url,count = item
                print '%d 2-tuple'%i
            else:
                print '%d something else: %r'%(i,type(item))
                continue
            nodes = url.rstrip('/').split('/')
            n = len(nodes)
            node_type = self.nodes[n-1]
            year,month,day,instrument,user,filename = (None,)*6
            ind = self.nodes.index('year-month')
            if ind < n: year,month = nodes[ind].split('-')
            ind = self.nodes.index('day')
            if ind < n: day = nodes[ind]
            ind = self.nodes.index('instrument')
            if ind < n: instrument = nodes[ind]
            ind = self.nodes.index('user')
            if ind < n: user = nodes[ind]
            ind = self.nodes.index('file')
            if ind < n: filename = nodes[ind]
            row = [node_type,year,month,day,instrument,user,count,filename,url]
            rows.append(row)
        db = pd.DataFrame(rows,columns=cols)
        return db
        
    def download(self,url,auth=''):
        '''retrieve the entire fits file stored at url.
        
        :param str url: web address of the fits file, either full ('https://...') or tail without the base name ('yyyy-mm/dd/...')
        :param tuple auth: authorization tuple or just a user name
        :rtype: pyfits.HDUlist
        
        There is an assumption that it is a fits file.
        Return the data as a pyfits HDUlist.
        '''
        if auth == '':
            nodes = url.split('/')
            auth = nodes[self.user_node_index]
        return super(self.__class__,self).download(url,auth=auth)
    
    def get_hdr(self,url,auth=''):
        '''retrieve the header only from a fits file stored at url

        :param str url: web address of the fits file, either full ('https://...') or tail without the base name ('yyyy-mm/dd/...')
        :param tuple auth: authorization 2-tuple, or just a user name
        :rtype: pyfits.Header
        '''
        if auth == '':
            nodes = url.split('/')
            auth = nodes[self.user_node_index]
        return super(self.__class__,self).get_hdr(url,auth=auth)        
        
    def hdr_db(self,url,filenames_only=False,verbose=True,subset=None):
        '''create a header information database for all the fits files at the url.
        The url is at the lowest ("user") level of the data archive tree:
        https://mthamilton.ucolick.org/data/yyyy-mm/dd/instrument/user

        :param str url: the url of the web page that has links to fits files ("user" level)
        :param bool filenames_only: option to return only a list of fits file names
        :param bool verbose: verbose printout while surfing
        :param list subset: optional list of the subset of files to be read. (otherwise read them all)
        :return: a :class:`DataBase` of the fits files header info
        :rtype: :class:`DataBase`
        '''
        if not url.startswith('https:'):
            url = os.path.join(self.baseUrl,url)
        nodes = url.split('/')
        # url must be at the user level
        assert self.nodes.index('user') == len(nodes)-1

        meta = OrderedDict(zip(self.nodes[2:],nodes[2:]))
        meta['date'] = '%s-%s'%(meta['year-month'],meta['day'])
        meta['url'] = url
        
        user = nodes[self.nodes.index('user')]
        auth = self.auth[user]
        if verbose:
            print '<hdr_db> getting web page %s'%url
            sys.stdout.flush()
        r = requests.get(url,auth=auth)
        if r.status_code != 200:
            print '<hdr_db> cannot open %s, response code %d'%(url,r.status_code)
            return
        if verbose:
            print '<hdr_db> parsing the web page...',
            sys.stdout.flush()
        soup = BeautifulSoup(r.text,'lxml')
        r.connection.close()
        rows = soup.find_all('table')[1].find_all('tr')
        meta['nfiles'] = len(rows)-1
        
        url_list = []
        filename_list = []
        iterator = rows[1:]
        if verbose:
            print 'done'
            print '<hdr_db> number of files: %d'%len(iterator)
            sys.stdout.flush()

        for row in iterator:
            cols = row.find_all('td')
            filename = cols[1].text
            if subset is not None:
                if filename not in subset:
                    continue
            url_list.append(os.path.join(url,filename))
            filename_list.append(filename)
            
        meta['files'] = filename_list
        if filenames_only:
            return url_list, meta
        
        db = super(self.__class__,self).hdr_db(url_list)
        db.meta = meta
        return db
        
#----------- generate SQL databases ----------
db_filename = 'sqlite.db'

def init():
    global dm, sql_engine
    dm = DataMine()
    sql_engine = sa.create_engine('sqlite:///%s'%db_filename)

def db_info():
    sq = 'select * from sqlite_master where type = "table";'
    print pd.read_sql_query(sq,sql_engine)

def make_db_files():
    url_list = dm.crawl('2016')
    db2016 = dm.db_from_urllist(url_list)
    url_list = dm.crawl('2017')
    db2017 = dm.db_from_urllist(url_list)
    sql_engine = sa.create_engine('sqlite:///%s'%db_filename)
    db2016.to_sql('db_files',sql_engine)
    db2017.to_sql('db_files',sql_engine,if_exists='append')

def user_files_filter(user='Don.Gavel'):
    '''get a list of the fits files belonging to
    a specific user
    '''
    cols = ['year','month','day','instrument','n_files','url']
    tbl_name = 'db_files'
    conditions = ['user = "%s"'%user,
                  'n_files > 0']
    s = 'select %s from %s where %s;'%(
        ','.join(cols),
        tbl_name,
        ' and '.join(conditions)
        )
    
    db_user = pd.read_sql_query(s,sql_engine)
    u = db_user['url'].str.split('/')
    u = ['/'.join(x[-4:]) for x in u]
    db_user['url'] = u
    return db_user

def dont_run_this_archive_imager_headers_to_SQL():
    '''an example of scraping all the archived fits
    file headers and stuffing them into a SQL database table.
    8151 rows (fits files from 2016-2017)
    '''
    tbl_name = 'image_hdrs'
    db_user = user_files_filter()  # this gives a SQL engine
    url_list = zip(db_user['url'],db_user['n_files'])
    hdrs_to_sql(url_list,tbl_name)

def hdrs_to_sql(url_list,tbl_name):
    '''take every user-level url in the url_list,
    which each have a list of fits files,
    scrape those files' headers and load them into the SQL database
    '''
    for url,n_files in url_list:
        print '<hdrs_to_sql> %s (%d files)'%(url,n_files)
        dbf = dm.hdr_db(url)
        ym,d,inst,user = url.split('/')
        date = '%s-%s'%(ym,d)
        dbf['NIGHT-OBS'] = date
        dbf['INSTRUMENT'] = inst
        dbf['USER'] = user
        dbf['URL'] = url
        dbf.to_sql(tbl_name, sql_engine, if_exists='append')

def dont_run_this_archive_telem_tt_headers_to_sql():
    '''scape all the archived fits headers from
    telemetry and tip/tilt files and save to tables
    in the SQL database
    '''
    global db_telem, db_tiptilt

    db_pub = user_files_filter(user='public')
    db_telem = db_pub[db_pub['instrument']=='AOtelemetry']
    db_tiptilt = db_pub[db_pub['instrument']=='AOsample']

    tbl_name = 'telem_hdrs'
    url_list = zip(db_telem['url'],db_telem['n_files'])
    hdrs_to_sql(url_list,tbl_name)

    tbl_name = 'tiptilt_hdrs'
    url_list = zip(db_tiptilt['url'],db_tiptilt['n_files'])
    hdrs_to_sql(url_list,tbl_name)

#----------- daily maintain SQL databases ----------

# on a daily time schdule:
# ------------------------
# check the archive at yyyy-mm/dd with yesterday's date
# crawl this new branch to get a list of new files
# append new files to the db_files SQL table
# scrape headers of the new files
# append these to the image_hdrs, telem_hdrs, and tiptilt_hdrs
# SQL tables as appropriate
# make a log of these operations
# if anything fails, send a text message or email

#------------- tests --------------

def test0():
    '''check the DataBase object works
    '''
    global df,q
    
    arr = np.random.normal(size=(10,3))
    cols = ['A','B','C']
    df = pd.DataFrame(arr,columns = cols)
    print '<test0> original data:\n',df
    col_expl = ['list of A','bunch of B','some C mixed in']
    col_expl = OrderedDict(zip(cols,col_expl))
    col_expl = pd.Series(col_expl)
    meta = OrderedDict((('name','test dataset'),
                        ('author','datamine.test0')))
    meta = pd.Series(meta)
    db = DataBase(df,col_expl,meta)
    db.HDFSave('test.h5','test0')
    print '<test0> read meta from HDF file\n',DataBase.HDFMeta('test.h5','test0')
    q = DataBase.HDFRead('test.h5','test0')
    print '<test0> read back DataBase from HDF file'
    print q
    print '<test0> q.df:\n',q.df
    print '<test0> q.col_expl:\n',q.col_expl
    print '<test0> q.meta:\n',q.meta
    print '<test0> original and read back equal?'
    return (df == q.df).all().all()
    
def test1(enabled=False):
    '''test the web crawler
    '''
    global dm, db
    
    dm = LickWebDB()
    if enabled:
        url_list = dm.crawl('2014-08/11',crawl_files=True)
        db = dm.db_from_urllist(url_list)

def test2():
    '''test the ability to download headers
    '''
    global hdr
    dm = LickWebDB()
    hdr = dm.get_hdr('2014-08/11/AO/Don.Gavel/s0002.fits')
    
def test3():
    '''test we can get a list of files from the user level web page
    '''
    global dm, flist,infodict
    
    dm = LickWebDB()
    flist,infodict = dm.hdr_db('2014-08/11/AO/Don.Gavel',filenames_only=True)
    pprint(infodict)

def test4(auth = 'Don.Gavel'):
    '''test that a header database can be formed from a list of URLs of fits files
    '''
    global dbf
    
    dm = WebDB()
    url = 'https://mthamilton.ucolick.org/data/2014-08/11/AO/Don.Gavel'
    url_list = ['s0002.fits','s0003.fits']
    url_list = [os.path.join(url,x) for x in url_list]
    pprint(url_list)
    dbf = dm.hdr_db(url_list,auth=auth)
    
def test5():
    '''check that HDF5 reading works
    '''
    store = pd.HDFStore('store.h5')
    print store
    for key in store.keys():
        print key
    store.close()

'''
db_user (user='Don.Gavel'):
    year month day  n_files                      url
0   2016    01  02       19  2016-01/02/AO/Don.Gavel
1   2016    01  03       45  2016-01/03/AO/Don.Gavel
2   2016    01  21      189  2016-01/21/AO/Don.Gavel
3   2016    01  22       79  2016-01/22/AO/Don.Gavel
4   2016    01  23       50  2016-01/23/AO/Don.Gavel
5   2016    03  22      340  2016-03/22/AO/Don.Gavel
6   2016    04  21       84  2016-04/21/AO/Don.Gavel
7   2016    04  22      153  2016-04/22/AO/Don.Gavel
8   2016    05  23       21  2016-05/23/AO/Don.Gavel
9   2016    05  24     1548  2016-05/24/AO/Don.Gavel
10  2016    06  18      233  2016-06/18/AO/Don.Gavel
11  2016    06  19      168  2016-06/19/AO/Don.Gavel
12  2016    07  08      434  2016-07/08/AO/Don.Gavel
13  2016    07  09      558  2016-07/09/AO/Don.Gavel
14  2016    11  27       38  2016-11/27/AO/Don.Gavel
15  2016    11  28       35  2016-11/28/AO/Don.Gavel
16  2016    12  16      388  2016-12/16/AO/Don.Gavel
17  2016    12  17      623  2016-12/17/AO/Don.Gavel
18  2017    01  10      108  2017-01/10/AO/Don.Gavel
19  2017    01  13      185  2017-01/13/AO/Don.Gavel
20  2017    03  14      282  2017-03/14/AO/Don.Gavel
21  2017    06  10      123  2017-06/10/AO/Don.Gavel
22  2017    06  11      129  2017-06/11/AO/Don.Gavel
23  2017    07  09      303  2017-07/09/AO/Don.Gavel
24  2017    07  10       78  2017-07/10/AO/Don.Gavel
25  2017    07  11     1361  2017-07/11/AO/Don.Gavel
26  2017    07  12       22  2017-07/12/AO/Don.Gavel
27  2017    09  03      270  2017-09/03/AO/Don.Gavel
28  2017    09  04      285  2017-09/04/AO/Don.Gavel
'''
