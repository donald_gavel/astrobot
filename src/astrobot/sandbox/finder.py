'''Finder supports searching disk directories for fits files associated with ShaneAO
and making pandas databases of the files and their fits headers information.

Files databases:
----------------
The most general routine is::

    df = dbFiles(top)

which makes a database of all the
fits files in a directory structure. It can take a directory name argument
(with, optionally, dots and tildes).

Headers databases:
------------------
From a files database, one can sort out sharcs or telemetry files and make
databases containing the fits headers information, ::

    df_sharcs = dbHdrs('sharcs',df)

makes a database from the headers in sharcs files.

::

    df_telem = dbHdrs('telemetry',df)

makes a database fromt the headers in telemetry files.

Dates:
------
Show all the dates where there is telemetry data, then generate a
headers database for a particular date ::

    dates('telemetry')
    dbtelem = dbHdrs('telemetry','2020-11-23') 

Caching:
--------
Search results are cached, so that subsequent searches are much faster. However,
if new data files are added, you will need to refresh the cache ::

    cache_state('stale') # this only applies to the next search, it then gets reset to 'good'
    dbtelem = dbHdrs('telemety','2020-11-23') # a new search

The current observing night's files cache is always stale, so as new files are added
subsequent calls to dbFiles and dbHdrs will automatically regenerate their databases.
'''
import os
import pandas as pd
import numpy as np
import datetime
import glob
import re
import json
import hashlib
from collections import deque
from tqdm import tqdm as progbar
import warnings
import astropy.units as u
from astropy.io import fits

from astrobot import q_helper
from astrobot import oprint
from astrobot.sharcs import Sharcs
from astrobot.tcolor import tcolor

try:
    utf_enabled = 'utf' in os.environ['LANG'].lower()
except:
    utf_enabled = False
progbar_disabled = True
pd.options.display.min_rows = 30
pd.options.display.max_colwidth = 30
    
home = os.path.expanduser('~')
workingDir = os.path.abspath('.')
realAtUcolickDataDir = '/net/real/local/data'
cache_path = os.path.join(home,'.astrobot')

dataVols = deque([
    '/Users/donaldgavel/data_lick/LittleWDdrive_backup_11-6-2015/ShaneAO_Data',
    '/Volumes/External 2TB Drive #1/LittleWDdrive_backup_11-6-2015/ShaneAO_Data',
    '/Volumes/External 2TB Drive #1/ShaneAO_Data',
    '/Volumes/External 2TB Drive #2/ShaneAO_Data',
    '/Volumes/LittleWDdrive/ShaneAO_Data',
    os.path.join(home,'telemetry'),
    os.path.join(home,'data'),
    os.path.join(home,'shaneao_data'),
    os.path.join(home,'ShaneAO/data'),
    '/Volumes/Donalds-Mac-Pro/donaldgavel/data',
    os.path.join(realAtUcolickDataDir),
    ])

default_dataVols = dataVols.copy()

'''
from contextlib import contextmanager
@contextmanager
def Vols(tempVols):
    global dataVols
    dataVols = tempVols
    try:
        yield True
    finally:
        dataVols = default_dataVols.copy()
'''

class Vols(object):
    '''context manager
    with finder.Vols(local_dataVols):
        finder.dbFiles(dir)
    see: https://book.pythontips.com/en/latest/context_managers.html
    '''
    def __init__(self,tempVols):
        global dataVols
        dataVols = tempVols
    def __enter__(self):
        return
    def __exit__(self,t,v,tb):
        global dataVols
        dataVols = default_dataVols.copy()
        
# regular expressions for the types of files:
filename_template = {
    'sharcs'  : 's[0-9]{4}.fits',  # Sharcs data files
    'telemetry': 'Data_[0-9]{4}.fits', # Shaneao telemetry data files
    'TTimage' : 'TT_Data_[0-9]{4}.fits', # Shaneao Tip/Tilt sensor image files
    'WFSimage': 'WFS_Data_[0-9]{4}.fits', # Shaneao Wavefront sensor image files
}

# default header key sets:
hdr_keys = {
    'sharcs': ['date-obs', 'date-beg', 'time-obs', 'object', 'itime', 'ra', 'dec',
               'ha','airmass','tubangle','loopstat', 'filt1nam', 'filt2nam', 'loopsub',
               'opmode','twtrgain', 'woofgain', 'wfsrate', 'ttrate', 'lasonsky'],
    'telemetry': ['date','time','mode','substate','cent',
                  'loop','rate','gain','wgain','alpha','tweeter_','woofer_b'],
    'TTimage':[],
    'WFSimage':[],
}

sort_key = {
    'sharcs': 'date-beg',
    'telemetry': 'time',
    'TTimage':'',
    'WFSimage':'',
}
   
sample_code = '''
# generic use case -
# find a date and create databases of the telemetry and sharcs file headers from that date
from astrobot.sandbox import finder

finder.dates('telemetry')
dbt = finder.dbHdrs('telemetry','2020-11-23')
finder.dates('sharcs')
dbs = finder.dbHdrs('sharcs','2020-11-23')

# refresh the cache after gathering more data
finder.cache_state('stale')
dbt = finder.dbHdrs('telemetry','2020-11-23')

'''

def findVol(dstype,date,dsn=None,quiet=False):
    '''find the volume that has this specific data set, or data date.
    Return the volume and the path to the data file(s).
        
    Parameters
    ----------
    dstype : str
        One of the data file types: 'sharcs', 'telemetry', 'TTimage', 'WFSimage' or 'all'
    date : str or Timestamp, or datetime.date
        The date associated with the dataset
    dsn : int
        The data set number.
    
    Returns
    -------
    (volume,path) : A tuple of strings
    '''
    types = list(filename_template.keys())
    if dstype == 'all':
        return {x:findVol(x,date,dsn) for x in types}
    if isinstance(dstype,list):
        return {x:findVol(x,date,dsn) for x in dstype}
    if dstype not in types:
        raise ValueError(f'{tcolor.fail}type must be one of {types}{tcolor.end}')
    prefix = filename_template[dstype].split('[')[0]
    
    if isinstance(date,(pd.Timestamp,datetime.datetime)): # Timestamp or datetime.date
        date = str(date.date())
    elif isinstance(date,datetime.date):
        date = str(date)
    elif isinstance(date,np.datetime64):
        date = str(date).split('T')[0]
    elif isinstance(date,str):
        pass
    else:
        raise TypeError(f'{tcolor.fail}date argument must be a string or one of the date types{tcolor.end}')
    
    if isinstance(dsn,str): # assume it is a string (such as a filename) with the data set number imbedded
        r = re.search('\d{4}',dsn)
        if r is None:
            raise ValueError(f'{tcolor.fail}dsn = {dsn} is not a valid dataset designation{tcolor.end}')
        dsn = int(r.group(0))
    
    if not quiet: print(f'<finder.findVol>looking for {date} {dstype} data...',flush=True)
    for vol in list(dataVols):
        #paths = glob.glob(os.path.join(vol,'**/%s'%date),recursive=True)
        ipaths = glob.iglob(os.path.join(vol,f'**/{date}'),recursive=True)
        paths = [x for x in progbar(ipaths,total=10,disable=progbar_disabled,ascii=not utf_enabled)]
        for path in paths:
            if dsn is None:
                files = glob.glob(os.path.join(path,'**/%s*.fits'%prefix),recursive=True)
                if len(files) != 0:
                    path = os.path.dirname(files[0])
                    #print(f'{tcolor.green}<finder.findVol> found {dstype} data for {date}{tcolor.end}')
                    if not quiet: print(f'{tcolor.green}found in {vol}{tcolor.end}')
                    return vol,path
            else:
                files = glob.glob(os.path.join(path,'**/%s%04d.fits'%(prefix,dsn)),recursive=True)
                if len(files) != 0:
                    if not quiet: print(f'{tcolor.green}found in {vol}{tcolor.end}')
                    return vol,files[0]
        dataVols.rotate(-1) # spin the deque. This leaves latest success on top
    
    print(f'{tcolor.warning}<finder.findVol> WARNING did not find {dstype} data for {date} in any of the dataVols{tcolor.end}')
    
    return None,None

def directory_structure(vol='all',dstype='all',date=None):
    f'''determine the structure of the directory system for storing ShaneAO data.
    We assume it is of the form /<pre>/<date>/<post>/<files>, where <date> is
    of the form 'YYYY-mm-dd'
    
    Arguments
    ---------
    dstype : str
        one of the ShaneAO datatypes: {list(filename_template.keys())} or 'all'
    vol : str
        a top level directory path (beginning with '/'). This can be any path above
        the level of <date>.
    date : str
        the date, in form 'YYYY-mm-dd'. If None then the first date found in the
        directory walk will be the one used to set the structure result
    
    Returns
    -------
    dict : dictionary with pre,date,post keys
    In the case where either argument is 'all', then a dictionary is returned,
    with keys of vol and dstype and values the (pre,date,post) tuple
    '''
    assert dstype in filename_template.keys() or dstype == 'all'

    if dstype == 'all' or isinstance(dstype,list):
        dstypes = dstype if isinstance(dstype,list) else filename_template.keys()
        r = {dst: directory_structure(vol=vol,dstype=dst) for dst in dstypes}
        return {key:val for key,val in r.items() if val}
    if vol == 'all' or isinstance(vol,list):
        vols = vol if isinstance(vol,list) else dataVols
        r = {v: directory_structure(vol=v,dstype=dstype) for v in vols}
        return {key:val for key,val in r.items() if val}
        
    fn = filename_template[dstype].split('[')[0] + '*'
    q = glob.iglob(os.path.join(vol,'**','*-*-*','**',fn),recursive=True)
    s = next(q,None)
    if s is None: return {}
    sl = s.split('/')
    pat = '\d{4}-\d{2}-\d{2}'
    k = [k for k,x in enumerate(sl) if re.match(pat,x)][0]
    return {'pre': '/'.join(sl[:k]), 'date':sl[k], 'post':'/'.join(sl[k+1:-1])}

def pp_directory_structure():
    '''pretty-print the directory structures in each volume
    '''
    w = 80
    for dstype in filename_template.keys():
        hdr = f'{dstype}'
        n = (w - len(hdr) - 2)//2
        hdr = ' '*n + hdr
        bling = '='*w
        print(f'\n{tcolor.bold}{bling}\n{hdr}\n{bling}{tcolor.end}')
        for vol in dataVols:
            r = directory_structure(vol=vol,dstype=dstype)
            color = {True:tcolor.green, False:tcolor.fail}[r != {}]
            print(f'{color}{vol}{tcolor.end}')
            if r != {}:
                print(r)

cached_filelist = {}
def monitor(vol=dataVols[-1]):
    '''monitor the volume for additional telemetry data files associated
    with the current observing night
    '''
    fpat = {'sharcs': 's(\d{4}).fits',
            'telemetry': 'Data_(\d{4}).fits',
            'TTimage': 'TT_Data_(\d{4}).fits',
            'WFSimage': 'WFS_Data_(\d{4}).fits'}

    d = directory_structure(vol=vol,dstype='telemetry')
    assert d != {}, f'{tcolor.fail}no data in {vol}{tcolor.end}'
    pre,post = d['pre'],d['post']
    today = obs_night()
    pat = fpat['telemetry']
    path = os.path.join(pre,today,post)
    filelist = os.listdir(path)
    filelist = [x for x in sorted(filelist) if re.match(pat,x)]
    if today not in cached_filelist:
        cached_filelist[today] = deque(filelist)
        return []
    else:
        new_files = [x for x in filelist if x not in cached_filelist[today]]
        if len(new_files) > 0:
            cached_filelist[today].extend(new_files)
            dsn = [int( re.findall(pat,x)[0] ) for x in new_files]
        else:
            dsn = []
    return dsn
    
def dbFiles(top,date='all', dstype='all'):
    ''' Make a database of FITS files in a directory structure
    
    Parameters
    ----------
    top : str or list
        str: the top level directory name - all FITS files in the directory
            and recursively all subdirectories will be included in the result.
        list: a list of file names - element by element
            Each element must include the absolute path.
    date: str
        a date string ('YYYY-mm-dd').
        Only the subdirectories from this date will be included in the result. 
    dstype: str
        (optional) type of the file: 'telemetry', 'sharcs', etc. Defaults to all types.
        
    Returns
    -------
    a pandas.DataFrame with directory, filename, type and date of each file
    '''
    if top == 'all':
        top = dataVols
    if isinstance(top,(list,deque)):
        dbs = [dbFiles(x,date=date,dstype=dstype) for x in top]
        df = pd.concat(dbs).reset_index(drop=True)
        return df
    if isinstance(top,str):
        topdir = top
        topdir = os.path.abspath(os.path.expanduser(topdir))
        #fileList = glob.glob(os.path.join(topdir,'**/*.fits'),recursive=True)
        fli = glob.iglob(os.path.join(topdir,'**/*.fits'),recursive=True)
        fileList = [x for x in progbar(fli,disable=progbar_disabled,ascii=not utf_enabled)]
        fnList = [os.path.split(x)[-1] for x in fileList]
        keys = ['filename','type','dsn','obs_date','file_write_time','path']
        memo = {'function':'dbFiles','topdir':topdir,'keys':keys,'files':fnList,'type':dstype,'date':date}
        
        # try to restore from cache
        df = from_cache(memo)
        if df is not None:
            return df
    else:
        raise Exception(f'{tcolor.fail}first argument must be a string or list{tcolor.end}')
        
    # elif isinstance(top,list):
    #     fileList = top
    #     memo = {'function':'dbFiles','arg':fileList}

    datePattern = re.compile('\d{4}-\d{2}-\d{2}')  # date pattern
    filePatterns = {}
    for key,pat in filename_template.items():
        cpat = re.compile(pat)
        filePatterns[key] = cpat
    
    vals = []
    end = '' if progbar_disabled else '\n'
    print(f'<finder.dbFiles> making a database of {dstype} files in {top}, date = {date}...',end=end,flush=True)
    for fullpath in progbar(fileList,disable=progbar_disabled,ascii=not utf_enabled):
        path,filename = os.path.split(fullpath)
        r = datePattern.findall(path)
        if len(r) > 0:
            #fdate = datetime.datetime.strptime(r[-1],'%Y-%m-%d')
            fdate = r[-1]
        else:
            fdate = ''
        if date not in ['all',fdate]:
            continue
        timestamp = os.path.getmtime(fullpath)
        time = datetime.datetime.fromtimestamp(timestamp)
        for key,cpat in filePatterns.items():
            r = cpat.match(filename)
            if r is not None:
                ftype = key
                break
        else:
            ftype = ''
            
        r = re.search('\d{4}',filename)
        if r is None: dsn = np.nan
        else: dsn = int(r.group(0))
        
        if dstype in [ftype,'all']:
            vals.append( (filename,ftype,dsn,fdate,time,fullpath) )
    if progbar_disabled: print(f'{tcolor.green}done.{tcolor.end}')
    df = pd.DataFrame(columns=keys, data=vals)
    df = set_metadata(df,memo) # df.metadata = memo
    
    # store to cache
    if 'topdir' in memo:
        cache(df)
    
    # because the cache restore does this, we need to do it first time around
    if 'obs_date' in df.columns:
        df['obs_date'] = df['obs_date'].apply(pd.Timestamp)
    if 'time' in df.columns:
        df['time'] = df['time'].apply(pd.Timestamp)
    
    return df

def dbHdrs(dstype,df):
    '''
    Create a database of files of a certain type, with header information
    
    Parameters
    ----------
    dstype : str
        One of the data file types: 'sharcs', 'telemetry', 'TTimage', 'WFSimage'
        
    df : DataFrame or str or date
        DataFrame: The files database, like that produced by a call to dbFiles()
            (files that are not files of the given type will be ignored).
        str: The name of the top level directory. Returns info
            on all the FITS files in this directory and its subdirectories
            (for files of the given type).
        date: The date (can also be a string with a YYYY-MM-DD format)
            In this case, the search is limited to directories with that date.
        
    Returns
    -------
    pandas.DataFrame database of files and file header information
    
    Examples
    --------
    ::
        db = dbHdrs('telemetry','2016-06-19')
        db = dbHdrs('telemetry',dataVols[0])
        db = dbHdrs('telemetry',dbFiles('~/telemetry'))
    '''
    date = None
    if isinstance(df,str):
        r = re.match('^\d{4}-\d{2}-\d{2}$',df)
        if r is not None: # its a date
            date = df
        else: # its a directory name
            q = df = dbFiles(df)
    elif isinstance(df,pd.DataFrame):
        q = df.copy()
    elif isinstance(df,(pd.Timestamp,datetime.datetime)):
        date = str(df.date())
    elif isinstance(df,datetime.date):
        date = str(df)
    elif isinstance(df,np.datetime64):
        date = str(df).split('T')[0]
    if date is not None:
        d = findVol(dstype,date)[1]
        if d is None: return pd.DataFrame()
        if date == obs_night(datetime.datetime.now()):
            cache_state('stale')
        q = df = dbFiles(d)
        
    q = q[q.type == dstype]
    unk = 'unknown'
    keys = hdr_keys[dstype]
    dfr = pd.DataFrame(columns=keys)
    
    memo = {'function':'dbHdrs',
            'type': dstype,
            'keys': keys,
            'files': list(q.path),
            }
    if hasattr(df,'metadata'):
        memo['df.metadata'] = df.metadata

    # try restoring the database from cache
    dfr = from_cache(memo)
    if dfr is not None:
        return dfr
    
    # open every fits file and read the headers.
    # This may take a while, so show a progress bar
    dfr = pd.DataFrame([])
    for qr in progbar(q.itertuples(),disable=progbar_disabled,total=len(q),ascii=not utf_enabled):
        filepath = qr.path
        with fits.open(filepath) as hdu:
            hdr = hdu[0].header
            row = {}
            for key in keys:
                row[key] = hdr.get(key,unk)
                
        dfr = dfr.append(pd.DataFrame([row],index=[qr.Index]))
        
    if dstype == 'telemetry': # convert date/time to timestamp
        dfr['time'] = _timestamp( list(dfr['date']), list(dfr['time']) )
    
    dfr = pd.concat([q,dfr],axis=1)
    
    # make sure column names are unique
    f_cols = ['f_'+col if col in keys else col for col in df.columns]
    dfr.columns = f_cols + keys
    
    # sort and reset the index
    if sort_key[dstype] != '':
        dfr = dfr.sort_values(by=sort_key[dstype]).reset_index(drop=True)
        
    set_metadata(dfr,memo) # dfr.metadata = memo
    
    # cache the result
    cache(dfr)
    
    # because the cache restore does this, we need to do it first time around
    if 'obs_date' in dfr.columns:
        dfr['obs_date'] = dfr['obs_date'].apply(pd.Timestamp)
    if 'time' in df.columns:
        dfr['time'] = dfr['time'].apply(pd.Timestamp)
        
    return dfr

def dates(dstype,vol=None):
    '''return a list of dates when data of a given type were taken.
    
    Parameters
    ----------
    dstype : str
        One of the valid data set types: 'sharcs', 'telemetry', 'TTimage', 'WFSimage'
    vol: str or list
        The volume (or volume list) to check. The default is to check all the dataVols.

    Returns
    -------
    DataFrame : A database of "unique" observation dates. A particular date entry may be
        duplicated if is available in multiple volumes.
    '''
    types = list(filename_template.keys())
    if dstype not in types:
        raise ValueError(f'{tcolor.fail}type must be one of {types}{tcolor.end}')
    if vol is None:
        dataVolsList = dataVols
    elif isinstance(vol,list):
        dataVolsList = vol
    else:
        dataVolsList = [vol]
    r = []
    for vol in dataVolsList:
        dbf = dbFiles(vol)
        dbf = dbf[dbf.type==dstype]
        if dbf.empty:
            continue
        the_dates = list(dbf.obs_date.unique())
        counts = []
        paths = []
        for date in the_dates:
            q = dbf[dbf.obs_date==date]
            path = os.path.split(q.iloc[0].path)[0].replace(vol,'')
            paths.append(path)
            count = len(q)
            counts.append(count)
        the_dates = [str(x).split('T')[0] for x in the_dates]
        q = pd.DataFrame(list(zip(the_dates,counts,paths)))
        q.columns = ['date','n_files','path']
        q['vol'] = vol
        q = q.sort_values(by='date')[['date','n_files','vol','path']]
        r.append(q)
    r = pd.concat(r).sort_values(by='date').reset_index(drop=True)
    return r

def obs_night(time=None):
    '''determine the observation night associated with
    a certain time. Times after midnight, but before noon
    are associated with the -previous- night's observation run
    
    Argument:
    ---------
    time: datetime
    
    Returns:
    --------
    string: YYYY-mm-dd
    '''
    if time is None: time = datetime.datetime.now()
    midnight = datetime.datetime(time.year,time.month,time.day)
    noon = midnight + datetime.timedelta(hours=12)
    if time > midnight and time < noon:
        r = time - datetime.timedelta(days=1)
    else:
        r = time
    return r.strftime('%Y-%m-%d')

# ----------- pandas DataFrame extensions ---------
def set_metadata(df,memo):
    '''insert metadata into the attributes of df without
    triggering the attribute access warning
    '''
    with warnings.catch_warnings():
        warnings.filterwarnings('ignore',category=UserWarning)
        df.metadata = memo
    return df
        
def dbFiles_Update(x):
    '''Update the database cache with any new files that have
    appeared since the cache was made
    
    Parameters
    ----------
    x : str
        the top level directory name
    
    Returns
    -------
    An updated database (pandas.DataFrame).
    '''
    raise Exception(f'{tcolor.fail}Depricated{tcolor.end}')
    df0 = dbFiles(x)
    max_date = df0.obs_date.max()
    file_list = listFiles(x)
    df = pd.DataFrame(columns = ['filename','type','obs_date','time','path'])
    datePattern = re.compile('[0-9]{4}-[0-9]{2}-[0-9]{2}')  # date pattern
    for fullpath in tqdm.tqdm(x,ascii=not utf_enabled):
        path,filename = os.path.split(fullpath)
        r = datePattern.findall(path)
        if len(r) > 0:
            date = datetime.datetime.strptime(r[-1],'%Y-%m-%d')
        else:
            continue
        if date > max_date:
            timestamp = os.path.getmtime(fullpath)
            time = datetime.datetime.fromtimestamp(timestamp)
            for key,cpat in filePatterns.items():
                r = cpat.match(filename)
                if r is not None:
                    type = key
                    break
            else:
                type = ''
            row = pd.DataFrame([{'filename':filename,
                                'type':type,
                                'obs_date':date,
                                'time':time,
                                'path':fullpath}])
            df = df.append(row,ignore_index=True)
    print('%d new files found'%len(df))
    if len(df) > 0:
        df = pd.concat([df0,df],ignore_index=True)
        cache(df,x)
    else:
        df = df0
    return df

# ----------- cache ------------
_cache_state = 'good'
def cache_state(v=None):
    '''Query or set the cache state
    
    Argument:
    ---------
    v : str or None
        'good' - caching is enabled and in a fresh state
        'stale' - cache is declared stale for just the next from_cache operation, then returns to 'good'
        'off' - cache is turned off. like 'stale' for all further from_cache operations, until restored to 'good'
        
    '''
    global _cache_state
    if v is None:
        return _cache_state
    assert v in ['good','stale','off']
    _cache_state = v

def cache(df=None):
    '''cache a data base.
    
    Parameters
    ----------
    df : pandas.DataFrame
        the database

    df must have an attribute 'metadata' which is a
    dictionary that uniquiely identifies the database. This will be used to
    generate the cache file name. It must be json-encodable
    as it will be stored in the cache file along with the database
    
    If df is None or a string, this will be equivalent to a call to cache_state
    (either query or set the caching mode to 'off', 'stale', or 'good')
    '''
    global _cache_state
    if isinstance(df,str) or df is None:
        return cache_state(v=df)
    if not os.path.isdir(cache_path):
        os.mkdir(cache_path)
    memo = df.metadata
    #cache_filename = hashlib.sha224(pickle.dumps(memo)).hexdigest()
    cache_filename = hashlib.sha224(str(memo).encode()).hexdigest()
    cache_filepath = os.path.join(cache_path,cache_filename)
    dbSave(df,cache_filepath)
    if _cache_state != 'off':
        _cache_state = 'good'

def from_cache(memo):
    '''try to get a cached database given the metadata
    
    Parameters
    ----------
    memo : dict
        The metadata dictionary. The cache filename is generated from the metadata
    '''
    if _cache_state in ('off','stale'):
        return None
    #cache_filename = hashlib.sha224(pickle.dumps(memo)).hexdigest()
    cache_filename = hashlib.sha224(str(memo).encode()).hexdigest()
    cache_filepath = os.path.join(cache_path,cache_filename)
    if os.path.isfile(cache_filepath):
        df = dbRestore(cache_filepath)
        return df
    else :
        return None

def cached():
    '''get metadata from each cached file'''
    files = os.listdir(path=cache_path)
    df = []
    for fn in files:
        with open(os.path.join(cache_path,fn)) as f:
            memo = json.loads(f.readline())
        memo['cached_file_name'] = fn
        df.append(memo)
    df = pd.DataFrame(df)
    return df
    
def dbSave(db,filename):
    '''save a database (pandas.DataFrame) to disk, with a memo
    
    Parameters
    ----------
    db : DataFrame
        the database to save
    memo : str or dict
        a descriptor string or dictionary with discriptor items for the
        saved database
    filename : str
        the file name
    '''
    s = db.to_json()
    memo = db.metadata
    
    with open(filename,'w') as f:
        f.write(json.dumps(memo) + '\n')
        f.write(s)

def dbRestore(filename):
    '''restore a database from disk file.
    
    Parameters
    ----------
    filename : str
        the filename
    
    Returns
    -------
    (db,memo) as a tuple
    '''
    with open(filename) as f:
        memo = json.loads(f.readline())
        s = f.readline()
    db = pd.read_json(s,convert_dates=['obs_date','time'])
    set_metadata(db,memo) # db.metadata = memo
    return (db)

# ---- support for dates and times interpretation ----
def _timestamp(date,time):
    '''convert date and time strings to pandas Timestamps
    '''
    if isinstance(date,list) and isinstance(time,list):
        r = [_timestamp(d,t) for d,t in zip(date,time)]
        return r
    if isinstance(date,str):
        pat = '%Y-%m-%d'
        date = datetime.datetime.strptime(date,pat).date()
    elif isinstance(date,datetime.date):
        pass
    else:
        raise TypeError
    if isinstance(time,str):
        pat = '%H%M%S'
        time = datetime.datetime.strptime(time,pat).time()
    elif isinstance(date,datetime.time):
        pass
    else:
        raise TypeError
    time = pd.Timestamp(datetime.datetime.combine(date,time))
    return time

# --------------------------------------------------
#  old out of date routines on their way out

def listFiles(x):
    ''' Make a database of FITS files in a directory structure
    
    Parameters
    ----------
    x : str
        the top level directory name - all FITS files in the directory
        and recursively all subdirectories will be included in the result

    Returns
    -------
    a list of FITS file names, with each filename's full directory path
    '''    
    x = glob.glob(os.path.join(x,'**/*.fits'),recursive=True)
    return x

def sharcs_info(df):
    '''
    Create a database of sharcs files with header information
    
    Parameters
    ----------
    df : DataFrame
        the database of files, like that produced by dbFiles, but
        limited to 'sharcs' type. (files that are not sharcs files will be ignored
    
    Returns
    -------
    pandas.DataFrame database of sharcs files and file headers
    '''
    
    q = df.copy()
    q = q[q.type == 'sharcs']
    keys = ['date-obs', 'date-beg', 'time-obs', 'object', 'itime', 'ra', 'dec',
            'ha','airmass','tubangle','loopstat', 'filt1nam', 'filt2nam', 'loopsub',
            'opmode','twtrgain', 'woofgain', 'wfsrate', 'ttrate', 'lasonsky']
    keys = [key.lower() for key in keys]
    unk = 'unknown'
    
    dfr = pd.DataFrame(columns=keys)
    for qr in tqdm.tqdm(q.itertuples(),total=len(q),ascii=not utf_enabled):
        filepath = qr.path
        with fits.open(filepath) as hdu:
            hdr = hdu[0].header
            row = {}
            for key in keys:
                row[key] = hdr.get(key,unk)
        dfr = dfr.append(pd.DataFrame([row],index=[qr.Index]))

    dfr = pd.concat([q,dfr],axis=1)
    return dfr

def sharcs_stars(df):
    '''filter the sharcs database for images of stars
    The argument should have been filtered through sharcs, and sharcs_info
    first
    '''
    q = df.copy()
    q = q[q.Object.notnull()]
    z = ['Flat', 'flat','Dark','dark','source', 'Sharp', 'sharp', 'Test', 'test', 'Opt', 'opt']
    z = '|'.join(z)
    q = q[~q.Object.str.contains(z)]
    return q

nixdir = '|'.join(['Library','Downloads'])

def summary():
    global df4
    sum_cols = ['date','name','object','itime','filt1nam','filt2nam','wfsrate','loopstat']
    df4 = df2[(~df2.filt2nam.str.contains('nknown')) & (df2.loopstat=='Closed')][sum_cols]
    df4 = df4[df4.date.notnull()]
    df4 = df4.sort_values(by='date')
    return df4

# ------ special support for Sharcs data analysis ----
cam = Sharcs()
dx = cam.ap.dx
x0 = cam.ap.x0
df = None # a fitsFiles database
df2 = None # a sharcs_info database

def getFile(n,df=None):
    '''get a file by database index
    '''
    if df is None:
        df = globals()['df2']
    e = df.loc[n]
    fn = os.path.join(e.dir,e['name'])
    with fits.open(fn) as hdu:
        data = hdu[0].data
        hdr = hdu[0].header
    r = u.Quantity(data)
    r.dx = dx
    r.x0 = x0
    r.header = hdr
    r.index = n
    r.name = '%s [%d] %s %s'%(e.object,n,e.date.strftime('%Y-%m-%d'),e['name'])
    return r

def findDark(n,df=None):
    '''find the nearest dark that can
    be paired with dataset n, where n is
    the index of an image in the database.
    We need a sharcs_info databse (with sharcs fits headers) to do this
    '''
    if df is None:
        df = globals()['df2']
    e = df.loc[n]
    itime = e.itime
    q = df[(df.date.notnull()) & (df.object.str.contains('Dark|dark')) & (df.itime == itime)]
    if q.empty: return None
    idx = (q.date-e.date).abs().idxmin()
    return idx

def findFlat(n,df=None):
    '''find the nearest flat that can
    be paired with dataset n, where n is
    the index of an image in the database.
    We need a sharcs_info database (with sharcs fits headers) to do this
    '''
    if df is None:
        df = globals()['df2']
    e = df.loc[n]
    filt1 = e.filt1nam
    filt2 = e.filt2nam
    q = df[(df.date.notnull()) & (df.object.str.contains('Flat|flat')) & (df.filt1nam == filt1) & (df.filt2nam == filt2)]
    if q.empty: return None
    idx = (q.date-e.date).abs().idxmin()
    return idx    

# ------------------------------------------------
# initializers and tests

example_script = '''
from astrobot.sandbox import finder
from astrobot.tcolor import tcolor
date = '2021-06-15'
finder.findVol('telemetry',date)
_q = finder.dates('telemetry'); print(_q)
print(f"{tcolor.cyan}{'2021-06-15' in list(_q['date'])}{tcolor.end}")
finder.dbHdrs('telemetry','2021-06-15')
finder.dbHdrs('sharcs','2021-06-15')
_q = finder.dates('sharcs'); print(_q)
date = _q.iloc[-1].date
print(f'{tcolor.cyan}date with sharcs data: {date}{tcolor.end}')
finder.dbHdrs('sharcs',date)
'''

def test(with_cache='off'):
    prev_state = cache_state()
    cache_state( with_cache )
    date = '2021-06-15'
    findVol('telemetry',date)
    _q = dates('telemetry')
    print(f"{tcolor.cyan}{'2021-06-15' in list(_q['date'])}{tcolor.end}")
    dbHdrs('telemetry','2021-06-15')
    dbHdrs('sharcs','2021-06-15')
    _q = dates('sharcs')
    date = _q.iloc[-1].date
    print(f'{tcolor.cyan}date with sharcs data: {date}{tcolor.end}')
    dbHdrs('sharcs',date)
    cache_state(prev_state)
    
def old_depricated_max_init():
    global df,df1,df2,df3
    df = getFilesDB()
    df1 = sharcs(df)
    df2 = sharcs_info(df1) # this one takes a while as it opens every file
    df3 = sharcs_stars(df2)

def old_depricated_init():
    '''This initializes the files database, df, and the sharcs headers database, df2,
    by reading them from the stored database in the user's HOME directory ('~')
    
    Returns
    -------
    None: but creates finder.df and finder.df2 which are pandas dataFrames with
    files and sharcs headers information respectively
    '''
    global df,df1,df2,df3
    HDF_file_path = os.path.join(os.path.expanduser('~'),'fits_files.hdf')
    with pd.HDFStore(HDF_file_path) as f:
        df = f['FitsFiles']
        df2 = f['SharcsHdrs']

def old_depricated_tes():
    n = 921734
    ndark = 920699
    image = getFile(n)
    dark = getFile(ndark)
    return (image,dark)
    
