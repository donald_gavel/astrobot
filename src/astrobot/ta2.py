#!/usr/bin/env python -i
'''ta2.py
Lick ShaneAO telemetry analysis
'''
import os
import numpy as np
from astropy import units as u
from astropy.io import fits
from scipy.interpolate import interp1d
from scipy import optimize

from astrobot import q_helper
from astrobot import oprint

import matplotlib.pyplot as plt

plt.ion()

home = os.path.expanduser('~')
workingDir = os.path.abspath('.')
realAtUcolickDataDir = '/net/real/local/data'

parameterFilesDirs = [
    os.path.join(home,'parameterFiles'),
    os.path.join(workingDir,'parameterFiles'),
    os.path.join(realAtUcolickDataDir,'parameterFiles')
]

# set the paths to search for telemetry data
# *** edit for your machine ***

dataVols = [
    '/Users/donaldgavel/data_lick/LittleWDdrive_backup_11-6-2015/ShaneAO_Data/telemetry',
    '/Volumes/External 2TB Drive #1/LittleWDdrive_backup_11-6-2015/ShaneAO_Data/telemetry',
    '/Volumes/External 2TB Drive #1/ShaneAO_Data/telemetry',
    '/Volumes/External 2TB Drive #2/ShaneAO_Data/telemetry',
    '/Volumes/LittleWDdrive/ShaneAO_Data/telemetry',
    os.path.join(home,'telemetry'),
    os.path.join(home,'data/telemetry'),
    os.path.join(home,'shaneao_data/telemetry'),
    os.path.join(home,'ShaneAO/data'),
    '/Volumes/Donalds-Mac-Pro/donaldgavel/data/telemetry',
    os.path.join(realAtUcolickDataDir,'telemetry'),
    ]

class Base():
    def pprint(self):
        oprint.pprint(self,expand=1)
    
    @property
    def pp(self):
        self.pprint()
    
    def copy(self):
        return copy.deepcopy(self)

    
class Dataset(Base):
    '''A telemetry-analysis data set
    '''
    mode_params = {
        '16x':{
            'nv':1382,
            'ns':144,
            'n_modes':182,
            'd':21.4*u.cm,
            'pixel_scale':1.56*u.arcsec,},
        '8x': {
            'nv':1174,
            'ns':40,
            'd':2.06*u.arcsec,
            'pixel_scale':20.6*u.arcsec,},
        }
    
    def __init__(self,date='2015-09-03',dsn=411):
        dsn = f'Data_{dsn:04d}.fits'
        dataVol = dataVols[0]
        path = os.path.join(dataVol,date,dsn)
        fileDir, fileName = os.path.split(path)
        assert os.path.isfile(path)
        with fits.open(path) as hdu:
            data = hdu[0].data
            header = hdu[0].header
            
        self.path = path
        self.fileName = fileName
        self.data = data
        self.header = header
        
        self.mode = mode = header['mode']
        self.substate = substate = header['substate']
        self.nt = nt = data.shape[0]-1
        if mode.endswith('LGS'):
            self.mode = mode = mode[:-3]
        ns,d,pixel_scale = [self.mode_params[mode][x] for x in ['ns','d','pixel_scale']]
        self.__dict__.update(self.mode_params[mode])
        self.na,self.nw,self.nf,self.ntt = na,nw,nf,ntt = [1024,52,14,2]
        self.rate = rate = header['rate']*u.Hz
        self.dt = dt = (1./self.rate).to(u.ms)
        
        # channels
        p = 0
        s = u.Quantity(data[1:,p:p+ns*2]).T; p = p+ns*2 # slopes
        tt = u.Quantity(data[1:,p:p+2]); p = p+2 # tip/tilts
        at = u.Quantity(data[1:,p:p+1024]); p = p+1024 # tweeter actuators
        aw = u.Quantity(data[1:,p:p+52]); p = p+52 # woofer actuators
        af = u.Quantity(data[1:,p:p+14]); p = p+14 # woofer filter coefficients
        utt = u.Quantity(data[1:,p:p+2]) # uplink tip/tilt
        keys = ['s','tt','at','aw','af','utt']
        vals = [s,tt,at,aw,af,utt]
        dd = {key:val for key,val in zip(keys,vals)}
        for key,val in dd.items():
            dd[key].dx = [dt,1*u.Unit('')]
            dd[key].x0 = [0*dt,u.Quantity(0.)]
            dd[key].axis_labels = ['time','chan']
        self.__dict__.update(dd)
        
    def var(self):
        '''Calculate the variance of slopes and phases
        '''
        pass
