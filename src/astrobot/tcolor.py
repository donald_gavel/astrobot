'''Text coloration.

from astrobot import tcolor
tcolor.test()

See:
----
https://stackoverflow.com/questions/287871/how-to-print-colored-text-to-the-terminal

'''
import os
import re

class tcolor:
    header = '\033[95m'
    blue = '\033[94m'
    cyan = '\033[96m'
    green = '\033[92m'
    orange = '\033[33m'
    yellow = '\033[93m'
    red = '\033[91m'
    fail = '\033[91m'
    end = '\033[0m'
    bold = '\033[1m'
    underline = '\033[4m'
    
    def test():
        test()

if 'DonaldGavel' in os.uname().nodename:
    tcolor.warning = tcolor.yellow
else:
    tcolor.warning = tcolor.orange

def gprint(s,*args,**kwargs):
    '''print text in the 'ok' (green) font color.
    remaining args and kwargs are sent to print
    '''
    s = f'{tcolor.green}{s}{tcolor.end}'
    print(s,*args,**kwargs)

def wprint(s,*args,**kwargs):
    '''print text in the 'warning' font color.
    remaining args and kwargs are sent to print
    '''
    s = f'{tcolor.warning}{s}{tcolor.end}'
    print(s,*args,**kwargs)

def eprint(s,*args,**kwargs):
    '''print text in the 'fail' font color.
    remaining args and kwargs are sent to print
    '''
    s = f'{tcolor.fail}{s}{tcolor.end}'
    print(s,*args,**kwargs)

# def cprint(color,s,*args,**kwargs):
#     '''print colored text, as in
#         cprint('blue','some text',...) # remaining args sent to print
#     '''
#     try:
#         prefix = getattr(tcolor,color)
#     except:
#         x = [x for x in dir(tcolor) if not x.startswith('__')]
#         z = f'{tcolor.fail}unknown color.{tcolor.end} color must be one of {x}'
#         raise Exception(z)
#     s = f'{prefix}{s}{tcolor.end}'
#     print(s,*args,**kwargs)

def cprint(color,s,*args,**kwargs):
    s = cstr(color,s)
    print(s,*args,**kwargs)
    
def cstr(color,s):
    '''return colored text. color must be the name of one of the attributes of tcolor:
    'red', 'green', 'warning', 'fail', etc.
    '''
    try:
        prefix = getattr(tcolor,color)
    except:
        x = [x for x in dir(tcolor) if not x.startswith('__')]
        z = f'{tcolor.fail}unknown color.{tcolor.end} color must be one of {x}'
        raise Exception(z)        
    s = f'{prefix}{s}{tcolor.end}'
    return s

shell_script = '''
#!/bin/sh
#
# Query a property from the terminal, e.g. background color.
#
# XTerm Operating System Commands
#     "ESC ] Ps;Pt ST"

oldstty=$(stty -g)

# What to query?
# 11: text background
Ps=${1:-11}

stty raw -echo min 0 time 0
# stty raw -echo min 0 time 1
printf "\\033]$Ps;?\\033\\\\"
# xterm needs the sleep (or "time 1", but that is 1/10th second).
sleep 0.00000001
read -r answer
# echo $answer | cat -A
result=${answer#*;}
stty $oldstty
# Remove escape at the end.
echo $result | sed 's/[^rgb:0-9a-f/]\\+$//' > query_term_response.txt
'''

def check_term():
    '''determine the background color of the terminal.
    return it as an r,g,b tuple
    '''
    try:
        with open('query_term.sh','w') as f:
            f.write(shell_script)
        os.system('. query_term.sh')
        with open('query_term_response.txt') as f:
            x = f.read()
        pat = 'rgb:(.{4})/(.{4})/(.{4})'
        rgb = re.findall(pat,x)[0]
        r,g,b = [int(x[-2:],16) for x in rgb]
        if r>200 and g>200 and b>200:
            tcolor.warning = tcolor.orange
        else:
            tcolor.warning = tcolor.yellow
        return r,g,b
    except:
        print(f'<tcolor.check_term> could not determine terminal background color')
        return None,None,None

def test():
    for v in dir(tcolor):
        if not v.startswith(('_','test')):
            print(f'{getattr(tcolor,v)} This text is {v} {tcolor.end}')
