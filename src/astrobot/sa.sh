#!/usr/bin/env bash

a=()
p=python
for var in "$@"
do
    case $var in
        '-ipy')
            p=ipython\ -i
            ;;
        '-i')
            p=python\ -i
            ;;
        -*)
            r=$var
            a+=\ $r
            ;;
        *)
            r=\'$var\'
            a+=\ $r
            ;;
    esac
done
if [ -z "$a" -a "$p" = python ]
then
    p=python\ -i
fi
cmd="$p"' -m astrobot.sandbox.ttan_seeing -- '"$a"
echo "$cmd"
eval "$cmd"
