"""
pprint is an enhanced pretty-print for objects
"""
from __future__ import print_function
import numpy as np
import pprint as up_pprint
import astropy.units as u
from collections import OrderedDict
import inspect
import sys
if sys.version_info.major == 3:
    from io import StringIO
else:
    from cStringIO import StringIO

if sys.version_info.major == 3:
    unicode = str

py2 = sys.version_info.major == 2

print_width = 60

def ppd(self,print_=True,head='\n',tail='\n'):
    '''Pretty-print the dictionary
    '''
    if isinstance(self,dict):
        r = head #'\n'
        for key,val in self.items():
            if isinstance(val,dict): # nested dictionary
                v = ppd(val,print_=False)
                v = v.replace('\n','\n    ')
            else:
                try:
                    v = '%s'%val
                    if py2 and hasattr(val,'unit'):
                        v += ' %s'%val.unit
                except:
                    try:
                        v = val.__repr__()
                    except:
                        v = '%s'%type(val)
                if len(v) > print_width:
                    v = v[:print_width-4] + ' ...' + '\x1b[0m' # end the text decoration, like color
                    try:
                        if hasattr(val,'unit'):
                            v += ' %s'%val.unit
                    except:
                        pass
                else:
                    if hasattr(val,'doc'):
                        v += ' %s'%val.doc
            s = '    %s = %s\n'%(key,v)
            r += s
        r += tail
        if tail == '': r = r.rstrip()
    else:
        r = '%r\n'%type(self)

    if print_:
        print(r)
    else:
        return r
    
def ppd_old(self,print_=True):
    """Pretty-print the dictionary
    """
    if isinstance(self,dict):
        try:
            r = json.dumps(self,indent=4)
        except:
            d = OrderedDict()
            valid_types = (int,long,float,complex,str)
            for key,val in zip(self.keys(),self.values()):
                if isinstance(val,valid_types):
                    d[key] = val
                else:
                    s = '%r'%val
                    d[key] = s if len(s) < 80 else s[:75] + '...'
            r = json.dumps(d,indent=4)
    else:
        r = ''
    if print_:
        print(r)
    else:
        return r

def pprint(self, short=False, expand=0, out='print'):
    """Pretty-print the object. This consists
    of reporting on the object's attributes.
    
    :param object self: an object of any type
    :param boolean short: short output. Just the name
        and object type, no attributes.
    :param boolean expand: expand all dictionaries in a nested indented form
        level 1 does only dicts, level 2 does lists of dicts
    :param str out: how to provide the output, either print to terminal ('print') or return as a string ('string')

    """
    if out == 'string':
        stdout_save = sys.stdout
        sys.stdout = output = StringIO()

    try:
        if isinstance(self,dict):
            ppd(self)
            return
        elif isinstance(self,(list,tuple)):
            for e in self:
                pprint(e,short=short,expand=expand)
            return
        elif not hasattr(self,'__dict__'):
            up_pprint.pprint(self)
            return
        bling = print_width*'='
        tab = ' '*4
        print(bling)
        if hasattr(self,'longName'):
            print(self.longName)
        elif hasattr(self,'name'):
            print(self.name)
        else:
            print('')
        print('    %s'%type(self),end='')
        try:
            print('    %r'%(self.shape,))
        except:
            print('')
        if isinstance(self,float): # object that inherits float
            print('value:%s%s'%(tab,self.__repr__()))
        print(print_width*'-')
        if short:
            return
        d = self.__dict__
        keys = sorted(d.keys())
        for key in keys:
            limit_length = True
            if key.endswith('_units'): continue
            if key.endswith('_doc'): continue
            val = d[key]
            docstr = None
            if key+'_doc' in d:
                docstr = d[key+'_doc']
                if not isinstance(docstr,(str,unicode)):
                    docstr = None
            units = None
            
            if isinstance(val,(str,unicode)):
                pv = "'"+val+"'"
            elif isinstance(val,(float,complex,int,bool,u.Quantity,u.Unit,u.CompositeUnit)):
                pv = '%r'%val
#                if hasattr(val,'doc'):
#                    pv += ' %r'%val.doc
            elif isinstance(val,(tuple,list)):
                if (len(val) < 5) and np.array([np.isscalar(x) or (hasattr(x,'isscalar') and x.isscalar) for x in val]).all():
                    pv = str(val)
                elif expand > 1:
                    pv = str(type(val)) + ' ' + str(len(val)) + ' entries'
                    for e in val:
                        r = ppd(e,print_=False)
                        if r == '':
                            r = str(type(e))
                        else:
                            limit_length = False
                        pv += '\n' + r
                else:
                    pv = str(type(val)) + ' ' + str(len(val)) + ' entries'
            elif expand > 0:
                limit_length = False
                if isinstance(val,dict):
                    pv = '\n'+ppd(val,print_=False)
                else:
                    pv = ('%r\n'%type(val)).replace('\n','\n\t')
                    #pv += ('\t%r'%val).replace('\n','\n\t').rstrip('\t\n')
                lines = pv.split('\n')
                pv = ''
                for line in lines:
                    pv += line[:print_width] + '\n' # limit each line
            else:
                pv = ('%r'%val).replace('\n','\n\t')
                #if '\n' in pv: limit_length = False
                
            if limit_length:
                if key.endswith('_doc'):
                    pass
                elif len(pv) > print_width:
                    pv = str(val.__class__).replace('\n','\n\t')
                    try:
                        if hasattr(val,'shape'):
                            pv += ' '+str(val.shape)
                    except:
                        pass
                    
            if key+'_units' in keys:
                units = d[key+'_units']
            line = key+':'+tab+pv
            if units is None:
                pass
            else:
                line += ' '+str(units)
            try:
                if hasattr(val,'longName') and isinstance(val.longName,(str,unicode)):
                    line += " '"+val.longName+"'"
                elif hasattr(val,'name') and isinstance(val.name,(str,unicode)):
                    line += " '"+val.name+"'"
                if hasattr(val,'doc') and isinstance(val.doc,(str,unicode)):
                    line += " '"+val.doc+"'"
                elif docstr is not None:
                    line += " '"+docstr+"'"
            except:
                pass
            print(line.rstrip('\n\t'))
        print(bling)
    finally:
        if out == 'string':
            sys.stdout = stdout_save
            return output.getvalue()

def methods(self,including_inherited = False):
    '''list all the methods associated with the object
    
    :param object self: an object instance
    :param bool including_inherited: include the inherited class methods
    '''
    if including_inherited:
        return [x for x in sorted(dict(inspect.getmembers(self.__class__,callable)).keys()) if not x.startswith('__')]
    return sorted([x for x in type(self).__dict__.keys() if (not x.startswith('__')) and callable(type(self).__dict__[x])])

def members(self):
    d = self.__dict__
    if inspect.isclass(self) or inspect.ismodule(self):
        return sorted([x for x in d if not callable(d[x]) and not x.startswith('__')])
    return sorted(d.keys())

def gist(self,gistables=(int,float,complex,str,unicode),restore_method = None):
    '''create the "gist" of the object: namely a dictionary of only
    the scalar valued instance variables in an object. Can be used for restoring
    the object with, possibly, some additional calculations to restore the larger
    data components
    '''
    d = self.__dict__
    r = {}
    r['__class__'] = [self.__class__.__module__,self.__class__.__name__]
    for key,val in d.iteritems():
        if isinstance(val,gistables):
            r[key] = val
    if restore_method:
        r['__restore_method__'] = restore_method
    return r

def from_gist(d,cls=None):
    '''restore an object from a gist. In order for this to work, the __init__
    method of the object must be able to accept zero arguments.
    '''
    if cls:
        pass
    elif '__class__' in d:
        mod_name,cls_name = d['__class__']
        mod = __import__(mod_name,fromlist=[cls_name])
        cls = getattr(mod,cls_name)
    else:
        raise Exception('<from_gist> no class')
    obj = cls.__new__(cls)
    obj.__dict__ = d
    if '__restore_method__' in d:
        obj.__getattribute__(d['__restore_method__'])()        
    return obj

#--------------- Tests -----------------

def test():
    pprint(1)
    pprint('hello')
    a = u.Quantity([[1,2],[3,4]])
    a.name='foo'
    pprint(a)
    d = OrderedDict()
    d['one'] = 'hello'
    d['two'] = 12.34
    d['three'] = 24
    d['four'] = np.array([1,2])
    pprint(d)

def test2():
    a = u.Quantity([[1,2],[3,4]])
    a.name='foo'
    bling = '*'*20
    print(bling+' members '+bling)
    print(members(a))
    print(bling+' methods '+bling)
    print(methods(a))
    print(bling+' all methods '+bling)
    print(methods(a,including_inherited=True))
    print(bling+' object members '+bling)
    print(members(a.__class__))
    print(bling+bling)
    
