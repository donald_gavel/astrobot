#!/usr/bin/env python -i
"""Control.py
Model feedback control systems, particularly for AO control
To run, activate the virtual environment and start python. Then

>>> from pwfs import control2 as c2
>>> c2.tests()

References
----------

| :download:`[1] <../doc_pdf/AtmosphericPowerSpectra/KolmogorovSpectrum.pdf>` Derivation of atmospheric power spectra relevant to adaptive optics control systems
| :download:`[2] <../doc_pdf/MonteCarloNoise/power_spectra.pdf>` Simulating stochastic noise processes in the discrete time and frequency domain
| :download:`[3] <none>` Adaptive optics control system models (TBW)

"""

# AO model in the frequency domain
from astropy import units as u
import numpy as np
from numpy import pi,exp
import pandas as pd
from scipy.interpolate import interp1d
import scipy.integrate
from scipy.special import gamma
from scipy import optimize
from fractions import Fraction
import inspect
import functools
import copy
import time
import tqdm
import itertools
import warnings

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
from matplotlib.figure import Figure

from astrobot import q_helper as qh
from astrobot import oprint
from astrobot.dotdict import DotDict
from astrobot import tile_figures

plt.rc('text', usetex=True)
i = 1j
# constants from [1] appendix
c0 = 8*np.sqrt(2)*((3/5)*gamma(6/5))**(5/6)
c1 = -c0*pi*(2**(8/3)*gamma(11/6))/(gamma(-5/6))*(2*pi)**(-11/3)
c2 = (gamma(5/6)*gamma(1/6))/(2**(8/3)*gamma(11/6)**2)
c3 = c0*gamma(8/3)*(2*pi)**(-8/3)
c4 = (c0*gamma(8/3)*(2*pi)**(-8/3)*pi)**(3/5)

isscalar = lambda x: (isinstance(x,u.Quantity) and x.isscalar) or isinstance(x,(int,float,complex))
call_trace = False

class Base():
    '''Base class of Spectrum and Signal
    '''
    sampling_defaults = {
        'fmin': 0*u.Hz,
        'fmax': 1*u.kHz,
        't0': 0*u.s,
        'tf': 1*u.s,
        'n': 1024,
        'kind':'linear',
    }
    
    def pprint(self):
        oprint.pprint(self,expand=1)
    
    @property
    def pp(self):
        self.pprint()
    
    def setattrs(self,**kwargs):
        self.__dict__.update(kwargs)
        return self
    
    def getattrs(self,v=None):
        if v is None:
            return vars(self)
        if not isinstance(v,list):
            v = [v]
        return {key:getattr(self,key) for key in v}

    def copy(self):
        return copy.deepcopy(self)

    def compose(self,*comp):
        if len(comp) == 1:
            comp = comp[0]
        self.comp = comp
        if len(comp) == 2: # unary ops
            arg,op = comp
            domain_change = self.domain_type != arg.domain_type
            if not domain_change:
                self.domain = arg.domain
            self.params = arg.params
        elif len(comp) == 3: # binary ops
            arg1,arg2,op = comp
            self.domain = arg1.domain_intersection(arg2)
            if isinstance(arg2,Base):
                self.params = {**arg2.params,**arg1.params}
            else:
                self.params = arg1.params
        return self
    
    def comp_list(self):
        '''get a list of the composing functions that are of Base class
        '''
        c = []
        if self.comp is None: return c
        if len(self.comp) == 2:
            arg_list = [self.comp[0]]
        elif len(self.comp) == 3:
            arg_list = [self.comp[x] for x in [0,1]]
        for arg in arg_list:
            if isinstance(arg,Base):
                c = c + [arg] + arg.comp_list()
        return c
    
    def comp_tree(self,level=1):
        '''traverse the composition tree
        '''
        if level==1:
            print()
            print(self.name)
            print(len(self.name)*'~')
        if self.comp is None: return
        tab = '|   '
        tabl = '|---'
        if len(self.comp) == 2:
            arg,op = self.comp
            arg_list = [arg]
        elif len(self.comp) == 3:
            arg1,arg2,op = self.comp
            arg_list = [arg1,arg2]
        else:
            raise Exception
        for arg in arg_list:
            if isinstance(arg,Base):
                print((level-1)*tab+tabl,arg.name)
                arg.comp_tree(level=level+1)
            else:
                print((level-1)*tab+tabl,arg)
        print((level-1)*tab+tabl,op)

    def domain_intersection(self,other):
        '''intersect the domains (valid ranges of the argument) of two system objects
        '''
        
        if not hasattr(other,'domain'):
            return self.domain
        if self.domain_name == other.domain_name == 's':
            return 's'
        if self.domain_name == 'f' and other.domain_name == 's':
            return self.domain_tuple
        if self.domain_name == 's' and other.domain_name == 'f':
            return other.domain_tuple
        if self.domain_name == 'f' and other.domain_name == 'f':
            fmin,fmax = self.domain_tuple[1:]
            ofmin,ofmax = other.domain_tuple[1:]
            fmin = max(fmin,ofmin)
            fmax = min(fmax,ofmax)
            return ('f',fmin,fmax)
        if self.domain_name == 't' and other.domain_name == 't':
            tmin,tmax = self.domain_tuple[1:]
            otmin,otmax = other.domain_tuple[1:]
            tmin = max(tmin,otmin)
            tmax = min(tmax,otmax)
            return ('t',tmin,tmax)
        raise Exception('cannot intersect %s and %s domain objects'%(self.domain_name,other.domain_name))

    @property
    def samples(self):
        if hasattr(self,'gened_samples'):
            return self.gened_samples
        elif hasattr(self,'original_samples'):
            return self.original_samples
        else:
            return None

    def __array_ufunc__(self,method,*args,**kwargs):
        # prevents numpy from casting an __rmul__ result to a numpy array
        # instead lets this class override it
        # see https://numpy.org/doc/stable/reference/arrays.classes.html#numpy.class.__array_ufunc__
        if method == np.multiply:
            return self.__mul__(args[1])
        elif method == np.divide:
            return self.__rtruediv__(args[1])
        elif method == np.add:
            return self.__add__(args[1])
        elif method == np.subtract:
            return self.__rsub__(args[1])
        elif method == np.equal:
            return False
        elif method == np.isfinite:
            return True
        return NotImplemented

    @property
    def domain_name(self):
        return tuple(self.domain)[0]

    @property    
    def domain_type(self):
        domains = {'s':'f', 'f':'f', 't':'t'} # translation
        return domains[self.domain_name]
    
    @property
    def domain_tuple(self):
        if isinstance(self.domain,tuple):
            return self.domain
        else:
            return (self.domain_type,-np.inf,np.inf)

class Spectrum(Base):
    '''Fourier or Laplace spectrum defined in the frequency domain.
    Can be defined as a function of f, or in terms of samples.
    The spectrum is two-sided, defined for positive and
    negative frequencies. One can specify only the positive side and
    it is implicitly assumed that the negative side is its complex-conjugate
    '''
    def from_signal(sig,**kwargs):
        '''
        Create a Spectrum given a signal S.
        
        Keywords t0,n,dt, and tf are used only if the
        Signal S is defined by a function. In that case
        the rules of S.gen_samples are used.        
        '''
        if sig.samples is None:
            sig.gen_samples(**kwargs)
        t,x,n,dt = ( sig.samples[x] for x in ['t','x','n','dt'])
        F = np.fft.fftshift( np.fft.fft(x)*dt ).to(x.unit/u.Hz)
        f = np.fft.fftshift(np.fft.fftfreq(n,d=dt)).to(u.Hz)
        f = np.block([f,-f[0]]) # tack on the Nyquist value at the highest positive frequency
        F = np.block([F,F[0]])
        if 'name' not in kwargs:
            kwargs['name'] = '{\cal{F}}\{%s\}'%sig.name
        return Spectrum((f,F),T=dt,**kwargs).compose(sig,'spectrum')
        
    def __init__(self,F,name='',**kwargs):
        '''
        $F(f) = \int_0^\infty x(t) dt$
        => F-units are x-units / Hz
        
        Parameters
        ----------
        F : 2-tuple of Quantity arrays or a function
            Definition of the Fourier spectrum, F(f)
            
            tuple: (f,F(f))
                where f and F(f) are equal length arrays
            function: F(f)
                given f produces F(f)
        name : str
        '''
        self.name = name
        if isinstance(F,tuple): # sampled data
            f,F = F
            n = len(f)
            df = f[1]-f[0]
            samples = DotDict({
                'f':f,
                'F':F,
                'df':df,
                'n':n,
                'creation':'original',
            })
            self.original_samples = samples
            self.F = interp1d(f,F,kind='cubic',bounds_error=False,fill_value=0.)
            self.domain = ('f',samples.f.min(),samples.f.max())
            self.params = {}
            self.unit = F.unit
        elif isinstance(F,Signal):
            q = Spectrum.from_signal(F,**kwargs)
            self.F = q.F
            self.domain = q.domain
            self.params = q.params
            self.unit = q.unit
            self.original_samples = q.original_samples
        elif callable(F): #  a function, F(f)
            params = inspect.signature(F).parameters
            domain = list(params.keys())[0]
            assert domain in ['f']
            self.F = F
            self.domain = domain
            self.signature = inspect.signature(F)
            self.params = {key:val.default for key,val in params.items() if val.default is not inspect._empty}
            self.unit = F(1*u.Hz).unit
        self.comp = None
        self.__dict__.update(kwargs)

    @property
    def source(self):
        return inspect.getsource(self.F)
            
    def gen_samples(self,fmin=0*u.kHz,fmax=1*u.kHz,kind='linear',n=1024):
        '''generate lists of frequency samples and corresponding Spectrum samples
        from fmin to fmax inclusive.
        
        Parameters:
        -----------
        fmin, fmax : Quantity, frequency-like
            The minimum and maximum frequencies over which to sample the spectrum
            (end points are included in the result).
            
        kind : str
            Sample spacing, either 'linear' or 'log'
        n : int
            The number of samples
        
        Returns:
        --------
        None
        
        Side effect:
        ------------
            The sample frequencies and spectrum values are stored
            in the object's gened_samples attribute.
        
        '''
        if isinstance(self.domain,tuple):
            fmax = min(fmax,self.domain[2])
        fmin = fmin.to(u.Hz)
        fmax = fmax.to(u.Hz)
        if kind == 'linear':
            df = (fmax-fmin)/(n-1) # n samples, n-1 intervals
            f = np.linspace(fmin,fmax,n)
        elif kind == 'log':
            df = None
            f = np.logspace(np.log10(fmin.value),np.log10(fmax.value),n)*u.Hz
        else:
            raise Exception('<gen_samples> kind must be "linear" or "log"')
        F = self(f)
        samples = DotDict({
            'f':f,
            'F':F,
            'n':n,
            'df':df,
            'kind':kind,
            'creation':'gen',
        })
        self.gened_samples = samples
    
    def gen_samples_FFT(self,fn=500*u.Hz,n=1024,**kwargs):
        '''generate a sampling of the spectrum suitable to send to an
        inverse fast Fourier transform algorithm (iFFT).
        
        Parameters
        ----------
        fn : Quantity, frequency-like
            The Nyquist frequency
        n : int
            The total number of samples
        
        Returns
        -------
        A tuple of (f,F(f))
        Also stores sampling information in the gened_samples field of the Spectrum object
        '''
        i = 1j
        pi = np.pi
        df = 2*fn/n
        f = np.linspace(-fn,fn-df,n)
        f = np.fft.ifftshift(f)
        F = self(f)
        samples = DotDict({
            'f':f,
            'F':F,
            'df':df,
            'n':n,
            'kind':'FFT',
            'creation':'gen'
        })
        self.gened_samples = samples

    def sample(self,**kwargs):
        kind = kwargs.get('kind','FFT')
        if kind=='FFT':
            self.gen_samples_FFT(**kwargs)
        else:
            self.gen_samples(**kwargs)
            
    def __call__(self,f=None,**kwargs):
        #if not self.bundle:
        #    kwargs = {key:val for key,val in kwargs.items() if key in self.params}
        if f is None: # just set the parameters and return a copy
            kwargs = {key:val for key,val in kwargs.items() if key in self.params}
            r = self.copy()
            r.F = functools.partial(r.F,**kwargs)
            r.params = {**self.params,**kwargs}
            return r
        if isinstance(f,list): f = u.Quantity(f)
        if isinstance(self.F,interp1d):
            f = f.to(u.Hz)
            return u.Quantity(self.F(f), self.unit)
        else:
            with np.errstate(divide='ignore',invalid='ignore'):
                r = self.F(f,**kwargs)
            return r

    def __add__(self,other):
        if isinstance(other,Spectrum):
            r = Spectrum(lambda f,**p: self(f,**p) + other(f,**p),
                         name=f'({self.name} + {other.name})')
        elif isscalar(other):
            r = Spectrum(lambda f,**p: self(f,**p) + other,
                         name=f'({self.name} + {other})')
        else:
            raise TypeError('cannot add a Spectrum to a %s'%other.__class__.__name__)
        return r.compose(self,other,'add')
    
    def __radd__(self,other):
        return self.__add__(other)    
    
    def __sub__(self,other):
        if isinstance(other,Spectrum):
            r = Spectrum(lambda f,**p: self(f,**p) - other(f,**p),
                         name = f'({self.name} - {other.name})')
        elif isscalar(other):
            r = Spectrum(lambda f,**p: self(f,**p) - other,
                         name = f'({self.name} - {other})')
        else:
            raise TypeError('cannot difference a Spectrum and a %s'%other.__class__.__name__)
        return r.compose(self,other,'sub')
    
    def __rsub__(self,other):
        if isinstance(other,Spectrum):
            r = Spectrum(lambda f,**p: other(f,**p) - self(f,**p),
                         name = f'({other.name} - {self.name})')
        elif isscalar(other):
            r = Spectrum(lambda f,**p: other - self(f,**p),
                         name = f'({other} - {self.name})')
        else:
            raise TypeError('cannot difference a Spectrum and a %s'%other.__class__.__name__)
        return r.compose(self,other,'rsub')
    
    def __mul__(self,other):
        if isinstance(other,TransferFunction):
            i = 1j
            r = Spectrum(lambda f,**p: self(f,**p) * other(2*pi*i*f,**p),
                         name = f'({self.name} * {other.name})')            
        elif isinstance(other,Spectrum):
            r = Spectrum(lambda f,**p: self(f,**p) * other(f,**p),
                         name = f'({self.name} * {other.name})')
        elif isscalar(other):
            r = Spectrum(lambda f,**p: self(f,**p) * other,
                         name = f'({self.name} * {other})')
        else:
            raise TypeError('cannot multiply a Spectrum by a %s'%other.__class__.__name__)
        return r.compose(self,other,'mul')
    
    def __rmul__(self,other):
        return self.__mul__(other)
    
    def __truediv__(self,other):
        if isinstance(other,Spectrum):
            r = Spectrum(lambda f,**p: self(f,**p) / other(f,**p),
                         name = f'({self.name} / {other.name})')
        elif isscalar(other):
            r = Spectrum(lambda f,**p: self(f,**p) / other,
                         name = f'({self.name} / {other})')
        else:
            raise TypeError('cannot divide a Spectrum and a %s'%other.__class__.__name__)
        return r.compose(self,other,'truediv')
    
    def __rtruediv__(self,other):
        if isinstance(other,Spectrum):
            r = Spectrum(lambda f,**p: other(f,**p) / self(f,**p),
                         name = f'({other.name} / {self.name})')
        elif isscalar(other):
            r = Spectrum(lambda f,**p: other / self(f,**p),
                         name = f'({other} / {self.name})')
        else:
            raise TypeError('cannot divide a Spectrum and a %s'%other.__class__.__name__)
        return r.compose(self,other,'rtruediv')

    def abs(self):
        return Spectrum(lambda f,**p: self(f,**p).abs(),
                        name = f'|{self.name}|').compose(self,'abs')
    
    def abs2(self):
        return Spectrum(lambda f,**p: (self.abs()**2)(f,**p),
                        name = f'|{self.name}|^2').compose(self,'abs2')
    
    def conj(self):
        return Spectrum(lambda f,**p: self(f,**p).conj(),
                        name = f'({self.name})^*').compose(self,'conj')
    
    def __pow__(self,power):
        name = latex_prep(self.name)
        bra,ket = '{','}'
        p = Fraction(power)
        if p.numerator < 10 and p.denominator < 10:
            e = f'{p}'
        else:
            e = power
        return Spectrum(lambda f: self(f)**power,
                        name = f'({self.name})^{bra}{e}{ket}').compose(self,power,'pow')
        
    def signal(self,**kwargs):
        '''Convert the Spectrum to a Signal
        via the Fourier Transform
        $x(t) = \int_{-\infty}^\infty F(f) df$
        
        Parameters
        ----------
        T : Quantity, time-like
            The sample period
        n : int
            The number of samples
        
        Returns
        -------
        A Signal object
        
        '''
        n = None
        if hasattr(self,'original_samples'):
            n,df = (self.original_samples[x] for x in ['n','df'])
        elif self.comp is not None:
            #for arg in list(self.comp[:-1]):
            for arg in self.comp_list():
                if isinstance(arg,Spectrum) and hasattr(arg,'original_samples'):
                    n,df = (arg.original_samples[x] for x in ['n','df'])
                    break
        if n is None:
            raise Exception('spectrum must be sampled')
        n = n-1
        dt = 1/(n*df)
        dt = dt.to(u.s)
        fn = (1 / (2*dt) ).to(u.Hz)
        self.gen_samples_FFT(fn=fn,n=n)
        f,F,df = (self.samples[x] for x in ['f','F','df'])
        t = np.linspace(0,(n-1)*dt,n)
        x = n*np.fft.ifft(F).real*df
        if 'name' not in kwargs:
            kwargs['name'] = '{\cal{F}}^{-1}\{%s\}'%self.name
        return Signal((t,x),**kwargs ).compose(self,'signal')
    
    def plot(self,fmin=None,fmax=None,n=None,kind=None,os=False,**kwargs):
        '''Plot the Spectrum on a log-log plot.
        It is assumed that the spectrum is Hermetian-symmetric
        (transform of a real signal), so only positive frequencies are plotted.
        
        Parameters:
        -----------
        fmin,fmax: Quantities with frequency-like units
            The plotting min and max, which will be clipped to fit
            in the spectrum's domain limits, if it has them.
            Defaults to 0 Hz and domain limit or 500 Hz.
        n: int
            The number of points to plot. Default is 1024
        kind: str
            Spacing of the frequency sample points, either 'linear' or 'log'.
            Default is 'linear'
        os: boolean
            Plot only the original_samples, if the spectrum has them.
            Defaults to False.
        kwargs:
            Remaining keyword parameters are sent to frequency_domain_plot
        '''
        sam_args = {'fmin':fmin,'fmax':fmax,'n':n,'kind':kind}
        fmin,fmax,n,kind = (self.sampling_defaults[key] if val is None else val for key,val in sam_args.items())
        
        if hasattr(self,'original_samples') and tuple(self.domain)[0] == 'f':
            fmin_os,fmax_os = self.original_samples.f.minmax()
            self.domain = ('f',fmin_os,fmax_os)
            
        if os and hasattr(self,'original_samples'):
            f,F = (self.original_samples[x] for x in ['f','F'])
            posf = f >= 0*u.Hz
            f = f[posf]
            F = F[posf]
        else:
            if isinstance(self.domain,tuple):
                fmin = max(fmin, self.domain[1])
                fmax = min(fmax, self.domain[2])
            else:
                if fmax == np.inf:
                    raise Exception('fmax must be specified')
            self.gen_samples(fmin=fmin,fmax=fmax,n=n,kind=kind)
            f,F = (self.samples[x] for x in ['f','F'])
        
        title = kwargs.pop('title',latex_prep(self.name))
        ylabels = kwargs.pop('ylabels',['Magnitude','Phase, rad'])
        subtitle = kwargs.pop('subtitle',None)
        pi_format = kwargs.pop('pi_format',False)
        if not subtitle and hasattr(self,'latex_formula'):
            subtitle = self.latex_formula.strip('$')
            if hasattr(self,'latex_name'):
                subtitle = self.latex_name.strip('$') + ' = ' + subtitle
            subtitle = '$' + subtitle + '$'
        kwargs.update({'title':title,
                       'subtitle':subtitle,
                       'ylabels':ylabels,
                       'pi_format':pi_format})
        
        frequency_domain_plot((f,F),**kwargs)

    def pole_zero_plot(self,**kwargs):
        ''' pole-zero plot
        '''
        assert all([hasattr(self,key) for key in ['poles','zeros']])
        title = kwargs.pop('title','Pole-Zero plot of {}'.format(self.name))
        subtitle = kwargs.pop('subtitle',None)
        if not subtitle and hasattr(self,'latex_formula'):
            subtitle = self.latex_formula.strip('$')
            if hasattr(self,'latex_name'):
                subtitle = self.latex_name.strip('$') + ' = ' + subtitle
            subtitle = '$' + subtitle + '$'

        kwargs.update({'title':title,'subtitle':subtitle})
        pole_zero_plot(self.poles,self.zeros,**kwargs)

    def powerSpectrum(self,**kwargs):
        r = PowerSpectrum(lambda f,**p: (self.abs2()*(1*u.Hz))(f,**p),
                          name = '\Phi_{%s}'%self.name,
                         )
        return r
        
class TransferFunction(Spectrum):
    '''Transfer function defined in the Laplace domain.
    '''
    def __init__(self,H,name='',**kwargs):
        '''
        Parameters
        ----------
        H : function
            Definition of the transfer function that takes one complex argument, s
            and returns a unitless complex number, the transfer function at s.
            If its a string it will be evaluated in a lambda function during the call.

        name : str
            The name assigned to the Transfer Function
        
        Notes:
        ------
        Other keywords are assigned as attributes of the TransferFunction
        
        '''
        self.name = name
        self.H = H
        params = inspect.signature(H).parameters
        domain = list(params.keys())[0]
        assert domain in ['s']
        self.params = {key:val.default for key,val in params.items() if val.default is not inspect.Signature.empty}
        self.domain = domain
        self.comp = None
        self.__dict__.update(kwargs)

    @property
    def source(self):
        return inspect.getsource(self.H)

    def __call__(self,s=None,**kwargs):
        if self.params:
            kwargs = {key:val for key,val in kwargs.items() if key in self.params} # ignore keywords that aren't parameters
        params = {**self.params,**kwargs} #union the existing and incoming parameters
        if s is None: # just set the parameters and return a copy
            r = self.copy()
            r.params = params
            r.H = functools.partial(r.H,**kwargs)
            if hasattr(self,'loop_transfer'):
                r.loop_transfer = self.loop_transfer(**kwargs)
            return r
        if call_trace:
            print(f'calling {self.name} with {params}')
        if isinstance(s,list): s = u.Quantity(s)
        H = self.H
        with np.errstate(divide='ignore',invalid='ignore'):
            r = H(s,**kwargs)
            if hasattr(r,'unit'): r = r.decompose()
        return r
    
    def __add__(self,other):
        if isinstance(other,TransferFunction):
            r = TransferFunction(lambda s,**p: self(s,**p) + other(s,**p),
                                 name = f'({self.name} + {other.name})')
        elif isscalar(other):
            r = TransferFunction(lambda s,**p: self(s,**p) + other,
                                 name = f'({self.name} + {other})')
        else:
            raise TypeError('cannot add a TransferFunction to a %s'%other.__class__.__name__)
        return r.compose(self,other,'add')
        
    def __radd__(self,other):
        return self.__add__(other)

    def __sub__(self,other):
        if isinstance(other,TransferFunction):
            r = TransferFunction(lambda s,**p: self(s,**p) - other(s,**p),
                                 name = f'({self.name} - {other.name})')
        elif isscalar(other):
            r = TransferFunction(lambda s,**p: self(s,**p) - other,
                                 name = f'({self.name} - {other})')
        else:
            raise TypeError('cannot difference a TransferFunction and a %s'%other.__class__.__name__)
        return r.compose(self,other,'sub')

    def __rsub__(self,other):
        if isinstance(other,TransferFunction):
            r = TransferFunction(lambda s,**p: other(s,**p) - self(s,**p),
                                 name = f'({other.name} - {self.name})')
        elif isscalar(other):
            r = TransferFunction(lambda s,**p: other - self(s,**p),
                                 name = f'({other} - {self.name})')
        else:
            raise TypeError('cannot difference a TransferFunction and a %s'%other.__class__.__name__)
        return r.compose(self,other,'rsub')

    def __mul__(self,other):
        if isinstance(other,TransferFunction):
            r = TransferFunction(lambda s,**p: self(s,**p) * other(s,**p),
                                 name = f'({other.name} * {self.name})')
        elif isinstance(other,PowerSpectrum):
            i = 1j
            r = PowerSpectrum(lambda f,**p: self(2*pi*i*f,**p) * other(f,**p),
                              name = f'({other.name} * {self.name})')
        elif isinstance(other,Spectrum):
            i = 1j
            r = Spectrum(lambda f,**p: self(2*pi*i*f,**p) * other(f,**p),
                         name = f'({other.name} * {self.name})')
        elif isscalar(other):
            r = TransferFunction(lambda s,**p: self(s,**p) * other,
                                 name = f'({other} * {self.name})')
        elif isinstance(other,Signal): # convolution
            return (self*other.spectrum()).signal()
        else:
            raise TypeError('cannot multiply a TransferFunction by a %s'%other.__class__.__name__)
        return r.compose(self,other,'mul')
        
    def __rmul__(self,other):
        return self.__mul__(other)

    def __truediv__(self,other):
        if isinstance(other,TransferFunction):
            r = TransferFunction(lambda s,**p: self(s,**p) / other(s,**p),
                                 name = f'({self.name} / {other.name})')
        elif isscalar(other):
            r = TransferFunction(lambda s,**p: self(s,**p) / other,
                                 name = f'({self.name} / {other})')
        else:
            raise TypeError('cannot divide a TransferFunction and a %s'%other.__class__.__name__)
        return r.compose(self,other,'truediv')
    
    def __rtruediv__(self,other):
        if isinstance(other,TransferFunction):
            r = TransferFunction(lambda s,**p: other(s,**p) / self(s,**p) ,
                                 name = f'({other.name} / {self.name})')
        elif isscalar(other):
            r = TransferFunction(lambda s,**p: other / self(s,**p) ,
                                 name = f'({other} / {self.name})')
        else:
            raise TypeError('cannot divide a TransferFunction and a %s'%other.__class__.__name__)
        return r.compose(self,other,'rtruediv')
    
    def __neg__(self):
        return TransferFunction(lambda s,**p: -self(s,**p),
                                name = f'(-{self.name})').compose(self,'neg')
    
    def abs(self):
        return TransferFunction(lambda s,**p: self(s,**p).abs(),
                                name = f'|{self.name}|').compose(self,'abs')
    
    def abs2(self):
        return TransferFunction(lambda s,**p: (self.abs()**2)(s,**p),
                                name = f'|{self.name}|^2').compose(self,'abs2')
    
    def conj(self):
        name = latex_prep(self.name)
        return TransferFunction(lambda s,**p: self(s,**p).conj(),
                                name = f'({self.name})^*').compose(self,'conj')
    
    def __pow__(self,power):
        name = latex_prep(self.name)
        bra,ket = '{','}'
        p = Fraction(power)
        if p.numerator < 10 and p.denominator < 10:
            e = f'{p}'
        else:
            e = power
        return TransferFunction(lambda s,**p: self(s,**p)**power,
                                name = f'({self.name})^{bra}{e}{ket}').compose(self,power,'pow')
    
    def impulse_response(self,Tsamp=None,window='blackman',n=1024):
        T = Tsamp
        if T is None:
            if 'T' in self.params:
                T = self.params['T']
            else:
                raise Exception('need to specify a sample period')
        fn = ( 1 / (2*T) ).to('Hz')
        df = 1/T
        f = np.linspace(0,fn,n+1)
        w = {'blackman':np.blackman,
             'hamming':np.hamming,
             'hanning':np.hanning}
        if window in w:
            w = w[window](2*n+1)[n:]
        else:
            w = 1
        H_pos_freq = self.H(i*2*pi*f)*w
        h = np.fft.irfft(H_pos_freq)*df
        t = np.arange(len(h))*T
        name = 'Impulse Response: {}'.format(self.name)
        return Signal((t,h),name=name).compose(self,'impulse_response:%s'%window)
    
    def plot(self,**kwargs):
        '''Plot the Transfer Function on a log-log plot.
        
        Keyword arguments:
        ------------------
        fmin, fmax, n, kind -- go to gen_samples
        title, subtitle, ylables -- go to frequency_domain_plot
        all other keywords are forwarded to matplotlib's axis.plot
        
        n : int
            Number of points to plot
        fmin, fmax : Quantity, frequency-like
            The minimum and maximum frequency range of the plot
        kind: str, 'linear' or 'log'
            frequency sample spacing
        title, subtitle: str
            title and subtitle shown at the top of the plot
        ylabels: tuple of strings
            The y axis labels of the magnitude and phase charts
        '''
        sam_args = dict([(key,kwargs.pop(key)) if key in kwargs else (key,self.sampling_defaults[key]) for key in ['fmin','fmax','n','kind']])
        self.gen_samples(**sam_args)
        f,H = (self.samples[x] for x in ['f','H'])
        
        title = kwargs.pop('title', latex_prep(self.name))
        ylabels = kwargs.pop('ylabels',None)
        subtitle = kwargs.pop('subtitle',None)
        if not ylabels and hasattr(self,'latex_name'):
            name = self.latex_name.strip('$')
            ylabels = ['$|{}|$'.format(name),'$\\angle {}$'.format(name)]
        if not subtitle and hasattr(self,'latex_formula'):
            subtitle = self.latex_formula.strip('$')
            if hasattr(self,'latex_name'):
                subtitle = self.latex_name.strip('$') + ' = ' + subtitle
            subtitle = '$' + subtitle + '$'
            
        frequency_domain_plot((f,H),title=title,subtitle=subtitle,ylabels=ylabels,**kwargs)

        fig = plt.gcf()
        ax = fig.axes[0]
        ax.axhline(1,color='black',lw=2)

    def bode_plot(self,**kwargs):
        '''produce a Bode plot.
        
        Keyword arguments:
        ------------------
        fmin,fmax,n,kind -- go to gen_samples
        title,fig,unwrap,ylabels -- go to bode_plot
        all other keywords are forwarded to matplotlib's axis.plot routine
        '''
        kwargs1 = {x:kwargs.pop(x) for x in ['fmin','fmax','n','kind'] if x in kwargs}
        self.gen_samples(**kwargs1)
        f = self.gened_samples.f
        s = self.gened_samples.s
        H = self
        if 'title' not in kwargs:
            kwargs['title'] = latex_prep(self.name)
            if hasattr(self,'latex_name') and hasattr(self,'latex_formula'):
                kwargs['title'] = '$' + self.latex_name.strip('$') + '=' + self.latex_formula.strip('$') + '$'
        bode_plot(f=f,H=H(s),**kwargs)
        ax1,ax2 = plt.gcf().axes
        gm = self.gain_margin()
        if gm is not np.nan:
            ax1.text(1.,1.,f'gain margin = {gm:.2f}',ha='right',va='bottom',transform=ax1.transAxes)
        pm = self.phase_margin().to(u.deg)
        if pm is not np.nan:
            ax2.text(1.,1.,f'phase margin = {pm:.2f}',ha='right',va='bottom',transform=ax2.transAxes)
        plt.suptitle(self.name+': '+self.name)
        
    def nyquist_plot(self,**kwargs):
        '''produce a Nyquist plot.
        
        Keyword arguments:
        ------------------
        fmin,fmax,n,kind -- go to gen_samples
        title,fig -- go to nyquist_plot
        all other keywords are forwarded to matplotlib's axis.plot routine
        '''
        kwargs1 = {x:kwargs.pop(x) for x in ['fmin','fmax','n','kind'] if x in kwargs}
        self.gen_samples(**kwargs1)
        f = self.gened_samples.f
        s = self.gened_samples.s
        H = self
        if 'title' not in kwargs: kwargs['title'] = latex_prep(self.name)
        nyquist_plot(f=f,H=H(s),**kwargs)

    def nichols_chart(self,**kwargs):
        '''produce a Nichols chart .
        
        Keyword arguments:
        ------------------
        fmin,fmax,n,kind -- go to gen_samples
        title,fig,unwrap -- go to nichols_chart
        all other keywords are forwarded to matplotlib's axis.plot routine
        '''
        kwargs1 = {x:kwargs.pop(x) for x in ['fmin','fmax','n','kind'] if x in kwargs}
        self.gen_samples(**kwargs1)
        f = self.gened_samples.f
        s = self.gened_samples.s
        H = self
        if 'title' not in kwargs: kwargs['title'] = latex_prep(self.name)
        nichols_chart(f=f,H=H(s),**kwargs)
        
    def gain_margin(self,**kwargs):
        '''Determine the gain margin if the TranferFunction
        is the loop transfer in closed loop.
        The gain margin is defined as the maximum factor by which an
        open loop transfer function can be multiplied and still
        remain stable in closed loop.
        1/(1+gH(s)) is stable for 0 > g > gain margin
        gain margin > 1 means 1/(1+H(s)) is stable
        gain margin < 1 means 1/(1+H(s)) is unstable 
        '''
        # look for the places where H(s) crosses the negative real axis
        # by detecting a 2 pi jump in phase
        n = kwargs.pop('n',self.sampling_defaults['n'])
        fmax = kwargs.pop('fmax',self.sampling_defaults['fmax'])
        self.gen_samples(n=n,fmax=fmax)
        f = self.gened_samples.f
        s = self.gened_samples.s
        H = self
        phase = H(s).phase
        phase_jump = np.abs(np.diff(phase))
        ks = np.where(phase_jump > (np.pi*u.rad/2))[0]
        if len(ks) == 0:
            return np.nan
        f0 = f[ks]
        i = 1j
        s = 2*pi*i*f0
        gm = 1./H(s).mag
        return min(gm)

    def phase_margin(self,**kwargs):
        '''Determine the phase margin if the TransferFunction
        is the loop transfer in closed loop.
        The phase margin is defined as the additional phase lag that
        H(s) could have and still remain stable in closed loop.
        1/(1+H(s)e^{-i phi}) is stable for 0 < phi < phase margin
        A positive phase margin means 1/(1+H(s)) is stable
        A negative phase margin means 1/(1+H(s)) is unstable
        '''
        n = kwargs.pop('n',self.sampling_defaults['n'])
        fmax = kwargs.pop('fmax',self.sampling_defaults['fmax'])
        self.gen_samples(n=n,fmax=fmax)
        f = self.gened_samples.f
        s = self.gened_samples.s
        H = self
        f0 = zero_crossing(f,H(s).mag,y0=1)
        if len(f0) == 0:
            return np.nan
        phase = interp1d(f,np.unwrap(H(s).phase))
        pm = (phase(f0) + np.pi)*u.rad
        #pm = H(s).phase + np.pi*u.rad
        return min(pm)
        
    @property
    def is_stable(self):
        '''Determine whether, the TransferFunction is stable
        under closed loop operation. If the object has
        'loop_tranfer' as an attribute, the loop_transfer
        tested. The test is 
        whether the magnitude of LT(s) < 1 at the freqency where
        its phase is -pi, i.e. whether the gain margin is > 1.
        '''
        if hasattr(self,'loop_transfer'):
            return self.loop_transfer.is_stable
        gm = self.gain_margin()
        return gm > 1.

    @property
    def crossover_frequency(self,**kwargs):
        n = kwargs.pop('n',self.sampling_defaults['n'])
        kmax = kwargs.pop('kmax',self.sampling_defaults['fmax'])
        self.gen_samples(n=n,kmax=kmax)
        f = self.gened_samples.f
        s = self.gened_samples.s
        H = self       
        return zero_crossing(f,H(s).mag,y0=1)
    
    def gen_samples(self,fmin=0*u.Hz,fmax=None,kind='linear',n=None,**kwargs):
        '''generate a list of frequency samples and
        corresponding samples along the s +imaginary axis
        from frequency 0 to fmax inclusive.
        
        Parameters
        ----------
        kind : str
            frequency sampling, either 'linear' or 'log'
        n : int
            the number of frequency samples
        fmax : Quantity
            the maximum frequency. Must have frequency-like units
        
        '''
        if fmax is None: fmax = self.sampling_defaults['fmax']
        if n is None: n = self.sampling_defaults['n']
        fmin = fmin.to(u.Hz)
        fmax = fmax.to(u.Hz)        
        i = 1j
        if kind == 'linear':
            df = (fmax-fmin)/(n-1)
            f = np.linspace(fmin,fmax,n)
        elif kind == 'log':
            df = None
            f = np.logspace(np.log10(fmax/(n*u.Hz)),np.log10(fmax/u.Hz),n)*u.Hz
        s = 2*pi*i*f
        H = self(s)
        samples = DotDict({
            'f':f,
            's':s,
            'H':H,
            'df':df,
            'n':n,
            'fmax':fmax,
            'kind':kind,
            'creation':'gen',
        })
        self.gened_samples = samples
        return f,s,H

def delay(tau):
    return TransferFunction(lambda s,tau=tau: exp(-s*tau),
                            name = 'delay',
                            formula = 'exp(-s*delta)',
                            latex_name = '$H_{\\delta}(s)$',
                            latex_formula = '$e^{-s\\delta}$',
                           )
def LPF(tau):
    return TransferFunction(lambda s,tau=tau: 1/(1+s*tau),
                            name = 'low pass filter',
                            formula = '(1/(1+s*tau))',
                            latex_name = '${LPF}(s)$',
                            latex_formula = '${1}/{1+s \\tau}'
                            )

def AOdefs(T=1*u.ms,
           tau=800*u.us,
           gamma = .999,  # integrator bleed
           fw = 200*u.Hz, # physical woofer 3db frequency
           alpha = 0.5, # crossover filter parameter - use None to compute based on fw
           g=1,
           globalize=False):
    f = 10*u.Hz # test frequency
    s = 2*pi*i*f
    TF = TransferFunction
    if alpha is None:
        alpha =  1/(2*np.pi*T*fw + 1) # crossover filter parameter
    
    def zoh_function(s,T=T):
        z = np.isclose(s.abs().value,0)
        r = (1-exp(-s*T))/(s*T)
        r[z] = 1.
        return r
    
    zoh = TF(lambda s,T=T: zoh_function(s,T=T),
             name = 'zoh',
             long_name='zero order hold',
             formula = '(1-exp(-s*T))/(s*T))',
             latex_name = '$H_{ZOH}(s)$',
             latex_formula = '${1-e^{-sT}}/{sT}$'
             )
    lpfilter = TF(lambda s,tau=tau: 1/(1+s*tau),
                  name = 'low pass filter',
                  formula = '(1/(1+s*tau))',
                  latex_name = '${LPF}(s)$',
                  latex_formula = '${1}/{1+s \\tau}'
                  )
    Hwfs = TF(lambda s,T=T: zoh(s,T=T),
            name = 'Hwfs',
            long_name='Wavefront sensor stare (sample and hold)',
            formula = '(1-exp(-s*T))/(s*T))',
            latex_name = '$H_{wfs}(s)$',
            latex_formula = zoh.latex_formula,
            )
    Hr = TF(lambda s,T=T: exp(-s*T),
            name = 'Hr',
            long_name = 'Sensor readout delay',
            formula = 'exp(-s*T)',
            latex_name = '$H_r(s)$',
            latex_formula = '$e^{-sT}$',
            )
    Hdel = TF(lambda s,tau=tau: exp(-s*tau),
            name = 'Hdel',
            long_name = 'Compute delay',
            formula = 'exp(-s*tau)',
            latex_name = '$H_{\\delta}(s)$',
            latex_formula = '$e^{-s\\tau}$',
            )
    Hc = TF(lambda s,gamma=gamma,T=T: (1/(1-gamma*exp(-s*T))),
            name = 'Hc',
            long_name='Feedback control law',
            formula = '(1/(1-gamma*exp(-s*T)))',
            latex_name = '$H_c(s)$',
            latex_formula = '$1/\\left(1 - \\gamma e^{-sT}\\right)$',
            )
    Hdm = TF(lambda s,T=T: zoh(s,T=T),
            name = 'Hdm',
            long_name='Deformable mirror (sample and hold)',
            formula='(1-exp(-s*T))/(s*T)',
            latex_name = '$H_{dm}(s)$',
            latex_formula = zoh.latex_formula,
            )
    Hw = TF(lambda s,fw=fw: 1/(1+s/(2*np.pi*fw)),
            name = 'Hw',
            long_name = 'Woofer roll-off',
            latex_name = '$H_w(s)$',
            latex_formula = '$1 / (1 + s / 2\\pi f_w)$',
            )
    Hf = TF(lambda s,T=T,alpha=alpha: exp(-s*T)*(1-alpha)/(1-alpha*exp(-s*T)),
            name = 'Hf',
            long_name = 'Woofer crossover filter',
            latex_name = '$H_f(s)$',
            latex_formula = '$z^{-1} (1-\\alpha) / (1-\\alpha z^{-1})$',
            )

    Hol = Hdm*Hc*Hdel*Hr*Hwfs
    Hol.setattrs(name = 'AO open loop transfer function',
             formula = 'Hdm(s)*Hc(s)*Hdel(s)*Hr(s)*Hwfs(s)',
             latex_name = '$H_{ol}(s)$',
             latex_formula = '$H_{dm}(s) H_c(s) H_\\delta(s) H_r(s) H_{wfs}(s)$')
    
    Holfw = Hwfs*Hr*Hdel*Hc*((1 - Hf)*Hdm + Hdm(T=5*u.ms)*Hw)
    Holfw.setattrs(name = 'AO open loop with woofer crossover',
               formula = 'Hwfs*Hr*Hdel*Hc*((1 - Hf)*Hdm + Hdm(T=5*u.ms)*Hw)',
               latex_name = '$H_{olwt}(s)$',
               latex_formula = '$(1-H_f(s)+Hw(s))$')
    
    g = TF(lambda s,g=.5: g,
           name = 'g',
           long_name = 'loop gain',
           latex_name = '$g$',
           latex_formula = '$g$')
    
    Hcl = 1 / (1 + g*Hol)
    Hcl.setattrs(name = 'AO closed loop transfer function',
             formula='1/(1+g*Hol(s))',
             latex_name = '$H_{cl}(s)$',
             latex_formula = '$1 / \\left( 1 + g H_{ol}(s) \\right)$',
             loop_transfer = g*Hol)

    Hn = g*Hol / (1 + g*Hol)
    Hn.setattrs(name = 'AO noise transfer function',
            formula = 'g*Hol(s)/(1+g*Hol(s)',
            latex_name = '$H_n(s)$',
            latex_formula = '$ g H_{ol}(s) / \\left( 1 + g H_{ol}(s) \\right)$',
            loop_transfer = g*Hol)
    
    if globalize:
        globals().update(locals())
    else:
        d = {}
        d.update(locals())
        keys = ['g','zoh','lpfilter','Hwfs','Hr','Hdel','Hc','Hdm','Hw','Hf','Hol','Holfw','Hcl','Hn']
        r = {key:d.get(key) for key in keys}
        return r

class PowerSpectrum(Spectrum):
    '''Power Spectrum defined for f>=0.
    
    Parameters
    ----------
    S : function
        A function that computes the power spectrum
        given a real frequency argument, `f`, and produces
        a real, positive, value `S(f)`.
        Note: this is :math:`S(f)` for :math:`f>0`, NOT
        :math:`S(f)+S(-f)` as used in [1]_.
    
    References
    ----------
    .. [1] Gavel, Derivation of atmospheric power spectra relevant to adaptive optics control systems,
       UCSC internal memo, July 2020.
       
    '''
    def __init__(self,S,name='',**kwargs):
        self.name = name
        if callable(S):
            self.S = S
            params = inspect.signature(S).parameters
            domain = list(params.keys())[0]
            if hasattr(S,'domain'): domain = S.domain
            if 'domain' in kwargs: domain = kwargs.pop('domain')
            assert tuple(domain)[0] == 'f'
            self.domain = domain
            self.signature = inspect.signature(S)
            self.params = {key:val.default for key,val in params.items() if val.default is not inspect.Signature.empty}
            self.unit = S(1*u.Hz).unit
        elif isinstance(S,tuple):
            f,S = S
            n = len(f)
            df = ( f[1]-f[0] )
            samples = DotDict({
                'f':f,
                'S':S,
                'df':df,
                'n':n,
            })
            self.original_samples = samples
            self.S = interp1d(f,S,kind='cubic')
            self.domain = ('f',samples.f.min(),samples.f.max())
            self.params = {}
            self.unit = S.unit
        else:
            raise Exception('S must be a callable function or samples tuple (f,S(f))')
        self.comp = None
        self.__dict__.update(kwargs)
    
    @property
    def source(self):
        return inspect.getsource(self.S)
    
    def __call__(self,f=None,**kwargs):
        '''compute the power spectrum at frequency f
        '''
        #if not self.bundle:
        #    kwargs = {key:val for key,val in kwargs.items() if key in self.params}
        if f is None: # just set the parameters and return a copy
            kwargs = {key:val for key,val in kwargs.items() if key in self.params}
            r = self.copy()
            r.S = functools.partial(r.S,**kwargs)
            r.params = {**self.params,**kwargs}
            return r
        if isinstance(f,list): f = u.Quantity(f)
        if isinstance(self.S,interp1d):
            f = f.to(u.Hz)
            return u.Quantity(self.S(f), self.unit)
        else:
            with np.errstate(divide='ignore',invalid='ignore'):
                r = self.S(f,**kwargs)
            return r

    def __mul__(self,other):
        if isinstance(other,TransferFunction):
            i = 1j
            r = PowerSpectrum(lambda f,**p: self(f,**p)*other(2*pi*i*f,**p),
                              name = f'({self.name} * {other.name})')
        elif isscalar(other):
            r = PowerSpectrum(lambda f,**p: self(f,**p)*other,
                              name = f'({other} * {self.name})')
        else:
            raise TypeError('cannot multiply a PowerSpectrum and a %s'%other.__class__.__name__)
        return r.compose(self,other,'mul')
    
    def __rmul__(self,other):
        return self.__mul__(other)
    
    def __add__(self,other):
        if isinstance(other,PowerSpectrum):
            r = PowerSpectrum(lambda f,**p: self(f,**p) + other(f,**p),
                              name = f'({self.name} + {other.name})')
        else:
            raise TypeError('cannot add a PowerSpectrum to a %s'%other.__class__.__name__)
        return r.compose(self,other,'add')
    
    def __sub__(self,other):
        raise TypeError('cannot subtract PowerSpectra')
    
    def __rsub__(self,other):
        raise TypeError('cannot subtract PowerSpectra')
    
    def __truediv__(self,other):
        if isscalar(other):
            r = PowerSpectrum(lambda f,**p: self(f,**p)/other,
                              name = f'({self.name} / {other})')
        else:
            r = super().__truediv__(other)
        return r.compose(self,other,'truediv')
        
    def plot(self,os=False,**kwargs):
        '''plot the power spectrum vs frequency on a log-log plot
        
        Parameters
        ----------
        n : int
            the number of samples. Default is 1024
        fmax, fmin : frequency-like Quantity
            the maximum and minimum frequencies of the plot. Default is 0 Hz to 500 Hz,
            or the within the domain of the power spectrum function
        title : str
            the title on top of the plot. Defaults to 'Power Spectrum'
        subtitle : str
            a subtitle to put on a line just under the title. Defaults to the name of the PowerSpectrum object.
        fig : matplotlib.figure.Figure, int, or str
            the matplotlib.figure.Figure on which to plot or over-plot. Default is create a new figure
        
            Additional keyword arguments are passed to frequency_domain_plot
        
        '''
        sam_args = dict([(key,kwargs.pop(key)) if key in kwargs else (key,self.sampling_defaults[key]) for key in ['fmin','fmax','n','kind']])
        fmin,fmax,n,kind = (sam_args[x] for x in ['fmin','fmax','n','kind'])

        if hasattr(self,'original_samples') and tuple(self.domain)[0] == 'f':
            fmin_os,fmax_os = self.original_samples.f.minmax()
            self.domain = ('f',fmin_os,fmax_os)
            
        if os and hasattr(self,'original_samples'):
            f,S = (self.original_samples[x] for x in ['f','F'])
            posf = f >= 0*u.Hz
            f = f[posf]
            S = S[posf]
        else:
            if isinstance(self.domain,tuple):
                fmax = min(fmax,self.domain[2])
                fmin = max(fmin,self.domain[1])
            else:
                if fmax == np.inf:
                    raise Exception('fmax must be specified')
            with np.errstate(divide='ignore',invalid='ignore'):
                self.gen_samples(kind=kind,n=n,fmin=fmin,fmax=fmax)
            f,S = (self.samples[x] for x in ['f','F'])
        
        title = kwargs.pop('title',latex_prep(self.name))
        ylabels = kwargs.pop('ylabels',None)
        subtitle = kwargs.pop('subtitle',None)
        fig = kwargs.get('fig',None)
        exfig = get_fig(fig)
        if not exfig and not ylabels: ylabels = ['$S(f)$','']
        if not subtitle and hasattr(self,'latex_formula'):
            subtitle = self.latex_formula.strip('$')
            if hasattr(self,'latex_name'):
                subtitle = self.latex_name.strip('$') + ' = ' + subtitle
            subtitle = '$' + subtitle + '$'
        kwargs.update({'title':title,
                       'subtitle':subtitle,
                       'ylabels':ylabels})

        frequency_domain_plot((f,S),**kwargs)
            
    def instance(self,dt=1*u.ms,n=1024,check=False,**kwargs):
        '''Use Monte-Carlo simulation  to generate
        a random signal instance having the power spectrum.
        
        Parameters
        ----------
        n : int
            The number of frequency points computed
            and time samples generated.
        check : boolean
            Optional: compute Parseval theorm compliance tests
            Tests results are approximate because of stochastics
        dt : Quantity (time-like units)
            Sample time. Default is 1 ms
        fn : Quantity (frequency-like units)
            Nyquist frequency. Overrides dt.
        
        Returns
        -------
        (t,x) : tuple
            where `t` is a list of the sample times and `x` is the list of sample data
            
        References
        ----------
        .. [2] Gavel, Simulating stochastic noise processes numerically
           in the discrete time and frequency domain, UCSC Memo, July, 2020.
        
        '''
        fn = ( 1/(2*dt) ).to(u.Hz)
        if 'fn' in kwargs or 'fmax' in kwargs:
            fn = kwargs.pop('fn',None)
            fn = kwargs.pop('fmax',fn)
            dt = ( 1/(2*fn) ).to(u.s)
        fmin = 0*u.Hz
        
        self.gen_samples(fmin=fmin,fmax=fn,n=n//2+1) # from zero to Nyquist, inclusive
        f,S = (self.samples[x] for x in ['f','F'])
        df = self.samples.df
        assert dt == 1/(n*df)
        
        re,im = np.random.normal(size=(2,n//2+1))
        N = (re + i*im)/np.sqrt(2)
        N[0] = re[0]
        N[-1] = re[-1]
        R = np.sqrt(S/df)*N # equation 36 in reference 2
        rn = np.fft.irfft(R,n=n)*n*df # n to undo the normalization, df to do the Riemann sum
        t = np.arange(0,n)*dt
        
        # check Parseval consistency
        if check:
            print('-------')
            print('check Parseval - these two numbers should be exactly the same')
            print('time domain:',(np.sum(rn*rn)*dt/(n*dt)).to(rn.unit**2))
            print('frequency domain:',(2*(np.sum(R*R.conj())-R[0]*R[0]/2-R[-1]*R[-1]/2).real*df/(n*dt)).to(rn.unit**2) )
        
        # integral of Power Spectrum = variance:
        if check:
            print('-------')
            print('integrate power spectrum - both pos and neg frequencies')
            print('the first two numbers should be approximately the same')
            print(((2*np.sum(S)-S[0])*df),'sum of samples of PS')
            print(self.integral(),'using PS.integral()')
            print((rn*rn).mean(),'mean square of generated noise')
            print(rn.var(),'variance of generated noise')
            print('-------')
        
        return Signal((t,rn),**kwargs).compose(self,'instance')
    
    def integrate(self,**kwargs):
        return self.integral(**kwargs)
    
    def integral(self,n=1024,fmin=0*u.Hz,fmax=500*u.Hz,one_side=False,ignore_nan_vals=True,use_scipy=False,**kwargs):
        '''Integrate the power spectrum to return the variance.
        Computes
        
        .. math:: \\int_{0}^{f_{\\text {max}}} S(f) \,\\text{d}f
        
        via Riemann sum.
        
        Parameters
        ----------
        n : int
            The number of samples to use in the Riemann sum
            (answer should be roughly independent of this, if sufficiently large)
        fmin, fmax : Quantity with frequency type units (e.g. Hz)
            The minimum and maximum frequency limits up to which to integrate
            (sorry, can't be infinity!)
        one_side : boolean
            an option for how to compute the integral,
            True: 2x integral from f = 0 to 'infinity' (actually to Nyquist)
            False: integral from f = -'infinity' to +'infinity'
            Either way should give roughly the same answer;
            it depends on the rapidity of the function and
            sampling near f=0.
        ignore_nan_vals : boolean
            integrate only the non-nan values, i.e. treat the nan
            values as if they were zero.
        kwargs : (more arguments)
            additional keyword arguments, which are passed to scipy.integrate
        
        Returns
        -------
        Quantity, having units of the integrand :math:`\\times` *Hz*
        '''
        with warnings.catch_warnings():
            warnings.filterwarnings('ignore',category=RuntimeWarning)
            if use_scipy:
                if not hasattr(self,'unit'):
                    self.unit = self(0*u.Hz).unit
                fmin = fmin.to(u.Hz).value
                fmax = fmax.to(u.Hz).value
                return scipy.integrate.quad(lambda f: self(f*u.Hz).value,fmin,fmax,**kwargs)[0]*2*self.unit*u.Hz
            i = 1j
            self.gen_samples(n=n,fmin=fmin,fmax=fmax)
            f,S,df = (self.samples[x] for x in ['f','F','df'])
            fmax = f.max()
            if one_side:
                S = samples.F
            else: # two side
                i0 = 1 if fmin == 0 else 0
                f = np.block([-f[i0:][::-1],f])
                S = self(f.abs())
    
            if ignore_nan_vals: # also means ignore infs
                _sum = np.ma.masked_invalid(S).sum()
                #_sum = np.nansum
            else:
                _sum = np.sum(S)
            
            v = _sum*df
            
            if one_side:
                S0 = self(0*u.Hz)
                if ignore_nan_vals and not np.isfinite(S0): S0 = u.Quantity(0,S0.unit)
                v = 2*v - S0*df # <---- add both sides but don't double count S(0)
            # NOTE: this presumes that the provided spectrum
            # is just one side of the two-sided spectrum, S(f)
            # NOT the S(f)+S(-f) used in [1]
        return v

    def rms(self,**kwargs):
        '''calculate the rms of the power spectrum in radians phase
        '''
        pi = np.pi
        var = self.integrate(**kwargs)
        sd = np.sqrt(var)
        return sd
        
    def depricated_gen_samples(self,kind='linear',n=1024,fmin=0*u.Hz,fmax=500*u.Hz):
        '''generate a list of frequency samples
        
        Parameters
        ----------
        n : int
            The number of samples
        fmin, fmax : Quantity with frequency units
            The minimum and maximum frequency up to which to sample
        kind : str
            The sample spacing method,
            can be 'linear' or 'log'
        
        '''
        samples = DotDict()
        samples.kind = kind
        samples.n = n
        if kind == 'linear':
            df = (fmax-fmin)/n
            samples.df = df
            f = np.arange(fmin/u.Hz,fmax/u.Hz,df/u.Hz)*u.Hz
        elif kind == 'log':
            f = np.logspace(np.log10(fmax/(n*u.Hz)),np.log10(fmax/u.Hz),n)*u.Hz
        samples.f = f
        samples.S = self(f)
        samples.fmax = fmax
        self.gened_samples = samples
        return samples

def KolmogorovSpectrum(r0=10*u.cm,
                       v=10*u.m/u.s):
    Sphi = PowerSpectrum(
        lambda f, v=v,r0=r0,f0=f0: ( (c3/2)*(v/r0)**(5/3)*f**(-8/3) ).to(1/u.Hz)*u.rad**2,
        name = 'Kolmogorov spectrum',
        comment = 'one side, S(f), f>0, not doubled',
        #unit = u.rad**2/u.Hz,
        formula = '(c3/2)*(v/r0)**(5/3)*f**(-8/3)',
        latex_name = '$S_{\\phi}(f)$',
        latex_formula = '$\\frac{1}{2} c_3 \\left(v/r_0\\right)^{5/3} f^{-8/3}$',        
    )
    return Sphi

def vonKarmanSpectrum(r0=10*u.cm,
                      v=10*u.m/u.s,
                      L0=3*u.m):
    f0 = (v/L0).to('Hz')
    ff = lambda f, f0: np.sqrt(f**2 + f0**2)
    Sphi = PowerSpectrum(
        lambda f, v=v,r0=r0,f0=f0: ( (c3/2)*(v/r0)**(5/3)*ff(f,f0)**(-8/3) ).to('1/Hz')*u.rad**2, # just one side, S_phi(f) f>0
        name = 'von Karman spectrum',
        comment = 'one side, S(f), f>0, not doubled',
        #unit = u.rad**2/u.Hz,
        formula = '(c3/2)*(v/r0)**(5/3)*sqrt(f**2+f0**2)**(-8/3)',
        latex_name = '$S_{\\phi}(f)$',
        latex_formula = '$\\frac{1}{2} c_3 \\left(v/r_0\\right)^{5/3} \\sqrt{f^2+f_0^2}^{-8/3}$',
        )
    return Sphi

def modelSpectrum(r0=10*u.cm,
                  v=10*u.m/u.s,
                  p=8/3,
                  q=1.,
                  f0=2.8*u.Hz,
                  Sn=85.6*u.nm**2/u.Hz):
    '''Model Power Spectrum.
    Make a model open-loop power spectrum with parameters compatible with
    telemetry_analysis Model.fit parameters. An example data set is
    date='2021-09-17' dsn=[0,1] (1 is the closed loop data).
    
    Using Holfw from AOdefs(T=2*u.ms) as a woofer-tweeter system model
    we calculate the observed closed-loop spectrum Scl = (S + Sn)*Hclfw().abs2()
    using the definition Hclfw = 1/(1+g*Holfw).
    g=.4, alpha=.4, fw=60*u.Hz, gamma=.98 has been the best fit so far.
    '''
    lam0 = 500*u.nm
    pi = np.pi
    Sphi = PowerSpectrum(
        lambda f,r0=r0,v=v,f0=f0,p=p,q=q: (c3*(v/r0)**(p-1)*(f**2+f0**(2*(p-q)/p)*f**(2*q/p))**(-p/2)).to(1/u.Hz)*(lam0/(2*pi))**2,
        name = 'model spectrum',
        comment = 'two sides, folded (doubled S(f))',
        formula = 'c3 (v/r0)^(p-1) / (f^2 + f0^(2(p-q)/p)) f^(2q/p))^(p/2)',
        latex_name = '$S_{m}(f)$',
        latex_formula = '$c_3 \\left(v/r_0\\right)^{p-1} (f^2 + f_0^{2(p-q)/p} f^{2q/p} )^{-p/2}$',
        )
    Snf = PowerSpectrum(
        lambda f,a=Sn: a*unity(f),
        name = 'model noise spectrum',
        comment = 'two sides, folded (doubled S(f))',
        formula = 'Sn',
        latex_name = '$S_n$',
        latex_formula = '$S_n$',
        )
    return Sphi,Snf
    
def GFSpectrum(r0=10*u.cm,
               r1=30*u.cm,
               v=1*u.m/u.s,
               p = 8/3,
               q = 2/3,
               lam0 = 550.*u.nm):
    k = 2*np.pi/lam0
    f1 = (v/r1).to(u.Hz)
    ff = lambda f, f1,p,q: (f**2 + f1**(2*(p-q)/p)*f**(2*q/p))**(-p/2)
    Sphi = PowerSpectrum(
        lambda f,v=v,r0=r0,f1=f1,p=p,q=q: (( (c3/2)*(v/r0)**(5/3)*ff(f,f1,p,q) )*(1/k**2)).to(u.nm**2/u.Hz),
        name = 'G-F Spectrum',
        #unit = u.nm**2/u.Hz,
    )
    return Sphi

def slope_sigma(flux = {'n':500.*u.ct,'d':20*u.cm,'T':1*u.ms}, d=20*u.cm, T=1*u.ms, psf={'shape':'gaussian','sigma':1*u.arcsec}, read_noise=4*u.ct):
    '''calculate the slope noise standard deviation given star flux and spot size parameters
    
    Parameters:
    -----------
    flux : Quantity or dict
        The flux in counts / m**2 s , or a dictionary defining it with n=counts,d=subap diameter, T=exposure time
        The dictionary defines a base case (easily read off the wavefront sensor) and is
        converted to flux by F = n / (d**2 * T)
    d : Quantity, length-like
        The size of the subaperture, as the square root of its area
    T : Quantity, time-like
        The wfs single-frame exposure time
    psf : dict = {'shape':str, param1: None, ... }
        Specification of the psf. Choices of shape are 'gaussian','moffat','airy','seeing'.
        Moffat uses additional parameters, alpha and beta, defaulting to 1 arcsec and 2.
        Gaussian uses one additional parameter, 'sigma', defaulting to 1 arcsec.
        Airy uses 's' as the spot-size parameter, defaulting to 1 arcsec.
        Seeing uses 'r0' as the spot size parameter, defaulting to 10 cm.
    rn : Quantity, counts
        The read noise standard deviation, in counts, per pixel on the detector.
    
    Returns:
    --------
    The slope standard deviation, in angle-like units
    '''    
    if isinstance(flux,dict):
        n0,d0,T0 = [flux[key] for key in ['n','d','T']]
        flux = n0/(d0**2*T0)
    N = flux*d**2*T
    n_pix = 4 # assuming quad cell for now
    SNR = N / np.sqrt(N*u.ct + read_noise**2*n_pix) # signal-to-noise ratio
    if psf['shape'] == 'gaussian':
        sigma = psf['sigma']
        G = (2/np.pi)/sigma # G is 'optical gain'
    else:
        raise NotImplementedError
    sigma_theta = 1 / (G*SNR)
    return sigma_theta

def phase_sigma(D = 3*u.m, **kwargs):
    '''calculate the phase measurement standard deviation of a Hartmann sensor.
    
    Parameters:
    -----------
    D : Quantity, length-like
        The telescope primary diameter
    kwargs
        These keywords are sent `slope_sigma` which computes the slope measurement error
        on a subaperture
    
    Returns:
    --------
    The wavefront standard deviation, in length-like units. If you want to convert it
    to phase at some wavelength $\\lambda$, multiply by $2 \\pi / \\lambda$
    '''
    sigma_n = slope_sigma(**kwargs)
    d = kwargs.get('d',10*u.cm)
    chi = np.log(D/d) / (2*np.pi)
    sigma_m = sigma_n * d * np.sqrt( chi/(2*np.pi) )
    return sigma_m.to_da(u.nm),sigma_n.to_da(u.arcsec)
    
def PSdefs(globalize=False):
    Sphi = vonKarmanSpectrum()
    v,r0,f0 = [Sphi.params[x] for x in ['v','r0','f0']]
    lam0 = 550*u.nm
        
    Sphi_nm = PowerSpectrum(
        lambda f, v=v,r0=r0,f0=f0,lam0=lam0: (((lam0/(2*np.pi))**2)*Sphi(f,v=v,r0=r0,f0=f0)).to_da(u.nm**2/u.Hz),
        #unit = u.nm**2/u.Hz,
    )
    attr_names = ['name','comment','formula','latex_name','latex_formula']
    Sphi_nm.setattrs(**Sphi.getattrs(attr_names))
    
    if globalize:
        globals().update(locals())
    else:
        d = {}
        d.update(locals())
        keys = ['Sphi','Sphi_nm',]
        r = {key:d.get(key) for key in keys}
        return r

PSdefs(globalize=True)

class Signal(Base):
    '''A time domain signal.
    Can be defined as a funtion of t, or in terms of samples.
    '''
    def from_spectrum(Sf,**kwargs):
        '''
        Create a Signal given its spectrum Sf.
        '''
        if hasattr(Sf,'original_samples'):
            f,F,n,df,dt = (Sf._original_samples[x] for x in ['f','F','n','df','dt'])
        else: #functional definition of spectrum
            Sf.gen_samples_FFT(**kwargs)
            f,F,n,df,dt = (Sf.gened_samples[x] for x in ['f','F','n','df','dt'])
        t0 = kwargs.get('t0',0.*u.s)
        tf = t0 + n*dt
        X = np.fft.ifft(F)*df
        t = np.arange(t0/dt.unit,tf/dt.unit,dt/dt.unit)*dt.unit
        return Signal((t,X),**kwargs)
    
    def __init__(self,x,name='',**kwargs):
        '''Create a Signal with two lists containing
        sample times and signal values (t,x) -or-
        a function of time, x(t)
        
        Parameters
        ----------
        x : 2-tuple (Quantity,Quantity) or function
            The pair (times, values) describing the time sequence.
        name : str
            (optional) The assigned name of the object
            
        '''
        self.name = name
        if isinstance(x,tuple):
            t,x = x
            n = len(t)
            dt = ( t[1]-t[0] )
            samples = DotDict({
                't':t,
                'x':x,
                'dt':dt,
                'n':n,
                'fn': ( 1/(2*dt) ).to(u.Hz)
            })
            self.original_samples = samples
            self.x = interp1d(t,x,kind='cubic')
            self.domain = ('t',samples.t.min(),samples.t.max())
            self.params = {}
            self.unit = x.unit
        elif callable(x):
            params = inspect.signature(x).parameters
            domain = list(params.keys())[0]
            assert domain in ['t']
            self.x = x
            self.domain = domain
            self.signature = inspect.signature(x)
            self.params = {key:val.default for key,val in params.items() if val.default is not inspect._empty}
            self.unit = x(0*u.s).unit
        self.comp = None
            
        self.__dict__.update(kwargs)
        
    @property
    def source(self):
        return inspect.getsource(self.x)
    
    def __mul__(self,other):
        if isscalar(other):
            r = Signal(lambda t,**p: other*self(t,**p),
                       name = f'({self.name} * {other})')            
        elif isinstance(other,Signal):
            r = Signal(lambda t,**p: other(t,**p)*self(t,**p),
                         name = f'({self.name} * {other.name})')
        elif isinstance(other,TransferFunction):
            return (other*self.spectrum()).signal()
        else:
            raise TypeError('cannot multiply a Signal with a %s'%other.__class__.__name__)
        return r.compose(self,other,'mul')
    
    def __rmul__(self,other):
        return self.__mul__(other)
    
    def __add__(self,other):
        if isinstance(other,Signal):
            r = Signal(lambda t,**p: other(t,**p)*self(t,**p),
                         name = f'({self.name} + {other.name})')
        elif isscalar(other):
            r = Signal(lambda t,**p: other*self(t,**p),
                         name = f'({self.name} + {other})')
        else:
            raise TypeError('cannot add a Signal to a %s'%other.__class__.__name__)
        return r.compose(self,other,'add')
    
    def __radd__(self,other):
        return self.__add__(self,other)
    
    def __sub__(self,other):
        if isinstance(other,Signal):
            r = Signal(lambda t,**p: self(t,**p) - other(t,**p),
                         name = f'({self.name} - {other.name})')            
        elif isscalar(other):
            r = Signal(lambda t,**p: self(t,**p) - other,
                         name = f'({self.name} - {other})')            
        else:
            raise TypeError('cannot difference a Signal and a %s'%other.__class__.__name__)
        return r.compose(self,other,'sub')
    
    def __rsub__(self,other):
        if isinstance(other,Signal):
            r = Signal(lambda t,**p: other(t,**p) - self(t,**p),
                         name = f'({other.name} - {self.name})')            
        elif isscalar(other):
            r = Signal(lambda t,**p: other - self(t,**p),
                         name = f'({other} - {self.name})')            
        else:
            raise TypeError('cannot difference a Signal and a %s'%other.__class__.__name__)
        return r.compose(self,other,'rsub')
    
    def __truediv__(self,other):
        if isinstance(other,Signal):
            r = Signal(lambda t,**p: self(t,**p)/other(t,**p),
                         name = f'({self.name} / {other.name})')            
        elif isscalar(other):
            r = Signal(lambda t,**p: self(t,**p)/other,
                        name = f'({self.name} / {other})')            
        else:
            raise TypeError('cannot divide a Signal and a %s'%other.__class__.__name__)
        return r.compose(self,other,'truediv')
    
    def __rtruediv__(self,other):
        if isinstance(other,Signal):
            r = Signal(lambda t,**p: other(t,**p)/self(t,**p),
                         name = f'({other.name} / {self.name})')            
        elif isscalar(other):
            r = Signal(lambda t,**p: other/self(t,**p),
                         name = f'({other} / {self.name})')            
        else:
            raise TypeError('cannot divide a Signal and a %s'%other.__class__.__name__)
        return r.compose(self,other,'rtruediv')
    
    def __neg__(self):
        return Signal(lambda t,**p: -self(t,**p),
                      name = f'-{self.name}').compose(self,'neg')

    def abs(self):
        return Signal(lambda t,**p: self(t,**p).abs(),
                      name = f'|{self.name}|').compose(self,'abs')
    
    def gen_samples(self,**kwargs):
        '''generate time samples and corresponding data samples
        from the function
        Sampling specification must be provided in keyword
        arguments. The specification can be any of:
            t0,dt,tf
            t0,n,tf
            t0,dt,n
        t0 is optional and defaults to zero.
        End points t0 and tf are both included in the samples.
        '''
        t0 = kwargs.get('t0',0*u.ms)
        dt = kwargs.get('dt',None)
        n = kwargs.get('n',None)
        tf = kwargs.get('tf',None)
        if sum(x is not None for x in [dt,n,tf]) != 2:
            raise Exception('two, and only two, of "dt", "n", "tf" must be specified as keyword arguments to define the sampling')
        if dt is None:
            dt = (tf - t0)/(n-1)
        elif n is None:
            n = int(((tf - t0)/dt + 1).to('').value)
        elif tf is None:
            tf = t0 + (n-1)*dt
        t = np.linspace(t0,tf,n)
        x = self(t)
        samples = DotDict({
            't':t,
            'x':x,
            'dt':dt,
            'n':n,
            't0':t0,
            'tf':tf,
        })
        self.gened_samples = samples

    def sample_like(self,other):
        '''use another signal's sampling to use as a template
        for this signals' sampling.
        
        Parameters:
        -----------
        Other: Signal
            The sigmal whose sampling is to be emulated
        '''
        dt,n = (other.samples[x] for x in ['dt','n'])
        self.gen_samples(dt=dt,n=n)
       
    def __call__(self,t=None,**kwargs):
        '''Return the value of the signal at time t
        
        Parameters
        ----------
        t : Quantity
            The time
        
        Only one of t can be specified, and must have
        time units
        
        Returns
        -------
        Quantity
            x(t)
        
        '''
        if t is None: # just set the parameters and return a copy
            kwargs = {key:val for key,val in kwargs.items() if key in self.params}
            r = self.copy()
            r.x = functools.partial(r.x,**kwargs)
            r.params = {**self.params,**kwargs}
            return r
        if isinstance(t,list): t = u.Quantity(t)
        x = self.x
        if isinstance(x,interp1d):
            t = t.to(self.original_samples.t.unit)
            return u.Quantity(x(t),self.unit)
        else:
            return x(t,**kwargs)
    
    def minmax(self):
        t,x = self.gened_samples.t,self
        return x(t).minmax()
    
    def mean(self):
        t,x = self.gened_samples.t,self
        return x(t).mean()
    
    def var(self):
        t,x = self.gened_samples.t,self
        return x(t).var()
    
    def std(self):
        t,x = self.gened_samples.t,self
        return x(t).std()
    
    def plot(self,**kwargs):
        '''Plot the signal vs time
        
        Keyword arguments:
        ------------------
        t0, dt, tf, n -- go to Signal.gen_samples
            if none of these are provided, then it plots the original_samples
            or 1024 samples over the object's domain.
        kind, title, subtitle, fig, label go to time_domain_plot
        other keywords are forwarded to matplotlib's axis.plot
        '''
        t0,tf,n,kind = (self.sampling_defaults[x] for x in ['t0','tf','n','kind'])
        sam_args = [x for x in ['t0','tf','n','kind'] if x  in kwargs]
        
        if not hasattr(self,'original_samples'): # case 1: functional definition
            if isinstance(self.domain,tuple):
                t0,tf = (self.domain[1], self.domain[2])
                n = 1024
            else:
                t0,tf,n = (self.sampling_defaults[x] for x in ['t0','tf','n'])
            dt = (tf-t0)/(n-1)
                    
        else: # case 2: original samples definition
            t0,tf = self.original_samples.t.minmax()
            dt = self.original_samples.dt
            n = self.original_samples.n
            
        if len(sam_args) > 0: # overrides
            t0 = kwargs.pop('t0',t0)
            tf = kwargs.pop('tf',tf)
            if 'dt' not in kwargs:
                n = kwargs.pop('n',n)
                dt = (tf-t0)/(n-1)
            else:
                dt = kwargs.pop('dt')
                n_ignore = kwargs.pop('n',None)
        
        self.gen_samples(t0=t0, tf=tf, dt=dt)
        t,x = (self.samples[x] for x in ['t','x'])

        title = kwargs.pop('title',latex_prep(self.name))
        ylabel = kwargs.pop('ylabel',None)
        if hasattr(self,'formula'): title = (title+'\n'+self.formula).strip()
        if not ylabel:
            ylabel = getattr(self,'ylabel','Signal')
        kwargs.update({
            'title':title,
            'ylabel':ylabel,
        })
        time_domain_plot((t,x),**kwargs)
    
    def LaplaceTransform(self,s):
        '''Calculate the discrete Laplace transform of the signal
        This will match the Fourier transform for s along the
        positive imaginary axis.
        
        The Laplace transform can be calculated for
        any point in the complex plane.
        
        Parameters
        ----------
        s : Quantity
            The Laplace variable (or array). s should
            be in units of frequency, but if it is a unitless
            number it will be assumed to have units of Hz
        
        Returns
        -------
        The value of the Laplace transform at s
        
        '''
        try:
            assert s.unit.physical_type == 'frequency'
        except:
            s = s*u.Hz
        t,x = self.t,self.x
        if s.isscalar:
            Fs = np.sum(x*np.exp(-s*t))
        else:
            Fs = [np.sum(x*np.exp(-sj*t)) for sj in s]
            Fs = u.Quantity(Fs)
        return Fs
        
    def FourierTransform(self):
        '''Take the discrete Fourier Transform of the signal.
        
        Returns
        -------
        The frequency array and the spectral value array, as a tuple
        '''
        if not hasattr(self,'gened_samples'): self.gen_samples()
        n,dt,t = (self.gened_samples[x] for x in ['n','dt','t'])
        df = (1/(n*dt)).to('Hz')
        fn = (1/(2*dt)).to('Hz') # Nyquist
        f = (np.arange(n)-n//2)*df
        #f = np.fft.fftshift(f.value)*f.unit
        f = np.fft.fftshift(f)
        x = self(t)
        #X = u.Quantity(np.fft.fft(x),x.unit)*dt.to(1/u.Hz)
        X = np.fft.fft(x)*dt.to(1/u.Hz)

        return(f,X)
    
    def spectrum(self,**kwargs):
        '''
        Create a Spectrum given a Signal.
        
        See Spectrum.from_signal
        '''
        return Spectrum.from_signal(self,**kwargs)

    def sample_and_hold(self,T,**kwargs):
        '''Sample and hold a signal.
        Create a new signal consisting of the original signal
        that has been sampled every T units of time and held
        constant for duration T.
        ( T > 2*dt is required )
        '''
        sam = self.samples
        if sam is None:
            raise Exception('signal must be subsampled first (see gen_samples)')
        dt,t = (sam[x] for x in ['dt','t'])
        t0,tf = t[0],t[-1]+dt
        assert T > 2*dt, 'T > 2*dt is required'
        Tn = u.Quantity.arange(t0,tf,T)
        xTn = self(Tn) # samples at spacing T
        x = np.repeat(xTn,int(T/dt)) # holds for T seconds
        x = x[:len(t)]
        if 'name' not in kwargs:
            kwargs['name'] = f'{self.name}_{{SH}}'
        kwargs.update({
            'Tsample':T,
        })
        return Signal((t,x),**kwargs)
    
    def sample(self,T,**kwargs):
        '''Sample a signal.
        Create a new signal consisting of samples of
        the original signal at every T units of time and
        zero elsewhere.
        ( T > 2*dt is requred ).
        '''
        sam = self.samples
        if sam is None:
            raise Exception('signal must be subsampled first (see gen_samples)')
        dt,t = (sam[x] for x in ['dt','t'])
        t0,tf = t[0],t[-1]+dt
        assert T > 2*dt, 'T > 2*dt is required'
        Tn = u.Quantity.arange(t0,tf,T)
        xTn = self(Tn) # samples at spacing T
        x = np.zeros(len(t))*self.unit
        ptr = range(0,len(t),int(T/dt))
        x[ptr] = xTn
        if 'name' not in kwargs:
            kwargs['name'] = f'{self.name}_{{S}}'
        kwargs.update({
            'Tsample':T,
        })
        return Signal((t,x),**kwargs)
    
def time_domain_plot(x,kind='line',title='',subtitle=None,fig=None,label=None,**kwargs):
    '''Plot the signal vs time.
    
    Parameters
    ----------
    x : 2-tuple of Quantities
        The time-domain signal to plot, given as a pair
        (times,values)
    kind : str
        The kind of plot ('stem','line','dot')
    title : str
        The plot title text, to put above the plot
    fig : int, str, or Figure
        The (optional) existing figure on which to plot the curve.
        Useful for putting multiple curves on one plot
        If not provided, plot will go in a new figure
    label : str
        The curve label. This will go in the plot legend
    '''
    kinds = ['stem','line','dot']
    if kind not in kinds:
        raise Exception('plot must be one of {}'.format(kinds))
    t,h = x
    
    assert t.unit.physical_type == 'time'
    
    h_imag = None
    if np.iscomplex(h).any():
        if np.isclose(h.imag,0).all():
            h_imag = None
        else:
            h_imag = h.imag
        h = h.real

    xlabel = 'time'
    if hasattr(t,'unit'): xlabel += ', {}'.format(t.unit)
    ylabel = kwargs.get('ylabel','h(t)')
    if hasattr(h,'unit'):
        if hasattr(h.unit,'long_names'):
            ylabel += ', {}s'.format(h.unit.long_names[0])
        else:
            ylabel += ' ' + _latex_unit(h.unit)
    
    fig_name = fig if isinstance(fig,str) else None
    exfig = get_fig(fig)
    figsize = kwargs.pop('figsize',None)
    if exfig: # existing figure
        assert exfig.kind == 'time_domain_plot'
        exfig.show()
        ax = exfig.axes[0]
        t = t.to(ax.xaxis.qunit)
        if title: plt.suptitle(title)
        if subtitle: ax.set_title(subtitle)
        if kwargs.pop('ylabel',None) is not None:
            ax.set_ylabel(ylabel)
    else: # new figure
        fig,ax = plt.subplots(1,1,figsize=figsize)
        if fig_name: fig.name = fig_name
        fig.kind = 'time_domain_plot'
        ax.xaxis.qunit = t.unit

        xlabel = 'time'
        if hasattr(t,'unit'): xlabel += ', {}'.format(t.unit)
        ylabel = kwargs.get('ylabel','h(t)')
        if hasattr(h,'unit'):
            if hasattr(h.unit,'long_names'):
                ylabel += ', {}s'.format(h.unit.long_names[0])
            else:
                ylabel += ' ' + _latex_unit(h.unit)
    
        plt.suptitle(title)
        ax.set_title(subtitle)
        ax.set_ylabel(ylabel)
        kwargs.pop('ylabel',None)
        plt.xlabel(xlabel)
    
    kwargs['label'] = label
    markersize = kwargs.pop('markersize',1)

    if h_imag is not None:
        kwargs['label'] = label + ' real part'
    if kind == 'stem':
        kwargs['markerfmt'] = kwargs.pop('markerfmt',',')
        kwargs['basefmt'] = kwargs.pop('basefmt',',')
        plt.stem(t.value,h.value,**kwargs)
        if h_imag is not None:
            kwargs['label'] = label + ' imaginary part'
            kwargs['linefmt'] = 'C1-'
            plt.stem(t,h_imag,**kwargs)
    elif kind == 'line':
        plt.plot(t,h,**kwargs)
        if h_imag is not None:
            kwargs['label'] = label + ' imaginary part'
            plt.plot(t,h_imag,**kwargs)
    elif kind == 'dot':
        kwargs['markersize'] = markersize
        plt.plot(t,h,'.',**kwargs)
        if h_imag is not None:
            kwargs['label'] = label + ' imaginary part'
            plt.plot(t,h_imag,'.',**kwargs)
    
    if not exfig:
        ax.xaxis.set_minor_locator(AutoMinorLocator())            
        ax.yaxis.set_minor_locator(AutoMinorLocator())
        plt.grid(True,which='major')
        plt.grid(True,which='minor',lw=.3,color='orange')
        ax.axhline(color='black',lw=1,alpha=.5)
        ax.axvline(color='black',lw=1,alpha=.5)
        if hasattr(t,'unit'): t = t.value
        # if hasattr(h,'unit'): h = h.value
        ax.set_xlim((t.min(),t.max()))
        # ax.set_ylim((h.min(),h.max()))
    # plt.xlabel(xlabel)
    
    if label is not None:
        plt.legend(markerscale=10/markersize)

def frequency_domain_plot(f=None,H=None,fig=None,title=None,subtitle=None,ylabels=None,unwrap=True,pi_format=True,**kwargs):
    '''plot magnitude and phase of a complex spectrum.
    
    Call signatures:
    
        frequency_domain_plot(f,H,**kwargs)
        frequency_domain_plot((f,H),**kwargs)
    
    Parameters
    ----------
    f : Quantity array, with units of frequency
        The frequencies.
        This first argument can be a tuple (f,H)
    H : Quantity or numpy array
        The complex values, one per frequency
    
    Keyword arguments
    -----------------
    Keywords are generally passed to the plot routine. Some keywords
    are of special relevance to this routine:
    
    title : str
        The main title to go on top of the figure
    subtitle : str
        A subtitle which goes under the title
    fig : int or matplotlib.figure.Figure
        The figure to plot in to. If a plot already exists in that
        figure, the curves are overlayed with previous curves
    label : str
        The label corresponding to this curve. It will be put in the
        legend on the magnitude graph only.
    ylabels : list of 2 str
        Alternatives for labels on the y axes. Default is
        '|H(f)|' and '\\angle H(f)'
    '''
    if isinstance(f,(list,tuple)):
        f,H = f
    
    assert f.unit.physical_type == 'frequency'
    
    xlabel = 'frequency'
    if hasattr(f,'unit'): xlabel += ', {}'.format(f.unit)
    # notnan = ~np.isnan(H)
    # f = f[notnan]
    # H = H[notnan]
    
    xscale = 'log'
    if any(f.value<0): xscale = 'symlog'
    if xscale == 'log':
        notzero = ~np.isclose(f.value,0)
        f = f[notzero]
        H = H[notzero]

    fig_name = fig if isinstance(fig,str) else None
    exfig = get_fig(fig) 
    figsize = kwargs.pop('figsize',None)
    if exfig: # existing figure
        if not H.unit.is_equivalent(exfig.unit):
            raise Exception('<frequency_domain_plot> units of new curve %s are not convertable to units of prior curves %s'%(H.unit,exfig.unit))
        H = H.to(exfig.unit)
        exfig.show()
        ax_list = exfig.axes
        if len(ax_list) == 2:
            ax1,ax2 = ax_list
        elif len(ax_list) == 1:
            ax1, = ax_list
            ax2 = None
        if title:
            plt.suptitle(title)
        if subtitle:
            ax1.set_title(subtitle)
        if ylabels:
            ylabels[0] = (ylabels[0] + ' %s'%_latex_unit(H.unit)).strip(', ')
            ax1.set_ylabel(ylabels[0])
            if ax2:
                ax2.set_ylabel(ylabels[1])
    else: # new figure
        if np.isreal(H).all():
            fig,ax1 = plt.subplots(1,1,figsize=figsize)
            ax2 = None
        else:
            fig,(ax1,ax2) = plt.subplots(2,1,sharex=True,figsize=figsize)
        if fig_name: fig.name = fig_name
        fig.unit = H.unit
        plt.suptitle(title)
        ax1.set_title(subtitle)
        if ylabels is None:
            ylabels = ['$|H(f)|$','$\\angle H(f)$, rad']
        ylabels[0] = (ylabels[0] + ' %s'%_latex_unit(H.unit)).strip(', ')

        ax1.set_ylabel(ylabels[0])
        if ax2:
            ax2.set_ylabel(ylabels[1])
    
    ax1.plot(f,np.abs(H),**kwargs)
    ax1.set_yscale('log')
    ax1.set_xscale(xscale)
    ax1.set_xlabel(xlabel)
    ax1.grid(True,which='both')
    if 'label' in kwargs:
        ax1.legend()
        kwargs.pop('label') # so it doesn't get passed to the 2nd graph
    
    if ax2 is not None:
        H_angle = np.angle(H).value #radians
        if unwrap:
            isnan = np.isnan(H)
            notnan = ~isnan
            H_angle[notnan] = np.unwrap(H_angle[notnan])
            H_angle[isnan] = np.nan
        ax2.plot(f,H_angle,**kwargs)
        ax2.set_xscale(xscale)
        ax2.grid(True,which='both')
        ax2.set_xlabel(xlabel)
        if pi_format:
            ax2.yaxis.set_major_locator(plt.MultipleLocator(np.pi / 2))
            ax2.yaxis.set_major_formatter(plt.FuncFormatter(multiple_formatter()))

# ---------- Auto-tuner --------
class Tuner():
    '''contains the model of the system, its parmeters, and the conditions,
    and a cost function definition and optimizer.
    '''
    def __init__(self,conditions=None,aoParams=None):
        self.conditions = DotDict({'r0': 10*u.cm,
                          'v': 2*u.m/u.s,
                          'L0': 10*u.m,
                          'psf': {'shape':'gaussian', 'sigma':0.5*u.arcsec},
                          'flux': {'n':500.*u.ct,'d':20*u.cm,'T':1*u.ms},
                          'lam0': 550*u.nm,
                         })
        v,L0,lam0 = [self.conditions[x] for x in ['v','L0','lam0']]
        self.conditions.r0.lam0 = lam0
        self.conditions.f0 = v/L0
        
        self.aoParams = DotDict({'fs': 1*u.kHz,
                        'g': 0.5,
                        'gamma': 0.998,
                        'tau': 800*u.us,
                        'd': 20*u.cm,
                        'D': 3*u.m,
                        'lam_wfs': 0.7*u.micron,
                        'read_noise': 4*u.ct,
                        })
        
        if conditions is not None:
            self.conditions.update(conditions)
        if aoParams is not None:
            self.aoParams.update(aoParame)
        
        self.fmax = 4*u.kHz
        self.n_calc = 1024
        self.n_calc_doc = 'number of sample points on the power spectra for evaluating cost function'
        
        # power spectra
        kwargs = {x:self.conditions[x] for x in ['r0','v','f0','lam0']}
        #self.Sphi = vonKarmanSpectrum(**kwargs)
        self.Sphi = Sphi_nm(**kwargs)
        
        # system transfer functions
        tfs = AOdefs()
        tf_names = ['zoh','Hwfs','Hr','Hdel','Hc','Hdm','Hol','Hcl','Hn']
        d = {x: tfs[x] for x in tf_names}
        self.__dict__.update(d)
        
        # optimizable parameters
        # continous
        gmargin = self.Hol.gain_margin()
        cols = [ 'name', 'descrip',   'initial','bounds', 'active']
        rows = [ ('g',      'loop gain',      0,    (0,gmargin),   True ),
                 ('gamma','integrator bleed', 0.5,  (0.,0.999), True ),
               ]
        self.cvars = pd.DataFrame(rows,columns=cols).set_index('name')
        # discrete
        cols = ['name', 'descrip','valids','active']
        rows = [ ('fs',  'sample rate',      (50,100,200,250,500,1000)*u.Hz, True),
                 ('d',   'subaperture size',     (20,40)*u.cm,               True),
                 ('cent', 'centroider',      ('COG','QUAD','QBIN'),          False ),
                 ('mode', 'guide star',       ('LGS','NGS') ,                False ),
               ]
        self.dvars = pd.DataFrame(rows,columns=cols).set_index('name')

        self.cost() # create the cost function
    
    def pprint(self):
        oprint.pprint(self,expand=1)
        
    @property
    def pp(self):
        self.pprint()
    
    def cost(self):
        '''create a cost function given the present nominal conditions and parameters.
        Those parameters being optimized are sent as
        arguments to the cost function during the optimization process.
        '''
        names = ['r0','v','L0','psf','flux','lam0']
        r0,v,L0,psf,flux,lam0 = [self.conditions[x] for x in names]
        names = ['fs','g','gamma','tau','d','D','lam_wfs','read_noise']
        fs,g,gamma,tau,d,D,lam_wfs, read_noise = [self.aoParams[x] for x in names]
        
        T = (1/fs).to(u.ms)
        f0 = v/L0
        flux =  flux['n']/(flux['d']**2*flux['T'])
        N = flux*d**2*T
        
        Hol = self.Hol = self.Hol(T=T,gamma=gamma,tau=tau)
        # check the gain isn't exceeding the gain margin
        gmargin = Hol.gain_margin()
        if (g > gmargin):
            raise Exception('gain %s is greater than the gain margin %s'%(g,gmargin))
        self.cvars.at['g','bounds'] = (0,Hol.gain_margin()) # set the optimization upper bound for gain to the gain margin
        Hcl = self.Hcl = self.Hcl(T=T,gamma=gamma,g=g,tau=tau)
        
        Sphi = self.Sphi(f0=f0,v=v,r0=r0)
        sigma_m,sigma_n = phase_sigma(flux=flux, d=d, D=D, T=T, psf=psf, read_noise=read_noise)
        sigma_m.rad = (sigma_m*(2*pi/lam0)).to_da(u.rad)
        sigma_m.doc = 'phase noise'
        sigma_n.doc = 'slope noise'
        
        Sn = PowerSpectrum(lambda f: (unity(f)*(sigma_m**2*T)).to_da(u.nm**2/u.Hz),
                            name='noise power spectrum',
                            unit = u.nm**2/u.Hz,
                            formula='sigma_m**2 T for all f',
                            latex_name = '$S_n(f)$',
                            latex_formula = '$\\sigma_m^2 T$',
                            sigma_m = sigma_m,
                            sigma_n = sigma_n,
                            )
        
        def func(args,query=False):
            '''the query flag is for calls from outside the optimizer
            '''
            g, gamma = args

            Hrej = Hcl(g=g, gamma=gamma, T=T)
            Hn = g*Hol(gamma=gamma, T=T)*Hcl(g=g, gamma=gamma, T=T)
            
            S = Hrej.abs2()*Sphi + Hn.abs2()*Sn
            
            Si = S.integral(fmax=self.fmax,n=self.n_calc).to_da(Sphi.unit*u.Hz)
            if query:
                return (Si,Hrej,Hn,S)
            
            self._c_optimizer_history.append((args,Si))
            return Si.value
        
        self.Sphi = Sphi
        self.Sn = Sn
        self.func = func
            
    def evaluate(self):
        '''evaluate the cost function at the current conditions and aoParams.
        '''
        keys = 'r0,v,L0,psf,flux,lam0'.split(',')
        r0,v,L0,psf,flux,lam0 = [self.conditions[key] for key in keys]
        keys = 'fs,g,gamma,tau,d,D,lam_wfs,read_noise'.split(',')
        fs,g,gamma,tau,d,D,lam_wfs,read_noise = [self.aoParams[key] for key in keys]
        
        Sphi,Sn = self.Sphi, self.Sn
        
        Si,Hrej,Hn,S = self.func((g,gamma),query=True)
        
        S1 = Hrej.abs2()*Sphi; S1.name = 'atmos part'
        S2 = Hn.abs2()*Sn;     S2.name = 'noise part'
        
        S1i = S1.integral(fmax=self.fmax,n=self.n_calc)
        S2i = S2.integral(fmax=self.fmax,n=self.n_calc)
        
        rms_wfe = pd.Series({'total': (np.sqrt(Si)).to_da(u.nm),
                             'atmos': (np.sqrt(S1i)).to_da(u.nm),
                             'noise': (np.sqrt(S2i)).to_da(u.nm),
                            })
        
        self.Hrej = Hrej
        self.Hn = Hn
        self.S = S
        self.S1 = S1
        self.S2 = S2
        
        self.rms_wfe = rms_wfe
        return rms_wfe
    
    def accept(self,idx=None):
        '''accept the optimization result, making the aoParams in accordance
        with the optimized variables
        
        Parameters
        ----------
        idx : int
            The index of the row in the _d_optimizer_result table.
            If None, then the top row is used.
        '''
        if self._optimizer_mark == 'c':
            cvars = self.cvars
            names = list(cvars.index)
            vals = list(cvars.optimum)
            for name,val in zip(names,vals):
                self.aoParams[name] = val
        elif self._optimizer_mark == 'd':
            if idx is None:
                r = self._d_optimizer_result.iloc[0] # top line of sorted results
            else:
                r = self._d_optimizer_result.loc[idx] 
            d,fs,g,gamma = r.d, r.rate, r.g, r.gamma
            self.change(d=d,fs=fs,g=g,gamma=gamma)
    
    def __call__(self,**kwargs):
        r = copy.deepcopy(self)
        r.change(**kwargs)
        return r
        
    def change(self,**kwargs):
        '''change one or many parameters and/or conditions using keywords.
        arguments must match type, size, and equivalent units of original values
        '''
        # type and unit checking
        for key,val in kwargs.items():
            if key in self.aoParams:
                types_match = type(val) == type(self.aoParams[key])
                if not types_match: types_match = isinstance(val,float) and isinstance(self.aoParams[key],float)
                if types_match and isinstance(val,u.Quantity):
                    units_match = val.unit.is_equivalent(self.aoParams[key].unit)
                else:
                    units_match = True
                if not types_match or not units_match:
                    raise TypeError('parameter %s: type does not match'%key)
            elif key in self.conditions:
                types_match = type(val) == type(self.conditions[key])
                if not types_match: types_match = isinstance(val,float) and isinstance(self.conditions[key],float)
                if types_match and isinstance(val,u.Quantity):
                    units_match = val.unit.is_equivalent(self.conditions[key].unit)
                else:
                    units_match = True
                if not types_match or not units_match:
                    raise TypeError('parameter %s: type does not match'%key)
            else:
                raise TypeError('parameter %s is not a condition or AO Parameter'%key)
        
        # all ok, set new values
        for key,val in kwargs.items():
            if key in self.aoParams:
                self.aoParams[key] = val
            elif key in self.conditions:
                self.conditions[key] = val
        
        # reinitialize
        self.cost()
        self.evaluate()
        return self
        
    def set_pcs(self,aoParams=None,conditions=None):
        '''aoParams or conditions must be OrderedDicts or Series with
        entries in the same order as in current aoParams and conditions
        '''
        if aoParams is None and conditions is None: return
        if aoParams is not None:
            if isinstance(aoParams,dict):
                aoParams = pd.Series(aoParams)
            types_match =  [type(x) for x in aoParams.values] == [type(x) for x in aoParams.values]
            keys_match = list(aoParams.keys()) == list(self.aoParams.keys())
            if keys_match and types_match:
                self.aoParams = aoParams
            else:
                raise Exception('<set_pcs> aoParams argument does not match that of self.aoParams')
        if conditions is not None:
            if isinstance(conditions,dict):
                conditions = pd.Series(conditions)
            types_match =  [type(x) for x in conditions.values] == [type(x) for x in conditions.values]
            keys_match = list(conditions.keys()) == list(self.conditions.keys())
            if keys_match and types_match:
                self.conditions = conditions
            else:
                raise Exception('<set_pcs> conditions argument does not match that of self.conditions')
        # reinitialize
        self.cost()
        self.evaluate()
        return self
        
    def optimize_c(self):
        '''run the optimizer on the continuous variables
        
            >>> optimize.minimize(func,(0,.999),bounds=[(0,.67),(.5,.999)])
                  fun: 0.2903093128864424
             hess_inv: <2x2 LbfgsInvHessProduct with dtype=float64>
                  jac: array([-1.33781874e-06, -9.00108857e+01])
              message: b'CONVERGENCE: NORM_OF_PROJECTED_GRADIENT_<=_PGTOL'
                 nfev: 51
                  nit: 6
               status: 0
              success: True
                    x: array([0.18665664, 0.999     ])
                    
        '''
        if not hasattr(self,'func'): self.cost()
        
        cvars = self.cvars[self.cvars.active]
        
        start = [x.value if isinstance(x,u.Quantity) else x for x in cvars.initial]
        lb,ub = list(zip(*cvars.bounds))
        keep_feasible = [True,]*len(cvars)
        bounds = optimize.Bounds(lb,ub,keep_feasible=keep_feasible)
        self._c_optimizer_history = []
        
        r = optimize.minimize(self.func,start,bounds=bounds)
        
        if r.success:
            units = [x.unit if isinstance(x,u.Quantity) else None for x in cvars.initial]
            final = [x*x.unit if unit is not None else x for x,unit in zip(r.x,units)]
            cvars['optimum'] = final
            self.cvars = cvars
        
        else:
            print('<optimize_c> warning: optimizer failed')
            
        self._optimizer_result = r
        self._optimizer_mark = 'c'
    
    def plot(self,sensor='scicam',match=None,transparency=1):
        '''plot the open and closed loop power spectra, and the atmospheric and
        noise generated parts of the closed loop spectrum
        
        Parameters
        ----------
        sensor : str
            'scicam' or 'wfs' - There are different transfer functions for the
            measurement noise depending on the sensor. For science images ('scicam')
            the wavefront error (appropriate for Strehl calculation) is
            $S_\phi*(1/(1+gH_{ol}))^2 + S_n*(gH_{ol}/(1+gH_{ol}))^2$.
            The wavefront sensor sees
            $S_\phi*(1/(1+gH_{ol}))^2 + S_n*(1/(1+gH_{ol}))^2$
            which is appropriate for comparing to the telemetry data.            
        match : int or Figure
            Another figure that we want to match the plot scales to.
        transparency : float
            0 < transparency < 1 makes the present figure semi-transparent so you can overlay graphs
        
        '''
        valid_sensors = ['scicam','wfs']
        if not sensor in valid_sensors:
            raise ValueError('sensor must be one of %s'%valid_sensors)
        
        keys = 'r0,v,L0,psf,flux,lam0'.split(',')
        r0,v,L0,psf,flux,lam0 = [self.conditions[key] for key in keys]
        keys = 'fs,g,gamma,tau,d,D,lam_wfs,read_noise'.split(',')
        fs,g,gamma,tau,d,D,lam_wfs,read_noise = [self.aoParams[key] for key in keys]

        if sensor == 'scicam':
            self.Sphi.plot(label=self.Sphi.name)
            
            fig = plt.gcf()
            if not hasattr(self,'S'): self.evaluate()
            
            self.S.plot(label='closed loop',fig=fig)
            self.S1.plot(label='closed loop %s'%self.S1.name,fig=fig)
            self.S2.plot(label='closed loop %s'%self.S2.name,fig=fig)
            
            plt.suptitle('Wavefront error to science camera')
        
        elif sensor == 'wfs':
            (self.Sphi + self.Sn).plot(label='open loop')
            
            fig = plt.gcf()
            
            (self.Hrej.abs2()*(self.Sphi + self.Sn)).plot(label='closed loop',fig=fig)
            
            plt.suptitle('Wavefront error measured by wfs')
        
        flux = (flux['n']/(flux['d']**2*flux['T'])).to(u.ct/(u.cm**2*u.s))
        title = '$r_0$=%s $v$=%s $m_V$=%s $\\quad$  $d$=%s $f_s$=%s g=%s'%(latex_q(r0.round(2)),latex_q(v.round(2)),np.round(v_mag(flux),2).value,latex_q(d.round(2)),latex_q(fs.round(2)),np.round(g,2))
        ax, = fig.axes
        ax.set_title(title,size=14)
        ax.set_ylabel('$S(f)$ {0:latex}'.format(self.Sphi.unit),size=14)
        ax.set_xlabel('Frequency {0:latex}'.format(u.Hz),size=14)
        
        if match is not None:
            figo = get_fig(match)
            axo = figo.axes[0]
            ylim = axo.get_ylim()
            xlim = axo.get_xlim()
            ax.set_ylim(ylim)
            ax.set_xlim(xlim)
        
        if transparency < 1:
            win = fig.canvas.manager.window
            win.attributes('-alpha',transparency)
    
    def plot_opt_hist(self):
        '''plot the optimizer history
        '''
        lam0 = self.conditions.lam0
        k = 2*pi/lam0
        h = [(np.sqrt(x[1])/k).to_da(u.nm) for x in self._optimizer_hist]
        h = u.Quantity(h)
        plt.figure()
        plt.title('Optimizer history')
        plt.plot(h)
        plt.xlabel('Iteration')
        plt.ylabel('RMS phase error [nm]')
        plt.grid(True)
    
    def plot_sens(self,paramName,pct=10):
        '''plot sensitivity of cost function to parameter variation
        
        Parameters:
        -----------
        param : str
            The name of the continuous variable to vary
        pct : float
            The percentage variation. The continuous variable will not
            go outside the bounds
        '''
        val = self.aoParams[paramName]
        lb,lu = self.cvars.at[paramName,'bounds']
        paramDescrip = self.cvars.at[paramName,'descrip']
        n = 20
        vmin,vmax = max(lb,val*(1-pct/100.)), min(lu,val*(1+pct/100.))
        vs = np.linspace(vmin,vmax,n)
        
        base = self.evaluate()[0]
        param_val_saved = self.aoParams[paramName]
        
        wfe = []
        for v in vs:
            self.aoParams[paramName] = v
            wfe.append( self.evaluate() )
        
        wfe = zip(*wfe)
        wfe_total,wfe_atmos,wfe_noise = [u.Quantity(x) for x in wfe]
        
        self.aoParams[paramName] = param_val_saved
        
        plt.figure()
        plt.plot(vs,wfe_total,label='total')
        plt.plot(vs,wfe_atmos,label='atmos part')
        plt.plot(vs,wfe_noise,label='noise part')
        ax = plt.gca()
        ax.set_ylabel('RMS phase error [nm]')
        ax.set_xlabel(paramDescrip)
        #ax.set_yscale('log')
        title = 'Sensitivity to %s (%s)'%(paramName,paramDescrip)
        plt.title(title)
        plt.grid(True,which='both')
        plt.legend()
        plt.plot([param_val_saved],base,'o',fillstyle='none')
        #ax.axvline(param_val_saved,linestyle = '--',color='gray',linewidth=.5)
        
    def optimize_d(self):
        '''optimize for the discrete variables
        '''
        rates = self.dvars.at['fs','valids']
        ds = self.dvars.at['d','valids']
        dmodes = {'16x':20*u.cm,'8x':40*u.cm}
        r0 = self.conditions.r0
        lam0 = self.conditions.lam0
        k = 2*pi/lam0
        
        g_save = self.aoParams['g']
        g0 = self.cvars.at['g','initial']
        gamma_save = self.aoParams['gamma']
        gamma0 = .99 #self.cvars.at['gamma','initial']
        
        trials = []
        for rate in tqdm.tqdm(rates):
            self.change(fs=rate,g=g0,gamma=gamma0) # reset gain to initial because previous gain might exceed new gain margin
            for d,mode in zip(ds,dmodes.keys()):
                self.change(d=d,g=g0,gamma=gamma0)
                gmargin = self.Hol.gain_margin()
                self.cvars.at['g','bounds'] = (0,gmargin) # set the optimization upper bound for gain to the gain margin
                self.optimize_c()
                self.accept()
                g = self.aoParams.g
                gamma = self.aoParams.gamma
                wfe_temporal = self.evaluate().total
                wfe_spatial = ( (0.3*(d/r0)**(5/3))/k ).to_da('nm')
                wfe_total = np.sqrt(wfe_temporal**2 + wfe_spatial**2)
                trial = [mode,d,rate,g,gamma,wfe_temporal,wfe_spatial,wfe_total]
                trials.append(trial)
        
        self.aoParams.g = g_save
        self.aoParams.gamma = gamma_save
        
        r = pd.DataFrame(trials,columns=['mode','d','rate','g','gamma','wfe_temporal','wfe_spatial','wfe_total'])
        r = r.sort_values(by='wfe_total')
        self._d_optimizer_result = r
        self._optimizer_mark = 'd'

def unity(x):
    '''the unit function
    '''
    return u.Quantity(np.ones(x.shape))

legacy_code = False
if legacy_code:
    def sample_hold(s,T):
        """sample and hold transfer function
        """
        n, = s.shape
        z1 = np.exp(-s[1:]*T)
        H = np.ones(n).astype(complex)
        H[1:] = (1 - z1)/(s[1:]*T)
        return H
        
    dm = sample_hold
    cam = sample_hold
    
    def integrator(s,T,gamma=1.):
        """feedback control leaky integrator
        """
        z1 = np.exp(-s[1:]*T)
        n, = s.shape
        H = np.ones(n).astype(complex)
        H[1:] = 1./(1.-gamma*z1)
        return H
    
    def delay(s,tau):
        """time delay
        """
        H = np.exp(-s*tau)
        return H
    
    def open_loopTF(s,T=1,gamma=1.,delta = 1.):
        """open loop transfer function
        gamma is the integrator bleed
        delta is the delay from measurement to DM drive.
        """
        Hol = cam(s,T)*dm(s,T)*integrator(s,T,gamma)*delay(s,delta*T)
        return Hol
    
    def closed_loopTF(s,gain,T=1,gamma=1.,delta=1.):
        """closed loop transfer function
        gamma is the integrator bleed
        delta is the delay from measurement to DM drive.
        """
        Hcl = 1./(1. + gain*open_loopTF(s,T,gamma=gamma,delta=delta))
        return Hcl

# ---------- time domain ---------------
def pulse_response(H):
    n, = H.shape
    hh = np.zeros(n*2).astype(complex)
    hh[0:n] = H - H*delay(s,n*T) # it has to go back to zero to be periodic
    pr = 2*np.real(np.fft.ifft(hh))
    pr = pr - pr[0]
    return pr

def step_response(H):
    p = pulse_response(integrator(s,T)*H)
    return p

# --------- standard control plots ----------
def bode_plot(f=None,H=None,title=None,fig=None,unwrap=True,ylabels=None,pi_format=True,**kwargs):
    '''Create a Bode plot of the complex system H
    
    Call signatures:
    
        bode_plot(f,H,**kwargs) where f is a frequency array and H is the values array
        bode_plot((f,H),**kwargs)

    Parameters
    ----------
    f : Quantity array, with units of frequency
        The frequencies.
        This first argument can be a tuple (f,H)
    H : Quantity or numpy array
        The complex values, one per frequency
    
    Keyword arguments
    -----------------
    Keywords are generally passed to the plot routine. Some keywords
    are of special relevance to this routine:
    
    fig : str, int, or matplotlib.figure.Figure
        The figure to plot in to. If a plot already exists in that
        figure, the curves are overlayed with previous curves.
    label : str
        The label corresponding to this curve. It will be put in the
        legend on the magnitude graph only.
    ylabels : list of 2 str
        Alternatives for labels on the y axes. Default is
        'Magnitude' and 'Phase'
    unwrap : bool
        Whether or not to unwrap the phase curve, so it doesn't jump
        modulo 2 pi
    '''
    if isinstance(f,(list,tuple)):
        f,H = f
    
    assert f.unit.physical_type == 'frequency'
    notnan = ~np.isnan(H)
    f = f[notnan]
    H = H[notnan]
    gain_crossover = zero_crossing(f,H.mag,y0=1)
    phase_crossover = zero_crossing(f,np.unwrap(H.phase),y0=-np.pi*u.rad)
    
    figsize = (7,7.5) # inches
    
    fig_name = fig if isinstance(fig,str) else 'Bode Plot'
    exfig = get_fig(fig) # resolve name, number, Figure
    if exfig:
        exfig.show()
        ax_mag,ax_phase = exfig.axes
        if ylabels is None and hasattr(exfig,'ylabels'): ylabels = exfig.ylabels
    else:
        fig,(ax_mag,ax_phase) = plt.subplots(2,1,sharex=True,figsize=figsize)
        fig.name = fig_name
        fig.ylabels = ylabels
        
    ax_mag.plot(f,np.abs(H),**kwargs)
    ax_mag.axhline(y=1,color='black')
    if len(gain_crossover) != 0:
        ax_mag.axvline(x=gain_crossover[0].value,color='red',linewidth=1,linestyle='dashed')
    if len(phase_crossover) != 0:
        ax_mag.axvline(x=phase_crossover[0].value,color='red',linewidth=1,linestyle='dashed')
    ax_mag.set_xscale('log',nonpositive='mask')#nonposx='clip')
    ax_mag.set_yscale('log',nonpositive='mask')#nonposy='clip')
    ax_mag.set_ylabel('Magnitude' if ylabels is None else ylabels[0])
    ax_mag.set_xlim(f[1].value,f[-1].value)
    ax_mag.grid(True,which='both')
    if title: ax_mag.set_title(title)
    if 'label' in kwargs:
        ax_mag.legend()
        kwargs.pop('label')

    H_angle = H.angle
    if unwrap:
        H_angle = u.Quantity(np.unwrap(H_angle),H_angle.unit)
    ax_phase.plot(f,H_angle,**kwargs)
    ax_phase.axhline(y=0,color='black')
    ax_phase.axhline(y=-pi,color='red',linewidth=1,linestyle='dashed')
    if len(gain_crossover) != 0:
        ax_phase.axvline(x=gain_crossover[0].value,color='red',linewidth=1,linestyle='dashed')
    if len(phase_crossover) != 0:
        ax_phase.axvline(x=phase_crossover[0].value,color='red',linewidth=1,linestyle='dashed')
    ax_phase.set_xscale('log',nonpositive='mask')#nonposx='clip')
    ax_phase.set_ylabel('Phase' if ylabels is None else ylabels[1])
    ax_phase.set_xlabel('Frequency, {}'.format(f.unit))
    ax_phase.set_xlim(f[1].value,f[-1].value)
    ax_phase.grid(True,which='both')
    if pi_format:
        ax_phase.yaxis.set_major_locator(plt.MultipleLocator(np.pi / 2))
        ax_phase.yaxis.set_major_formatter(plt.FuncFormatter(multiple_formatter()))

    plt.suptitle(fig_name)

def nyquist_plot(f=None,H=None,title=None,fig=None,**kwargs):
    '''Create a Nyquist plot of the complex system H
    
    Call signatures:
    
        nyquist_plot(f,H,**kwargs) where f is a frequency array and H is the values array
        nyquist_plot((f,H),**kwargs)
        nyquist_plot(H=H,**kwargs) the Nyquist plot doesn't actually use f explicitly (the curve is parameterized by f)

    Parameters
    ----------
    f : Quantity array, with units of frequency
        The frequencies.
        This argument can be a tuple (f,H)
        f is optional if H is given
    H : Quantity or numpy array
        The complex values, one per frequency
    
    Keyword arguments
    -----------------
    Keywords are generally passed to the plot routine. Some keywords
    are of special relevance to this routine:
    
    fig : str, int, or matplotlib.figure.Figure
        The figure to plot in to. If a plot already exists in that
        figure, the curves are overlayed with previous curves.
    label : str
        The label corresponding to this curve. It will be put in the
        legend on the magnitude graph only.

    Notes
    -----
    Eventually be able to put ticks along the curve at the
    parametric frequencies:
    [x._loc for x in ax.xaxis.get_major_ticks()]
    [x._loc for x in ax.xaxis.get_minor_ticks()]
    '''
    if isinstance(f,(list,tuple)):
        f,H = f

    figsize = (7,7.5) # inches

    fig_name = fig if isinstance(fig,str) else 'Nyquist Plot'
    exfig = get_fig(fig)
    if exfig:
        exfig.show()
        ax, = exfig.axes
    else:
        fig = plt.figure(figsize=figsize)
        fig.name = fig_name
        ax = plt.gca()

    #H = H[1:]
    notnan = ~np.isnan(H)
    H = H[notnan]

    if not exfig:
        w = 1.5
        ax.set_xlim(-w,w)
        ax.set_ylim(-w,w)
        ax.xaxis.set_minor_locator(AutoMinorLocator())            
        ax.yaxis.set_minor_locator(AutoMinorLocator())
        ax.grid(True,which='major')
        ax.grid(True,which='minor',lw=.3,color='orange')
        ax.set_xlabel(r'Real part $H_{ol}(s)$')
        ax.set_ylabel(r'Imaginary part $H_{ol}(s)$')
        dth = 2*np.pi/100.
        th = np.arange(0,2*np.pi+dth,dth)
        ax.plot(np.cos(th),np.sin(th),'k--')
        ax.plot([-1],[0],'ko')
        ax.axhline(color='black')
        ax.axvline(color='black')
        
    lines = ax.plot(np.real(H), np.imag(H),**kwargs)
    label = kwargs.get('label',None)
    if not hasattr(ax,'curves'): ax.curves = Curves()
    ax.curves.add(label,lines)
    
    ax.set_title(fig_name)
    if title: ax.set_title(fig_name +'\n' + title)
    if 'label' in kwargs: ax.legend()

def nichols_chart(f,H,title=None,fig=None,unwrap=True,pi_format=True,**kwargs):
    figsize = (7,7.5) # inches

    fig_name = fig if isinstance(fig,str) else 'Nichols Chart'
    exfig = get_fig(fig)
    if exfig:
        exfig.show()
        ax, = exfig.axes
    else:
        fig = plt.figure(figsize=figsize)
        fig.name = fig_name

    H = H[~np.isnan(H)]
    phase = H.phase
    if unwrap: phase = np.unwrap(phase)
    plt.plot(phase,H.mag,**kwargs)
    ax = plt.gca()
    ax.set_yscale('log')
    plt.ylabel('magnitude')
    plt.xlabel('phase')
    plt.grid(True,which='both')
    if pi_format:
        ax.xaxis.set_major_locator(plt.MultipleLocator(np.pi / 2))
        ax.xaxis.set_major_formatter(plt.FuncFormatter(multiple_formatter()))
    ax.axhline(1,color='black',lw=2)
    ax.axvline(pi,color='red',linestyle='dotted')
    ax.axvline(-pi,color='red',linestyle='dotted')
    plt.plot([-pi],[1],'ro')
    ax.set_title(fig_name)
    if title: ax.set_title(fig_name + '\n' + title)
    if 'label' in kwargs: ax.legend()

def pole_zero_plot(poles,zeros,**kwargs):
    ''' pole-zero plot
    '''
    fig = kwargs.pop('fig',None)
    exfig = get_fig(fig)
    if not exfig:
        plt.figure()
    else:
        exfig.show()
    
    for p in poles:
        p = u.Quantity([p])
        plt.plot(p.real,p.imag,'x',color='red')
    for z in zeros:
        z = u.Quantity([z])
        plt.plot(z.real,z.imag,'o',color='green')
    
    if exfig:
        ax = exfig.axes[0]
        ax.autoscale()
    else:
        ax = plt.gca()
    ax.set_aspect(1)
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    lim = u.Quantity(xlim + ylim).abs().max()
    ax.set_xlim(-lim,lim)
    ax.set_ylim(-lim,lim)

    title = kwargs.pop('title','Pole-Zero plot')
    subtitle = kwargs.pop('subtitle',None)
    
    plt.suptitle(title)
    plt.title(subtitle)
    if not exfig:
        ax.set_xlabel('Real')
        ax.set_ylabel('Imag')
        ax.axhline(0,linewidth=1,alpha=.5,color='black')
        ax.axvline(0,linewidth=1,alpha=.5,color='black')            

# --------- zero crossing, gain and phase margins, stability -------
def zero_crossing(x,y,y0=0):
    ''' Find the points on x where y crosses y0
    '''
    z = np.sign(y-y0)
    z[z==0] = 1
    ks = np.where(np.diff(z))[0]
    zc = []
    for k in ks:
        x1,x2 = x[k:k+2]
        y1,y2 = y[k:k+2]
        dx,dy = x2-x1,y2-y1
        x0 = x1 - (dx/dy)*(y1-y0)
        zc.append(x0)
    zc = u.Quantity(zc)
    zc = zc[~np.isnan(zc)]
    return zc

def phase_margin(S):
    '''Determine the phase margin of the Transfer Function S.
    The phase margin is defined as the additional phase lag that
    S could have and still remain stable in closed loop.
    1/(1+S e^{-i phi}) is stable for 0 < phi < phase margin
    A positive phase margin means 1/(1+S) is stable
    A negative phase margin means 1/(1+S) is unstable
    '''
    f0 = zero_crossing(S.f,np.abs(S.H),y0=1)
    if len(f0) == 0:
        return np.nan
    i = 1j
    s = i*2*np.pi*f0
    pm = np.angle(S(s)) + np.pi*u.rad
    return min(pm)

def gain_margin(S):
    '''Determine the gain margin of the Transfer Function S.
    The gain margin is defined as the maximum factor by which an
    open loop transfer function S can be multiplied and still
    remain stable in closed loop.
    1/(1+gS) is stable for 0 > g > gain margin
    A gain margin > 1 means 1/(1+S) is stable
    A gain margin < 1 means 1/(1+S) is unstable
    '''
    # look for the places where S crosses the negative real axis
    # by detecting a 2 pi jump in "angle"
    phase = np.angle(S.H)
    phase_jump = np.abs(np.diff(phase))
    ks = np.where(phase_jump > (np.pi*u.rad/2))
    if len(ks) == 0:
        return np.nan
    f0 = S.f[ks]
    # we could do this instead as follows
    # f0 = zero_crossings(S.f, np.unwrap(np.angle(S.H)), y0=-np.pi)
    # but this would not catch any subsequent crossings (-3 pi, -5 pi, etc.)
    i = 1j
    s = i*2*np.pi*f0
    gm = 1./np.abs(S(s))
    return min(gm)

def is_stable(S):
    '''Determine whether an open loop Transfer Function S
    would be stable in closed loop. This is determined by
    whether the magnitude of S < 1 at the freqency where phase of S
    is -pi, i.e. the gain margin is > 1.
    '''
    gm = gain_margin(S)
    return gm > 1.

def crossover_frequency(S):
    return zero_crossing(S.f, np.abs(S.H), y0=1)

def pi_phase_shift_frequency(S):
    return zero_crossing(S.f, np.unwrap(np.angle(S.H)), y0=-np.pi)

# ---------- 2-D (spatial) spectra -----------

class Spectrum2D():
    '''Fourier domain spectrum, 'k-space', where k = (kx,ky)
    '''
    def __init__(self,S,name='',**kwargs):
        '''
        .. math::
        
        S(k) &= \\int {\\cal C}(\\xi) e^{i 2 \\pi k \\cdot \\xi} d^2\\xi \\
             &= - \\frac{1}{2} \\int {\\cal D}(\\xi) e^{i 2 \\pi k \\cdot \\xi} d^2\\xi
        
        Parameters:
        -----------
        S : function
            A function of a 2-space k-vector (a 2 element Quantity with units of
            type 1/length)
        name : str
            Then name of the function
        latex_formula : str
            A Latex string (surrounded by $) that is the formula for the spectrum
        latex_name : str
            A latex string (surrounded by $). Optional in case you want the
            name to have math symbols
        
        '''
        self.S = S
        self.name = name
        if not callable(S):
            raise Exception('S musts be a callable function')
        params = inspect.signature(S).parameters
        d = [(pname,param.default) for (pname,param) in params.items() if pname != 'k']
        if np.array([x[1]==inspect.Signature.empty for x in d]).any():
            raise Exception('arguments other than k must be keyword argument with defaults')
        d = dict(d)
        self.avs = list(d.keys())
        self.__dict__.update(d)
        self.__dict__.update(kwargs)
        self.domain = 'k'
    
    def gen_samples(self,**kwargs):
        '''Generate a sampling of the 2-D spectrum in k-space
        
        Parameters:
        -----------
        kmax : Quantity, units of kind 1/length
            The extent in k-space to sample. The resulting grid will
            cover -kmax < kx < kmax, -kmax < ky < kmax
        n : int
            The number of samples across one dimension. The resulting
            grid is :math:`n\\times n`
        '''
        if hasattr(self,'gened_samples'):
            kmax = self.gened_samples.kmax
            n = self.gened_samples.n
        else:
            d = 20*u.cm
            kmax = 1/(2*d)
            n = 64
        kmax = kwargs.pop('kmax',kmax)
        n = kwargs.pop('n',n)
        dk = 2*kmax/n
        kx = np.arange(-n//2,n//2)*dk
        ky = kx
        scope = self._scope()
        ks = itertools.product(kx,ky)
        with np.errstate(divide='ignore',invalid='ignore'):
            S = [self.S(u.Quantity(k),**scope) for k in tqdm.tqdm(ks,total=n*n)]
        S = u.Quantity(S).reshape((n,n))
        S.dx = u.Quantity([dk,dk])
        S.x0 = -(n//2)*S.dx
        S.name = self.name
        samples = DotDict({
            'k':[kx,ky],
            'S':S,
            'n':n,
            'dk':dk,
            'kmax':kmax,
        })
        self.gened_samples = samples
        return(ks,S)
    
    def _scope(self):
        d = dict([(x,getattr(self,x)) for x in self.avs])
        return d
            
    @property
    def kmax(self):
        '''
        kmax : Quantity, units of kind 1/length
            The extent in k-space to sample. The resulting grid will
            cover -kmax < kx < kmax, -kmax < ky < kmax
        
        Re-setting kmax will cause the samples to be regenerated.
        '''
        return self.gened_samples.kmax
    
    @kmax.setter
    def kmax(self,k):
        if isinstance(k,(tuple,list)):
            kwargs = dict(zip(('kmax','n'),k))
        else:
            kwargs = {'kmax':k}
        self.gen_samples(**kwargs)
    
    def sample(self,**kwargs):
        '''Generate a sampling of the 2-D spectrum in k-space
        
        Parameters:
        -----------
        kmax : Quantity, units of kind 1/length
            The extent in k-space to sample. The resulting grid will
            cover -kmax < kx < kmax, -kmax < ky < kmax
        n : int
            The number of samples across one dimension. The resulting
            grid is :math:`n\\times n`
        '''
        self.gen_samples(**kwargs)
    
    @property
    def samples(self):
        return self.gened_samples
    
    def __call__(self,k=None,**kwargs):
        scope = self._scope()
        if not all([kw in scope for kw in kwargs]):
            raise Exception('keyword arguments can only be one of: {}'.format(self.avs))
        if k is None: # just set the parameters and return a copy
            r = copy.deepcopy(self)
            r.__dict__.update(kwargs)
            return r
        
        with np.errstate(divide='ignore',invalid='ignore'):
            if isinstance(k,list):
                S = [self.S(kk,**scope) for kk in k]
                S = u.Quantity(S)
            else:
                S = self.S(k,**scope)
        return S
    
    def __mul__(self,other):
        if isinstance(other,Spectrum2D):
            r = Spectrum2D(lambda k: self(k)*other(k))
        elif isscalar(other):
            r = Spectrum2D(lambda k: self(k)*other)
        else:
            raise TypeError
        return r
    
    def __rmul__(self,other):
        return self.__mul__(other)
    
    def __truediv__(self,other):
        if isinstance(other,Spectrum2D):        
            r = Spectrum2D(lambda k: self(k)/other(k))
        elif isscalar(other):
            r = Spectrum2D(lambda k: self(k)/other)
        else:
            raise TypeError
        return r
    
    def __rtruediv__(self,other):
        if isinstance(other,Spectrum2D):        
            r = Spectrum2D(lambda k: other(k)/self(k))
        elif isscalar(other):
            r = Spectrum2D(lambda k: other/self(k))
        else:
            raise TypeError
        return r
    
    def __sub__(self,other):
        if isinstance(other,Spectrum2D):        
            r = Spectrum2D(lambda k: self(k) - other(k))
        elif isscalar(other):
            r = Spectrum2D(lambda k: self(k) - other)
        else:
            raise TypeError
        return r
    
    def __rsub__(self,other):
        if isinstance(other,Spectrum2D):        
            r = Spectrum2D(lambda k: other(k) - self(k))
        elif isscalar(other):
            r = Spectrum2D(lambda k: other - self(k))
        else:
            raise TypeError
        return r
    
    def __add__(self,other):
        if isinstance(other,Spectrum2D):        
            r = Spectrum2D(lambda k: self(k) + other(k))
        elif isscalar(other):
            r = Spectrum2D(lambda k: self(k) + other)
        else:
            raise TypeError
        return r
    
    def __radd__(self,other):
        return self.__add__(other)
    
    def __neg__(self):
        return Spectrum2D(lambda k: -self(k))
        
    def __array_ufunc__(self,method,*args,**kwargs):
        # prevents numpy from casting an __rmul__ result to a numpy array
        # instead lets this class override it
        # see https://numpy.org/doc/stable/reference/arrays.classes.html#numpy.class.__array_ufunc__
        if method == np.multiply:
            return self.__mul__(args[1])
        elif method == np.divide:
            return self.__rtruediv__(args[1])
        elif method == np.add:
            return self.__add__(args[1])
        elif method == np.subtract:
            return self.__rsub__(args[1])
        elif method == np.equal:
            return False
        elif method == np.isfinite:
            return True
        return NotImplemented

    def abs(self):
        return Spectrum2D(lambda k: self(k).abs())

    def conj(self):
        return Spectrum2D(lambda k: self(k).conj())

    def pprint(self):
        oprint.pprint(self,expand=1)
    
    @property
    def pp(self):
        self.pprint()
    
    def show(self,**kwargs):
        '''Display an image of the 2-D spectrum
        
        Parameters:
        -----------
        scale : str or tuple: (str,float)
            Image display scaling.
            The scale can be 'linear','log', or 'pow'. It can also be a tuple
            with the second item being a scale parameter::
             
                ('linear',1.) - intensity = value (parameter not used)
                ('log',decades) - intensity = log(value), with decades of display (default: 3 decades)
                ('pow',power) - intensity = val**power (default power=0.5 (square root stretch))

        Notes:
        ------
        Remaining keywords are sent to plt.imshow. useful ones are::
         
            cmap - color map, default: 'gray'
            interpolation - default: 'nearest'
            origin - default: 'lower'
         '''
        if not hasattr(self,'gened_samples'): self.gen_samples()
        S = self.gened_samples.S
        unit = S.unit
        dx,dy = S.dx
        xlabel = kwargs.pop('xlabel','$k_x$') + ' $\\left[{0}\\right]$'.format(latex_unit(dx.unit))
        ylabel = kwargs.pop('ylabel','$k_y$') + ' $\\left[{0}\\right]$'.format(latex_unit(dy.unit))
        scale  = kwargs.pop('scale','linear')
        if isinstance(scale,tuple):
            scale,scale_param = scale
        else:
            scale_param = None
        if scale == 'log':
            if scale_param is None:
                S = u.Quantity(np.log10(S.value)).asQ(S)
            else:
                Smax = S[~np.isinf(S)&~np.isnan(S)].max().value
                S = u.Quantity(np.log10(S.value+10**(-scale_param)*Smax))
        elif scale == 'pow':
            if scale_param is None: scale_param = 0.5
            S = u.Quantity(np.power(S.value,scale_param)).asQ(S)
        
        S.scale = (unit,scale,scale_param)
        S.show(**kwargs)
        ax = plt.gca()
        ax.set_xlabel(xlabel,size=14)
        ax.set_ylabel(ylabel,size=14)
        ax.axhline(S.dx[1].value/2,color='black',linewidth=.5)
        ax.axvline(S.dx[0].value/2,color='black',linewidth=.5)
    
    def plot(self,axis='x',scale=('log','log'),**kwargs):
        '''plot a lineout of the 2-D spectrum along
        the axis from k = 0 to k = kmax.
        
        Parameters:
        -----------
        axis : str
            The axis to plot, either 'x' or 'y'
        
        '''
        if not hasattr(self,'gened_samples'): self.gen_samples()
        S = self.reload(c)
        unit = S.dx.unit
        if axis == 'x':
            Ss = S.slice(y=0*unit,x=(0*unit,))
        elif axis == 'y':
            Ss = S.slice(y=(0*unit,),x=0*unit)
        Ss.plot(**kwargs)
        ax = plt.gca()
        if scale[0] == 'log': ax.set_xscale('log')
        if scale[1] == 'log': ax.set_yscale('log')
        plt.grid(True,which='both')
        ax.set_xlabel('$k \\left[ {0} \\right]$'.format(latex_unit(unit)),size=14)
    
    def integrate(self):
        '''Integrate the 2-D spectrum (useful for signal variance given
        power spectrum)
        '''
        if not hasattr(self,'gened_samples'): self.gen_samples()
        S = self.gened_samples.S
        dk = self.gened_samples.dk
        nans = np.isnan(S)
        if nans.any():
            print('<integrate> caution - some samples are NaN')
            I = S[~nans].sum()*dk**2
        else:
            I = S.sum()*dk**2
        return I

def Kolmog_k_spectrum(r0=20*u.cm):
    S = Spectrum2D(lambda k,r0=r0: c1*r0**(-5/3)*(k.dot(k))**(-11/6),
                   name='Kolmogorov',
                   latex_name = '$S_\\phi(k)$',
                   latex_formula='$0.023 r_0^(-5/3) k^(-11/3)$',
                   formula = '0.023 r0**(-5/3) k**(-11/3)',
                   )
    return S

def DM_k_spectrum(d=40*u.cm):
    def boxLPF(k,d):
        kx,ky = k
        if kx.abs() < 1/(2*d) and ky.abs() < 1/(2*d):
            return 1.
        else:
            return 0.
    D = Spectrum2D(lambda k,d=d: boxLPF(k,d),
                   name = 'Deformable Mirror',
                   latex_name = '$F_{\\text{dm}}(k)$',
                   latex_formula='$|k_x| < \\frac{1}{2d}; \\quad |k_y| < \\frac{1}{2d}$',
                   formula = '|kx| < 1/2d; |ky| < 1/2d',
                   )
    return D

Sphi2 = Kolmog_k_spectrum(r0=20*u.cm)
DM2 = DM_k_spectrum(d=40*u.cm)

# ------ star magnitude to and from flux -----
# approximate v magnitude

def to_flux(m,as_dict=False):
    if not isinstance(m,u.Magnitude):
        m = u.Magnitude(m)
    F0 = 9.e9*u.ct/(u.m**2*u.s) # approximate v-filter flux of mv=0 star
    F = F0*m.physical
    if as_dict:
        d = 20*u.cm
        T = 1*u.ms
        n = (F*d**2*T).to(u.ct)
        flux = {'n':n, 'd':d, 'T':T}
        return flux
    return F
    
def v_mag(flux):
    if isinstance(flux,dict):
        flux = flux['n']/(flux['d']**2*flux['T'])
    F0 = 9.e9*u.ct/(u.m**2*u.s) # approximate v-filter flux of mv=0 star
    F = flux
    m = (u.Magnitude(F) - u.Magnitude(F0)).to(u.mag()) # u.Magnitude( float(2.5*np.log10(F0/F)) )
    return m

# ------ latex string manipulation ------

def latex_q(q):
    '''format a Quantity with units in latex format
    '''
    val = q.value
    unit = q.unit
    return '{0}{1:latex}'.format(val,unit)

def latex_unit(unit):
    '''format a unit string with latex format but without the $ delimiters
    '''
    return '{0:latex}'.format(unit).strip('$')

def _latex_unit(unit):
    if str(unit) == '': return ''
    s = '{0:latex}'.format(unit).strip('$')
    s = '$[%s]$'%s
    return s

LATEX_PREP_FONT = 'sf'

def latex_prep(name,font=LATEX_PREP_FONT):
    '''prepare a plane text string for latex display
    
    font is the font specifier, e.g. 'rm' or 'sf' is good for reading plane text.
    See http://merkel.texture.rocks/Latex/Manual/fonts2e.html
    for a list of available fonts.
    font='' means use the usual latex math mode font (a slant script)
    '''
    if name.startswith('$') and name.endswith('$'): return name # already formatted for latex
    if name.startswith('(') and name.endswith(')'): name = name[1:-1]
    if name.startswith('|(') and name.endswith(')|'): name = '|'+name[2:-2]+'|'
    name = name.replace(' ','\ ')
    name = name.replace('^*','^\\ast')
    name = name.replace('*','\\times')
    name = name.replace('&','\\&')
    if font != '': font = '\\'+font
    name = f'${font} {name}$'
    return name

# ---------- matplotlib helpers ----------
def get_figs():
    '''Get a dictionary of all the figures that have names,
    keyed by name
    '''
    nums = plt.get_fignums()
    figs = [plt.figure(num) for num in nums]
    r = [(x.name,x) for x in figs if hasattr(x,'name')]
    return dict(r)

def get_fig(f):
    '''Retrieve an existing figure by name, number, or Figure instance
    '''
    namedfigs = get_figs()
    fignums = plt.get_fignums()
    if isinstance(f,Figure):
        if f.number in fignums:
            return f
        else:
            return None
    elif isinstance(f,int):
        if f in fignums:
            f = plt.figure(f)
            return f
        else:
            return None
    elif isinstance(f,str):
        if f in namedfigs:
            return namedfigs[f]
        else:
            return None

def set_fig(name):
    '''Retrieve an existing, or create a new figure by name.
    '''
    fig = get_fig(name)
    if not fig:
        fig = plt.figure()
        fig.name = name
    plt.figure(fig.number)
    return fig

class Curves:
    '''container for multiple curves in a graph
    '''
    def __init__(self):
        self.curves = []
    
    def add(self,label,lines):
        self.curves.append(DotDict({'label':label,'lines':lines}))
    
    def remove(self,label):
        '''remove a curve and its legend entry and refresh the legend display.
        Curves can be refered to by index or label. The latest curve is
        at the end of the list
        
        Parameters
        ----------
        label : int or str
            The curve index or string label
        '''
        curve = None

        if isinstance(label,int): # removed the i'th curve
            index = label
        elif isinstance(label,str): # remove the labeled curve
            index = None
            for ind,c in enumerate(self.curves):
                if c.label == label:
                    index = ind
            if index is None:
                raise Exception('no curve with label {}'.format(label))
        curve = self.curves.pop(index)
        ax = curve.lines[0].axes
        [l.remove() for l in curve.lines]
        if curve.label is not None:
            if len(self.curves) > 0:
                ax.legend()
            else:
                legend = ax.get_legend()
                if legend is not None: legend.remove()
        
# from https://stackoverflow.com/questions/40642061/how-to-set-axis-ticks-in-multiples-of-pi-python-matplotlib
def multiple_formatter(denominator=2, number=np.pi, latex='\pi'):
    def gcd(a, b):
        while b:
            a, b = b, a%b
        return a
    def _multiple_formatter(x, pos):
        den = denominator
        num = np.int(np.rint(den*x/number))
        com = gcd(num,den)
        (num,den) = (int(num/com),int(den/com))
        if den==1:
            if num==0:
                return r'$0$'
            if num==1:
                return r'$%s$'%latex
            elif num==-1:
                return r'$-%s$'%latex
            else:
                return r'$%s%s$'%(num,latex)
        else:
            if num==1:
                #return r'$\frac{%s}{%s}$'%(latex,den)
                return r'$%s / %s$'%(latex,den)
            elif num==-1:
                #return r'$\frac{-%s}{%s}$'%(latex,den)
                return r'$-%s / %s$'%(latex,den)
            else:
                #return r'$\frac{%s%s}{%s}$'%(num,latex,den)
                return r'$%s%s / %s$'%(num,latex,den)
    return _multiple_formatter

class Multiple:
    def __init__(self, denominator=2, number=np.pi, latex='\pi'):
        self.denominator = denominator
        self.number = number
        self.latex = latex
        
    def locator(self):
        return plt.MultipleLocator(self.number / self.denominator)
    
    def formatter(self):
        return plt.FuncFormatter(multiple_formatter(self.denominator, self.number, self.latex))
    
def test_formatter():
    x = np.linspace(-np.pi, 3*np.pi,500)
    plt.figure()
    plt.plot(x, np.cos(x))
    plt.title(r'Multiples of $\pi$')
    ax = plt.gca()
    ax.grid(True)
    ax.set_aspect(1.0)
    ax.axhline(0, color='black', lw=2)
    ax.axvline(0, color='black', lw=2)
    ax.xaxis.set_major_locator(plt.MultipleLocator(np.pi / 2))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(np.pi / 12))
    ax.xaxis.set_major_formatter(plt.FuncFormatter(multiple_formatter()))
    plt.show()

if legacy_code:
    def old_init():
        n = 1024
        T = 1.*u.ms
        cltf = ClosedLoopTF(n,T,gain=.5,gamma=.99)
        oltf = cltf.oltf
        t = np.arange(2*n)*T
        x = np.cos(2*np.pi*u.rad*10*u.Hz*t)*np.exp(-t/(500*u.ms))
        sig = Signal(t,x)
        globals().update(locals())
        
    def old_test():
        n = 1024
        T = 1.*u.ms
        cltf = ClosedLoopTF(n,T,gain=.5,gamma=.99)
        s = cltf.s[1]
        oltf = cltf.oltf
        (cltf.abs**2).plot(title='Closed Loop Transfer Function')
        f = cltf.f
        f0 = 1*u.Hz
        spectrum = ( np.sqrt(f**2+f0**2)**(-8/3) ).value
        S = Signal(f,spectrum,name='Atmos')
        S.fplot(label='atmos',
                title='Residual spectrum, before and after close-loop',
                ylabels = ('$S_\\phi(f)$',None),)
        fig = plt.gcf()
        print('open loop: {}'.format(S.X.sum()))
        gains = [.1,.3,.5,.8,1.]
        for gain in gains:
            cltf.gain = gain
            e = (cltf.abs**2)*S
            e.fplot(fig=fig.number,label='gain={}'.format(gain))
            print('gain={0}: {1}'.format(gain,e.X.sum()))
        globals().update(locals())
    
    n = 1024
    T = 1*u.ms
    def testa(n=n,T=T):
        i = 1j
        oltf = ClosedLoopTF(n,T)
        s = i*2*np.pi*oltf.df
        print('the value of oltf at s={} is {}'.format(s,oltf(s)))
        q = oltf*2
        print('the value of q=oltf*2 at s={} is {}'.format(s,q(s)))
        globals().update(locals())
        
    def test1(oplot=False):
        """step response of the control
        g*Hol / (1 + g*Hol)
        """
        global figs
        name = 'test1'
        try:
            fig = figs[name]
        except:
            fig = plt.figure() # inches
            figs[name] = fig
            wplace = wlocs[name][0:2]
            #fig.canvas.manager.window.move(*wplace)
            #fig.canvas.manager.window.raise_()
            plt.pause(1e-6)
        if oplot:
            pass
        else:
            plt.clf()
        f,df,fn = frequencies(n,T)
        t = T*np.arange(n)
        gains = np.arange(.1,1.,.1)
        first = True
        for gain in gains:
            label = str(gain)
            H = gain*open_loopTF(s,T,gamma=gamma)*closed_loopTF(s,gain,T,gamma=gamma)*delay(s,100*T)
            p = step_response(H)
            plt.plot(t,p,label=label)
            if first:
                ax = plt.gca()
                ax.set_xlim(0.09,0.15)
                ax.set_xlabel('time, seconds')
                first = False
            plt.pause(.01)
        plt.grid('on')
        plt.legend(title='gain')
        plt.title('step response of control')
        plt.pause(1e-6)
        
    def test2(oplot=False):
        """step response of the residual
        1 / (1 + g*Hol)
        """
        global figs
        name = 'test2'
        try:
            fig = figs[name]
        except:
            fig = plt.figure() # inches
            figs[name] = fig
            wplace = wlocs[name][0:2]
            #fig.canvas.manager.window.move(*wplace)
            #fig.canvas.manager.window.raise_()
            plt.pause(1e-6)
        if oplot:
            pass
        else:
            plt.clf()
        f,df,fn = frequencies(n,T)
        t = T*np.arange(n)
        gains = np.arange(.1,1.,.1)
        first = True
        for gain in gains:
            label = str(gain)
            H = closed_loopTF(s,gain,T,gamma=gamma)*delay(s,100*T)
            p = step_response(H)
            plt.plot(t,p,label=label)
            if first:
                ax = plt.gca()
                ax.set_xlim(0.09,0.15)
                ax.set_xlabel('time, seconds')
                first = False
            plt.pause(.01)
        plt.grid('on')
        plt.legend(title='gain',loc=4)
        plt.title('step response of residual')
        plt.pause(1e-6)
    
    def test3(gamma=1.,T=1.e-3):
        """step and frequency response as a function of delay
        """
        global figs
        colors = ['b','g','r','c','m','y','k']
        n = 1024
        f,df,fn = frequencies(n,T)
        t = T*np.arange(n)
        s = i*2*np.pi*f
        delay_set = [1,1.5,2]
        gain_set = [.2,.5]
        for gain in gain_set:
            fig = plt.figure()
            first = True
            color_index = 0        
            for delta in delay_set:
                Hcl = gain*open_loopTF(s,T,gamma=gamma,delta=delta)*closed_loopTF(s,gain,T=T,gamma=gamma,delta=delta)
                pr = step_response(Hcl*delay(s,0.1))
                label = str(delta)+'xT'
                plt.plot(t,pr,label=label,color=colors[color_index])
                if first:
                    ax = plt.gca()
                    ax.set_xlim(0.09,0.15)
                    ax.set_xlabel('time, sec')
                    plt.grid('on')
                    first = False
                color_index = (color_index + 1) % len(colors)
                plt.pause(.01)
            plt.legend(title='delay')
            plt.title('step response closed loop')
            plt.pause(1e-6)
            
            oplot = False
            first = True
            color_index = 0
            for delta in delay_set:
                bode_plot(f,closed_loopTF(s,gain,T,gamma=gamma,delta=delta),
                          plot_phase=True,oplot=oplot,color=colors[color_index])
                color_index = (color_index + 1) % len(colors)
                if first:
                    oplot = True
                    first = False
                color_index = (color_index + 1) % len(colors)
                plt.pause(.001)    
    
    def test4(gain_set=[0.2,0.8,0.99,1.6],gamma=0.9,delay=0.0048,T=0.004):
        """This is Alex's model
        """
        colors = ['b','g','r','c','m','y','k']
        n = 1024
        f,df,fn = frequencies(n,T)
        t = T*np.arange(n)
        s = i*2*np.pi*f
        delta = delay/T
        oplot = False
        for gain in gain_set:
            label = str(gain)
            bode_plot(f,closed_loopTF(s,gain,T,gamma=gamma,delta=delta),
                      plot_phase=True,oplot=oplot,label=label)
            oplot = True
        plt.subplot(211)
        plt.legend(title='gain',loc=4)
        title = 'Closed Loop Frequency Response\n'
        title += 'f = '+str(1./T)+'Hz, gamma '+str(gamma)+', delay '+str(delay*1000)+'ms'
        plt.title(title)
        
        oplot = False
        color_index = 0
        for gain in gain_set:
            label = str(gain)
            nyquist_plot(f,gain*open_loopTF(s,T,gamma=gamma,delta=delta),
                         oplot=oplot,color=colors[color_index],label=label)
            color_index += 1
            oplot = True
        plt.legend(title='gain',loc=4)
        
    def main(close_plots = True):
        global figs
        if close_plots:
            plt.close('all')
            figs = {}
        test1()
        test2()
        test0()
        nyquist_plot(f,open_loopTF(s,T,gamma=gamma))
        fig = plt.gcf()
        #wplace = wlocs[name][0:2]
        #fig.canvas.manager.window.move(*wplace)
        #fig.canvas.manager.window.raise_()
    
    if __name__ == '__main__':
        main()
        r = raw_input('press return to exit')
    
plt.ion()
