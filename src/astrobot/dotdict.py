"""dotdict - a dictionary object that supports dot access notation
"""
import numpy as np
import astropy.units as u
from astrobot import oprint

class DotDict(dict):
    """dot.notation access to dictionary attributes"""
    
    def __repr__(self):
        return oprint.ppd(self,print_=False,head='',tail='')
    
    def copy(self):
        return DotDict(super(DotDict,self).copy())
    
    def __getattr__(self,attr):
        if attr in [ '__name__','__class__','__objclass__']:
            return 'DotDict'
        return dict.__getitem__(self,attr)
    
    def __deepcopy__(self,memo):
        return self.copy()
    
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

def typize(d):
    """convert dictionary values from strings to strings, ints, floats, and Quantities or lists thereof
    according to representation
    """
    r = {}
    for key,val in d.items():
        val = val.split('#')[0]  # strip off comments (don't put a #-character in a value!)
        if val.startswith("'"):
            val = val.strip("'")
        elif val.startswith('['): # list, like [1,2,3] arcsec
            q = val[1:].split(']')
            vals = q[0].split(',')
            unit = q[1]
            q = []
            for x in vals:
                try:
                    z = int(x)
                except:
                    try:
                        z = float(x)
                    except:
                        pass
                q.append(z)
            if unit != '':
                q = u.Quantity(q,unit)
            else:
                q = np.array(q)
            val = q
        else:
            try:
                val = int(val)
            except:
                try:
                   val = float(val)
                except:
                    try:
                        val = u.Quantity(val)
                    except:
                        pass
        r[key] = val
    return DotDict(r)
