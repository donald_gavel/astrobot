'''
wfs.py - models for Shack-Hartmann wavefront sensors
'''
import os
from astropy import units as u
from astropy.io import fits
import numpy as np
import pandas as pd
import configparser
from pkg_resources import resource_filename

from astrobot import dotdict
from astrobot import q_helper as qh
from astrobot import oprint
from astrobot.img import nextpow2,zeropad

import matplotlib.pyplot as plt
import matplotlib.patches as patches

rc_configFile = resource_filename('astrobot','data/wfs.cfg')
i = 1.j

class Telescope():
    '''Top-level class of telescope. Use one of the sub-classes to
    create a specific telescope.
    '''
    def __init__(self,configFile=rc_configFile):
        cp = configparser.ConfigParser()
        cp.optionxform = str # keep the keys case sensitive
        cp.read(configFile)
        d = dotdict.typize(dict(cp.items('telescope')))
        self.__dict__.update(d)
        
    @property
    def pp(self):
        oprint.pprint(self)
    
    def psf(self,pixel=0.03*u.arcsec,wavelength=1.2*u.micron):
        ap = self.aperture
        ap.wavelength = wavelength
        n,m = ap.shape
        ap = ap.zeropad(n = u.Quantity([n,m])*2)
        otf = ap.oft().axto(u.arcsec)
        otf = otf.crop(size=(n,m))
        if pixel is not None:
            otf = otf.zoomto(pixel*n)
        psf = otf.abs2()
        return psf

class Shane_Telescope(Telescope):
    def __init__(self,configFile=rc_configFile,n=1024):
        super().__init__(configFile)
        self.f = self.f_number*self.d_pri
        self.platescale = (1.*u.radian/self.f).to(u.arcsec/u.mm)
        L = 1.2*self.d_pri
        du = L/n
        self.aperture = self.ap((n,n),du)
        
    def ap(self,nn,du):
        '''Define an apertuere, based on the telescope parameters
        
        Parameters:
        -----------
        nn - tuple of 2 ints
            The shape of the result array (ny,nx)
        du - Quantity with length compatible units
            The fine grid sampling of the aperture
            
        Returns:
        --------
        Quantity
            An array with 1's in the aperture, 0's outside the aperture
        '''
        m,n = nn
        D,Ds = self.d_pri, self.d_sec
        c=(m//2-.5,n//2-.5)
        ap = u.Quantity.circle(nn,r=D/2,dx=du,c=c) - u.Quantity.circle(nn,r=Ds/2,dx=du,c=c)
        ap.name = f'Shane Telescope aperture, D = {D}, Ds = {Ds}'
        return ap
        
class WFS():
    def __init__(self,configFile=rc_configFile,sections=[]):
        cp = configparser.ConfigParser()
        cp.optionxform = str # keep the keys case sensitive
        cp.read(configFile)
        self.cp = cp
        ccd = dotdict.typize(dict(cp.items('ccd')))
        ccd_dtype = np.__dict__[ccd.pop('dtype','uint16')]
        ccd_unit = ccd.pop('unit','DN')
        self.ccd = u.Quantity(np.zeros(ccd.shape),ccd_unit).astype(ccd_dtype).set_attributes(**ccd)
        
        for sect in sections:
            _d = dotdict.typize(dict(cp.items(sect)))
            self.__dict__.update(_d)

    @property
    def pp(self, ):
        oprint.pprint(self)
        
class Hartmann_WFS(WFS):

    def ccd_image(self,phi):
        '''create a CCD image given a wavefront phase
        
        Parameters
        ----------
        phi: 2-D Quantity
            The phase grid. This must "cover" the aperture at least. Any size and
            dx is acceptable, but should provide a reasonable sampling in the pupil plane
            that is many fine pixels per r0 and many fine pixels per subaperture.
            The units of phi can be length-like or radians phase. In the case of
            length-like units the Quantity must have a "wavelength" attribute.
            
        Returns
        -------
        2-D Quantity, the CCD array
        '''
        
        if not hasattr(self,'slocs') or phi.shape != self.phiap.shape or (phi.dx != self.phiap.dx).any():
            self.prep_ccd_image(phi)
            
        if phi.unit.physical_type == 'length':
            phi = (phi*2*np.pi/phi.wavelength).to_da(u.dimensionless_unscaled)
        elif phi.unit.physical_type == 'angle':
            phi = phi.to_da(u.dimensionless_unscaled)
        else:
            assert phi.unit == u.dimensionless_unscaled

        # Create images for each lenslet and transfer to the CCD array
        
        # 1) Transfer phase/wavefront data into subaps
        #ap = self.phiap*self.haps
        ap = self.phiap
        wf = self.subap_cube
        wf[:] = 0
        wf.flat[self.dmap] = ap.flat[self.smap] * np.exp(i*phi.flat[self.smap])
        # 2) Fourier-image the subaps (pupil plane to focal plane)
        wf = np.fft.fft2(wf)
        wf = np.fft.fftshift(wf,axes=(-2,-1))
        wf = np.abs(wf)**2
        self.subap_cube = wf.astype(complex)

    def prep_ccd_image(self,phi=None,n=256,L=120*u.percent):
        '''prepare the indirect addressing arrays for
        imaging the wavefront through lenslets onto the CCD
        
        Parameters
        ----------
        phi - Quantity
            a "representative" phase screen having size and sampling (dx) properties
            the same as those of phase screens that will be passed in on subsequent
            ccc_image calls. This can be None in which case the phase screen will
            be automatically defined as having n samples across and total width & height
            of L% of the telescope diameter. The properties for subsequent phase
            screens can be read from the properties of self.phiap.
        
        n = int
            only used if phi is not provided (phi=None). The size of the automatically
            defined phase screen.
        
        L - percent Quantity (or float)
            the physical size of the phase screen if the phase screen properties are
            to be automatically defined (phi=None). In percentage of a pupil diameter
            (this should be greater than 100%).
        '''
        d,n_p = self.d, self.n_p

        if phi is None:
            D = self.tele.d_pri
            L = (D*L).to(u.m)
            phi = u.Quantity(np.zeros((n,n)))
            phi.dx = [L/n,]*2
            phi.x0 = [-(n//2)*dx for dx in phi.dx]

        du = phi.dx[0]
        M,N = phi.shape
        ap = self.tele.ap((M,N),du)
        ns = ms = rint(d/du)
        count = rint((N//2*du)/d)
        xphi = np.array(range(-count,count))*ns + N//2
        count = rint((M//2*du)/d)
        yphi = np.array(range(-count,count))*ms + M//2
        slocs = [(y,x) for y in yphi for x in xphi]        
        
        illums = [ap[y:y+ms,x:x+ns].sum()/(ns*ms) for y in yphi for x in xphi]
        illumed = np.where(np.array(illums) >= self.illum_pc)[0]
        n_subaps = len(illumed)
        slocs = np.array(slocs)[illumed]

        a = np.ones((ms,ns))
        ms2,ns2 = [2*nextpow2(k) for k in [ms,ns]]
        a2 = zeropad(a,(ns2,ms2))
        #source = [ np.ravel_multi_index(( np.meshgrid(range(x,x+ns),range(y,y+ms)) ),(M,N),order='F').flatten() for y,x in slocs ]
        source = [ np.ravel_multi_index( np.mgrid[y:y+ms, x:x+ns] , (M,N) ) for y,x in slocs ]
        
        dest0 = np.ravel_multi_index(np.where(a2>0),(ms2,ns2))
        dest = [ dest0 + k*ns2*ms2 for k in range(n_subaps) ]
        
        # kx,ky = np.fft.fftfreq(N,1/N),np.fft.fftfreq(M,1/M)
        # kx,ky = np.meshgrid(kx,ky)
        # s = np.exp(-i*np.pi*(kx/ns2+ky/ms2))
        
        # tilt a subaperture to cause a -1/2 a pixel shift in both x and y in the final image
        kx,ky = np.mgrid[-ns2//2:ns2//2],np.mgrid[-ms2//2:ms2//2]
        kx,ky = np.meshgrid(kx,ky)
        s = np.exp(-i*np.pi*(kx/ns2+ky/ms2))
        # distribute the subapertures over the aperture
        s = np.array([s for k in range(n_subaps)])
        haps = np.ones((M,N)).astype(complex) # make it ones, not zeros, to preserve the ap magnitude
        haps.flat[source] = s.flat[dest]
        ap = ap*haps # encodes the subaperture pixel shifts in the phase of the aperture
        
        self.phiap = ap # aperture on the phi grid
        self.subap_cube = np.zeros((n_subaps,ms2,ns2)).astype(complex) # storage for subap wavefronts during imaging
        self.slocs = slocs # lower-left corner pixel locations (y,x) of subaps on the phi grid
        self.n_subaps = len(slocs) # number of (illuminated) subaps
        self.dmap = np.array(dest).flatten() # destination indices into subap_cube
        self.smap = np.array(source).flatten() # source indices from phi
        
    def show(self):
        '''draw the subap boxes on the aperture and number them
        '''
        if not hasattr(self,'slocs'):
            self.prep_ccd_image()
        slocs = self.slocs
        n_subaps = self.n_subaps
        m,n = self.phiap.shape
        dx,dy = self.phiap.dx
        d = self.d.value
        self.phiap.mag.show()
        ax = plt.gca()
        for k,(y,x) in enumerate(slocs):
            x = (x-n//2)*dx.value
            y = (y-m//2)*dx.value
            ax.annotate(str(k),(x+d/2,y+d/2),horizontalalignment='center',verticalalignment='center')
            dum = ax.add_patch(patches.Rectangle((x,y),d,d,facecolor='grey',alpha=.5,edgecolor='red'))
        plt.draw()
        
class Lick_WFS(Hartmann_WFS):
    def __init__(self,mode='16x',configFile=rc_configFile):
        self.name = f'ShaneAO WFS, Hartmann {mode}'
        self.mode = mode
        self.tele = Shane_Telescope(configFile=configFile)
        super().__init__(configFile=configFile,sections=[f'{mode}WFS'])
        
def rint(a):
    '''round a float and return the result as an integer type
    '''
    if isinstance(a,u.Quantity):
        a = a.to('')
    if isinstance(a,np.ndarray) and a.size > 1:
        return vrint(np.array(a))
    return int(np.rint(a))

vrint = np.vectorize(lambda x: int(np.rint(x)))

# ------------ tests --------------
def test_tele():
    global tele,psf
    tele = Shane_Telescope()
    tele.aperture.show()
    psf = tele.psf()
    psf.show(scale=('log',10))
    psfz = psf.zoomto(1*u.arcsec)
    psfz.show(scale='log')

def test_WFS():
    global w
    w = WFS()

def test_maps(mode='16x',n=512,**params):
    global w,phi
    if mode == 'both':
        for mode in ['8x','16x']:
            test_maps(mode=mode,n=n)
        return
    w = Lick_WFS(mode)
    if n == 'ccd':
        n = 160
        dx = (w.d/w.n_p)*u.pix
    else:
        L = 1.2*w.tele.d_pri
        dx = L/n
    phi = u.Quantity(np.random.normal(size=(n,n))).round(2)
    phi.dx = [dx,dx]
    w.__dict__.update(params)
    w.prep_ccd_image(phi)
    print(f'number of subaps: {w.n_subaps}')
    w.show()

def test_maps2(n=32,ntimes = 10,phi_test='zero'):
    '''make a phase screen and "image" it with the WFS
    Tests the indirect addressing scheme and the imaging
    and measures the average imaging time.
    
    n (int) is the number of pixels on the phase screen fine grid that
    span one subaperture "diameter". The resulting "pulled out" subapertures
    by the ccd_image algorithm are of a size that is the next power of 2
    greater than or equal to n
    '''
    import time
    global w,phi
    w = Lick_WFS()
    #n = 32 # subaps are 32x32 fine grid pixels
    N = 512
    if phi_test == 'random':
        phi = u.Quantity(np.random.normal(size=(N,N))).round(2)
        phipop = 10.
    elif phi_test == 'atmos':
        from astrobot import atmos
        a = atmos.Screen().gen_screen()
        phi = a.screen.copy()
        phi.wavelength = 700*u.nm
        phipop = phi.max()
    else: # phi_test == 'zero'
        phi = u.Quantity(np.zeros((N,N)))
        phipop = 10.
    phi.dx = [1,1]*(w.d/n)
    t0 = time.time()
    w.prep_ccd_image(phi)
    tf = time.time()
    dt = ((tf-t0)*u.second).to(u.ms)
    print(f'prep time: {dt.round(2)}')
    t0 = time.time()
    for k in range(ntimes):
        w.ccd_image(phi)
    tf = time.time()
    dt = ((tf-t0)*u.second).to(u.ms)/ntimes
    print(f'image time: {dt.round(2)} (average over {ntimes} calls)')
    
    phi.flat[w.smap] += phipop  # make the subapertures pop out
    phi += w.phiap.mag*phipop # make the aperture pop out
    phi.show()
    print(f'figure {plt.gcf().number}',flush=True)
    plt.pause(.1)
    
    u.Quantity(w.subap_cube).mag.show(scale=('log',4)) # a movie of the pulled out subaps
    print(f'figure {plt.gcf().number}',flush=True)
    plt.pause(.1)
    
    w.show()
    print(f'figure {plt.gcf().number}',flush=True)
    plt.pause(.1)
    
    scube = u.Quantity(w.subap_cube).mag
    z = (1/3)*w.phiap.mag
    z.flat[w.smap] = scube.flat[w.dmap]/scube.max()
    z.show(scale='pow')
    print(f'figure {plt.gcf().number}',flush=True)
    plt.pause(.1)

    