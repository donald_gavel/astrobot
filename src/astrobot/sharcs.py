"""
define array size, image field of view, and bad pixel map of Sharcs
"""
import os
import sys
import io
import numpy as np
import pandas as pd
import types
from scipy.ndimage import median_filter
from pkg_resources import resource_stream

import astropy.units as u
from astropy import stats
from astropy.io import fits

from astrobot import img
from astrobot import q_helper
from astrobot import oprint
from astrobot import finder
from astrobot.tcolor import cstr,gprint,wprint,eprint
from astrobot.bad_median import bad_med
from astrobot.astrodate import p_date

class Sharcs(object):
    def __init__(self,date=None,**kwargs):
        self.name = 'ShARCS'
        self.description = 'Shane Adaptive infraRed Camera and Spectrograph'
        self.location = 'Lick Observatory'
        self.telescope = 'Shane 3m'
        self.instrument = 'Shane Adaptive Optics System'
        self.detector = 'Hawaii 2-RG (2k x 2k)'
        self.n = n = 2048
        self.dx = dx = 0.033*u.arcsec
        self.dx.doc = 'pixel scale on sky'
        self.field_center = field_center = [747,1090] # pixels [row,col]
        self.field_radius = field_radius = 450 # pixels
        self.amps_delta = 64 # every so many pixels in x, a new amplifier & bias level
        x0,y0 = [-dx*field_center[x] for x in [1,0]]
        self.ap = u.Quantity(img.circle((n,n),field_center,field_radius))
        self.ap.dx = [dx,dx]
        self.ap.x0 = [x0,y0]
        self.ap.name='AO field of view on Sharcs detector'
        f = resource_stream('astrobot','data/badpix_prelim_g9_140409_full.fits')
        with fits.open(f) as hdu:
            badpix = hdu[0].data
        self.bad_pixel_map = u.Quantity(badpix).asQ(self.ap)
        self.bad_pixel_map.name = 'Sharcs detector bad pixel map'
        self.bad_pixel_map.filename = 'badpix_prelim_g9_140409_full.fits'
        self.bad_pixel_map[767,1078] = 0
        self.bad_pixel_map[766,1078] = 0
        self.bad_pixel_map[765,1077] = 1
        self.bad_pixel_map[812,1104] = 1
        self.bad_pixel_map[813,1104] = 1
        
        if date is not None:
            self.load(date)
        
        for k,v in kwargs.items(): setattr(self,k,v)
        
    def _pprint(self):
        f = io.StringIO()
        sys.stdout = f
        self.pp
        sys.stdout = sys.__stdout__
        r = f.getvalue()
        f.close()
        return r
    
    @property
    def pp(self):
        oprint.pprint(self)
    
    def __getitem__(self,key):
        db = self.db
        if isinstance(key,slice):
            nmax = key.stop
            n = list(range(*key.indices(nmax)))
            return self[n]
        elif isinstance(key,tuple): # [date,n] or [n,date]
            date,n = key
            if isinstance(date,(int,list)):
                n,date = key
            date = pd.Timestamp(finder.obs_night(date))
            q = db[db['obs_date']==date]
            if isinstance(n,list):
                sh1 = Sharcs()
                sh1.db = q
                r = sh1[n]
                r.sharcs = self
                return r
            else:
                filename = f's{n:04d}.fits'
                q = q[ q['filename']==filename]
                if len(q) == 0:
                    raise Exception(cstr('fail',f'no file {filename}'))
                e = q.iloc[0]
                r = ShQ(e.obs_date,e.dsn)
                r.name = f'{date.date()} [{n}] {e.filt1nam} band'
                r.sharcs =self
                return r
        elif isinstance(key,int):
            filename = f's{key:04d}.fits'
            q = db[db['filename']==filename]
            if len(q) == 0:
                raise Exception(cstr('fail',f'no file {filename}'))
            elif len(q) == 1:
                e = q.iloc[0]
                r = ShQ(e.obs_date,e.dsn)
                r.sharcs = self
                return r
            else:
                return q
        elif isinstance(key,list): # list of ints
            return [self[x] for x in key]
    
    def read_image(self,e):
        path = e.path
        with fits.open(path) as hdu:
            image = hdu[0].data
            hdr = hdu[0].header
        if image.shape == (self.n,self.n):
            n = m = 1100
            r1,c1 = np.array(self.field_center) - n//2
            image = image[r1:r1+n,c1:c1+n]
            image = np.fliplr(np.rot90(image))
        else:
            n,m = image.shape

        image = u.Quantity(image)
        image.dx = [self.dx,]*2
        image.x0 = [-(x//2)*self.dx for x in [m,n]]
        if n == 1100:
            image.ap = self.ap[r1:r1+n,c1:c1+n].asQ(image)
            image.ap.name = 'aperture'
            image.badpix = self.bad_pixel_map[r1:r1+n,c1:c1+n].asQ(image)
            image.badpix.name = 'badpix'
        image.camera = self
        image.header = hdr
        image.__dict__.update(e)
        image.obs_date = str(image.obs_date.date())
        image.filenum = key
        image.debad = types.MethodType(debad, image)
        date, band,key,obj = [getattr(image,x) for x in ['obs_date','filt1nam','filenum','object']]
        image.name = f'{date} s{key:04d}, {band}, {obj}'
        return image        
        
    def makedb(self,date):
        '''make a database of Sharcs images and their FITS file
        headers taken on the observing night
        associated with 'date'. The resulting database is stored as
        attribute 'db'.
        
        Argument:
        ---------
        date : str (or datetime.datetime, pd.Timestamp object) or list
            The time/date of the observation. The "observing night"
            is the night before any early morning observing time.
            Local time is assumed.
            If a list of dates is provided, they are combined into a
            single database.
        
        '''
        if isinstance(date,list):
            db = []
            for d in date:
                self.makedb(d)
                db.append(self.db.copy())
            db = pd.concat(db).reset_index(drop=True)
            db = db.sort_values(by=['obs_date','filename'])
            self.db = db
            return self
        obs_date = finder.obs_night(date)
        db = finder.dbHdrs('sharcs',obs_date)
        db = db.sort_values(by = 'filename')
        self.db = db
        return self
    
    def load(self,date):
        return self.makedb(date)
    
    @property
    def dba(self):
        if not hasattr(self,'db'):
            raise Exception(cstr('fail','no database. use self.load(date) to create a database'))
        cols = ['obs_date',
                'filename',
                'dsn',
                'object',
                'ra',
                'dec',
                'airmass',
                'loopstat',
                'filt1nam',
                'filt2nam',
                'itime',
                'coadds']
        return self.db[cols]
    
    def eval(self,image):
        '''evaluate an image for Strehl and FWHM
        image must have its fits header attached as attribute 'header'
        '''
        pass
        
class ShQ(u.Quantity):
    '''Sharcs Quantity. A Quantity array specific to a Sharcs image.
    The original raw array is cropped to the field and flipped/rotated
    sky-right.
    
    Usage 1: (load from data files)
        q = ShQ(date,filenum)
        
    filenum can be a list, in which case the resulting array
    will be a data cube, with the first index being the 'cube' layer.
    
    Usage 2: (instantiate from list of Sharcs Quantities)
        q = ShQ([a,b,...])
    with ShQ's a, b, ... will create a data cube, with properties gleaned from
    the first one in the list.
    '''
    def __new__(cls,*args,**kwargs):
        
        if len(args) == 1: # signature: ShQ([q]) => expect argument to be a list of ShQ
            q = args[0]
            if isinstance(q,list):
                d0 = q[0]
                self = super().__new__(cls,[x.view(u.Quantity) for x in q])
            elif isinstance(q,np.ndarray):
                d0 = q
                self = super().__new__(cls,q.view(u.Quantity))
            for k,v in d0.__dict__.items(): setattr(self,k,v)
        
        else: # signature: ShQ(date,dsn)
            assert len(args) == 2, f'ShQ requires 2 arguments: (date,dsn)'
            date,dsn = args
            date = p_date(date)
            sh = Sharcs()
            n = m = 1100
            r1,c1 = np.array(sh.field_center) - n//2
    
            if isinstance(dsn,list):
                filenames = [f's{x:04d}.fits' for x in dsn]
                vol,[path,] = finder.findVol('sharcs',date,filenames[0],quiet=True)
                with fits.open(path) as hdu:
                    hdr = hdu[0].header
                path = os.path.split(path)[0]
                z = []
                for filename in filenames:
                    with fits.open(os.path.join(path,filename)) as hdu:
                        data = hdu[0].data
                    data = data[r1:r1+n,c1:c1+n]
                    data = np.fliplr(np.rot90(data))
                    z.append(data.copy())
                data = np.array(z)
                filename = filenames
            else:
                filename = f's{dsn:04d}.fits'
                vol,[path,] = finder.findVol('sharcs',date,dsn,quiet=True)
                with fits.open(path) as hdu:
                    data = hdu[0].data
                    hdr = hdu[0].header
                data = data[r1:r1+n,c1:c1+n]
                data = np.fliplr(np.rot90(data))
    
            self = super().__new__(cls,data)
            self.sharcs = sh
            self.filename = filename
            self.date = date
            self.dx = [sh.dx]*2
            self.x0 = [-n//2*du for du in self.dx]
            self.header = hdr
            if isinstance(filename,list) and len(filename) > 1: self.header_note = f'header for {filename[0]}'
            self.dsn = dsn
            self.band = hdr['filt1nam']
            self.ap = sh.ap[r1:r1+n,c1:c1+n]
            self.bad_pix = np.fliplr(np.rot90(sh.bad_pixel_map[r1:r1+n,c1:c1+n]))
            
        for k,v in kwargs.items(): setattr(self,k,v)
        return self
    
    def __array_finalize__(self,obj):
        if obj is None: return
        if not hasattr(obj,'__dict__'): return
        d = obj.__dict__
        for k,v in d.items(): setattr(self,k,v)
    
    def darks(self,date=None):
        '''get and average the darks associated with this data
        The darks can be from a different date
        '''
        db = getattr(self.sharcs,'db',None)
        if db is None:
            raise Exception('no database of darks. Set attribute sharcs to a Sharcs object with an observation database')
        if date is None:
            date = self.date # generally use darks from the same night as the data
        else: # unless overridden
            date = p_date(date)
        db = db[db.obs_date == pd.Timestamp(date)]
        dark_db = db[db.object.str.contains('dark')]
        itime = self.header['ITIME']

        # did we already load and average these darks? If so, use those
        key = f'{itime}ms,{date}' if date else f'{itime}ms'
        if not hasattr(self.sharcs,'darks'): self.sharcs.darks = {}
        dark = self.sharcs.darks.get(key,None)
        if dark is not None:
            self._dark = dark
            return dark
        
        # otherwise, load and average the darks
        dark_db = dark_db[dark_db.itime.astype(float) == float(itime)] # use only darks with this exposure time
        dsn = list(dark_db.dsn)
        if len(dark_db) == 0:
            raise Exception(cstr('fail',f'no darks at {itime}ms taken on {date}'))
        dark = ShQ(date,dsn).mean(axis=0)
        dark.db = dark_db
        dark.name = f'average dark, {itime} ms exposure'
        dark.itime = u.Quantity(itime,'ms')
        self._dark = dark
        
        # save this dark load-and-average in the sharcs object
        self.sharcs.darks[key] = dark
        return dark
    
    @property
    def dark(self):
        if hasattr(self,'_dark'): return self._dark
        else:
            self.darks()
            return self._dark
    
    def process_flats(self,date=None):
        '''process the flats associated with this data.
        The flats can be from a different date.
        '''
        db = getattr(self.sharcs,'db',None)
        if db is None:
            raise Exception('no database of darks. Set attribute sharcs to a Sharcs object with an observation database')
        if date is not None:
            date = p_date(date)
        else:
            date = self.date # flats assumed taken on data's date
        flats_db = db[(db.obs_date == pd.Timestamp(date)) & (db.object.str.contains('flat'))]
        band = self.header['filt1nam']
        
        # did we already calculate the flat-field for this band? If so, use it
        key = f'{band},{date}'
        if not hasattr(self.sharcs,'flats'): self.sharcs.flats = {}
        flat = self.sharcs.flats.get(key,None)
        if flat is not None:
            self._flat = flat
            return flat
        
        flats_db = flats_db[flats_db.filt1nam==band]
        if len(flats_db) == 0:
            raise Exception(cstr('fail',f'no flats in {band} band taken on {date}'))
        dsn = list(flats_db.dsn)
        flats = [ShQ(date,n, sharcs=self.sharcs) for n in dsn]
        flats = [x - x.dark for x in flats]
        flat = ShQ(flats).mean(axis=0) # maybe this should be a median?
        
        # process this mean flat to make it roughly around 1
        flat = bad_med(flat,flat.bad_pix,flat.ap)
        ind = np.where(flat.ap == 1)
        #ind = np.ravel_multi_index(ind,flat.shape)
        q = np.median(flat[ind])
        flat = (flat/q)*flat.ap + (1-flat.ap)
        
        flat.db = flats_db
        flat.name = f'average flat, {band} band (debad)'
        flat.band = band
        flat.dsn = dsn
        flat.filename = [x.filename for x in flats]
        for x,n in zip(flats,dsn): x.name = f's{n:04d} flat-dark {band} band'
        
        flat.raw_flats = flats
        self._flat = flat
        
        # save this flat field in the sharcs object
        self.sharcs.flats[key] = flat
        return flat
    
    @property
    def flat(self):
        if hasattr(self,'_flat'): return self._flat
        else:
            self.process_flats()
            return self._flat
    
    def debad(self,nsd=1,steps=[1,2,3]):
        '''returns a copy of self, debad-ed
        '''
        return debad(self,nsd=nsd,steps=steps)
    
    def pipe(self,darks_date=None,flats_date=None,show=False):
        '''a standard pipeline
        '''
        if darks_date is None: darks_date = getattr(self.sharcs,'darks_date',None)
        if flats_date is None: flats_date = getattr(self.sharcs,'flats_date',None)
        if darks_date is None: darks_date = self.date
        if flats_date is None: flats_date = self.date
        print(f'processing darks',flush=True)
        dark = self.darks(date=darks_date)
        print(f'processing flats',flush=True)
        flat = self.process_flats(date=flats_date)
        b = (self-dark)/flat
        print(f'repairing bad pixels',flush=True)
        b = b.debad(nsd=.5,steps=[1,2,3])[-1]
        self.piped = b
        print(f'done',flush=True)
        if show:
            if self.band == 'Ks': llim = np.median(b[np.where(b.ap==1)])
            else: llim = 0.
            np.clip(b,.9*llim,np.inf).show(scale='pow',cmap='hot')
        return b
    
def flatten(list_of_lists):
    '''flatten a list with list components
    see: https://stackabuse.com/python-how-to-flatten-list-of-lists/
    '''
    if len(list_of_lists) == 0:
        return list_of_lists
    if isinstance(list_of_lists[0], list):
        return flatten(list_of_lists[0]) + flatten(list_of_lists[1:])
    return list_of_lists[:1] + flatten(list_of_lists[1:])

def debad(self,nsd = 5,steps = [1,2]):
    '''clear the image of bad pixels.
    Step 1 is a modest clean-up given the bad pixel map
    Step 2 is a refinement, using the data to build a revised bad pixel map
    '''
    step = 0
    ap = self.ap
    badpix = self.bad_pix
    name = self.name
    band = self.header['filt1nam']
    pipe = [self]
    im = self
    stepstr = ''
    
    if 1 in steps:
        step = 1
        stepstr = f'{stepstr}{step}'
        step_name = 'bad_median'
        im = bad_med(im, badpix, ap)*ap
        im = u.Quantity(im).asQ(self)
        im.step_name = step_name
        im.step = step
        im.name = f'{name} debad{stepstr}'
        pipe.append(im.copy())
    
    if 2 in steps:
        step = 2
        stepstr = f'{stepstr}{step}'
        step_name = f'bads beyond {nsd} madstd'
        med_diff = im - u.Quantity(median_filter(im,3)).asQ(self)
        sd = stats.mad_std(im)
        badpix = np.where(med_diff.abs() > nsd*sd,1,0)
        im = bad_med(im, badpix, ap)*ap
        im = u.Quantity(im).asQ(self)
        im.step_name = step_name
        im.step = step
        im.name = f'{name} debad{stepstr}'
        pipe.append(im.copy())
    
    if 3 in steps:
        step = 3
        stepstr = f'{stepstr}{step}'
        step_name =  f'bads replaced by medians'
        im_mf = u.Quantity(median_filter(im,3)).asQ(self)
        med_diff = im - im_mf
        sd = stats.mad_std(im)
        badpix = np.where(med_diff.abs() > nsd*sd,1,0)
        im = np.where(badpix,im_mf,im)        
        im = u.Quantity(im).asQ(self)
        im.step_name = step_name
        im.step = step
        im.name = f'{name} debad{stepstr}'
        pipe.append(im.copy())
    
    return pipe

    
    
    