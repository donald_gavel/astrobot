#!/usr/bin/env python -i
'''
control systems
Model and analyze feedback control systems.
Model systems in the s and z domains. Model signals in the time and Laplace domains.
'''
from astropy import units as u
import numpy as np
from numpy import pi, exp
from scipy.interpolate import interp1d
import types
import inspect
import ast
import astunparse
from astrobot import oprint
from astrobot.tcolor import tcolor, gprint, wprint, eprint, cstr
from astrobot.dotdict import DotDict

import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
from matplotlib.figure import Figure

plt.rc('text',usetex=True)
i = 1j
isscalar = lambda x: (isinstance(x,u.Quantity) and x.isscalar) or isinstance(x,(int,float,complex))

class Base():
    '''Base class of Spectrum and Signal
    '''
    sampling_defaults = {
        'fmin': 0 * u.Hz,
        'fmax': 1 * u.kHz,
        't0': 0 * u.s,
        'tf': 1 * u.s,
        'n': 1024,
        'kind': 'linear',
    }

    def pprint(self):
        oprint.pprint(self, expand=1)

    @property
    def pp(self):
        self.pprint()

    def setattrs(self, **kwargs):
        self.__dict__.update(kwargs)
        return self

    def getattrs(self, v=None):
        if v is None:
            return vars(self)
        if not isinstance(v, list):
            v = [v]
        return {key: getattr(self, key) for key in v}

    def copy(self):
        return copy.deepcopy(self)

    def compose(self, *comp):
        if len(comp) == 1:
            comp = comp[0]
        self.comp = comp
        if len(comp) == 2:  # unary ops
            arg, op = comp
            domain_change = self.domain_type != arg.domain_type
            if not domain_change:
                self.domain = arg.domain
            self.params = arg.params
        elif len(comp) == 3:  # binary ops
            arg1, arg2, op = comp
            self.domain = arg1.domain_intersection(arg2)
            if isinstance(arg2, Base):
                self.params = {**arg2.params, **arg1.params}
            else:
                self.params = arg1.params
        return self

    def comp_list(self):
        '''get a list of the composing functions that are of Base class
        '''
        c = []
        if self.comp is None: return c
        if len(self.comp) == 2:
            arg_list = [self.comp[0]]
        elif len(self.comp) == 3:
            arg_list = [self.comp[x] for x in [0, 1]]
        for arg in arg_list:
            if isinstance(arg, Base):
                c = c + [arg] + arg.comp_list()
        return c

    def comp_tree(self, level=1):
        '''traverse the composition tree
        '''
        if level == 1:
            print()
            print(self.name)
            print(len(self.name) * '~')
        if self.comp is None: return
        tab = '|   '
        tabl = '|---'
        if len(self.comp) == 2:
            arg, op = self.comp
            arg_list = [arg]
        elif len(self.comp) == 3:
            arg1, arg2, op = self.comp
            arg_list = [arg1, arg2]
        else:
            raise Exception
        for arg in arg_list:
            if isinstance(arg, Base):
                print((level - 1) * tab + tabl, arg.name)
                arg.comp_tree(level=level + 1)
            else:
                print((level - 1) * tab + tabl, arg)
        print((level - 1) * tab + tabl, op)

    def domain_intersection(self, other):
        '''intersect the domains (valid ranges of the argument) of two system objects
        '''

        if not hasattr(other, 'domain'):
            return self.domain
        if self.domain_name == other.domain_name == 's':
            return 's'
        if self.domain_name == 'f' and other.domain_name == 's':
            return self.domain_tuple
        if self.domain_name == 's' and other.domain_name == 'f':
            return other.domain_tuple
        if self.domain_name == 'f' and other.domain_name == 'f':
            fmin, fmax = self.domain_tuple[1:]
            ofmin, ofmax = other.domain_tuple[1:]
            fmin = max(fmin, ofmin)
            fmax = min(fmax, ofmax)
            return ('f', fmin, fmax)
        if self.domain_name == 't' and other.domain_name == 't':
            tmin, tmax = self.domain_tuple[1:]
            otmin, otmax = other.domain_tuple[1:]
            tmin = max(tmin, otmin)
            tmax = min(tmax, otmax)
            return ('t', tmin, tmax)
        raise Exception('cannot intersect %s and %s domain objects' % (self.domain_name, other.domain_name))

    @property
    def samples(self):
        if hasattr(self, 'gened_samples'):
            return self.gened_samples
        elif hasattr(self, 'original_samples'):
            return self.original_samples
        else:
            return None

    def __array_ufunc__(self, method, *args, **kwargs):
        # prevents numpy from casting an __rmul__ result to a numpy array
        # instead lets this class override it
        # see https://numpy.org/doc/stable/reference/arrays.classes.html#numpy.class.__array_ufunc__
        if method == np.multiply:
            return self.__mul__(args[1])
        elif method == np.divide:
            return self.__rtruediv__(args[1])
        elif method == np.add:
            return self.__add__(args[1])
        elif method == np.subtract:
            return self.__rsub__(args[1])
        elif method == np.equal:
            return False
        elif method == np.isfinite:
            return True
        return NotImplemented

    @property
    def domain_name(self):
        return tuple(self.domain)[0]

    @property
    def domain_type(self):
        domains = {'s': 'f', 'f': 'f', 't': 't'}  # translation
        return domains[self.domain_name]

    @property
    def domain_tuple(self):
        if isinstance(self.domain, tuple):
            return self.domain
        else:
            return (self.domain_type, -np.inf, np.inf)

class Signal(Base):
    '''A time domain signal.
    Can be defined as a funtion of t, or in terms of samples.
    '''

    def from_spectrum(Sf, **kwargs):
        '''
        Create a Signal given its spectrum Sf.
        '''
        if hasattr(Sf, 'original_samples'):
            f, F, n, df, dt = (Sf._original_samples[x] for x in ['f', 'F', 'n', 'df', 'dt'])
        else:  # functional definition of spectrum
            Sf.gen_samples_FFT(**kwargs)
            f, F, n, df, dt = (Sf.gened_samples[x] for x in ['f', 'F', 'n', 'df', 'dt'])
        t0 = kwargs.get('t0', 0. * u.s)
        tf = t0 + n * dt
        X = np.fft.ifft(F) * df
        t = np.arange(t0 / dt.unit, tf / dt.unit, dt / dt.unit) * dt.unit
        return Signal((t, X), **kwargs)

    def __init__(self, x, name='', **kwargs):
        '''Create a Signal with two lists containing
        sample times and signal values (t,x) -or-
        a function of time, x(t)

        Parameters
        ----------
        x : 2-tuple (Quantity,Quantity) or function
            The pair (times, values) describing the time sequence.
        name : str
            (optional) The assigned name of the object

        '''
        self.name = name
        if isinstance(x, tuple):
            t, x = x
            n = len(t)
            dt = (t[1] - t[0])
            samples = DotDict({
                't': t,
                'x': x,
                'dt': dt,
                'n': n,
                'fn': (1 / (2 * dt)).to(u.Hz)
            })
            self.original_samples = samples
            self.x = interp1d(t, x, kind='cubic')
            self.domain = ('t', samples.t.min(), samples.t.max())
            self.params = {}
            self.unit = x.unit
        elif callable(x):
            params = inspect.signature(x).parameters
            domain = list(params.keys())[0]
            assert domain in ['t']
            self.x = x
            self.domain = domain
            self.signature = inspect.signature(x)
            self.params = {key: val.default for key, val in params.items() if val.default is not inspect._empty}
            self.unit = x(0 * u.s).unit
        self.comp = None

        self.__dict__.update(kwargs)

    @property
    def source(self):
        return inspect.getsource(self.x)

    def __mul__(self, other):
        if isscalar(other):
            r = Signal(lambda t, **p: other * self(t, **p),
                       name=f'({self.name} * {other})')
        elif isinstance(other, Signal):
            r = Signal(lambda t, **p: other(t, **p) * self(t, **p),
                       name=f'({self.name} * {other.name})')
        elif isinstance(other, TransferFunction):
            return (other * self.spectrum()).signal()
        else:
            raise TypeError('cannot multiply a Signal with a %s' % other.__class__.__name__)
        return r.compose(self, other, 'mul')

    def __rmul__(self, other):
        return self.__mul__(other)

    def __add__(self, other):
        if isinstance(other, Signal):
            r = Signal(lambda t, **p: other(t, **p) * self(t, **p),
                       name=f'({self.name} + {other.name})')
        elif isscalar(other):
            r = Signal(lambda t, **p: other * self(t, **p),
                       name=f'({self.name} + {other})')
        else:
            raise TypeError('cannot add a Signal to a %s' % other.__class__.__name__)
        return r.compose(self, other, 'add')

    def __radd__(self, other):
        return self.__add__(self, other)

    def __sub__(self, other):
        if isinstance(other, Signal):
            r = Signal(lambda t, **p: self(t, **p) - other(t, **p),
                       name=f'({self.name} - {other.name})')
        elif isscalar(other):
            r = Signal(lambda t, **p: self(t, **p) - other,
                       name=f'({self.name} - {other})')
        else:
            raise TypeError('cannot difference a Signal and a %s' % other.__class__.__name__)
        return r.compose(self, other, 'sub')

    def __rsub__(self, other):
        if isinstance(other, Signal):
            r = Signal(lambda t, **p: other(t, **p) - self(t, **p),
                       name=f'({other.name} - {self.name})')
        elif isscalar(other):
            r = Signal(lambda t, **p: other - self(t, **p),
                       name=f'({other} - {self.name})')
        else:
            raise TypeError('cannot difference a Signal and a %s' % other.__class__.__name__)
        return r.compose(self, other, 'rsub')

    def __truediv__(self, other):
        if isinstance(other, Signal):
            r = Signal(lambda t, **p: self(t, **p) / other(t, **p),
                       name=f'({self.name} / {other.name})')
        elif isscalar(other):
            r = Signal(lambda t, **p: self(t, **p) / other,
                       name=f'({self.name} / {other})')
        else:
            raise TypeError('cannot divide a Signal and a %s' % other.__class__.__name__)
        return r.compose(self, other, 'truediv')

    def __rtruediv__(self, other):
        if isinstance(other, Signal):
            r = Signal(lambda t, **p: other(t, **p) / self(t, **p),
                       name=f'({other.name} / {self.name})')
        elif isscalar(other):
            r = Signal(lambda t, **p: other / self(t, **p),
                       name=f'({other} / {self.name})')
        else:
            raise TypeError('cannot divide a Signal and a %s' % other.__class__.__name__)
        return r.compose(self, other, 'rtruediv')

    def __neg__(self):
        return Signal(lambda t, **p: -self(t, **p),
                      name=f'-{self.name}').compose(self, 'neg')

    def abs(self):
        return Signal(lambda t, **p: self(t, **p).abs(),
                      name=f'|{self.name}|').compose(self, 'abs')

    def gen_samples(self, **kwargs):
        '''generate time samples and corresponding data samples
        from the function
        Sampling specification must be provided in keyword
        arguments. The specification can be any of:
            t0,dt,tf
            t0,n,tf
            t0,dt,n
        t0 is optional and defaults to zero.
        End points t0 and tf are both included in the samples.
        '''
        t0 = kwargs.get('t0', 0 * u.ms)
        dt = kwargs.get('dt', None)
        n = kwargs.get('n', None)
        tf = kwargs.get('tf', None)
        if sum(x is not None for x in [dt, n, tf]) != 2:
            raise Exception(
                'two, and only two, of "dt", "n", "tf" must be specified as keyword arguments to define the sampling')
        if dt is None:
            dt = (tf - t0) / (n - 1)
        elif n is None:
            n = int(((tf - t0) / dt + 1).to('').value)
        elif tf is None:
            tf = t0 + (n - 1) * dt
        t = np.linspace(t0, tf, n)
        x = self(t)
        samples = DotDict({
            't': t,
            'x': x,
            'dt': dt,
            'n': n,
            't0': t0,
            'tf': tf,
        })
        self.gened_samples = samples

    def sample_like(self, other):
        '''use another signal's sampling to use as a template
        for this signals' sampling.

        Parameters:
        -----------
        Other: Signal
            The sigmal whose sampling is to be emulated
        '''
        dt, n = (other.samples[x] for x in ['dt', 'n'])
        self.gen_samples(dt=dt, n=n)

    def __call__(self, t=None, **kwargs):
        '''Return the value of the signal at time t

        Parameters
        ----------
        t : Quantity
            The time

        Only one of t can be specified, and must have
        time units

        Returns
        -------
        Quantity
            x(t)

        '''
        if t is None:  # just set the parameters and return a copy
            kwargs = {key: val for key, val in kwargs.items() if key in self.params}
            r = self.copy()
            r.x = functools.partial(r.x, **kwargs)
            r.params = {**self.params, **kwargs}
            return r
        if isinstance(t, list): t = u.Quantity(t)
        x = self.x
        if isinstance(x, interp1d):
            t = t.to(self.original_samples.t.unit)
            return u.Quantity(x(t), self.unit)
        else:
            return x(t, **kwargs)

    def minmax(self):
        t, x = self.gened_samples.t, self
        return x(t).minmax()

    def mean(self):
        t, x = self.gened_samples.t, self
        return x(t).mean()

    def var(self):
        t, x = self.gened_samples.t, self
        return x(t).var()

    def std(self):
        t, x = self.gened_samples.t, self
        return x(t).std()

    def plot(self, **kwargs):
        '''Plot the signal vs time

        Keyword arguments:
        ------------------
        t0, dt, tf, n -- go to Signal.gen_samples
            if none of these are provided, then it plots the original_samples
            or 1024 samples over the object's domain.
        kind, title, subtitle, fig, label go to time_domain_plot
        other keywords are forwarded to matplotlib's axis.plot
        '''
        t0, tf, n, kind = (self.sampling_defaults[x] for x in ['t0', 'tf', 'n', 'kind'])
        sam_args = [x for x in ['t0', 'tf', 'n', 'kind'] if x in kwargs]

        if not hasattr(self, 'original_samples'):  # case 1: functional definition
            if isinstance(self.domain, tuple):
                t0, tf = (self.domain[1], self.domain[2])
                n = 1024
            else:
                t0, tf, n = (self.sampling_defaults[x] for x in ['t0', 'tf', 'n'])
            dt = (tf - t0) / (n - 1)

        else:  # case 2: original samples definition
            t0, tf = self.original_samples.t.minmax()
            dt = self.original_samples.dt
            n = self.original_samples.n

        if len(sam_args) > 0:  # overrides
            t0 = kwargs.pop('t0', t0)
            tf = kwargs.pop('tf', tf)
            if 'dt' not in kwargs:
                n = kwargs.pop('n', n)
                dt = (tf - t0) / (n - 1)
            else:
                dt = kwargs.pop('dt')
                n_ignore = kwargs.pop('n', None)

        self.gen_samples(t0=t0, tf=tf, dt=dt)
        t, x = (self.samples[x] for x in ['t', 'x'])

        title = kwargs.pop('title', latex_prep(self.name))
        ylabel = kwargs.pop('ylabel', None)
        if hasattr(self, 'formula'): title = (title + '\n' + self.formula).strip()
        if not ylabel:
            ylabel = getattr(self, 'ylabel', 'Signal')
        kwargs.update({
            'title': title,
            'ylabel': ylabel,
        })
        time_domain_plot((t, x), **kwargs)

    def LaplaceTransform(self, s):
        '''Calculate the discrete Laplace transform of the signal
        This will match the Fourier transform for s along the
        positive imaginary axis.

        The Laplace transform can be calculated for
        any point in the complex plane.

        Parameters
        ----------
        s : Quantity
            The Laplace variable (or array). s should
            be in units of frequency, but if it is a unitless
            number it will be assumed to have units of Hz

        Returns
        -------
        The value of the Laplace transform at s

        '''
        try:
            assert s.unit.physical_type == 'frequency'
        except:
            s = s * u.Hz
        t, x = self.t, self.x
        if s.isscalar:
            Fs = np.sum(x * np.exp(-s * t))
        else:
            Fs = [np.sum(x * np.exp(-sj * t)) for sj in s]
            Fs = u.Quantity(Fs)
        return Fs

    def FourierTransform(self):
        '''Take the discrete Fourier Transform of the signal.

        Returns
        -------
        The frequency array and the spectral value array, as a tuple
        '''
        if not hasattr(self, 'gened_samples'): self.gen_samples()
        n, dt, t = (self.gened_samples[x] for x in ['n', 'dt', 't'])
        df = (1 / (n * dt)).to('Hz')
        fn = (1 / (2 * dt)).to('Hz')  # Nyquist
        f = (np.arange(n) - n // 2) * df
        # f = np.fft.fftshift(f.value)*f.unit
        f = np.fft.fftshift(f)
        x = self(t)
        # X = u.Quantity(np.fft.fft(x),x.unit)*dt.to(1/u.Hz)
        X = np.fft.fft(x) * dt.to(1 / u.Hz)

        return (f, X)

    def spectrum(self, **kwargs):
        '''
        Create a Spectrum given a Signal.

        See Spectrum.from_signal
        '''
        return Spectrum.from_signal(self, **kwargs)

    def sample_and_hold(self, T, **kwargs):
        '''Sample and hold a signal.
        Create a new signal consisting of the original signal
        that has been sampled every T units of time and held
        constant for duration T.
        ( T > 2*dt is required )
        '''
        sam = self.samples
        if sam is None:
            raise Exception('signal must be subsampled first (see gen_samples)')
        dt, t = (sam[x] for x in ['dt', 't'])
        t0, tf = t[0], t[-1] + dt
        assert T > 2 * dt, 'T > 2*dt is required'
        Tn = u.Quantity.arange(t0, tf, T)
        xTn = self(Tn)  # samples at spacing T
        x = np.repeat(xTn, int(T / dt))  # holds for T seconds
        x = x[:len(t)]
        if 'name' not in kwargs:
            kwargs['name'] = f'{self.name}_{{SH}}'
        kwargs.update({
            'Tsample': T,
        })
        return Signal((t, x), **kwargs)

    def sample(self, T, **kwargs):
        '''Sample a signal.
        Create a new signal consisting of samples of
        the original signal at every T units of time and
        zero elsewhere.
        ( T > 2*dt is requred ).
        '''
        sam = self.samples
        if sam is None:
            raise Exception('signal must be subsampled first (see gen_samples)')
        dt, t = (sam[x] for x in ['dt', 't'])
        t0, tf = t[0], t[-1] + dt
        assert T > 2 * dt, 'T > 2*dt is required'
        Tn = u.Quantity.arange(t0, tf, T)
        xTn = self(Tn)  # samples at spacing T
        x = np.zeros(len(t)) * self.unit
        ptr = range(0, len(t), int(T / dt))
        x[ptr] = xTn
        if 'name' not in kwargs:
            kwargs['name'] = f'{self.name}_{{S}}'
        kwargs.update({
            'Tsample': T,
        })
        return Signal((t, x), **kwargs)

class Spectrum(Base):
    '''Fourier or Laplace spectrum defined in the frequency domain.
    Can be defined as a function of f, or in terms of samples.
    The spectrum is two-sided, defined for positive and
    negative frequencies. One can specify only the positive side and
    it is implicitly assumed that the negative side is its complex-conjugate
    '''

    def from_signal(sig, **kwargs):
        '''
        Create a Spectrum given a signal S.

        Keywords t0,n,dt, and tf are used only if the
        Signal S is defined by a function. In that case
        the rules of S.gen_samples are used.
        '''
        if sig.samples is None:
            sig.gen_samples(**kwargs)
        t, x, n, dt = (sig.samples[x] for x in ['t', 'x', 'n', 'dt'])
        F = np.fft.fftshift(np.fft.fft(x) * dt).to(x.unit / u.Hz)
        f = np.fft.fftshift(np.fft.fftfreq(n, d=dt)).to(u.Hz)
        f = np.block([f, -f[0]])  # tack on the Nyquist value at the highest positive frequency
        F = np.block([F, F[0]])
        if 'name' not in kwargs:
            kwargs['name'] = '{\cal{F}}\{%s\}' % sig.name
        return Spectrum((f, F), T=dt, **kwargs).compose(sig, 'spectrum')

    def __init__(self, F, name='', **kwargs):
        '''
        $F(f) = \int_0^\infty x(t) dt$
        => F-units are x-units / Hz

        Parameters
        ----------
        F : 2-tuple of Quantity arrays or a function
            Definition of the Fourier spectrum, F(f)

            tuple: (f,F(f))
                where f and F(f) are equal length arrays
            function: F(f)
                given f produces F(f)
        name : str
        '''
        self.name = name
        if isinstance(F, tuple):  # sampled data
            f, F = F
            n = len(f)
            df = f[1] - f[0]
            samples = DotDict({
                'f': f,
                'F': F,
                'df': df,
                'n': n,
                'creation': 'original',
            })
            self.original_samples = samples
            self.F = interp1d(f, F, kind='cubic', bounds_error=False, fill_value=0.)
            self.domain = ('f', samples.f.min(), samples.f.max())
            self.params = {}
            self.unit = F.unit
        elif isinstance(F, Signal):
            q = Spectrum.from_signal(F, **kwargs)
            self.F = q.F
            self.domain = q.domain
            self.params = q.params
            self.unit = q.unit
            self.original_samples = q.original_samples
        elif callable(F):  # a function, F(f)
            params = inspect.signature(F).parameters
            domain = list(params.keys())[0]
            assert domain in ['f']
            self.F = F
            self.domain = domain
            self.signature = inspect.signature(F)
            self.params = {key: val.default for key, val in params.items() if val.default is not inspect._empty}
            self.unit = F(1 * u.Hz).unit
        self.comp = None
        self.__dict__.update(kwargs)

    @property
    def source(self):

        if isinstance(self.F,types.FunctionType):
            return get_lambda_source(self.F)
        elif isinstance(self.F,interp1d):
            return self.original_samples
        else:
            wprint(f'cannot retrieve source code for {type(self.F)}')
            return None

    def gen_samples(self, fmin=0 * u.kHz, fmax=1 * u.kHz, kind='linear', n=1024):
        '''generate lists of frequency samples and corresponding Spectrum samples
        from fmin to fmax inclusive.

        Parameters:
        -----------
        fmin, fmax : Quantity, frequency-like
            The minimum and maximum frequencies over which to sample the spectrum
            (end points are included in the result).

        kind : str
            Sample spacing, either 'linear' or 'log'
        n : int
            The number of samples

        Returns:
        --------
        None

        Side effect:
        ------------
            The sample frequencies and spectrum values are stored
            in the object's gened_samples attribute.

        '''
        if isinstance(self.domain, tuple):
            fmax = min(fmax, self.domain[2])
        fmin = fmin.to(u.Hz)
        fmax = fmax.to(u.Hz)
        if kind == 'linear':
            df = (fmax - fmin) / (n - 1)  # n samples, n-1 intervals
            f = np.linspace(fmin, fmax, n)
        elif kind == 'log':
            df = None
            f = np.logspace(np.log10(fmin.value), np.log10(fmax.value), n) * u.Hz
        else:
            raise Exception('<gen_samples> kind must be "linear" or "log"')
        F = self(f)
        samples = DotDict({
            'f': f,
            'F': F,
            'n': n,
            'df': df,
            'kind': kind,
            'creation': 'gen',
        })
        self.gened_samples = samples

    def gen_samples_FFT(self, fn=500 * u.Hz, n=1024, **kwargs):
        '''generate a sampling of the spectrum suitable to send to an
        inverse fast Fourier transform algorithm (iFFT).

        Parameters
        ----------
        fn : Quantity, frequency-like
            The Nyquist frequency
        n : int
            The total number of samples

        Returns
        -------
        A tuple of (f,F(f))
        Also stores sampling information in the gened_samples field of the Spectrum object
        '''
        i = 1j
        pi = np.pi
        df = 2 * fn / n
        f = np.linspace(-fn, fn - df, n)
        f = np.fft.ifftshift(f)
        F = self(f)
        samples = DotDict({
            'f': f,
            'F': F,
            'df': df,
            'n': n,
            'kind': 'FFT',
            'creation': 'gen'
        })
        self.gened_samples = samples

    def sample(self, **kwargs):
        kind = kwargs.get('kind', 'FFT')
        if kind == 'FFT':
            self.gen_samples_FFT(**kwargs)
        else:
            self.gen_samples(**kwargs)

    def __call__(self, f=None, **kwargs):
        # if not self.bundle:
        #    kwargs = {key:val for key,val in kwargs.items() if key in self.params}
        if f is None:  # just set the parameters and return a copy
            kwargs = {key: val for key, val in kwargs.items() if key in self.params}
            r = self.copy()
            r.F = functools.partial(r.F, **kwargs)
            r.params = {**self.params, **kwargs}
            return r
        if isinstance(f, list): f = u.Quantity(f)
        if isinstance(self.F, interp1d):
            f = f.to(u.Hz)
            return u.Quantity(self.F(f), self.unit)
        else:
            with np.errstate(divide='ignore', invalid='ignore'):
                r = self.F(f, **kwargs)
            return r

    def __add__(self, other):
        if isinstance(other, Spectrum):
            r = Spectrum(lambda f, **p: self(f, **p) + other(f, **p),
                         name=f'({self.name} + {other.name})')
        elif isscalar(other):
            r = Spectrum(lambda f, **p: self(f, **p) + other,
                         name=f'({self.name} + {other})')
        else:
            raise TypeError('cannot add a Spectrum to a %s' % other.__class__.__name__)
        return r.compose(self, other, 'add')

    def __radd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):
        if isinstance(other, Spectrum):
            r = Spectrum(lambda f, **p: self(f, **p) - other(f, **p),
                         name=f'({self.name} - {other.name})')
        elif isscalar(other):
            r = Spectrum(lambda f, **p: self(f, **p) - other,
                         name=f'({self.name} - {other})')
        else:
            raise TypeError('cannot difference a Spectrum and a %s' % other.__class__.__name__)
        return r.compose(self, other, 'sub')

    def __rsub__(self, other):
        if isinstance(other, Spectrum):
            r = Spectrum(lambda f, **p: other(f, **p) - self(f, **p),
                         name=f'({other.name} - {self.name})')
        elif isscalar(other):
            r = Spectrum(lambda f, **p: other - self(f, **p),
                         name=f'({other} - {self.name})')
        else:
            raise TypeError('cannot difference a Spectrum and a %s' % other.__class__.__name__)
        return r.compose(self, other, 'rsub')

    def __mul__(self, other):
        if isinstance(other, TransferFunction):
            i = 1j
            r = Spectrum(lambda f, **p: self(f, **p) * other(2 * pi * i * f, **p),
                         name=f'({self.name} * {other.name})')
        elif isinstance(other, Spectrum):
            r = Spectrum(lambda f, **p: self(f, **p) * other(f, **p),
                         name=f'({self.name} * {other.name})')
        elif isscalar(other):
            r = Spectrum(lambda f, **p: self(f, **p) * other,
                         name=f'({self.name} * {other})')
        else:
            raise TypeError('cannot multiply a Spectrum by a %s' % other.__class__.__name__)
        return r.compose(self, other, 'mul')

    def __rmul__(self, other):
        return self.__mul__(other)

    def __truediv__(self, other):
        if isinstance(other, Spectrum):
            r = Spectrum(lambda f, **p: self(f, **p) / other(f, **p),
                         name=f'({self.name} / {other.name})')
        elif isscalar(other):
            r = Spectrum(lambda f, **p: self(f, **p) / other,
                         name=f'({self.name} / {other})')
        else:
            raise TypeError('cannot divide a Spectrum and a %s' % other.__class__.__name__)
        return r.compose(self, other, 'truediv')

    def __rtruediv__(self, other):
        if isinstance(other, Spectrum):
            r = Spectrum(lambda f, **p: other(f, **p) / self(f, **p),
                         name=f'({other.name} / {self.name})')
        elif isscalar(other):
            r = Spectrum(lambda f, **p: other / self(f, **p),
                         name=f'({other} / {self.name})')
        else:
            raise TypeError('cannot divide a Spectrum and a %s' % other.__class__.__name__)
        return r.compose(self, other, 'rtruediv')

    def abs(self):
        return Spectrum(lambda f, **p: self(f, **p).abs(),
                        name=f'|{self.name}|').compose(self, 'abs')

    def abs2(self):
        return Spectrum(lambda f, **p: (self.abs() ** 2)(f, **p),
                        name=f'|{self.name}|^2').compose(self, 'abs2')

    def conj(self):
        return Spectrum(lambda f, **p: self(f, **p).conj(),
                        name=f'({self.name})^*').compose(self, 'conj')

    def __pow__(self, power):
        name = latex_prep(self.name)
        bra, ket = '{', '}'
        p = Fraction(power)
        if p.numerator < 10 and p.denominator < 10:
            e = f'{p}'
        else:
            e = power
        return Spectrum(lambda f: self(f) ** power,
                        name=f'({self.name})^{bra}{e}{ket}').compose(self, power, 'pow')

    def signal(self, **kwargs):
        '''Convert the Spectrum to a Signal
        via the Fourier Transform
        $x(t) = \int_{-\infty}^\infty F(f) df$

        Parameters
        ----------
        T : Quantity, time-like
            The sample period
        n : int
            The number of samples

        Returns
        -------
        A Signal object

        '''
        n = None
        if hasattr(self, 'original_samples'):
            n, df = (self.original_samples[x] for x in ['n', 'df'])
        elif self.comp is not None:
            # for arg in list(self.comp[:-1]):
            for arg in self.comp_list():
                if isinstance(arg, Spectrum) and hasattr(arg, 'original_samples'):
                    n, df = (arg.original_samples[x] for x in ['n', 'df'])
                    break
        if n is None:
            raise Exception('spectrum must be sampled')
        n = n - 1
        dt = 1 / (n * df)
        dt = dt.to(u.s)
        fn = (1 / (2 * dt)).to(u.Hz)
        self.gen_samples_FFT(fn=fn, n=n)
        f, F, df = (self.samples[x] for x in ['f', 'F', 'df'])
        t = np.linspace(0, (n - 1) * dt, n)
        x = n * np.fft.ifft(F).real * df
        if 'name' not in kwargs:
            kwargs['name'] = '{\cal{F}}^{-1}\{%s\}' % self.name
        return Signal((t, x), **kwargs).compose(self, 'signal')

    def plot(self, fmin=None, fmax=None, n=None, kind=None, os=False, **kwargs):
        '''Plot the Spectrum on a log-log plot.
        It is assumed that the spectrum is Hermetian-symmetric
        (transform of a real signal), so only positive frequencies are plotted.

        Parameters:
        -----------
        fmin,fmax: Quantities with frequency-like units
            The plotting min and max, which will be clipped to fit
            in the spectrum's domain limits, if it has them.
            Defaults to 0 Hz and domain limit or 500 Hz.
        n: int
            The number of points to plot. Default is 1024
        kind: str
            Spacing of the frequency sample points, either 'linear' or 'log'.
            Default is 'linear'
        os: boolean
            Plot only the original_samples, if the spectrum has them.
            Defaults to False.
        kwargs:
            Remaining keyword parameters are sent to frequency_domain_plot
        '''
        sam_args = {'fmin': fmin, 'fmax': fmax, 'n': n, 'kind': kind}
        fmin, fmax, n, kind = (self.sampling_defaults[key] if val is None else val for key, val in sam_args.items())

        if hasattr(self, 'original_samples') and tuple(self.domain)[0] == 'f':
            fmin_os, fmax_os = self.original_samples.f.minmax()
            self.domain = ('f', fmin_os, fmax_os)

        if os and hasattr(self, 'original_samples'):
            f, F = (self.original_samples[x] for x in ['f', 'F'])
            posf = f >= 0 * u.Hz
            f = f[posf]
            F = F[posf]
        else:
            if isinstance(self.domain, tuple):
                fmin = max(fmin, self.domain[1])
                fmax = min(fmax, self.domain[2])
            else:
                if fmax == np.inf:
                    raise Exception('fmax must be specified')
            self.gen_samples(fmin=fmin, fmax=fmax, n=n, kind=kind)
            f, F = (self.samples[x] for x in ['f', 'F'])

        title = kwargs.pop('title', latex_prep(self.name))
        ylabels = kwargs.pop('ylabels', ['Magnitude', 'Phase, rad'])
        subtitle = kwargs.pop('subtitle', None)
        pi_format = kwargs.pop('pi_format', False)
        if not subtitle and hasattr(self, 'latex_formula'):
            subtitle = self.latex_formula.strip('$')
            if hasattr(self, 'latex_name'):
                subtitle = self.latex_name.strip('$') + ' = ' + subtitle
            subtitle = '$' + subtitle + '$'
        kwargs.update({'title': title,
                       'subtitle': subtitle,
                       'ylabels': ylabels,
                       'pi_format': pi_format})

        frequency_domain_plot((f, F), **kwargs)

    def pole_zero_plot(self, **kwargs):
        ''' pole-zero plot
        '''
        assert all([hasattr(self, key) for key in ['poles', 'zeros']])
        title = kwargs.pop('title', 'Pole-Zero plot of {}'.format(self.name))
        subtitle = kwargs.pop('subtitle', None)
        if not subtitle and hasattr(self, 'latex_formula'):
            subtitle = self.latex_formula.strip('$')
            if hasattr(self, 'latex_name'):
                subtitle = self.latex_name.strip('$') + ' = ' + subtitle
            subtitle = '$' + subtitle + '$'

        kwargs.update({'title': title, 'subtitle': subtitle})
        pole_zero_plot(self.poles, self.zeros, **kwargs)

    def powerSpectrum(self, **kwargs):
        r = PowerSpectrum(lambda f, **p: (self.abs2() * (1 * u.Hz))(f, **p),
                          name='\Phi_{%s}' % self.name,
                          )
        return r

class TransferFunction(Spectrum):
    '''Transfer function defined in the Laplace (s) domain.
    '''

    def __init__(self, H, name='', **kwargs):
        '''
        Parameters
        ----------
        H : function
            Definition of the transfer function that takes one complex argument, s
            and returns a unitless complex number, the transfer function at s.
            If its a string it will be evaluated in a lambda function during the call.

        name : str
            The name assigned to the Transfer Function

        Notes:
        ------
        Other keywords are assigned as attributes of the TransferFunction

        '''
        self.name = name
        self.H = H
        if name == '': self.name = self.source.split(':')[1] # attempt to pull it out of the lambda function
        params = inspect.signature(H).parameters
        domain = list(params.keys())[0]
        assert domain in ['s']
        self.params = {key: val.default for key, val in params.items() if val.default is not inspect.Signature.empty}
        self.domain = domain
        self.comp = None
        self.__dict__.update(kwargs)

    @property
    def source(self):
        return get_lambda_source(self.H)

    def __call__(self, s=None, **kwargs):
        if self.params:
            kwargs = {key: val for key, val in kwargs.items() if
                      key in self.params}  # ignore keywords that aren't parameters
        params = {**self.params, **kwargs}  # union the existing and incoming parameters
        if s is None:  # just set the parameters and return a copy
            r = self.copy()
            r.params = params
            r.H = functools.partial(r.H, **kwargs)
            if hasattr(self, 'loop_transfer'):
                r.loop_transfer = self.loop_transfer(**kwargs)
            return r
        if call_trace:
            print(f'calling {self.name} with {params}')
        if isinstance(s, list): s = u.Quantity(s)
        H = self.H
        with np.errstate(divide='ignore', invalid='ignore'):
            r = H(s, **kwargs)
            if hasattr(r, 'unit'): r = r.decompose()
        return r

    def __add__(self, other):
        if isinstance(other, TransferFunction):
            r = TransferFunction(lambda s, **p: self(s, **p) + other(s, **p),
                                 name=f'({self.name} + {other.name})')
        elif isscalar(other):
            r = TransferFunction(lambda s, **p: self(s, **p) + other,
                                 name=f'({self.name} + {other})')
        else:
            raise TypeError('cannot add a TransferFunction to a %s' % other.__class__.__name__)
        return r.compose(self, other, 'add')

    def __radd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):
        if isinstance(other, TransferFunction):
            r = TransferFunction(lambda s, **p: self(s, **p) - other(s, **p),
                                 name=f'({self.name} - {other.name})')
        elif isscalar(other):
            r = TransferFunction(lambda s, **p: self(s, **p) - other,
                                 name=f'({self.name} - {other})')
        else:
            raise TypeError('cannot difference a TransferFunction and a %s' % other.__class__.__name__)
        return r.compose(self, other, 'sub')

    def __rsub__(self, other):
        if isinstance(other, TransferFunction):
            r = TransferFunction(lambda s, **p: other(s, **p) - self(s, **p),
                                 name=f'({other.name} - {self.name})')
        elif isscalar(other):
            r = TransferFunction(lambda s, **p: other - self(s, **p),
                                 name=f'({other} - {self.name})')
        else:
            raise TypeError('cannot difference a TransferFunction and a %s' % other.__class__.__name__)
        return r.compose(self, other, 'rsub')

    def __mul__(self, other):
        if isinstance(other, TransferFunction):
            r = TransferFunction(lambda s, **p: self(s, **p) * other(s, **p),
                                 name=f'({other.name} * {self.name})')
        elif isinstance(other, PowerSpectrum):
            i = 1j
            r = PowerSpectrum(lambda f, **p: self(2 * pi * i * f, **p) * other(f, **p),
                              name=f'({other.name} * {self.name})')
        elif isinstance(other, Spectrum):
            i = 1j
            r = Spectrum(lambda f, **p: self(2 * pi * i * f, **p) * other(f, **p),
                         name=f'({other.name} * {self.name})')
        elif isscalar(other):
            r = TransferFunction(lambda s, **p: self(s, **p) * other,
                                 name=f'({other} * {self.name})')
        elif isinstance(other, Signal):  # convolution
            return (self * other.spectrum()).signal()
        else:
            raise TypeError('cannot multiply a TransferFunction by a %s' % other.__class__.__name__)
        return r.compose(self, other, 'mul')

    def __rmul__(self, other):
        return self.__mul__(other)

    def __truediv__(self, other):
        if isinstance(other, TransferFunction):
            r = TransferFunction(lambda s, **p: self(s, **p) / other(s, **p),
                                 name=f'({self.name} / {other.name})')
        elif isscalar(other):
            r = TransferFunction(lambda s, **p: self(s, **p) / other,
                                 name=f'({self.name} / {other})')
        else:
            raise TypeError('cannot divide a TransferFunction and a %s' % other.__class__.__name__)
        return r.compose(self, other, 'truediv')

    def __rtruediv__(self, other):
        if isinstance(other, TransferFunction):
            r = TransferFunction(lambda s, **p: other(s, **p) / self(s, **p),
                                 name=f'({other.name} / {self.name})')
        elif isscalar(other):
            r = TransferFunction(lambda s, **p: other / self(s, **p),
                                 name=f'({other} / {self.name})')
        else:
            raise TypeError('cannot divide a TransferFunction and a %s' % other.__class__.__name__)
        return r.compose(self, other, 'rtruediv')

    def __neg__(self):
        return TransferFunction(lambda s, **p: -self(s, **p),
                                name=f'(-{self.name})').compose(self, 'neg')

    def abs(self):
        return TransferFunction(lambda s, **p: self(s, **p).abs(),
                                name=f'|{self.name}|').compose(self, 'abs')

    def abs2(self):
        return TransferFunction(lambda s, **p: (self.abs() ** 2)(s, **p),
                                name=f'|{self.name}|^2').compose(self, 'abs2')

    def conj(self):
        name = latex_prep(self.name)
        return TransferFunction(lambda s, **p: self(s, **p).conj(),
                                name=f'({self.name})^*').compose(self, 'conj')

    def __pow__(self, power):
        name = latex_prep(self.name)
        bra, ket = '{', '}'
        p = Fraction(power)
        if p.numerator < 10 and p.denominator < 10:
            e = f'{p}'
        else:
            e = power
        return TransferFunction(lambda s, **p: self(s, **p) ** power,
                                name=f'({self.name})^{bra}{e}{ket}').compose(self, power, 'pow')

    def impulse_response(self, Tsamp=None, window='blackman', n=1024):
        T = Tsamp
        if T is None:
            if 'T' in self.params:
                T = self.params['T']
            else:
                raise Exception('need to specify a sample period')
        fn = (1 / (2 * T)).to('Hz')
        df = 1 / T
        f = np.linspace(0, fn, n + 1)
        w = {'blackman': np.blackman,
             'hamming': np.hamming,
             'hanning': np.hanning}
        if window in w:
            w = w[window](2 * n + 1)[n:]
        else:
            w = 1
        H_pos_freq = self.H(i * 2 * pi * f) * w
        h = np.fft.irfft(H_pos_freq) * df
        t = np.arange(len(h)) * T
        name = 'Impulse Response: {}'.format(self.name)
        return Signal((t, h), name=name).compose(self, 'impulse_response:%s' % window)

    def plot(self, **kwargs):
        '''Plot the Transfer Function on a log-log plot.

        Keyword arguments:
        ------------------
        fmin, fmax, n, kind -- go to gen_samples
        title, subtitle, ylables -- go to frequency_domain_plot
        all other keywords are forwarded to matplotlib's axis.plot

        n : int
            Number of points to plot
        fmin, fmax : Quantity, frequency-like
            The minimum and maximum frequency range of the plot
        kind: str, 'linear' or 'log'
            frequency sample spacing
        title, subtitle: str
            title and subtitle shown at the top of the plot
        ylabels: tuple of strings
            The y axis labels of the magnitude and phase charts
        '''
        sam_args = dict([(key, kwargs.pop(key)) if key in kwargs else (key, self.sampling_defaults[key]) for key in
                         ['fmin', 'fmax', 'n', 'kind']])
        self.gen_samples(**sam_args)
        f, H = (self.samples[x] for x in ['f', 'H'])

        title = kwargs.pop('title', latex_prep(self.name))
        ylabels = kwargs.pop('ylabels', None)
        subtitle = kwargs.pop('subtitle', None)
        if not ylabels and hasattr(self, 'latex_name'):
            name = self.latex_name.strip('$')
            ylabels = ['$|{}|$'.format(name), '$\\angle {}$'.format(name)]
        if not subtitle and hasattr(self, 'latex_formula'):
            subtitle = self.latex_formula.strip('$')
            if hasattr(self, 'latex_name'):
                subtitle = self.latex_name.strip('$') + ' = ' + subtitle
            subtitle = '$' + subtitle + '$'

        frequency_domain_plot((f, H), title=title, subtitle=subtitle, ylabels=ylabels, **kwargs)

        fig = plt.gcf()
        ax = fig.axes[0]
        ax.axhline(1, color='black', lw=2)

    def bode_plot(self, **kwargs):
        '''produce a Bode plot.

        Keyword arguments:
        ------------------
        fmin,fmax,n,kind -- go to gen_samples
        title,fig,unwrap,ylabels -- go to bode_plot
        all other keywords are forwarded to matplotlib's axis.plot routine
        '''
        kwargs1 = {x: kwargs.pop(x) for x in ['fmin', 'fmax', 'n', 'kind'] if x in kwargs}
        self.gen_samples(**kwargs1)
        f = self.gened_samples.f
        s = self.gened_samples.s
        H = self
        if 'title' not in kwargs:
            kwargs['title'] = latex_prep(self.name)
            if hasattr(self, 'latex_name') and hasattr(self, 'latex_formula'):
                kwargs['title'] = '$' + self.latex_name.strip('$') + '=' + self.latex_formula.strip('$') + '$'
        bode_plot(f=f, H=H(s), **kwargs)
        ax1, ax2 = plt.gcf().axes
        gm = self.gain_margin()
        if gm is not np.nan:
            ax1.text(1., 1., f'gain margin = {gm:.2f}', ha='right', va='bottom', transform=ax1.transAxes)
        pm = self.phase_margin().to(u.deg)
        if pm is not np.nan:
            ax2.text(1., 1., f'phase margin = {pm:.2f}', ha='right', va='bottom', transform=ax2.transAxes)
        plt.suptitle(self.name + ': ' + self.name)

    def nyquist_plot(self, **kwargs):
        '''produce a Nyquist plot.

        Keyword arguments:
        ------------------
        fmin,fmax,n,kind -- go to gen_samples
        title,fig -- go to nyquist_plot
        all other keywords are forwarded to matplotlib's axis.plot routine
        '''
        kwargs1 = {x: kwargs.pop(x) for x in ['fmin', 'fmax', 'n', 'kind'] if x in kwargs}
        self.gen_samples(**kwargs1)
        f = self.gened_samples.f
        s = self.gened_samples.s
        H = self
        if 'title' not in kwargs: kwargs['title'] = latex_prep(self.name)
        nyquist_plot(f=f, H=H(s), **kwargs)

    def nichols_chart(self, **kwargs):
        '''produce a Nichols chart .

        Keyword arguments:
        ------------------
        fmin,fmax,n,kind -- go to gen_samples
        title,fig,unwrap -- go to nichols_chart
        all other keywords are forwarded to matplotlib's axis.plot routine
        '''
        kwargs1 = {x: kwargs.pop(x) for x in ['fmin', 'fmax', 'n', 'kind'] if x in kwargs}
        self.gen_samples(**kwargs1)
        f = self.gened_samples.f
        s = self.gened_samples.s
        H = self
        if 'title' not in kwargs: kwargs['title'] = latex_prep(self.name)
        nichols_chart(f=f, H=H(s), **kwargs)

    def gain_margin(self, **kwargs):
        '''Determine the gain margin if the TranferFunction
        is the loop transfer in closed loop.
        The gain margin is defined as the maximum factor by which an
        open loop transfer function can be multiplied and still
        remain stable in closed loop.
        1/(1+gH(s)) is stable for 0 > g > gain margin
        gain margin > 1 means 1/(1+H(s)) is stable
        gain margin < 1 means 1/(1+H(s)) is unstable
        '''
        # look for the places where H(s) crosses the negative real axis
        # by detecting a 2 pi jump in phase
        n = kwargs.pop('n', self.sampling_defaults['n'])
        fmax = kwargs.pop('fmax', self.sampling_defaults['fmax'])
        self.gen_samples(n=n, fmax=fmax)
        f = self.gened_samples.f
        s = self.gened_samples.s
        H = self
        phase = H(s).phase
        phase_jump = np.abs(np.diff(phase))
        ks = np.where(phase_jump > (np.pi * u.rad / 2))[0]
        if len(ks) == 0:
            return np.nan
        f0 = f[ks]
        i = 1j
        s = 2 * pi * i * f0
        gm = 1. / H(s).mag
        return min(gm)

    def phase_margin(self, **kwargs):
        '''Determine the phase margin if the TransferFunction
        is the loop transfer in closed loop.
        The phase margin is defined as the additional phase lag that
        H(s) could have and still remain stable in closed loop.
        1/(1+H(s)e^{-i phi}) is stable for 0 < phi < phase margin
        A positive phase margin means 1/(1+H(s)) is stable
        A negative phase margin means 1/(1+H(s)) is unstable
        '''
        n = kwargs.pop('n', self.sampling_defaults['n'])
        fmax = kwargs.pop('fmax', self.sampling_defaults['fmax'])
        self.gen_samples(n=n, fmax=fmax)
        f = self.gened_samples.f
        s = self.gened_samples.s
        H = self
        f0 = zero_crossing(f, H(s).mag, y0=1)
        if len(f0) == 0:
            return np.nan
        phase = interp1d(f, np.unwrap(H(s).phase))
        pm = (phase(f0) + np.pi) * u.rad
        # pm = H(s).phase + np.pi*u.rad
        return min(pm)

    @property
    def is_stable(self):
        '''Determine whether, the TransferFunction is stable
        under closed loop operation. If the object has
        'loop_tranfer' as an attribute, the loop_transfer
        tested. The test is
        whether the magnitude of LT(s) < 1 at the freqency where
        its phase is -pi, i.e. whether the gain margin is > 1.
        '''
        if hasattr(self, 'loop_transfer'):
            return self.loop_transfer.is_stable
        gm = self.gain_margin()
        return gm > 1.

    @property
    def crossover_frequency(self, **kwargs):
        n = kwargs.pop('n', self.sampling_defaults['n'])
        kmax = kwargs.pop('kmax', self.sampling_defaults['fmax'])
        self.gen_samples(n=n, kmax=kmax)
        f = self.gened_samples.f
        s = self.gened_samples.s
        H = self
        return zero_crossing(f, H(s).mag, y0=1)

    def gen_samples(self, fmin=0 * u.Hz, fmax=None, kind='linear', n=None, **kwargs):
        '''generate a list of frequency samples and
        corresponding samples along the s +imaginary axis
        from frequency 0 to fmax inclusive.

        Parameters
        ----------
        kind : str
            frequency sampling, either 'linear' or 'log'
        n : int
            the number of frequency samples
        fmax : Quantity
            the maximum frequency. Must have frequency-like units

        '''
        if fmax is None: fmax = self.sampling_defaults['fmax']
        if n is None: n = self.sampling_defaults['n']
        fmin = fmin.to(u.Hz)
        fmax = fmax.to(u.Hz)
        i = 1j
        if kind == 'linear':
            df = (fmax - fmin) / (n - 1)
            f = np.linspace(fmin, fmax, n)
        elif kind == 'log':
            df = None
            f = np.logspace(np.log10(fmax / (n * u.Hz)), np.log10(fmax / u.Hz), n) * u.Hz
        s = 2 * pi * i * f
        H = self(s)
        samples = DotDict({
            'f': f,
            's': s,
            'H': H,
            'df': df,
            'n': n,
            'fmax': fmax,
            'kind': kind,
            'creation': 'gen',
        })
        self.gened_samples = samples
        return f, s, H

def get_lambda_source(lambda_func, position=0):
    'https://stackoverflow.com/questions/59498679/how-can-i-get-exactly-the-code-of-a-lambda-function-in-python'
    code_string = inspect.getsource(lambda_func).lstrip()
    class LambdaGetter(ast.NodeTransformer):
        def __init__(self):
            super().__init__()
            self.lambda_sources = []

        def visit_Lambda(self, node):
            self.lambda_sources.append(astunparse.unparse(node).strip()[1:-1])

        def get(self, code_string):
            tree = ast.parse(code_string)
            self.visit(tree)
            return self.lambda_sources
    return LambdaGetter().get(code_string)[position]