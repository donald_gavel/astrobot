# -*- mode: python; coding: utf-8 -*-
'''
r0 meter
Use the telemetry data file to determine r0 and wind velocity

to do:
- reconstruct phases; deal with finding the reconstrucion matrix on real
- get data from Lick archives; with alternative from rtc macine
- fit power law to reconstructed phase spectra
- check that it is open loop data
- fit fried-tyler model to slope spectra
- estimate r0 from slope rms - done, but needs full-aperture tilt-subtracted case
- estimate wind from power spectra, given r0
- use a wfs config file instead of the sauce2.wfs object
- use astropy tables instead of pandas


reference:
[1] Gavel, D. T., Kupke, R., Rudy, A. R., Srinath, S., & Dillon, D. (2016). Lick
    Observatory’s Shane telescope adaptive optics system (ShaneAO): research
    directions and progress. Proceedings of SPIE, Astronomical Telescopes and
    Instrumentation, 9909.
    http://proceedings.spiedigitallibrary.org/proceeding.aspx?doi=10.1117/12.2233202
[2] ShaneAO data telemetry documentation
    http://lao.ucolick.org/ShaneAO/software-docs/telemetry.html

'''
import os
import pyfits
from astropy.time import Time, TimeDelta
from astropy import units
from sauce2 import wfs
from astrobot import graphics
import numpy as np
import pandas as pd
from astrobot.oprint import pprint

class Dataset(object):
    ref_url = [
        'http://proceedings.spiedigitallibrary.org/proceeding.aspx?doi=10.1117/12.2233202',
        'http://lao.ucolick.org/ShaneAO/software-docs/telemetry.html'
    ]
    
    def __init__(self,date='example',filenum = 0):
        src = 'real.ucolick.org:/data/telemetry/'
        if date == 'example':
            date = '2018-04-03'
        srcfilename = 'Data_%04d.fits'%filenum
        if date is 'today':
            hour = TimeDelta(3600.,format='sec')
            date = Time(Time.now()-6*hour,out_subfmt='date').iso
        
        src = os.path.join(src,date,srcfilename)
        
        dst = os.path.expanduser('~/data/telemetry')
        dst = os.path.join(dst,date)
        
        commands = [
            'mkdir %s'%dst,
            'scp %s %s/'%(src,dst)
        ]
        self.get_data_commands = commands
        self.source_file = src
        self.source_file_doc = 'original telemetry data file'
        self.datafile = os.path.join(dst,srcfilename)
        self.datafile_doc = 'local copy of telemetry data file'
        telemetry_table = {
            'collumns': ['16xLGS','8xLGS','16x','8x'],
            'index': ['wfs centroids x',
                    'wfs centroids y',
                    'tip/tilt sensor',
                    'tweeter DM',
                    'woofer DM',
                    'filter',
                    'uplink tt'],
            'data': [
                [144,40,144,40],
                [144,40,144,40],
                [2,2,0,0],
                [1024,1024,1024,1024],
                [52,52,52,52],
                [14,14,14,14],
                [2,2,0,0]
                ]
        }
        
        dftt = pd.DataFrame(telemetry_table['data'],
                            columns = telemetry_table['collumns'],
                            index = telemetry_table['index'])
        self.dftt = dftt
        self.dftt_doc = 'telemetry data contents table'
        self.date = date
        self.filename = srcfilename
        
        self.figs = {}

    def pprint(self):
        pprint(self)

    def read(self):
        '''open the telemetry data file and read in the header and data
        '''
        datafile = self.datafile
        hdulist = pyfits.open(datafile)
        hdr = hdulist[0].header
        data = hdulist[0].data
        hdulist.close()
        self.hdr = hdr
        self.data = data[1:,:]
        self._slope_indices()
        x_inds,y_inds = self.sx_inds,self.sy_inds
        s_x = np.std(self.data[:,x_inds])
        s_y = np.std(self.data[:,y_inds])
        self.std_slopes = np.array([s_x,s_y])
        self.std_slopes_units = 'pixels'
        self.std_slopes_doc = 'standard deviations of one-axis slopes in 100% illuminated subapertures'
        
    def _slope_indices(self):
        '''
        determine the indices for the wfs slopes x and y
        '''
        dftt = self.dftt
        w = wfs.Wfs()
        cols = w.subap_info['columns']
        n = cols.index('subap_number')
        f = cols.index('illum_frac')
        illums = [(x[n],x[f]) for x in w.subap_info['data']]
        full_illums = [x[n] for x in w.subap_info['data'] if x[f] > 0.999]

        offsets = []
        ptr = 0
        mode = self.hdr['MODE']
        assert mode in dftt.columns
        for n in dftt[mode]:
            offsets.append(ptr)
            ptr += n
        
        dfl = pd.DataFrame(dftt[mode])
        dfl['offset'] = offsets
        dfl.columns = ['count','offset']
        
        self.sx_inds,self.sy_inds = [[x + dfl.offset['wfs centroids %s'%axis] for x in full_illums] for axis in ['x','y']]
        self.sx_inds_all,self.sy_inds_all =[[x + dfl.offset['wfs centroids %s'%axis] for x,_ in illums] for axis in ['x','y']]

        self.d_subap = w.d* units.Unit(w.d_units.strip('s'))
        self.d_subap_doc = 'subap diameter at telescope pupil'
        
        self.wavelength = 0.7* units.Unit('micron')
        self.wavelength_doc = 'center wavelength of wfs measurement'

        self.pixel_scale = w.pixel_scale* units.Unit(w.pixel_scale_units.split()[0])
        
    def calc_r0(self):
        '''calculate r0 from the formula
        $ \sigma_\theta^2 = 0.170 (d / r_0 )^{5/3} (\lambda / d )^2 $ (ref1, eqn 11)
        '''
        if not hasattr(self,'data'):
            self.read()
        d = self.d_subap
        lam = self.wavelength
        lambda_over_d = (lam/d).to('',equivalencies=units.dimensionless_angles())
        sigma_theta = np.average(self.std_slopes)* self.pixel_scale
        
        r0 = d* ( np.sqrt(0.170)*lambda_over_d/sigma_theta)**(6./5.)
        r0 = r0.to('m',equivalencies=units.dimensionless_angles()).to('cm')
        self.r0 = r0
        self.r0_doc = 'r0 calculated from slope variance in 100% illuminated subapertures'

    def calc_wind(self):
        '''calculate the wind by fitting phase power spectra slope to model,
        and understanding r0, disambiguate the wind velocity
        '''
        self.wind = 'to do: \n'+self.calc_wind.__doc__
        bling = '*'*60
        print bling
        print self.wind
        print bling
        return
        
    def __getattr__(self,attr):
        if attr == 'r0':
            if not attr in self.__dict__:
                self.calc_r0()
            return self.r0
        elif attr == 'wind':
            if not attr in self.__dict__:
                self.calc_wind()
            return self.wind
        else:
            raise AttributeError, "'%s' object has no attribute '%s'"%(self.__class__.__name__,attr)

    def spectra(self):
        '''Fourier transform the data that has slopes x and y
        '''
        if not hasattr(self,'data'):
            self.read()
        if not hasattr(self,'sx_inds_all'):
            self._slope_indices()
        data = self.data
        x_inds,y_inds = self.sx_inds, self.sy_inds
        x_inds_all, y_inds_all = self.sx_inds_all, self.sy_inds_all
        hdr = self.hdr
        
        d = data[:,x_inds+y_inds]
        d_all = data[:,x_inds_all+y_inds_all]
        # average the power spectra
        
        fs = float(hdr['RATE'])
        n = hdr['NAXIS2']-1
        dt = 1./fs
        T = n*dt
        t = np.arange(0,T,dt)
        df = 1./T
        f = np.arange(df,(n/2)*df,df)
        
        fd = np.fft.fft(d,axis=0)
        fda = np.average(np.abs(fd)**2,axis=1)
        fda = fda[1:n/2]
        
        fd_all = np.fft.fft(d_all,axis=0)
        fda_all = np.average(np.abs(fd_all)**2,axis=1)
        fda_all = fda_all[1:n/2]
        
        self.t = t
        self.t_units = 's'
        self.dt = dt
        self.dt_units = 's'
        self.f = f
        self.f_units = 'Hz'
        self.df = df
        self.df_units = 'Hz'
        self.n = n
        self.n_doc = 'number of valid time points in telemetry data'
        
        self.avg_spectrum_slopes_illum = fda
        self.avg_spectrum_slopes_all = fda_all
        
    def graph_spectra(self):
        ''' graph the power spectra
        '''
        date,filename = self.date,self.filename
        if not hasattr(self,'avg_spectrum_slopes_all'):
            self.spectra()
        
        f = self.f
        fda = self.avg_spectrum_slopes_illum
        fda_all = self.avg_spectrum_slopes_all

        plt = graphics.epl()
        plt.ion()
        
        if 'spectra' in self.figs:
            fignum = self.figs['spectra']
            plt.figure(fignum)
        else:
            fig = plt.figure()
            self.figs['spectra'] = fig.number
            
        plt.clf()
        
        plt.plot(f,fda,label='100% illum')
        plt.xscale('log')
        plt.yscale('log')
        plt.grid('on')
        
        plt.plot(f,fda_all,label='all')
        plt.legend()
        
        plt.title('power spectra of average tilts\n'+'%s %s'%(date,filename))
        plt.xlabel('frequency, Hz')
        plt.ylabel('centroid pixels $^2$ / Hz')

    def graph_tilt(self,nr=0,axis='x',npts=0,oplot=False):
        '''rms the tip/tilts
        '''
        if not hasattr(self,'data'):
            self.read()
        if npts == 0:
            npts = self.data.shape[0]

        plt = graphics.epl()
        plt.ion()
        
        if 'yarnball' in self.figs:
            fignum = self.figs['yarnball']
            plt.figure(fignum)
        else:
            fig = plt.figure()
            self.figs['yarnball'] = fig.number

        data = self.data
        x_inds,y_inds = self.sx_inds_all, self.sy_inds_all
        std_x,std_y = self.std_slopes
        ths = np.linspace(0,2*np.pi,50)

        if not oplot:
            plt.clf()
            plt.xlabel('slope x')
            plt.ylabel('slope y')
            plt.xlim(-1,1)
            plt.ylim(-1,1)
        if oplot:
            #plt.cla()
            ax = plt.gca()
            del ax.lines[:]
        
        sx,sy = [data[:,inds[nr]] for inds in [x_inds,y_inds]]
        sx,sy = [x - np.average(x) for x in [sx,sy]]
        sdx,sdy = [np.std(x) for x in [sx,sy]]
        sx,sy = [x[:npts] for x in [sx,sy]]
        plt.plot(sx,sy)
        r = [sdx*np.cos(th) for th in ths],[sdy*np.sin(th) for th in ths]
        plt.plot(r[0],r[1])
        r = [std_x*np.cos(th) for th in ths],[std_y*np.sin(th) for th in ths]
        plt.plot(r[0],r[1])
        plt.title('subap tilts yarn ball, subap %d'%nr)
    
    def movie_tilt(self,start=0,end=0,npts=0):
        '''movie the tilt yarnballs
        '''
        plt = graphics.epl()
        if not hasattr(self,'data'):
            self.read()
        if end == 0:
            end = len(self.sx_inds_all)
        oplot = False
        for k in range(start,end):
            self.graph_tilt(nr=k,npts=npts,oplot=oplot)
            oplot = True
            plt.pause(.01)

# ----------------- tests --------------

def test():
    ds = Dataset()
    ds.graph_spectra()
    ds.graph_tilt()
