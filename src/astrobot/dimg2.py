# dimg.py - data graphics displayer
#   "stores" the data as a pointer to the original data so it doesn't have to copy
#   Data is cast consistently to an astropy Quantity so it has units, title, and other attributes
#   Keeps a structure containing graphic options like color, scale, etc.

from functools import wraps # This convenience func preserves name and docstring

import numpy as np
from astropy.units import Quantity
import astropy.units as u
import matplotlib.pyplot as plt
from matplotlib import rc

plt.ion()
rc('text',usetex=True)
rc('font',family='serif')

def test():
    n,m = 100,100
    x = np.arange(n)-n/2
    x,y = np.meshgrid(x,x)
    r2 = x**2+y**2
    sd = n/5.
    g = np.exp(-r2/sd**2)
    g = Quantity(g).set_attr(
        name=r'Gaussian = $e^{-(x/\sigma)^2}$',
        labels=['x axis','y axis'],
        dx=0.02*u.urad,
        dy=0.02*u.urad,
        x0= -1*u.urad,
        y0= -1*u.urad,
    )
    p = Dimg2d(g,title=g.name).show()
    globals().update(locals())

def add_method(cls,wfunc=None):
    # https://medium.com/@mgarod/dynamically-add-a-method-to-a-class-in-python-c49204b85bd6
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            #print('<wrapper> {}'.format(func.__name__))
            return func(*args, **kwargs)
        if wfunc is not None:
            doc = '(wraps {}.{})'.format(wfunc.__module__,wfunc.__name__)
            doc += '\n' + wfunc.__doc__
            wrapper.__doc__ = doc
        setattr(cls,func.__name__,wrapper)
        return wrapper
    return decorator

@add_method(u.Quantity)
def set_attr(self, **kwargs):
    '''Set or change any attributes on the Quantity.
    Attributes are given by keyword arguments in the call.

    Example
    -------
    >>> q.set_attributes(name='foo',wavelength=2*u.micron,bling=False)

    Returns
    -------
    Quantity
        The initial object, but with attributes set.
        Note: Side-effect is to -change- the attributes of self (not create a copy).
    '''
    self.__dict__.update(kwargs)
    return self

class Dimg():
    def __init__(self,data,**kwargs):
        """Creat an instance of Dimg containing the data
        and parameters for the plot"""
        self.data = data
        for key, val in kwargs.items():
            setattr(self,key,val)

class Dimg1d(Dimg):
    pass

class Dimg2d(Dimg):
    def __init__(self,data: Quantity,**kwargs):
        defaults = {'fig':None,
                    'figsize':None,
                    'title':'z',
                    'labels':['x','y'],
                    'dx':Quantity(1),
                    'dy':Quantity(1),
                    }
        kwargs = defaults | data.__dict__ | kwargs
        n,m = data.shape
        dx,dy = [kwargs.get(k) for k in ['dx','dy']]
        x0 = kwargs.get('x0',-dx*n/2)
        y0 = kwargs.get('y0',-dy*n/2)
        kwargs.update({'x0':x0,'y0':y0})

        extent = [x0.value, x0.value+n*dx.value, y0.value, y0.value+m*dy.value]

        im_kwargs = {
            'cmap': 'gray',
            'norm': 'linear',
            'aspect':'equal',
            'interpolation': 'nearest',
            'origin': 'lower',
            'extent': extent,
        }
        im_kwargs.update({key:kwargs[key] for key in im_kwargs if key in kwargs})
        self.im_kwargs = im_kwargs
        super().__init__(data,**kwargs)

    def show(self):
        self.fig = plt.figure(self.fig,figsize=self.figsize)
        ax = plt.gca()
        im = plt.imshow(self.data,**self.im_kwargs)
        plt.title(self.title)
        plt.xlabel(self.labels[0])
        plt.ylabel(self.labels[1])
        n,m = self.data.shape
        extent = self.im_kwargs['extent']
        origin = self.im_kwargs['origin']

        def format_coord(xx, yy):
            row,col = row_col(xx,yy)
            if col>=0 and col<m and row>=0 and row<n:
                return 'x=%1.4g y=%1.4g (%d,%d)'%(xx,yy, row,col)
            else:
                return ''

        def format_cursor_data(zz):
            if hasattr(zz,'unit'):
                unit = zz.unit
                zz = zz.value
            else:
                unit = None
            if np.iscomplexobj(zz):
                sign = '+'
                if zz.imag<0: sign = '-'
                r = 'z = %1.4g %s %1.4gj'%(zz.real,sign,abs(zz.imag))
            else:
                r = 'z = %1.4g'%zz
            if unit:
                r += f' {unit}'
            if len(r) < 24:
                r += ' '*(24-len(r)) + '.'
            return r

        def get_cursor_data(event):
            xx,yy = event.xdata, event.ydata
            row,col = row_col(xx,yy)
            zz = self.data[row,col] # refer to the original (unscaled) array
            return zz

        def row_col(xx,yy):
            """Translate a mouse position to the row and column
            of the data array
            """
            col = int(xx+0.5)
            row = int(yy+0.5)
            if extent is not None:
                n,m = self.data.shape
                l,r,b,t = extent
                row = int(((yy-b)/(t-b))*n)
                col = int(((xx-l)/(r-l))*m)
                if (origin != 'lower'):
                    row = n-row-1
            return row,col

        ax.format_coord = format_coord
        im.format_cursor_data = format_cursor_data
        im.get_cursor_data = get_cursor_data

        plt.show()
        return self

class Dimg3d(Dimg):
    def show(self):
        pass

