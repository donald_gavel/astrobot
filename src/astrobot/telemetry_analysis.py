'''Analyze and plot AO telemetry data.

Unix command line
-----------------
    python -i -m astrobot.telemetry_analysis -- [--help] [-s <starname>]
        [-d <date>] [-n <dsn>] [--plot]

Options
-------
    -d, --date <date>
        The observation date. 'today' refers to the observation night,
        which in the wee hours of the morning is yesterday. Default is 'today'
        
    -h, --help
        Print this help and exit
        
    -n, --dsn <dsn>
        dsn refers to Data Set Number, which can be a single number or
        comma delimited set of numbers, as in '2,3,4'. An enclosing square
        bracket, e.g. '[2,3,4]' also works. The data set number refers to
        the particular telemetry file, such as '2' refers to Data_0002.fits.
        'all' is an option that refers to all the data sets of a particular
        observing date. 'all' is the default if only the date is provided.
    
    -p, --plot
        After gathering and analyzing the date, produce a plot. The
        program suspends until the plot is closed.
    
    -s, --star <starname>
        The name of the star. If provided along with data set information this
        will allow analysis of the optical throughput to the wavefront sensor.
        The star is looked up using the Simbad data service. If it is the only
        option provided, the star information will be displayed. 

Install
-------
    git clone git@bitbucket.org:donald_gavel/astrobot.git
    pip install -e .

Example
-------
    python -i -m astrobot.telemetry_analysis -- -s 'HD 121968' '2021-07-16' '5,6' --plot

Use within Python
-----------------
    One can start up the python interpreter and use the code from there.
    The following unix command will drop the user into the
    telemetry_analysis module namespace:
    
        python -i -m astrobot.telemetry_analysis

    and from there:
    
        ds = Set('today',[4,5])
        ds.set_star('HD 121968')
        ds.fit()
        ds.plot()

    or 'chain' the commands:
    
        ds = Set('today',[4,5]).set_star('HD 121968').fit().plot()
        
Tests
-----
    The following command runs the self-tests:
    
        tests()

'''
import sys
if any([x == s for x in ['-h','--help'] for s in sys.argv]):
    import pydoc
    pydoc.pager(__doc__)
    sys.exit()

import os
import sys
import datetime
import dateutil.parser as dparser
import re
import glob
from collections import deque
import types
from copy import copy
import pydoc
import atexit
from io import StringIO
import subprocess

import pandas as pd
from pandas import Timestamp
import numpy as np
from numpy import pi
import scipy
import astropy.units as u
from astropy.io import fits
from scipy.special import gamma
from scipy.interpolate import UnivariateSpline
from scipy.interpolate import interp1d
from scipy import optimize

from astrobot import img
from astrobot import oprint
from astrobot.oprint import pprint as pp
from astrobot import q_helper
from astrobot.q_helper import get_fig
from astrobot import finder
from astrobot.finder import home, workingDir, realAtUcolickDataDir, dataVols
from astrobot.dotdict import DotDict
from astrobot import fmatrix
from astrobot import zernike
from astrobot import centroider_lookup_table_data as lut
from astrobot.tcolor import tcolor, gprint, wprint, eprint, cstr
from astrobot import photometry as photom
from astrobot.astrodate import p_date, utc_from_local

import matplotlib.pylab as plt

utf_enabled = 'utf' in os.environ.get('LANG','none').lower()

plt.ion()

# load in information from the past observing run(s), if any
info = dict(
    filename = 'ObsDB_2021A.ecsv',
    comment = 'Results of telemetry analysis of data from ShaneAO engineering runs in 2021A',
    comment2 = '(commissioning of the wavefront sensor upgrade)',
    rows = 'one line per telemetry data file',
    )
dbObs = None

try:
    filename = info['filename']
    photom.init(name=filename,from_saved=True)
    dbObs = photom.db
    finder.set_metadata(dbObs,info)
    print(f'{tcolor.green}Read observation data from {filename}{tcolor.end}')
except:
    print(f'{tcolor.warning}WARNING can not find observation data file {filename}{tcolor.end}')

# search for the parameter files directory, depending on machine
parameterFilesDirs = [
    os.path.join(home,'parameterFiles'),
    os.path.join(workingDir,'parameterFiles'),
    os.path.join(realAtUcolickDataDir,'parameterFiles')
]

paramDir = None
dataVol = None
default_date = '2015-09-03'
default_dsn = [411,412]

def findParameters():
    '''find the directory that has system parameters
    '''
    for directory in parameterFilesDirs:
        if os.path.isdir(directory):
            paramDir = directory
            print(f'{tcolor.green}<telemetry_analysis.findParameters> found directory for parameters: {tcolor.end}',end='')
            print(paramDir)
            return paramDir
    paramDir = None
    print(f'{tcolor.warning}<telemetry_analysis.findParameters> WARNING did not find the parameterFiles directory{tcolor.end}')
    pp(parameterFilesDirs)
    return paramDir
paramDir = findParameters()

def dbase(top):
    '''Create a database (pandas dataFrame) of all the telemetry files
    in a directory and subdirectories
    
    Parameters:
    -----------
    top : str
        The name of the top-level directory. May have '.' and '~'
    '''
    topdir = os.path.abspath(os.path.expanduser(top))
    filesdb = finder.dbFiles(topdir)
    db = finder.dbHdrs('telemetry',filesdb)
    return db

db = None
db_cache = {}

# search for the data directory, depending on machine
# data_paths = {
#               'Lick':'/net/real/local/data',
#               #'LickTest':'/Users/donaldgavel/ShaneAO/net_real/local/data',
#               'home':'/Users/donaldgavel/ShaneAO/data', #/2021-08-25/AOtelemetry/Data_xxxx.fits
#              }
#
# data_paths = {} # FLAG
#
# for key,path in data_paths.items():
#     if os.path.isdir(path):
#         place = key
#         print(f'{tcolor.green}Found data path at {place}: {tcolor.end}{data_paths[place]}')
#         break
# else:
#     place = 'home'
#     print(cstr('warning','No path to observatory data. Using the package sample data directory'))
#     #raise Exception(cstr('fail',f'neither {dgDataDir} nor {realAtUcolickDataDir} is a valid path to data'))

# Turbulence zernike coefficients strengths (x (D/r0)**5/3) from R. Noll, JOSA, 1976
noll_coefs_base = [0.,
    1.0299, 0.582 , 0.134 , 0.111 , 0.088 , 0.0648, 0.0587, 0.0525, 0.0463, 0.0401, 0.0377,
    0.0352, 0.0328, 0.0304, 0.0279, 0.0267, 0.0255, 0.0243, 0.0232, 0.0220, 0.0208
]

# constants defining the Kolmogorov spectrum
c0 = 8*np.sqrt(2)*((3/5)*gamma(6/5))**(5/6)
c3 = c0*gamma(8/3)*(2*pi)**(-8/3)

'''Example usage scripts
from astrobot import telemetry_analysis as ta
date = 'Sept 3, 2015'
dsn = [411,412]
ds = ta.Set(date,dsn)
m = ta.Model().prep(ds)
m.fit()
m.results
m.plot()

import pandas as pd
from astrobot import finder
date = '2015-09-03'
date = ta.p_date('3 sept 2015') # p_date = "(lazy) parse date"
date = ta.p_date('3/9/15')
date = ta.p_date('2015 3/9')
date = ta.p_date('sept 3 2015')
dsn=[411,412] # in no particular order, open, closed it will figure that out.
              # At least one should be open loop however, to do seeing estimation
volume,path_to_file = finder.findVol('telemetry',date,dsn[0])
fileDir,fileName = os.path.split(path_to_file)
# dbase...
top = fileDir
topdir = os.path.abspath(os.path.expanduser(top))
filesdb = finder.dbFiles(topdir)
db = finder.dbHdrs('telemetry',filesdb)

# in the real-time monitoring scenario:
ta.Model().prep(ta.Set('',finder.monitor(vol))).fit().plot()

# now, a little simpler:
#  (the '' for the first argument means date = today's observing date)
ds = ta.Set('',finder.monitor(vol)).model().fit().plot()

#-------------------------------------
# NEW IMPROVED !!!
# Much simpler and intuitive interface
#-------------------------------------
# read in many datafiles, maybe even the whole directory
# do parameter fits to all the open loop data in one command
# plot data one per plot or all in one plot
# look at tables of data sets (dbData) and models (dbModels)
#--------------------------------------
from astrobot import telemetry_analysis as ta
from astrobot import photometry as photom
from astrobot import finder

date = '2021-07-16'
dsn = [3,4,5,6,7,8,9]  # or dsn='all'
ds = ta.Set(date,dsn).fit()
ds.plot()
[data.plot() for data in ds.datasets if data.loop == 'open']
ds.dbData
ds.dbModels
opens = ds.db[ds.db.loop == 'open']
ds.plot(opens)


'''
    
class Model(object):
    '''A model for the temporal power spectrum of open-loop phase, with
    mechanisms for optimizing parameters to fit telemetry data
    
    Open-loop seeing analysis:
    --------------------------
    The open-loop seeing temporal power spectrum is parameterized by
    
        r0 - Fried seeing parameter (Quantity [length])
        v - wind velocity (Quantity [length/time])
        f0 - break frequency (Quantity [1/time])
        Sn - measurement noise floor (Quantity [length**2/frequency] )
        p - mid frequency power law exponent (float) (VonKarman spectra have p=8/3)
        q - low frequency power law exponent (float) (VonKarman spectra have q=0)
    
    The power spectrum represents mean=square optical-path-difference of
    atmospheric phase (plus the measurement noise floor) in units of [length**2]
    per unit frequency. The formula for the model is given in the attribute
    `formula` or `latex_formula`.
    
    To fit the model parameters to data, a ShaneAO telemetry dataset needs to be
    read in and prepared. The Set object reconstructs the phases from recorded
    slopes data using a Shack-Hartmann reconstructor (solving slope =
    grad(phase)) (not the AO control law, which only reconstructs a subset of
    modes).
    
    r0 is estimated directly from the rms tilts using a well known formula. This
    has proven the most reliable way of determing r0 [reference 1], and breaks
    the (v/r0) ambiguity in the temporal power spectrum. r0 must be solved for
    iteratively since rms tilt implicitly dependent on r0: the conversion from
    centroider readings to slopes depends on spot size which depends on r0.
    
        < theta(spot_size(r0))^2 > = 0.170 * (d/r0)^(5/3) ( lambda/d )^2     --- solve for r0
    
    Centroider output is a nonlinear function of slope depending on the
    centroider used (center-of-gravity, quad-cell, etc.) and spot size (r0). See
    Figure 2 in [reference 1]. The code uses pre-calculated nonlinearity
    compensation curves for a set of r0's, and uses spline interpolation to
    cover a continuous r0 range.
    
    The model fitting algorithm takes the sample power spectrum and fits the
    model equation at 100 logarithmically spaced frequencies, with residual
    weighting slighly favoring higher frequencies. Five parameters: v,f0,Sn,p,
    and q are solved for. The valid ranges and initial values of these
    parameters can be examined in attribute `df`
    
        >>> Model().df
            variable               value                          bounds                                  descriptor     units
          0        v           5.0 m / s         [0.0 m / s, 20.0 m / s]                                        wind     m / s
          1       f0              0.4 Hz               [0.5 Hz, 50.0 Hz]  break frequency low to mid freq power laws        Hz
          2       Sn        1.0 nm2 / Hz  [0.1 nm2 / Hz, 100.0 nm2 / Hz]                     measurement noise floor  nm2 / Hz
          3        p  2.6666666666666665                      [2.5, 2.8]                     mid frequency power law          
          4        q                 0.0                      [0.0, 2.0]                     low frequency power law          

    After fitting, the parameters can be examined, compared to initial guesses,
    and either accepted or adjusted (the later can be useful for 'what-if'
    analysis, such as: What if r0 were a little bigger would the spectra look
    like?). Data can be graphed and overlayed with the model spectrum.
    
    Here is a typical pattern of calls:
    
        ds = Set(date='2015-04-28',dsn=[6,7]).read()  # read in data sets
        m = Model().prep(ds).fit().plot()  # fit parameters to data and plot
        m.results # examine results
        m.set(r0=10*u.cm, v=4*u.m/u.s).plot()  # modify the results and plot
        m.undo() # go back to the fit parameters
        m.undo() # go back to the original parameters
    
    Fit parameters (most especially r0,v, and p) can inform the AO operator of
    seeing conditions and whether good AO closed loop performance can be
    expected under these conditions.
    
    Closed-loop performance prediction:
    -----------------------------------
    Predicting actual closed-loop performance requires a closed loop model with
    additional parameters such as gain, bleed, woofer-tweeter crossover and so
    on. This model is rather complicated and is presently under development. For
    now, we can generate a quasi error-budget and get an idea of what to expect.
    The error budget consistes of basically four dominant terms:
    
        fitting error - due to the finite spatial sampling of the deformable mirror
        bandwidth error - due to the power spectrum of turbulence and the bandwidth rejection of the controller
        noise error - error added due to measurment noise
        calibration error - baseline rms wavefront error, e.g. non-common-path error, after internal calibration
    
    Fitting error is simply determined from Hartman subapersize d and the estimated r0:
    
        sigma_fit^2 = 0.3 (d/r0)^(5/3)
        
    To compute bandwidth error we'll assume a generic (quasi emperical model for
    now until I can get a good model based on control parameters) controller
    frequency rejection curve applied to the open loop power spectrum model,
    without the noise term, integrated over frequency.
    
    Noise error is the integral of Sn(f), but this depends on the guide star
    brightness. The fit parameter Sn is relevant to the open-loop telemetry's
    guide star which may be different magnitude and color than the one to be
    used for science observing. So the user must enter in magnitude and color of
    both stars (one used for the telemetry, and the one to be used in science
    observing) to compute this term.
    
    Calibration error can be entered by the user, either in the form of Strehl
    at a certain wavelength or rms wavefront error in units of length.
    
    Reference:
    ----------
    [1] Gavel, D. T., Kupke, R., Rudy, A. R., Srinath, S., & Dillon, D. (2016). Lick Observatory’s
        Shane telescope adaptive optics system (ShaneAO): research directions and progress.
        Proceedings of SPIE, Astronomical Telescopes and Instrumentation, 9909. Retrieved
        from http://proceedings.spiedigitallibrary.org/proceeding.aspx?doi=10.1117/12.2233202
    '''
    def __init__(self,**kwargs):
        
        #  default parameters
        self.lam0 = kwargs.get('lam0',550.*u.nm)
        self.r0 = kwargs.get('r0',10*u.cm)
        xargs = ['f','r0','lam0','target','weights']
        
        c = ['variable','value','bounds','descriptor']
        df = [
            ['v' , 5*u.m/u.s, [0, 20]*u.m/u.s, 'wind'  ],
            ['f0', 0.4*u.Hz,  [.5,50]*u.Hz,    'break frequency low to mid freq power laws'],
            ['Sn', 1*u.nm**2/u.Hz, [.1,10000]*u.nm**2/u.Hz, 'measurement noise floor'],
            ['p',  8/3,       [2.5,2.8],       'mid frequency power law'],
            ['q',  0,         [0,2],           'low frequency power law'],
        ]
        self.df_table = df
        df = pd.DataFrame(data = df, columns = c)
        df['value'] = df.value.apply(u.Quantity)
        df['bounds'] = df.bounds.apply(u.Quantity)
        df.set_index('variable',inplace=True)
        self.df = df
        
        self.formula = '( c3*(v/r0)**(p-1)*(f**2+f0**(2*(p-q)/p)*f**(2*q/p))**(-p/2)*(lam0/(2*pi))**2 + Sn ).to(u.nm**2/u.Hz)'
        
        def cost_func(x):
            units = [val.unit for val in self.df.value]
            v,f0,Sn,p,q = [u.Quantity(val,unit) for val,unit in zip(x,units)]
            f,r0,lam0,target,weights = [getattr(self,key) for key in self._xargs]
            try:
                Sa = c3*(v/r0)**(p-1)*(f**2+f0**(2*(p-q)/p)*f**(2*q/p))**(-p/2)*(lam0/(2*pi))**2
                Sa = u.Quantity(Sa.decompose().value,'m2 s')
                S = ( Sa + Sn ).to(u.nm**2/u.Hz)
            except:
                print(f'{tcolor.fail}====== Error in cost_func ========{tcolor.end}')
                print('x = ',x)
                print('args',v,f0,Sn,p,q)
                print('xargs',f.unit,r0,lam0)
                raise Exception
                
            value = np.log10(S.value)
            cost = (( (value - target)*weights )**2).mean()
            return cost
        
        self.cost_func = cost_func
        
        #args = dict(zip(df.variable,df.value))
        args = dict(zip(df.index,df.value))
        xargs = dict(zip(xargs,[getattr(self,k,None) for k in xargs]))
        self.__dict__.update({**args,**xargs,**kwargs})
        self._args = list(args.keys())
        self._xargs = list(xargs.keys())
        self._prevx = deque() # stores the 'undo' stack
            
    def  __call__(self,f=None,**kwargs):
        if f is None:
            d = {**self.args,**self.xargs,**kwargs}
            m = Model(**d)
            if hasattr(self,'data'): m.prep(self.data)
            return m
        elif len(kwargs) > 0:
            d = {**self.args,**self.xargs,**kwargs}
            m = Model(**d)
            if hasattr(self,'data'): m.prep(self.data)
            return m(f)
        #v,f0,Sn,p,q = [getattr(self,key) for key in self.df.variable]
        v,f0,Sn,p,q = [getattr(self,key) for key in self.df.index]
        fsamples,r0,lam0,target,weights = [getattr(self,key) for key in self.xargs]
        Sa = c3*(v/r0)**(p-1)*(f**2+f0**(2*(p-q)/p)*f**(2*q/p))**(-p/2)*(lam0/(2*pi))**2
        Sa = u.Quantity(Sa.decompose().value,'m2 s')
        S = ( Sa + Sn ).to(u.nm**2/u.Hz)
        #S = ( c3*(v/r0)**(p-1)*(f**2+f0**(2*(p-q)/p)*f**(2*q/p))**(-p/2)*(lam0/(2*pi))**2 + Sn ).to(u.nm**2/u.Hz)
        return S
        
    def set_bounds(self,bounds):
        '''set new bounds
        
        Arguments:
        ----------
        bounds: dict
            dictionary with entries { variable_name: [lower,upper]*unit }
        '''
        df = self.df
        for key,val in bounds.items():
            #ind = df[df.variable == key].index[0]
            #df.at[ind,'bounds'] = u.Quantity(val)
            df.at[key,'bounds'] = u.Quantity(val)
        return self
        
    def fit(self):
        '''fit model parameters to data
        '''
        if self.target is None:
            raise Exception(f'{tcolor.fail}no data. you need to load open-loop data using :py:meth:`~telemetry_analysis.Model.prep`{tcolor.end}')
        df = self.df
        lb = [x[0].value for x in df.bounds]
        ub = [x[1].value for x in df.bounds]
        units = [x[0].unit for x in df.bounds] # make sure start values have these units
        #keys = list(df.variable)
        keys = list(df.index)
        vals = [u.Quantity(self[key]) for key in keys] # attributes override values in df
        keep_feasible = [True,]*len(lb)
        bounds = optimize.Bounds(lb,ub,keep_feasible=keep_feasible)
        start = [val.to(unit).value for val,unit in zip(vals,units)]
        r = optimize.minimize(self.cost_func,start,bounds=bounds)
        self.optimizer_result = r
        self.accept()
        return self
    
    @property
    def results(self):
        x = self.optimizer_result
        y = self.params
        return DotDict({'optimizer result':x,'optimum parameters':y})
    
    @property
    def params(self):
        #keys = ['r0'] + list(self.df.variable)
        keys = ['r0'] + list(self.df.index)
        r = {key:self[key] for key in keys}
        return DotDict(r)
    
    def history(self,n=None):
        '''return the whole stack of prior parameter settings, or one element from it.
        
        Argument:
        ---------
        n - int
            Index into the stack. -1 is most recent, -2 is the next most
            recent, etc., n=0 is the original
        
        Returns:
        --------
        without arguments: a deque containing dictionaries
        with argument n: the n'th parameter dictionary in the deque
        
        Note:
        -----
        The present parameters are not in the stack. Use `params` to get those.
        '''
        if n is None:
            return pd.DataFrame(self._prevx)
        else:
            return pd.DataFrame(self._prevx).iloc[n]
    
    def set(self,**kwargs):
        self._prevx.append(self.params)
        for key in kwargs.keys():
            setattr(self,key,kwargs[key])
        return self
        
    def __getitem__(self,key):
        return getattr(self,key)
    
    def __setitem__(self,key,val):
        setattr(self,key,val)
        
    @property
    def args(self):
        args = self._args
        return DotDict({arg:getattr(self,arg) for arg in args})
    
    @property
    def xargs(self):
        xargs = self._xargs
        return DotDict({arg:getattr(self,arg) for arg in xargs})
    
    @property
    def pp(self):
        oprint.pprint(self)
        
    def prep(self,data):
        '''setup the optimization by providing data to fit the model.
        data is one of the telemetry_analysis datasets e.g. Set(...).read().data[k]
        or can be the Set itself.
        
        Arguments:
        ----------
        data - Quantity or Set
            an open-loop dataset from ShaneAO telemetry. If the argument is
            a Set, which contains multiple telemetry data sets, the first data
            set in the list that is open-loop will be used.
        
        Returns:
        --------
            the Model object

        Example usage:
        --------------
        
            m = Model()
            ds = Set(date='2016-05-24',dsn=[7,6]).read() # read the data from fits files
            ds.recon_phase().power_spectrum() # calculate the average power spectra of the data
            m.prep(ds)
            m.fit()
            m.result # examine the result
            m.plot()
            
        or, more compactly:
        
            ds = Set(date='2016-05-24',dsn=7)
            m = Model().prep(ds)
            m.fit().accept().plot()
            
        '''
        if isinstance(data,Set):
            ds = data
            self.dataset = ds
            if not hasattr(ds,'data'):
                ds.read()
            if any([not  hasattr(x,'P') for x in ds.data.values()]):
                ds.recon_phase().power_spectrum()
            open_loop_dsns = [x.dsn for x in ds.data.values() if x.loop == 'open']
            if not open_loop_dsns:
                print(f'{tcolor.fail}ERROR no open loop data found in data set {ds}{tcolor.end}')
                return self
            for i,dsn in enumerate(open_loop_dsns):
                if i==0:
                    self.prep(ds.data[dsn])
                else:
                    m = self.copy().prep(ds.data[dsn])
            return self
        if data.loop != 'open':
            print(f'{tcolor.warning}WARNING: data is not open loop{tcolor.end}')
        if not hasattr(data,'P'):
            raise Exception(f'{tcolor.fail}no power spectrum. you need to run :py:meth:`~telemetry_analysis.Set.power_spectrum`{tcolor.end}')
        P = data.P
        f = data.P.f
        data.S = S = interp1d(P.f,P,kind='linear')
        n = data.nt
        fs = data.rate
        df = (fs/n).to('Hz').value
        fn = (fs/2.).to('Hz').value
        nls = 100
        fsamples = np.logspace(np.log10(df*2),np.log10(fn),nls)*u.Hz
        Ssamples = S(fsamples)*u.nm**2/u.Hz
        target = np.log10(Ssamples.value)
        weights = 1./np.abs(target)
        r0 = data.r0['r0 tilts']
        lam0 = data.r0['lam0']
        
        # make a first estimate of the noise floor by averaging a high-frequency part of the power spectrum
        nave = min(50,(n//2)//10)
        self.Sn = P[n//2-nave:n//2].mean()
        
        self.f = fsamples
        self.weights = weights
        self.target = target
        self.r0 = r0
        self.lam0 = lam0
        self.data = data
        data.model = self
        return self
    
    def est_r0_depricated(self):
        '''estimate the value of the seeing parameter from the data. Do this in two
        ways: formula (11) (using slope statistics) and (12) (using phase statistics).
        
            < theta^2 > = 0.170 (d/r0)^(5/3) (lambda/d)^2           (11)
            < phi^2 > = 1.0299 (D/r0)^(5/3)                         (12*)
        
        *note formula (12) is wrong in the paper. D = full aperture diameter (3 meters)
        and 1.0299 factor is Noll's (whole-aperture) piston-removed residual coefficient.
        
        Returns:
        --------
        a dictionary of results.
        
        '''
        data = self.data
        d = self.data.d
        D = 3.0*u.m
        if hasattr(self,'ds'):
            ds = self.ds
        else:
            ds = Set()
            ds.data = [data]
        ds.recon_phase().power_spectrum()
        r0_slopes = data.model['r00']
        lam0 = data.model['lam0']
        k = 2*np.pi / lam0
        
        rms_phase = np.sqrt(data.ph.var(axis=1).mean())
        rms_phase_radians = (rms_phase*k).to_da(u.rad)
        D_over_r0 = (rms_phase_radians.to_da('')**2/0.111)**(3/5)
        r0 = (D/D_over_r0).to(u.cm)
        #r0 = ( D*rms_phase_radians**(-6/5) *(0.111)**(3/5) ).to_da(u.cm)
        #r0 = ( d*rms_phase_radians**(-6/5) *(4*np.pi**2*0.170)**(3/5) ).to_da(u.cm)
        r0_phases = r0
        return {'r0_phases':r0_phases,
                'rms_phase':rms_phase,
                'r0_slopes':r0_slopes,
                'rms_slopes':data.rms_tilt,
                'lam0':lam0}
    
    def est_r0(self):
        '''estimate the value of the seeing parameter as evidenced by slope variance
        in the telemetry data (forumula 11 in [reference 1]).
        '''
        data = self.data
        d = self.data.d
        if hasattr(self,'ds'):
            ds = self.ds
        else:
            ds = Set()
            ds.data = [data]
        ds.recon_phase().power_spectrum()
        r0_slopes = data.model['r00']
        return r0_slopes
        
    def accept(self):
        '''change the model parameters to those in the argument list
        '''
        x = self.optimizer_result.x
        #keys = list(self.df.variable)
        keys = list(self.df.index)
        units = [val.unit for val in self.df.value]
        vals = [u.Quantity(val,unit) for val,unit in zip(x, units)]
        prev_params = {key:self[key] for key in ['r0']+keys}
        self._prevx.append(prev_params)
        for key,val in zip(keys,vals): self[key] = val
        self.df.value = vals
        return self
    
    def undo(self):
        '''undo an accept
        '''
        q = self._prevx.pop()
        self.r0 = q['r0']
        #keys = list(self.df.variable)
        keys = list(self.df.index)
        vals = [q[key] for key in keys]
        for key,val in zip(keys,vals): self[key] = val
        self.df.value = vals
        return self
    
    def summary(self,form = 'text'):
        if form == 'text':
            s = ''
            for k,v in self.params.items():
                s += f'{k} = {v:2.4}\n'
            s = s.strip()
            return s
        elif form == 'dict':
            return self.params
        elif form == 'fits':
            p = self.params
            d = {'r0':('AO_R0','r0'),
                 'v':('AO_V','wind speed'),
                 'f0':('A0_F0','low freq break'),
                 'Sn':('AO_SN','noise floor'),
                 'p':('AO_P','mid freq power law f^-p'),
                 'q':('AO_Q','low freq power law f^-q'),
            }
            c = []
            for key,(name,comment) in d.items():
                val = self.params[key].round(2).value
                unit = self.params[key].unit
                comm = f'{unit} A0 telemetry {comment}'
                card = fits.Card(name,val,comm)
                c.append(card)
            return c
        elif form == 'keywords':
            p = self.params
            telemkwds = ['TELEM_R0','TELEM_VWIND','TELEM_F0','TELEM_SN','TELEM_P','TELEM_Q']
            telemkeys = ['r0','v','f0','Sn','p','q']
            for kwd,key in zip(telemkwds,telemkeys):
                val = self.params[key].round(2).value
                cmd = ['modify','-s','saoseeing',f'{kwd}={val}']
                subprocess.run(cmd); print(' '.join(cmd))
        
    def plot(self,frange=None,nls=100,fig=None):
        '''Plots the the model power spectrum. If the model has data attached
        (see :method:`prep`) plot it and then the model curve on
        top of it. Otherwise plot just the model, but
        in that case, the frequency range (frange) argument must be provided.
        
        Arguments:
        ----------
        frange : Quantity (2-vector)
            The minimum and maximum frequency range of the plot
        nls : int
            The number of sampled points of the model spectrum
        fig : Figure or int
            The plot figure (number) if over-ploting
            
        '''
        plt.figure(fig)
        if frange is not None:
            fmin,fmax = frange.to(u.Hz).value
            if np.isclose(fmin,0.): fmin = fmax*1e-4
            dsn = None
        else:
            if hasattr(self,'data'):
                data = self.data
                fn = data.rate/2
                n = data.nt
                f,P = data.P.f[1:n//2], data.P[1:n//2]
                fmin,fmax = f[0].value,f[-1].value
                
                name = data.name.strip('.fits')
                dsn = p_dsn(name)
                date = p_date(data.obs_date)
                cent = data.header['cent']
                rate = data.rate
                mode = data.mode
                if fig is None:
                    #label = 'data %s %0.4d'%(self.data.obs_date.date(),self.data.dsn)
                    label = f'Data {dsn:04d}'
                    title = f'{date} {name} {mode} {rate} {cent}'
                    plt.plot(f,P,label=label)
                    plt.title(title)
            else:
                raise Exception('either provide a frequency range or data')
        f = np.logspace(np.log10(fmin),np.log10(fmax),nls)*u.Hz
        if dsn: label = f'{dsn:04d} Model'
        else: label = 'Model'
        label = label + f' {self.r0.round(2)} {self.v.round(2)}'
        plt.plot(f,self(f),label=label)
        plt.legend()
        ax =  plt.gca()
        if fig is None:
            ax.set_ylabel('nm$^2$ / Hz')
            ax.set_xlabel('frequency, Hz')
            ax.set_xscale('log')
            ax.set_yscale('log')
            plt.grid(1,which='both')
        s = ''
        for k,v in self.params.items():
            s += f'{k} = {v:2.4}\n'
        s = s.strip()
        ax.text(0.05,0.05,s,ha='left',va='bottom',transform=ax.transAxes,
                bbox = dict(facecolor='white',edgecolor='gray'))
        return self
    
class Set(object):
    """A telmetry_analysis data set
    """

    def __init__(self,date='2015-09-03',dsn=[411,412],read=True):
        """Create a data analysis object.
        
        Parameters
        ----------
        date : str
            the observing night of the data, as 'YYYY-mm-dd', or in a 'loose' format (see p_date)
            If date = None or '', use the present moment's observing night
        dsn : int or list of ints
            data set number, eg. Data_0411.fits is number 411
        """
        date = p_date(date)
        self.name = f'{date} {dsn}'
        self.date = date
        if dsn == 'all':
            dbf = finder.dbFiles('all',date=date,dstype='telemetry')
            dsn = sorted(list(dbf['filename']))
        dsn = p_dsn(dsn)
        self.paramDir = paramDir
        self.date = date = pd.to_datetime(date).date()
        if not isinstance(dsn,list): dsn = [dsn]
        dsfiles = [f'Data_{x:04d}.fits' for x in dsn]

        if len(dsn) == 0:
            raise Exception(f'{tcolor.fail}no datasets found for date {str(date)}{tcolor.end}')
        # if place in 'home':
        volume,fileDir = finder.findVol('telemetry',date)
        if isinstance(fileDir,list): fileDir = fileDir[0]
        if fileDir is None:
            try:
                raise Exception(f'{tcolor.fail}{date} Data_*.fits -- not found in any of finder.dataVols{tcolor.end}')
            except Exception as err:
                self.error = f'{tcolor.fail}files not found{tcolor.end}'
                print(err)
                return
        # elif place in ['Lick','LickTest']:
        #     volume = data_paths[place]
        #     fileDir = os.path.join(volume,'telemetry',str(date))

        if str(date) == p_date('today'):
            with finder.Cache('off'):
                db = dbase(fileDir)
        else:
            db = dbase(fileDir)
        
        db1 = db[db['obs_date'] == pd.Timestamp(date)].copy()
        #fne = [f for f in dsfiles if not os.path.isfile(os.path.join(fileDir,f))]
        #if len(fne) > 0:
        #    do = 'do' if len(fne) > 1 else 'does'
        #    raise Exception(f'{tcolor.fail} {", ".join(fne)} {do} not exist in {fileDir}{tcolor.end}')
        #idx = [db1[db1.filename == x].index[0] for x in dsfiles]
        #db1 = db1.loc[idx].copy()
        files = list(db1['filename'])
        for k,fn in zip(dsn.copy(),dsfiles):
            if fn not in files:
                print(f'{tcolor.warning}WARNING: data set {fn} not found{tcolor.end}')
                dsn.remove(k)
        dsfiles = [f'Data_{x:04d}.fits' for x in dsn]
        pat = 'Data_(\d{4})\.fits'
        dsn_column = [int(re.findall(pat,x)[0]) for x in files]
        db1['dsn'] = dsn_column
        db1.set_index('dsn',inplace=True)
        idx = [db1[db1.filename == x].index[0] for x in dsfiles]
        db1 = db1.loc[idx].copy()
        self.db = db1
        self.date = date
        self.dsfiles = dsfiles
        self.dsn = dsn
        self.fileDir = fileDir
        self.fileDirdb = db
        
        self.lam = 700*u.nm
        self.lam.doc = 'central wavelength of the wavefront sensor light'
        
        if read: self.read()
        
    def __getitem__(self,key):
        '''access one dataset =or=
        put together a (usually temporary) Set consisting of some of the datasets,
        accessed by data set number(s) -or=
        access columns of the database
        
        Argument:
        ---------
        key : int or list of ints or str or list of str
            int : Data set number
            int,int,... : multiple data set numbers
            'open' : all the open-loop datasets
            'closed' : all the closed-loop datasets
            str : column of the database
            [str, str,...] : multiple columns of the database
        '''
        if isinstance(key,tuple):
            if all([isinstance(k,int) for k in key]): # multiple data set numbers
                r = self.copy()
                r.dsn = list(key)
                r.db = self.db.loc[list(key)]
                r.data = {k:self[k] for k in key}
                return r
            elif all([isinstance(k,str) for k in key]): # multiple database columns
                return self.db[list(key)]
            else:
                print(f'{tcolor.fail}KeyError: {key}{tcolor.end}',flush=True)
                raise KeyError                
        elif isinstance(key,list): # multiple database columns
            return self.db[key]
        elif isinstance(key,int): # one dataset
            return self.data[key]
        elif isinstance(key,str): # one column of the databse -or- open or closed
            if key in ['open','closed']:
                dsn = self.db[self.db['loop']==key].index.to_list()
                return self.__getitem__(tuple(dsn))
            else:
                return self.db[key]
        else:
            print(f'{tcolor.fail}KeyError: {key}{tcolor.end}',flush=True)
            raise KeyError
    
    def _dba(self):
        return pd.DataFrame([x.db for x in self.datasets])

    @property
    def dba(self):
        db = self._dba()
        def_cols = ['date','filename','time','loop','rate','cent','mode','substate']
        [def_cols.append(x) for x in ['r0','v','Sn','star','mV','trans'] if x in db.columns]
        dates = [x.date() for x in db['time']]
        times = [x.time() for x in db['time']]
        db['date'] = dates
        db['time'] = times
        return db[def_cols]
        
    def copy(self):
        '''produce a shallow copy of the Set
        '''
        return copy(self)

    def read(self):
        '''The read() operation brings in the data from telemetry file(s).
        '''
        self.data = {}
        db = super(Set,self).__getattribute__('db')
        print(f'reading {self.dsn}: ',end='',flush=True)
        for ind,e in db.iterrows():
            print(f'{ind}..',end='',flush=True)
            date = e.obs_date
            dsn = e.name
            data = _data_read(date=date,dsn=dsn)
            self.data[data.dsn] = data
        
        print(f'{tcolor.green}done{tcolor.end}')
        return self
    
    def set_star(self,star):
        [self[k].set_star(star) for k in self.dsn]
        return self
    
    @property
    def star(self):
        db = self.dba
        if 'star' in db.columns:
            return db[['star','mV','trans']]
        else:
            print('no star data')
            return None
            
    def slopes_to_2d(self,mode):
        '''put the slopes vector(s) into 2d arrays
        '''
        pass
    
    def __repr__(self):
        if hasattr(self.date,'strftime'):
            datestr = self.date.strftime('%Y-%m-%d')
        else:
            datestr = self.date
        err = getattr(self,'error','[]')
        dsn = getattr(self,'dsn',err)
        return f'{self.__class__} {datestr} {dsn}'
        
    def _pp(self):
        keys = np.sort(list(self.__dict__.keys()))
        for key in keys:
            value = self.__dict__[key]
            if (type(value) is np.ndarray):
                value = 'ndarray '+str(value.shape)
            if (type(value) is np.matrixlib.defmatrix.matrix):
                value = 'matrix '+str(value.shape)
            if key.startswith('ps_'):
                modes = value[3]
                value = 'power spectrum'+ \
                        str(value[0].shape)
                if type(modes) is list and len(modes) > 5:
                    value += ' modes: '+str(type(modes))+'('+str(len(modes))+')'
                else:
                    value += ' modes: '+str(modes)
            print(key,': ', value)
    
    @property
    def pp(self):
        oprint.pprint(self)
    
    def pprint(self):
        oprint.pprint(self)

    def var(self,verbose=True,force=False):
        """Calculate the variance of slopes and phases for all open loop data sets
        Force = True forces the calculation, even if the data set does not have open loop data
        """
        ol_data = None
        for data in self.data:
        
            # slopes
            unit_conversion = (data.pixel_scale/data.centroider_gain)
            data.s.cvar = (data.s*unit_conversion).var(axis=0).mean()
        
            # phases
            if not hasattr(data,'ph'): self.recon_phase()
            data.ph.cvar = data.ph.var(axis=0).mean()
        
            # compute the implied r0
            # done already. results are in data.model
            # from Tyler, 1994, eqn 77
            if force or data.loop == 'open':
                lam0 = data.model.lam0
                k = 2*np.pi / lam0
                sig2_theta = data.s.cvar
                sig2_phi = data.ph.cvar * k**2
                d_over_r0 = (((sig2_theta/0.170)/(lam0/data.d)**2)**(3./5.) ).to_da('')
                r0 = data.d/d_over_r0
                data.r0_tilt_var = r0
                # from Noll, 1976 Table IV
                D_over_r0 = ((sig2_phi/.111)**(3./5.)).to_da('')
                D = 3.0*u.m
                r0 = (D/D_over_r0).to(u.cm)
                data.r0_phase_var = r0
                
                ol_data = data
        
        if verbose:
            if ol_data is not None:
                print(f'Data set {ol_data.filename}')
                print(f'r0 estimate from tilts: {ol_data.r0_tilt_var}')
                print(f'r0 estimate from phases: {ol_data.r0_phase_var}')
            else:
                print('no open loop data')
        
    def power_spectrum(self,plot=False,fig=None):
        '''calculate the average power spectrum of the reconstructed phases
        '''
        for dsn,data in self.data.items():
            data.power_spectrum()
        
        if plot: # plot the models as dashed lines
            self.plot(fig=fig)

        return self
    
    def plot(self,dsn='all',fig=None,models=True):
        '''plot the power spectra'''
        
        if isinstance(dsn,str) and dsn == 'all':
            dsns = self.data.keys()
        else:
            if isinstance(dsn,pd.DataFrame):
                dsn = dsn.index.to_list()
            if not isinstance(dsn,list):
                dsns = list(dsn)
            else:
                dsns = dsn.copy()
        # plot opens first
        opens = [x for x in dsns if self[x].loop == 'open']
        clos = [x for x in dsns if self[x].loop == 'closed']
        dsns = opens + clos
        k = 0
        for dsn in dsns:
            data = self.data[dsn]
            if not hasattr(data,'P'): self.power_spectrum()
            # f,P = data.P.f, data.P
            data.plot(model=models,fig=fig)
            fig = plt.gcf().number
        return self
    
    def fit(self):
        '''fit models to all the open loop data
        '''
        for data in self.datasets:
            if data.loop == 'open':
                data.fit()
        return self
        
    def old_fit(self):
        '''fit model parameters to the open loop data
        '''
        for dsn,data in self.data.items():
            if data.loop != 'open':
                continue
            if not hasattr(data,'P'):
                self.power_spectrum(plot=False)
            P = data.P
            f = data.P.f
            data.S = S = interp1d(P.f,P,kind='linear')
            n = data.nt
            fs = data.rate
            df = (fs/n).to('Hz').value
            fn = (fs/2.).to('Hz').value # the Nyquist frequency
            nls = 100
            fsamples = np.logspace(np.log10(df*2),np.log10(fn),nls)*u.Hz
            Ssamples = S(fsamples)*u.nm**2/u.Hz
            lam = data.model.lam0
            r0 = data.model.r00
            target = np.log10(Ssamples.value)
            weights = 1./np.abs(target)
            def func(args,lam=lam,r0=r0,target=target,weights=weights,f=fsamples):
                '''arguments are the model parameters:
                v, f0, Sn
                '''
                v,f0,Sn = args
                # restore the units
                v = v*u.m/u.s
                f0 = f0*u.Hz
                Sn = Sn*u.nm**2/u.Hz
                # evaluate the model
                S =( c3*(v/r0)**(5/3)*(f**2+f0**2)**(-8/6)*(lam/(2*pi))**2 + Sn ).to(u.nm**2/u.Hz)
                #cost = (((S - Ss)**2).mean()**(.5)).value
                
                # cost -- mean square relative deviation on a log scale
                value = np.log10(S.value)
                cost = (( (value - target)*weights )**2).mean()
                
                return cost
            def func_v2(args,lam=lam,r0=r0,target=target,weights=weights,f=fsamples):
                '''arguments are the model parameters:
                v, f0, Sn, p, q
                '''
                v,f0,Sn,p,q = args
                # restore the units
                v = v*u.m/u.s
                f0 = f0*u.Hz
                Sn = Sn*u.nm**2/u.Hz
                # evaluate the model
                S = ( c3*(v/r0)**(5/3)*(f**2+f0**((p-q)/p)*f**(2*q/p))**(-p/2)*(lam/(2*pi))**2 + Sn ).to(u.nm**2/u.Hz)
                #cost = (((S - Ss)**2).mean()**(.5)).value
                
                # cost -- mean square relative deviation on a log scale
                value = np.log10(S.value)
                cost = (( (value - target)*weights )**2).mean()
                
                return cost
            data.cost_function = func_v2
            
            data.model.bounds = {
                'v':[0,20]*u.m/u.s,
                'f0':[.5,50]*u.Hz,
                'Sn':[.1,100]*u.nm**2/u.Hz,
                'p':u.Quantity([2.5,2.8]),
                'q':u.Quantity([0,1]),
            }
            keys = data.model.bounds.keys()
            lb = [data.model.bounds[key].value[0] for key in keys]
            ub = [data.model.bounds[key].value[1] for key in keys]
            keep_feasible = [True,]*len(data.model.bounds)
            bounds = optimize.Bounds(lb,ub,keep_feasible=keep_feasible)
            start = [data.model[key].value for key in keys]
            r = optimize.minimize(func,start,bounds=bounds)
            data.optimizer_result = r
            
        return self
    
    def accept(self):
        '''accept the optimizer result as the new model
        '''
        parameter_units = [u.m/u.s, u.Hz, u.nm**2/u.Hz]
        for dsn,data in self.data.items():
            if data.loop != 'open':
                continue
            for key,val,unit in zip(['v','f0','Sn'], data.optimizer_result.x, parameter_units):
                data.model[key] = val*unit
                
        return self
    
    @property
    def dbModels(self):
        '''return a pandas data frame summary of model fitting results,
        
        Returns:
        --------
        pandas.DataFrame :
            with rows of data file name and parameters of each model
        '''
        self.model()
        models = self._models
        names = [x.data.name.strip('.fits') for x in models]
        ind = [x.data.dsn for x in models]
        df = pd.DataFrame(index=ind)
        df['name'] = names
        df2 = pd.DataFrame([dict(x.params) for x in models],index=ind)
        for col in df2.columns:
            df[col] = df2[col].apply(lambda x: np.round(x,2))
        return df
    
    @property
    def dbData(self):
        return self.db
    
    def model(self):
        '''create models for all the open loop data sets.        
        '''
        for dsn,data in self.data.items():
            if not hasattr(data,'P'): self.power_spectrum()
            if data.loop == 'open':
                if not isinstance(data.model,Model):
                    data.model = Model().prep(data)
            else:
                data.model = None
        self._models = [x.model for n,x in self.data.items() if isinstance(x.model,Model)]
        return self
    
    @property
    def models(self):
        self.model()
        return self._models
    
    @property
    def datasets(self):
        return [self[k] for k in self.dsn]
    
    def power_spectrum_old(self,y='slopes',mode=list(range(10)),plot=True,dual_plot=True,pegf=None,pegf2=None,title='',v=2.,r0=.1,r1=1.):
        """Calculate the power spectrum of data telmetry
        pegf2 = [f,p] - put a second power law line. p= power law, f = peg frequency
        """
        assert y in ['slopes','acts','modes','phases','x slopes','y slopes']
        if type(mode) is list:
            assert np.array(mode).max() < self.n_modes_max
        else:
            assert mode < self.n_modes_max
        if self.wfsm_cl is None:
            cl = None
        else:
            cl = 'ok'
        if (y == 'modes'):
            ol = np.array(self.wfsm_ol)
            if cl is not None: cl = np.array(self.wfsm_cl)
        if (y == 'slopes'):
            ol = np.array(self.wfs_ol_m)
            if cl is not None: cl = np.array(self.wfs_cl_m)
            mode = list(range(2*self.ns))
        if (y == 'x slopes'):
            ol = np.array(self.wfs_ol_m[:,0:self.ns])
            if cl is not None: cl = np.array(self.wfs_cl_m[:,0:self.ns])
            mode = list(range(self.ns))
        if (y == 'y slopes'):
            ol = np.array(self.wfs_ol_m[:,self.ns:2*self.ns])
            if cl is not None: cl = np.array(self.wfs_cl_m[:,self.ns:2*self.ns])
            mode = list(range(self.ns))
        if (y == 'acts'):
            illum = np.where(self.ap.flatten())[0]
            ol = np.array(self.act_ol_m).reshape((self.nt,self.naa*self.naa))
            ol = ol[:,illum] # illuminated actuators
            ol = ol - np.average(ol,axis=0)
            if cl is not None:
                cl = np.array(self.act_cl_m).reshape((self.nt,self.naa*self.naa))
                cl = cl[:,illum]
                cl = cl - np.average(cl,axis=0)
            mode = list(range(len(illum)))
        if (y == 'phases'):
            illum = np.where(self.ap.flatten())[0]
            ol = np.array(self.p_ol).reshape((self.nt,self.naa*self.naa))
            ol = ol[:,illum] # illuminated actuators
            ol = ol - np.average(ol,axis=0)
            self.p_ol_illum = ol
            if cl is not None:
                cl = np.array(self.p_cl).reshape((self.nt,self.naa*self.naa))
                cl = cl[:,illum]
                cl = cl - np.average(cl,axis=0)
                self.p_cl_illum = cl
            mode = list(range(len(illum)))
            # Greenwood/Fried model
            D = 3.
            #r1 = D/4.
            #v = 2.
            #r0 = 0.1
            lam0 = 0.5
            fs = self.rate
            N = self.nt
            df = fs/float(N)
            f = np.arange(0,(N/2)*df,df)
            k = 2*np.pi / (lam0*microns)
            #f_phi = 0.07713*(v/r0)**(5./3.)*(f**(-8./3.))
            c3 = 1./4.
            f0 = v/(np.pi*D)
            x = f/f0
            f_phi = 2.079*f0**(-1)*(D/r0)**(5./3.)*c3*x**(-8./3.)
            # low freq part of model
            f0 = v/(np.pi*D)
            x = f/f0
            c1 = (r1/D)**2
            c3 = 1./4.
            x3 = np.sqrt(c3/c1)
            f3 = x3*f0
            f3k = np.round(f3/df).astype(int)
            if f3k in range(len(f)):
                f_phi[1:f3k] = 2.079*f0**(-1)*(D/r0)**(5./3.)*c1*x[1:f3k]**(-2./3.)
                #f_phi[1:f3k] = 3.045*(v/r0)**(5./3.)*(r1/v)**2*f[1:f3k]**(-2./3.)
            norm = (1./k)**2 / nm**2
            self.phasePSD_model = norm*f_phi[1:]
            self.phasePSD_model_f = f.copy()[1:]
            self.model = generic_object.Object(r0=r0,v=v,r1=r1) # pass this on to the plotter
        df = self.rate/float(self.nt)
        f = np.arange(0,(self.nt/2)*df,df)

        if type(mode) is list:
            ol_f = np.zeros((self.nt))
            if cl is None:
                cl_f = None
            else:
                cl_f = np.zeros((self.nt))
            # average over modes
            for m in mode:
                ol_f += np.abs(np.fft.fft(ol[:,m]))**2
                if cl_f is not None:
                    cl_f += np.abs(np.fft.fft(cl[:,m]))**2
            ol_f /= float(len(mode))
            if cl_f is not None:
                cl_f /= float(len(mode))
        else:
            ol_f = np.abs(np.fft.fft(ol[:,mode]))**2
            if cl_f is not None:
                cl_f = np.abs(np.fft.fft(cl[:,mode]))**2
        # normalization of squared fft of data to power spectrum is dt**2/T
        # resulting ps units are (slopes,phase, whatever)^2 / Hz
        dt = 1./self.rate
        N = self.nt
        T = N*dt
        ol_f *= dt**2/T
        if cl_f is not None:
            cl_f *= dt**2/T
        if y.endswith('slopes'):
            ol_f *= (self.pixel_scale/self.centroider_gain)**2
            if cl_f is not None:
                cl_f *= (self.pixel_scale/self.centroider_gain)**2
            units = 'arcsec'
        if y == 'phases':
            ol_f *= ((self.pixel_scale/self.centroider_gain)*arcsec*self.d/nm)**2
            self.p_ol_illum *= (self.pixel_scale/self.centroider_gain)*arcsec*self.d/nm
            if cl_f is not None:
                cl_f *= ((self.pixel_scale/self.centroider_gain)*arcsec*self.d/nm)**2
                self.p_cl_illum *= (self.pixel_scale/self.centroider_gain)*arcsec*self.d/nm
            units = 'nm'
        if y == 'acts':
            units = 'DN'
        if y == 'modes':
            units = 'mode coef'

        if cl_f is not None:
            ps = [y,f[1:],ol_f[1:self.nt//2],cl_f[1:self.nt//2],mode,units]
        else:
            ps = [y,f[1:],ol_f[1:self.nt//2],None,mode,units]
        if (y == 'slopes'):
            self.ps_slopes = ps
        if (y == 'x slopes'):
            self.ps_x_slopes = ps
        if (y == 'y slopes'):
            self.ps_y_slopes = ps
        if (y == 'acts'):
            self.ps_acts = ps
        if (y == 'modes'):
            self.ps_modes = ps
        if (y == 'phases'):
            self.ps_phases = ps
        if plot:
            self.power_spectrum_plot(y=y,pegf=pegf,pegf2=pegf2,title=title,dual_plot=dual_plot)

    def zernike(self,nz=30,skip=100,plot=True,oplot=False,r0=.1):
        """calculate the variance of zernike coefficients
        and plot along with a (D/r0) fit to the Noll coefficients
        """
        # check first that we are looking at open loop data
        if self.hdr_ol['LOOP'] == 'open':
            p_ol = self.p_ol
            print('open loop data is dsn %d'%self.dsn[0])
            dsn = self.dsn[0]
        else:
            if hasattr(self,'hdr_cl'):
                if self.hdr_cl['LOOP'] == 'open':
                    p_ol = self.p_cl
                    print('open loop data is dsn %d'%self.dsn[1])
                    dsn = self.dsn[1]
                else:
                    raise Exception('data set [%d,%d] does not have open loop data'%(self.dsn[0],self.dsn[1]))
            else:
                raise Exception('data set [%d] does not have open loop data'%self.dsn[0])

        u = p_ol - np.mean(p_ol,axis=0)
        nt,nx,ny = p_ol.shape
        r = 15. # radius of beam on tweeter
        dr = 1./np.float(r)
        w = 1./np.pi # Noll normalization
        ap = img.circle((nx,ny),r=r)
        table = zernike.ztable(nz+1,noll=True)
        a = []
        zfits = []
        for t in range(0,nt,skip):
            c = []
            p = u[t,:,:]*(self.pixel_scale*arcsec/self.centroider_gain)*self.d/nm
            zfit = zernike.fit(p,r,nz)[0]
            zfits.append(zfit)
            #c = [0., 0., 0.] # piston, tip, tilt ignored
            for k in range(0,nz+1):
                n,m = table[k]
                z = zernike.zernike2(nx,ny,r,n,m)
                c.append(np.sum(u[t,:,:]*z*ap*w*dr**2))
                
            a.append(c)
        self.zfits = np.array(zfits)
        self.zfits_units = 'nanometers'
        a = np.array(a)
        # convert to units of radians
        lam0 = 500.*nm
        a *= (2*np.pi/lam0)*(self.pixel_scale*arcsec/self.centroider_gain)*self.d
        self.zcoefs = a
        var_data = np.var(a,axis=0)
        if nz < len(noll_coefs_base)+1:
            noll_coefs = noll_coefs_base[:nz+2]
        else:
            noll_coefs = noll_coefs_base[:]
            p = np.sqrt(3.)/2.
            for j in range(len(noll_coefs_base),nz+2):
                noll_coefs.append(0.2944*j**(-p))
        noll_coefs = np.array(noll_coefs)
        # the variance contribution of zernike j is Del(k+1) - Del(k)
        var_model = (noll_coefs - np.roll(noll_coefs,-1))[:-1]
        var_model[0:3] = 0. # piston, tip, tilt ignored
        # ignore tip/tilt, equal weight all others
        wts = np.ones((nz+1))
        wts[0:3] = 0
        print(len(var_model),len(var_data),len(wts))
        D_over_r0 = np.sum(wts*var_model*var_data)/np.sum(wts*var_model**2)**(3./5.)
        D = 3.0 # shane telescope diameter
        self.r0_est = D / D_over_r0
        D_over_r0 = D/r0 # use the argument-supplied r0
        self.noll_coefs = noll_coefs
        self.var_model = var_model
        self.var_data = var_data
        if plot:
            if not oplot:
                plt.figure()
            plt.plot(var_data,'x',label='open loop data #%d'%dsn)
            select = list(range(3,nz+1))
            plt.plot(select,var_model[3:]*D_over_r0**(5./3.),'-',label='model r0=%.2f'%r0)
            if not oplot:
                plt.yscale('log')
                plt.grid(True,which='both')
                plt.xlabel('Zernike # (Noll J)')
                plt.ylabel(r'radians$^2$')
                plt.legend(frameon=False)
                atext = 'date: %s\n'%self.date
                atext += 'data set: %d\n'%dsn
                xy = (0.62,0.65)
                plt.annotate(s=atext,xy=xy,xycoords='figure fraction')
        return
        
    def power_spectrum_plot(self,y='slopes',oplot=False,pegf=None,title='',pegf2=None, dual_plot=False):
        """assume that the power_spectrum method has already
        been called
        """
        if (y=='slopes'):
            ps = self.ps_slopes
            title += 'wavefront slope power spectrum'
            power_law = r'$f^{-2/3}$'
            p = -2./3.
        if (y=='x slopes'):
            ps = self.ps_x_slopes
            title += 'wavefront x slope power spectrum'
            power_law = r'$f^{-2/3}$'
            p = -2./3.
        if (y=='y slopes'):
            ps = self.ps_y_slopes
            title += 'wavefront y slope power spectrum'
            power_law = r'$f^{-2/3}$'
            p = -2./3.
        if (y == 'acts'):
            ps = self.ps_acts
            title += 'reconstructed actuators power spectrum'
            power_law = r'$f^{-8/3}$'
            p = -8./3.
        if (y == 'modes'):
            ps = self.ps_modes
            title += 'reconstructed modes power spectrum'
            power_law = None
        if (y == 'phases'):
            ps = self.ps_phases
            title += 'reconstructed phase power spectrum'
            power_law = r'$f^{-8/3}$'
            p = -8./3.
        items,f,ol,cl,mode,units = ps
        xlabel = 'frequency, Hz'
        ylabel = units + r'$^2$ / Hz'
        df = f[0]
        if pegf is not None:
            n0 = int(pegf/df)
            a = ol[n0]
        if 'LOOP' in list(self.hdr_ol.keys()):
            label = self.hdr_ol['LOOP']+' %04d'%self.dsn[0]
        else:
            label = 'open'+' %04d'%self.dsn[0]
        
        if y == 'phases':
            if not oplot:
                if dual_plot:
                    plt.figure(figsize = (8,12))
                else:
                    plt.figure()
            if dual_plot:
                plt.subplot(211)
                N,dt = self.nt,self.dt
                t = np.arange(0,N*dt,dt)
                pt = self.p_ol_illum.shape[1]/2
                plt.plot(t,self.p_ol_illum[:,pt])
                plt.plot(t,self.p_cl_illum[:,pt])
                plt.title('single phase point')
                plt.xlabel('time, sec')
                plt.ylabel('nanometers')
                plt.grid(True)
                plt.subplot(212)
        else:
            if not oplot:
                plt.figure()
        
        plt.loglog(f,ol,label=label)
        if cl is not None:
            if 'LOOP' in list(self.hdr_cl.keys()):
                label = self.hdr_cl['LOOP']+' %04d'%self.dsn[1]
            else:
                label = 'closed'+' %04d'%self.dsn[1]
            plt.loglog(f,cl,label=label)
        if power_law is not None and pegf is not None:
            plt.loglog(f,a*(f/pegf)**p,'c--',label=power_law)
        if pegf2 is not None:
            pegf = pegf2[0]
            n0 = int(pegf/df)
            a = ol[n0]
            p = pegf2[1]
            l = pegf2[2]
            plt.loglog(f,a*(f/pegf)**p,'c--',label=r'$f^{-%s}$'%l)
        plt.grid(True)
        plt.legend(loc=3)
        if type(mode) is list:
            pass
            #title = title + ' avg of '+items+' '+str(mode[0])+'...'+str(mode[-1])
            #atext = 'avg of '+items+' '+str(mode[0])+'...'+str(mode[-1])+'\n'
        else:
            title = title + 'mode '+str(mode)
            #atext = ' mode '+str(mode)+'\n'
        plt.title(title)
        atext = 'date: '+self.date+'\n'
        atext += 'data sets: '+str(self.dsn)+'\n'
        atext += 'rate: '+str(self.rate)+' Hz'+'\n'
        xy = (.6,.65)
        if y == 'phases':
            if dual_plot:
                xy = (xy[0],xy[1]*.5)
            r0,v,r1 = self.model.r0, self.model.v, self.model.r1
            atext += 'model r0: %.2f m\n'%r0
            atext += 'model  v: %.2f m/s\n'%v
            atext += 'model r1: %.2f m\n'%r1
        plt.annotate(s=atext,xy=xy,xycoords='figure fraction')
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        if (y == 'phases'):
            plt.plot(self.phasePSD_model_f,self.phasePSD_model,'r--',label='G-F model')
        plt.legend(loc=3,frameon=False)
        
    def slopes_to_2d_old(self,ext_slopes=None,mode_filter=False):
        """put the 288 element slopes vector(s) into
        2d arrays
        """
        n = self.sap.shape[0]
        nt = self.nt
        ns = self.ns
        
        if ext_slopes is not None: # external slopes
            ss = ext_slopes.shape
            if (len(ss) == 2):
                nt = ss[1]
                slopes = ext_slopes
            else:
                nt = 1
                slopes = ext_slopes.reshape((nt,2*ns))
            s2dx = np.zeros((nt,n*n))
            s2dx[:,self.smap] = slopes[:,0:ns]
            s2dx = s2dx.reshape((nt,n,n))
            s2dy = np.zeros((nt,n*n))
            s2dy[:,self.smap] = slopes[:,ns:ns*2]
            s2dy = s2dy.reshape((nt,n,n))
            r = np.array([s2dx,s2dy])
            return r

        s2d = []
        if mode_filter:
            slopes_set = (self.wfs_ol_m,self.wfs_cl_m)
        else:
            slopes_set = (self.wfs_ol,self.wfs_cl)
        for slopes in slopes_set:
            if slopes is not None:
                s2dx = np.zeros((nt,n*n))
                s2dx[:,self.smap] = slopes[:,0:ns]
                s2dx = s2dx.reshape((nt,n,n))
                s2dy = np.zeros((nt,n*n))
                s2dy[:,self.smap] = slopes[:,ns:ns*2]
                s2dy = s2dy.reshape((nt,n,n))
                r = np.array([s2dx,s2dy])
                s2d.append(r)
            else:
                s2d.append(None)
        
        self.s2d_ol,self.s2d_cl = s2d

    def recon_phase(self,data=None,alpha=1.0,dewaffle=True,iterate_r0=False):
        """reconstruct the phase history given the slope history
        
        Parameters
        ----------
        data : Quantity
            The slopes raw data, an array of size nv x nt where nv is the size
            of the telemetry vector and nt is the number of time samples. Typically this is
            one of the telemetry data arrays read in by read()
        dewaffle : boolean
            If true, use the dewafflizing reconstructor. If false, use a
            pure Fourier reconstructor
        alpha : float
            The penalty weighting given to waffle modes for the dewafflizing
            reconstructor
        iterate_r0 : bool
            Whether or not to iterate on accounting for spot size (r0-dependent)
            variations in the centroid-to-tilt transfer curve. With False,
            a linear transfer curve is assumed: tilt = plate_scale * centroid
        
        Returns
        -------
        None. The phase reconstruction results are stored as attributes of the
            data Quantity. Attributes created:
            phase : the history of wavefront reconstructed phases
            phase2d : history phases, in 2d format
            model : (open loop data only), a dictionary of r0, f0, and wind speed
                characterizing the open loop atmosphere, fitting the
                rms tilts and power spectra features
        """
        if data is None: # reconstruct all the data sets
            for dsn,data in self.data.items():
                data.recon_phase(alpha=alpha,dewaffle=dewaffle,iterate_r0=iterate_r0)
            return self
        if isinstance(data,int): # it maybe is refering to the data set number
            data = self.data[data]
        data.recon_phase(alpha=alpha,dewaffle=dewaffle,iterate_r0=iterate_r0)
        return self
    
    def bad_data(self,s,sc):
        '''exit due to bad data
        '''
        line1 = 'centroids max: %r'%s.abs().max()
        line2 = 'slopes max: %r'%sc.abs().max()
        raise Exception('NaNs or extremely large values found in slopes data\n    %s\n    %s'%(line1,line2))
        
    def disp_phase(self,skip=1,aperture = True,remove_average = True,display=True):
        if (self.mode == '16x'):
            #n,d,ds = 16,15,4
            n,d,ds = 32,30,8
        if (self.mode == '8x'):
            #n,d,ds = 8,7,2
            n,d,ds = 32,30,8
        p = np.array([self.p_ol,self.p_cl])
        p = p[:,list(range(0,self.nt,skip)),:,:]
        if aperture:
            ap = img.circle((n,n),(n/2,n/2),d/2.) - img.circle((n,n),(n/2,n/2),ds/2.)
            p = p*ap
        if remove_average:
            for k in range(len(p)):
                p[k] = p[k] - np.average(p[k],axis=0)
        if display:
            t = np.arange(self.nt)*self.dt*skip
            label = self.date+' '+str(self.dsn)
            dimg.play(p,label=label,times=t,tunits='sec')
        else:
            return p
    
    def disp_acts(self,skip=1,dt_ms = 50,cmap='bone',aperture=True,remove_average=True,display=True):
        """generate a movie of the actuator commands that would
        be generated by the slopes in the dataset
        """
        p = np.array([self.act_ol_m,self.act_cl_m])
        p = p[:,list(range(0,self.nt,skip)),:,:]
        
        naa = self.naa
        if (aperture):
            p = p*self.ap
        if (remove_average):
            for k in range(len(p)):
                p[k] = p[k] - np.average(p[k],axis=0)
            
        if display:
            dt = 1./float(self.rate)
            times = np.arange(self.nt)*dt*skip
            tunits = 'sec'
            dimg.play(p,dt_ms=dt_ms,cmap=cmap,times=times,tunits=tunits)
        else:
            return p

# --------------------------
# bound methods to data instances
# (which are u.Quantity arrays)
# --------------------------
# plot_data gets bound as method "plot" to instances of data in a Set

def _data_read(date='2015-09-03',dsn=411):
    date = p_date(date)
    filename = f'Data_{dsn:04d}.fits'
    # if place == 'home':
    volume,path_to_file = finder.findVol('telemetry',date,dsn,quiet=True)
    # elif place in ['Lick','LickTest']:
    #     volume = data_paths[place]
    #     path_to_file = os.path.join(volume,'telemetry',date,f'Data_{dsn:04d}.fits')
    if isinstance(path_to_file,list): path_to_file = path_to_file[0]
    if path_to_file is None:
        raise Exception(f'{tcolor.fail}{date} {filename} -- not found in any of finder.dataVols{tcolor.end}')
    fileDir,fileName = os.path.split(path_to_file)

    # db = dbase(fileDir)
    db = finder.dbHdr(path_to_file)
    e = db[db.filename == fileName].iloc[0]
    e.name = dsn
    with fits.open(path_to_file) as hdu:
        data = hdu[0].data
        hdr = hdu[0].header
    data = u.Quantity(data)
    data.db = e
    data.filename = f'Data_{dsn:04d}.fits' # e.filename
    data.path = path_to_file
    data.obs_date = Timestamp(date) # e.obs_date
    data.obs_date.doc = 'observing night'
    date = hdr['DATE']
    time = datetime.datetime.strptime(hdr['TIME'],'%H%M%S').time().strftime('%H:%M:%S')
    data.obs_time = Timestamp(date+'T'+time)
    data.obs_time.doc = 'time of observation'
    data.header = hdr
    data.rate = hdr['RATE']*u.Hz
    data.rate.doc = 'wfs frame rate'
    data.loop = hdr['LOOP']
    data.cent = hdr['CENT']
    mode = hdr['MODE']
    data.substate = substate = hdr['SUBSTATE']
    if mode.endswith('LGS'):
        mode = mode[:-3]
    if substate not in ['lgs','ngs']:
        print(f'{tcolor.warning}WARNING substate for {data.filename} is "{substate}"{tcolor.end}')
    data.mode = mode
    data.control_matrix_name = hdr['CONTROLM']
    
    H,A,C = get_HAC(mode,data.obs_date)
    data.H = H
    data.A = A
    data.C = C

    #data.n_modes = A.shape[1]
    nt1,nv = data.shape
    
    sigma = 1*u.arcsec
    if data.mode == '16x':
        data.centroider_gain = np.sqrt(2*np.pi)*sigma # gaussian tilt sensitivity
    elif data.mode == '8x':
        if data.cent in ['QUAD','BINQUAD']:
            data.centroider_gain = 1.5*np.sqrt(2*np.pi)*sigma
        elif data.cent == 'COG':
            data.centroider_gain = np.sqrt(2*np.pi)*sigma
    '''The theory is that the 8x optics blur the spot to 1.5 x the
    size of the 16x optics, BUT the COG algorithm mitigates this
    by taking a "true" COG.
    Calibrated using TTimages and telemetry data from 2021 engineering nights
    '''
        
    if data.mode == '16x':
        assert nv == 1382,'Data vector size and mode are not in agreement'
        data.ns = 144 # number of subapertures
        data.n_modes_max = 182 # number of non-zero singular modes
        data.d = 21.4*u.cm
        data.pixel_scale = 1.56*u.arcsec
        data.n_modes = 224
        
    elif data.mode == '8x':
        assert nv == 1174,'Data vector size and mode are not in agreement'
        data.ns = 40
        # data.r = 4
        # data.rs = 1
        data.n_modes = 48
        data.n_modes_max = data.n_modes # modify when we look at the SVD of C
        data.d = 42.8*u.cm
        data.pixel_scale = 2.06*u.arcsec
    else:
        raise Exception(f'{tcolor.fail}mode {data.mode} not supported (file = {data.filename}){tcolor.end}')

    data.ns_doc = 'number of subapertures'
    data.n_modes_doc = 'size of mode space (= number of poked modes)'
    data.n_modes_max_doc = 'number of non-zero singular modes'
    data.d.doc = 'size of subaperture'
    data.pixel_scale.doc = 'ShaneAODocument.pdf p36'
    data.centroider_gain.doc = 'conversion factor centroider output to ray tilt angle'
    data.dt = (1./data.rate).to(u.ms)
    data.dt.doc = 'sample period'
    data.nt = nt1 - 1
    data.nt_doc = 'number of time samples'

    slocs,smap = gen_slocs(data.mode)
    data.slocs = slocs
    data.smap = smap
    data.ap = np.where(slocs > 0,1,0)
    data.lam = 700.*u.nm
    data.lam.doc = 'center wavelength of wavefront sensor'
    data.D = 3*u.m
    data.D.doc = 'telescope diameter'
    
    ns = data.ns
    
    data.dx = [data.dt, u.Quantity(1)]
    data.name = os.path.splitext(data.filename)[0] #e.filename
    data.dsn = int(data.name[5:9]) # assumes 'Data_[0-9]{4}.fits'
    data.model = None
    
    # see https://lao.ucolick.org/ShaneAO/software-docs/telemetry.html
    ns = data.ns
    p = 0
    s = u.Quantity(data[1:,p:p+ns*2]).T; p = p+ns*2
    tt = u.Quantity(data[1:,p:p+2]); p = p+2
    at = u.Quantity(data[1:,p:p+1024]); p = p+1024
    aw = u.Quantity(data[1:,p:p+52]); p = p+52
    af = u.Quantity(data[1:,p:p+14]); p = p+14
    utt = u.Quantity(data[1:,p:p+2])
    
    data.s = s; data.s.name = 'wfs slopes'
    data.tt = tt; data.tt.name = 'tip/tilt sensed'
    data.at = at; data.at.name = 'tweeter commands'
    data.aw = aw; data.aw.name = 'woofer commands'
    data.af = af; data.af.name = 'filter states'
    data.utt = utt; data.utt.name = 'uplink tip/tilt commands'
    
    # bind instance methods to the data (an astropy.Quantity)
    data.plot = types.MethodType( _data_plot, data )
    data.set_star = types.MethodType( _data_set_star, data )
    data.calc_trans = types.MethodType( _data_calc_trans, data )
    data.power_spectrum = types.MethodType( _data_power_spectrum, data )
    data.est_r0 = types.MethodType( _data_est_r0, data )
    data.recon_phase = types.MethodType( _data_recon_phase, data)
    data.fit = types.MethodType( _data_fit, data)
    data.summary =  types.MethodType( _data_summary, data)
    
    # attach information about the star, if it is available
    data.set_star(dbObs,warn=False)
    
    return data
    
def _data_plot(self,model=True,fig=None):
    '''plot a data power spectrum on a log-log plot
    
    Argument:
    ---------
    self : u.Quantity array
        Telemetry data decorated with appropriate attributes (see Set.__init__)
    '''
    if not hasattr(self,'P'): self.power_spectrum()
    n = self.nt
    f,P = self.P.f[1:n//2], self.P[1:n//2]
    fig = get_fig(fig)
    if fig is None:
        first_one = True
        fig = plt.figure()
    else:
        first_one = False
    plt.autoscale(True)
    name = self.name.strip('.fits')
    dsn = p_dsn(name)
    date = p_date(self.obs_date)
    cent = self.header['cent']
    rate = self.rate
    mode = self.mode
    loop = self.loop
    label = f'{dsn:04d} {loop} {mode} {int(rate.value)} {cent}'
    if loop == 'closed':
        label += f' {self.substate}'

    plt.plot(f,P,label=label)
    ax = plt.gca()

    if first_one:
        ax.set_ylabel('nm$^2$ / Hz')
        ax.set_xlabel('frequency, Hz')
        ax.set_xscale('log')
        ax.set_yscale('log')
        plt.grid(1,which='both')
        
    #title = f'{date} {name} {mode} {rate} {cent}'
    title = self.P.doc
    plt.title(title)

    if model and hasattr(self,'model') and isinstance(self.model,Model):
        self.model.plot(fig=fig.number)
    if len(ax.texts) > 0:
        s = ax.texts[-1].get_text()
        ax.texts[-1].remove()
    else:
        s = ''
    
    if not hasattr(self,'trans'): self.calc_trans()
    if self.trans is not None:
        starname = self.star
        if isinstance(self.star,dict): starname = self.star['name']
        s = '\n'.join([s,f'{self.dsn}: T = {self.trans.value:0.3}% {starname}']).strip()
    else:
        if hasattr(self,'star'):
            if isinstance(self.star,str): starname = self.star
            elif isinstance(self.star,dict): starname = self.star['name'] + ' ???'
            else: starname = '' # self.star == nan
        else: starname = ''
        s = '\n'.join([s,f'{self.dsn}: T = ? {starname} no star data']).strip()
    
    ax.text(0.05,0.05,s,ha='left',va='bottom',transform=ax.transAxes,
            bbox = dict(facecolor='white',edgecolor='gray'))
        
    plt.legend()

def _data_set_star(self,star=None,warn=True):
    '''assign a star to a telemetry datum
    
    Argument:
    ---------
    star : dict or pandas.DataFrame
        The star dictionary should have key/values for:
            'name' : str
                name of the star that a simbad query can recognize
            'mV' : float
                V magnitude
            'B-V' : float
                B-V color (alternative: supply the star temperature as 'T')
            'T' : float
                Star temperature in degrees Kelvin (an alternative to 'B-V')
            'RA' : str
                RA of the star (a format that astropy.coords can deal with)
            'Dec' : str
                Dec of the star (a format that astropy.coords can deal with)
                
        Alternatively, star can be the observation database (dbObs is default)
        which will allow looking up the star's information.
    
    warn : bool
        whether or not to print a warning message if the star cannot be found
        in the observation database
    '''
    assert isinstance(star,(str,dict,pd.DataFrame)) or star is None
    if isinstance(star,pd.DataFrame) or star is None:
        db = star
        if db is None:
            db = dbObs
        if db is not None:
            date, filename = p_date(self.obs_date), self.filename
            q = dbObs[(dbObs['date'] == date) & (dbObs['file'] == filename)]
            if q.empty:
                expl = ''
                star = None
            else:
                q = q.iloc[0]
                star = {'name': q['object'],
                        'mV': q['mV'],
                        'B-V': q['B-V'],
                        'T': q['T'],
                        'RA':q['RA'],
                        'Dec':q['Dec'],
                        }
        else: # no star data available
            expl = ' (no observation stars database)'
            star = None
    # make compatible with photom's star definition
    if isinstance(star,dict):
        if 'mV' in star:
            star['mag'] = ('V',star['mV'])
        if 'B-V' in star:
            star['color'] = ('B-V',star['B-V'])
        elif 'T' in star:
            star['color'] = u.Quantity(star['T'],u.Kelvin)

    self.star = star
    self.calc_trans()
    star = self.star
    if isinstance(star,dict):
        self.db['star'] = star.get('name',np.nan)
        self.db['mV'] = star.get('mV',np.nan)
        if self.trans is None: self.db['trans'] = np.nan
        else: self.db['trans'] = self.trans.to('').value
    elif isinstance(star,str):
        self.db['star'] = star
        for x in ['mV','trans']: self.db[x] = np.nan
    else:
        self.star = np.nan
        for x in ['star','mV','trans']: self.db[x] = np.nan
        date,dsn = self.obs_date.date(), self.dsn
        if warn: print(f'{tcolor.warning}no star data for {date} [{dsn}] {expl}{tcolor.end}')
    return self

def _data_calc_trans(self):
    '''compute the transmissivity to the wavefront sensor in this observation.
    '''
    if hasattr(self,'star') and self.star is not None:
        try:
            t,star = photom.trans(self.star,self)
            self.star = star
        except Exception as e:
            print(str(e).replace(tcolor.fail,tcolor.warning))
            t = None
    else:
        t = None
    self.trans = t
    return self

def _data_power_spectrum(self):
    fs = self.rate
    dt = self.dt
    n = self.nt
    df = fs/n
    f = np.arange(n)*df
    if not hasattr(self,'ph'):
        self.recon_phase()
    ph = self.ph

    # P = ( 2*(np.abs(np.fft.fft(ph)*ph.unit*dt)**2).mean(axis=0)*df ).to(ph.unit**2/u.Hz) # astropy version 3
    P = ( 2*(np.abs(np.fft.fft(ph)*dt)**2).mean(axis=0)*df ).to(ph.unit**2/u.Hz) # astropy version 4

    P.f = f
    self.P = P
    date = self.obs_date.strftime('%Y-%m-%d')
    self.P.doc = f'average power spectrum of reconstructed phases {date}'
    return self

def _data_est_r0(self,iterate_r0=False,verbose=False):
    '''Convert the centroid measurements into actual slopes on sky,
    and estimate r0 from the rms slope values.
    
    Argument:
    ---------
    iterate_r0 : bool
        whether or not to try to solve the implicit dependence of r0
        in the centroids to slopes conversion. This is an iterative process
        because centroid to slopes function depends on Hartmann spot
        size which depends on r0 which depends rms slopes.
        Otherwise (iterate_r0 = False) the slopes are assumed linear
        in centroids and proportional to
        pixel scale regardless of r0 (slope = pixel_scale x centroid),
        which might be a reasonable approximation for COG centroiders in
        not too extreme seeing.
    
    Result:
    =======
    The r0 estimate is put in the r0 property. The slopes are put
    in the slopes property.
    
    Returns:
    --------
    The object (self), allowing chained calls.
    '''
    
    s = self.s
    
    if self.loop == 'closed':        
        sc = s * self.centroider_gain
        rms_tilt = np.sqrt(sc.var(axis=1).mean())
        self.slopes = sc
        self.slopes.doc = 'slopes assumed linear in centroids'
        self.rms_tilt = rms_tilt
    
    elif self.loop == 'open':
        name = self.name.strip('.fits')
        
        r00 = 12*u.cm # first guess
        if not hasattr(self,'r0'): self.r0 = {'lam0':550*u.nm}
        lam0 = self.r0['lam0'] # where r0 is calculated
        d = self.d
        lam = self.lam
        df = lut.df
        i = closest(df.r00,r00)
        max_iter = 5
        if iterate_r0:
            for iter in range(max_iter):
                spli = df.loc[i].spli
                sc = spli(s,ext=3)*u.arcsec  # ext=3 means clip at limit
                #rms_tilt = sc.std(axis=1).mean()
                rms_tilt = np.sqrt(sc.var(axis=1).mean())
                if np.isnan(rms_tilt) or rms_tilt > (6*u.arcsec):
                    self.slopes = sc
                    self.bad_data(s,sc)
                r0_est = ( d*rms_tilt**(-6/5)*(0.170)**(3/5)*(lam/d)**(6/5) ).to_da(u.cm) # formula 11 from Gavel, SPIE 2016
                r00_est = ( r0_est*(lam0/lam)**(6/5) ).to(u.cm)
                r00_est_r = r00_est.round(2)
                print(f'{r00_est_r}')
                i_new = closest(df.r00,r00_est)
                if i_new == i:
                    gprint(f'{name} converged to {r00_est_r}')
                    break
                i = i_new
            else:
                print(f'{tcolor.warning}warning: max iterations reached on r0 estimator for {name}, final value = {r00_est_r}{tcolor.end}')
            self.slopes = sc
            self.slopes.doc = 'nonlinearity-corrected slopes'
        else:
            sc = s*self.centroider_gain
            rms_tilt = np.sqrt(sc.var(axis=1).mean())
            r0_est = ( d*rms_tilt**(-6/5)*(0.170)**(3/5)*(lam/d)**(6/5) ).to_da(u.cm) # formula 11 from Gavel, SPIE 2016
            r00_est = ( r0_est*(lam0/lam)**(6/5) ).to(u.cm)
            r00_est_r = r00_est.round(2)
            if verbose: gprint(f'Data_{self.dsn:04d}: r0 = {r00_est_r}')
            self.db['r0'] = r00_est.value
            self.slopes = sc
            self.slopes.doc = 'slopes assumed linear in centroids'
    
        self.r0['r0 tilts'] = r00_est
        self.r0['rms tilt'] = rms_tilt
        self.rms_tilt = rms_tilt
        
    else:
        eprint(f'loop = {loop} is neither "open" or "closed"')
        raise Exception()
    
    return self

def _data_recon_phase(self,alpha=1.0,dewaffle=True,iterate_r0=False):
    n = {'16x':16,'8x':8}[self.mode]
    if dewaffle:
        R = fmatrix.recon_dewaffle(alpha=alpha,n=n,return_Fourier=False).real
    else:
        R = fmatrix.recon_Fourier(n=n).real

    if not hasattr(self,'slopes'): self.est_r0()
    sc = self.slopes
    d = self.d
    
    ns2,nt = sc.shape
    ns = ns2//2
    
    s2d = np.zeros((n*n*2,nt))*sc.unit
    s2d[self.smap,:] = sc[0:ns,:]
    s2d[n*n+self.smap,:] = sc[ns:ns2,:]
    
    phase = R @ s2d
    
    phase = ( u.Quantity(phase)*d ).to_da(u.nm)
    
    self.phase2d = phase.reshape((n,n,nt)).transpose([2,0,1]) # disp.play-able
    self.ph = phase[self.smap,:] # just the illuminated subaps, as vectors, ready for power spectral analysis

    if self.loop == 'open':
        # use Noll's formula for phase variance to get another estimate of r0
        D = self.D
        if not hasattr(self,'r0'): self.r0 = {'lam0':550*u.nm}
        lam0 = self.r0['lam0']
        k = 2*np.pi/lam0
        rms_phase = np.sqrt(self.ph.var(axis=1).mean())
        rms_phase_radians = (rms_phase*k).to_da('')
        D_over_r0 = (rms_phase_radians**2/0.111)**(3/5)
        r0 = (D/D_over_r0).to(u.cm)
        self.r0['r0 phase'] = r0
        self.r0['rms phase'] = rms_phase
        self.rms_phase = rms_phase
    
    return self

def _data_fit(self):
    if self.loop == 'open':
        if not hasattr(self,'P'): self.power_spectrum()
        if not hasattr(self,'model') or not isinstance(self.model,Model):
            self.model = Model().prep(self)
        self.model.fit()
        self.db['v'] = self.model.v.value
        self.db['Sn'] = self.model.Sn.value
    else:
        name = self.name.strip('.fits')
        wprint(f'closed loop data {name}: no model fit')
    return self

def _data_summary(self,tab='  ',form='text'):
    if form == 'text':
        f = StringIO()
        mode,rate,cent,loop = [getattr(self,x) for x in ['mode','rate','cent','loop']]
        gprint(f'{self.name} {mode} {rate} {cent} {loop}',file=f)
        if hasattr(self,'model') and isinstance(self.model,Model):
            print( tab + self.model.summary().replace('\n','\n'+tab) , file=f )
        if self.trans is not None:
            starname = self.star
            if isinstance(self.star,dict): starname = self.star['name']
            print( tab + f'star = {starname}' , file=f )
            print( tab + f'T = {self.trans.round(1)}',file=f)
        else:
            if hasattr(self,'star'):
                if isinstance(self.star,str): starname = self.star
                elif isinstance(self.star,dict): starname = self.star['name']
                else: starname = '(unknown)'
            else: starname = ''
            print( tab + f'star = {starname}', file=f)
            print( tab + f'T = ?' , file= f )
        s = f.getvalue().strip()
        f.close()
        return s
    elif form == 'dict':
        d = {}
        if hasattr(self,'model') and isinstance(self.model,Model):
            d = self.model.summary(form='dict')
        if self.trans is not None:
            starname = self.star
            if isinstance(self.star,dict): starname = self.star['name']
            d['star'] = starname
            d['trans'] = self.trans
        return d
    elif form == 'fits':
        c = []
        card = fits.Card('TELMFILE',self.filename,'AO telemetry data file')
        c.append(card)
        if 'UTC' in self.header:
            time = self.header['UTC']
        else:
            time = utc_from_local(self.obs_time).to_datetime().isoformat()
        card = fits.Card('TELMTIME',time,'UTC AO telemetry data time')
        c.append(card)
        if hasattr(self,'model') and isinstance(self.model,Model):
            c = c + self.model.summary(form='fits')
        if self.trans is not None:
            starname = self.star
            if isinstance(self.star,dict): starname = self.star['name']
            card = fits.Card('GIDESTAR',starname,'AO telemetry guide star')
            c.append(card)
            card = fits.Card('WFS_TRAN',self.trans.round(2).value,'% AO telemetry est trans to wfs')
            c.append(card)
        return c
    elif form == 'keywords':
        if hasattr(self,'model') and isinstance(self.model,Model):
            cmd = ['modify','-s','saoseeing',f'TELEMFILE={self.filename}']
            subprocess.run(cmd); print(' '.join(cmd))
            self.model.summary(form = 'keywords')
        else:
            return
        if self.trans is not None:
            starname = self.star
            if isinstance(self.star,dict): starname = self.star['name']
            wfstran = self.trans.round(2).value
        else:
            starname = '<undef>'
            wfstran = 0.0
        cmd = ['modify','-s','saoseeing',f'GUIDESTAR={starname}']
        subprocess.run(cmd); print(' '.join(cmd))
        cmd = ['modify','-s','saoseeing',f'WFS_THRUPUT={wfstran}']
        subprocess.run(cmd); print(' '.join(cmd))
        if 'UTC' in self.header:
            time_str = self.header['UTC']
        else:
            time_str = utc_from_local(self.obs_time).to_datetime().isoformat()
        time_str += '+00:00' # make datetime aware this is UTC
        time = datetime.datetime.fromisoformat(time_str).timestamp()
        cmd = ['modify','-s','saoseeing',f'TELEMTIME={time}']
        subprocess.run(cmd); print(' '.join(cmd))
        
def get_HAC(mode,date):
    ''' get the H,A and C matrices (modal-push, modal-vector, and control)
    matrices associated with a given mode.
    This may depend on the date of the data set, especially around the time
    of wavefront sensor upgrade in 2020, when the x and y axes of the
    WFS flipped at that point.
    
    Parameters
    ----------
    mode : str
        The mode '16x', '16xLGS', '8x', or '8xLGS'
    date : datetime.date
        The date of the observation
    
    Returns
    -------
    H,A,C as a tuple       
    
    '''
    if isinstance(date,str):
        date = pd.Timestamp(date)
    columns = ['name','mode','epoch','path']
    files_ = [
        ['A','16x','pre','reconMatrix_16x/At.fits'],
        ['H','16x','pre','reconMatrix_16x/H.fits'],
        ['C','16x','pre','controlMatrix_backup-2020-10-22/controlMatrix_16x.fits'],
        ['A','8x','pre','reconMatrix_8x/At.fits'],
        ['H','8x','pre','reconMatrix_8x/H.fits'],
        ['C','8x','pre','controlMatrix_backup-2020-10-22/controlMatrix_8x.fits'],
        ['A','16x','post','reconMatrix_16x/2020-10-21/At.fits'],
        ['H','16x','post','reconMatrix_16x/2020-10-21/H.fits'],
        ['C','16x','post','reconMatrix_16x/2020-10-21/controlMatrix_16x.fits'],
        ['A','8x','post','reconMatrix_16x/2020-10-21/At.fits'],
        ['H','8x','post','reconMatrix_16x/2020-10-21/H.fits'],
        ['C','8x','post','reconMatrix_16x/2020-10-21/controlMatrix_8x.fits'],
    ]
    files = [
        ['A','16x','pre','reconMatrix_16x/Backup_Pre2020Upgrade/At.fits'],
        ['H','16x','pre','reconMatrix_16x/Backup_Pre2020Upgrade/H.fits'],
        ['C','16x','pre','controlMatrix_backup-2020-10-22/controlMatrix_16x.fits'],
        ['A','8x','pre','reconMatrix_8x/Backup_Pre2020Upgrade/At.fits'],
        ['H','8x','pre','reconMatrix_8x/Backup_Pre2020Upgrade/H.fits'],
        ['C','8x','pre','controlMatrix_backup-2020-10-22/controlMatrix_8x.fits'],
        ['A','16x','post','reconMatrix_16x/2020-10-21/At.fits'],
        ['H','16x','post','reconMatrix_16x/2020-10-21/H.fits'],
        ['C','16x','post','controlMatrix_16x.fits'],
        ['A','8x','post','reconMatrix_8x/2020-10-19/At.fits'],
        ['H','8x','post','reconMatrix_8x/2020-10-19/H.fits'],
        ['C','8x','post','controlMatrix_8x.fits'],
    ]
    dbParam = pd.DataFrame(files,columns=columns)
    
    if mode.endswith('LGS'):
        mode = mode[:-3]
        lgs = True
    else:
        lgs = False
        
    #t_upgrade = pd.to_datetime('2020-10-25').date()
    #epoch = 'pre' if date < t_upgrade else 'post' # <--- this gets a deprication warning
    '''/Users/donaldgavel/astrobot/src/astrobot/telemetry_analysis.py:846:
    FutureWarning: Comparison of Timestamp with datetime.date is deprecated
    in order to match the standard library behavior. In a future version
    these will be considered non-comparable.Use 'ts == pd.Timestamp(date)'
    or 'ts.date() == date' instead. '''
    t_upgrade = pd.Timestamp('2020-10-25')
    epoch = 'pre' if date < t_upgrade else 'post'

    if paramDir is None:
        return None,None,None

    for matrix_name in ['H','A','C']:
        path = dbParam[(dbParam['name']==matrix_name) & (dbParam['mode'] == mode) & (dbParam['epoch'] == epoch)].iloc[0].path
        path = os.path.join(paramDir,path)
        with fits.open(path) as hdu:
            mat = hdu[0].data
            header = hdu[0].header
        info = {'name':matrix_name,'path':path,'hdr':header}
        
        if matrix_name == 'H':
            mat = H = u.Quantity(mat)
        if matrix_name == 'A':
            A = mat
            m,nx,ny = A.shape
            A = A.reshape((m,nx*ny)) # make it a 2d array so it can be transformed to a matrix
            mat = A = u.Quantity(A).T
        elif matrix_name == 'C':
            C = mat
            mat = C = u.Quantity(C[0:1024,0:288]) # we are only going to use the part that maps slopes to tweeter commands
        
        mat.__dict__.update(info)
    
    return H,A,C

def gen_slocs(mode):
    '''generate the x,y subaperture locations
    
    Parameters
    ----------
    mode : str
        The Hartmann sensor mode, either '16x' or '8x'
        
    Returns
    -------
    (slocs,smap) where
        slocs is a 2d array of size 16x16 or 8x8 with subapertures numbered.
        smap is a n_subaps length vector with integer values being the flattened-array
        indices of the subaps.
        
    '''
    if mode == '16x':
        ns = 144
        n = 16
        rp = 7
        rs = 2
    elif mode == '8x':
        ns = 40
        n = 8
        rp = 4
        rs = 2
    sap = img.circle((n,n),(n/2-.5,n/2-.5),rp)
    sap -= img.circle((n,n),(n/2-.5,n/2-.5),rs)
    smap = np.where(sap.reshape((n*n)))[0]
    slocs = np.zeros((n*n))-1
    slocs[smap] = list(range(ns))
    slocs = slocs.reshape((n,n))
    slocs = slocs.astype(int)
    smap = smap
    return slocs,smap

# ------------------------------
def closest(s,v):
    '''find the index of the item in s that is closest to v
    
    Parameters:
    -----------
    s : list or array or Series
        The array/list in which to search
    v : float or Quantity
        The value to search for
    
    Returns:
    --------
    int
        The index of s that is closest to v
    '''
    s = u.Quantity(s)
    return np.argmin(np.abs(s-v))

# --- easy date and data set number translators ---
# def p_date(date=None):
#     ''' parse the string "date", which has some reasonable text
#     representation of a date, and return the standard YYYY-mm-dd format
#     representation for the observing night. Time is assumed local time.
#     If a time after midnight but before noon is given, the result is
#     the -previous- day, since that is the date of the observing night.
#     
#     Argument:
#     ---------
#     date: str, datetime.time, datetime.datetime, or pandas.Timestamp
#         a "fuzzy" denotation of a date, either one of the date-like objects
#         above or a string that is recognizable by the
#         dparser.parse routine. Today/now year, month, day, hour, etc. are
#         default, including the case where date is an empty string ''
#     
#     Returns:
#     ========
#     str: observing night, in the form YYYY-mm-dd
#     
#     '''
#     if isinstance(date,(pd.Timestamp,datetime.date,datetime.datetime)): date = str(date)
#     now = datetime.datetime.now()
#     if date in ['','today','now',None]: date = str(now)
#     r = dparser.parse(date,fuzzy=True)
#     midnight = datetime.datetime(r.year,r.month,r.day)
#     noon = datetime.datetime(r.year,r.month,r.day,12)
#     if r > midnight and r < noon: #morning hours are previous night's observing run
#         r = r - datetime.timedelta(days = 1)
#     s = r.strftime('%Y-%m-%d')
#     return s

def p_dsn(dsn):
    ''' parse the data number list
    Argument:
    ---------
    dsn: int, list, or str
        A list of integers
        or a string which reasonably encodes the list of data set numbers.
        
    Examples:
    ---------
        dsn = [25, 26]
        dsn = '25, 26'  comma  separator
        dsn = '25 26'   space separator
        dsn = 'Data 24, 25 a comment'  extra text
        
    Returns:
    --------
        a list of integers
    '''
    if isinstance(dsn,list):
        r = [p_dsn(x) for x in dsn]
        return [int(x) for x in r if x is not None]
    elif isinstance(dsn,str):
        dsn = re.split(',|\s',dsn)
        if len(dsn) == 1:
            dsn = dsn[0]
        else:
            r = [p_dsn(x) for x in dsn]
            return [int(x) for x in r if x is not None]
    if isinstance(dsn,int):
        return dsn
    elif isinstance(dsn,str):
        pat = '\d+'
        r = re.search(pat,dsn)
        if r is not None:
            r = int(r.group(0))
        return r
    else:
        raise Exception(f'{tcolor.fail}failed on: {dsn}{tcolor.end}')

# =========================================================================================
# code to support generating centroider transfer curves using simulated PSFs and CCD pixels
# =========================================================================================

def psf(**kwargs):
    '''make a point spread function of the atmosphere + aperture
    
    Parameters
    ----------
    r0 : Quantity, length-like
        The Fried seeing parameter, defined at lam0
    d : Quantity, length-like, or None
        The diameter of the aperture. It is assumed circular unless
        d is a 2-vector, in which case it is rectangular, d[0] x d[1]
        If d is None, then the aperture is not used (i.e. the PSF
        is the 'infinite aperture' approximation)
    lam : Quantity, length-like
        The wavelength of the light
    lam0 : Quantity, length-like
        The wavelength at which r0 is defined
    n : int
        The number of pixels across the final PSF image
    platescale : Quantity, angle-like/pix
        The plate scale: e.g. 0.1*u.arcsec/u.pix
    
    Returns
    -------
    PSF : Quantity, 2-d array
        The point spread function. The object contains as attributes
        r0, d, lam, the MTF, and the structure function Dphi
    
    '''
    r00 = kwargs.pop('r0',10*u.cm)
    d = kwargs.pop('d',None)
    lam = kwargs.pop('lam',550*u.nm)
    lam0 = kwargs.pop('lam0',550*u.nm)
    n = kwargs.pop('n',256)
    platescale = kwargs.pop('platescale',0.01*u.arcsec/u.pix)
    r0 = ( r00*(lam/lam0)**(6/5) ).to(u.cm)
    
    if d is None:
        du = 4*r0/n
        MTF0 = 1
    else:
        if d.size == 1:
            du = d*4/n
            ap = u.Quantity.circle((n,n),r=d/2/du)
        else:
            du = d.max()*4/n
            ap = u.Quantity.rectangle((n,n),d,du)
        MTF0 = (ap.ft().abs()**2).ftinv()
        MTF0 = MTF0/MTF0.max()

    x = y = (np.arange(n)-n//2)*du
    x,y = np.meshgrid(x,y)
    r = np.sqrt(x**2+y**2).set_attributes(dx=[1,1]*du,x0=[1,1]*(-du*n/2))
    dtheta = (lam/(n*du)).to_da(u.arcsec)

    Dphi = ( c0*(r/r0)**(5/3) ).asQ(r)
    MTF = np.exp(-(1/2)*Dphi).asQ(Dphi).normalized('peak')
    PSF = (MTF*MTF0).ft().real
    
    PSF.set_attributes(dx=[1,1]*dtheta, x0=[1,1]*(-dtheta*n/2))
    PSF = u.Quantity(scipy.ndimage.shift(PSF,(-.5,-.5))).asQ(PSF) # half pixel shift to keep peak centered
    PSF = PSF.zoomto(platescale*n*u.pix).normalized('integral')
    PSF.set_attributes(r0=r0, r00=r00, lam=lam, lam0=lam0, d=d, Dphi=Dphi, MTF=MTF, MTF0=MTF0)
    
    return PSF

psf_tes_t_script = ''' test script
import telemetry_analysis as ta
import astropy.units as u
import numpy as np
r0 = 10*u.cm
d = [20,20]*u.cm
lam = 0.55*u.micron
lam0 = lam
platescale = 0.1*u.arcsec/u.pix
n = 256

du = 4*r0/n
x = y = (np.arange(n)-n//2)*du
x,y = np.meshgrid(x,y)
r = np.sqrt(x**2+y**2).set_attributes(dx=[1,1]*du,x0=[1,1]*(-du*n/2))
dtheta = (lam/(n*du)).to_da(u.arcsec)

c0 = ta.c0
MTF0 = 1
Dphi = ( c0*(r/r0)**(5/3) ).asQ(r)
MTF = np.exp(-(1/2)*Dphi).asQ(Dphi).normalized('peak')
PSF = (MTF*MTF0).ft().real

PSF.set_attributes(dx=[1,1]*dtheta, x0=[1,1]*(-dtheta*n/2))

psf = ta.psf(n=n,r0=r0,d=d,lam=lam,lam0=lam0,platescale=platescale)
'''

very_old_probably_doesnt_work_anymore_tes_t_script = '''test script

r0s = np.arange(5,30,1)*u.cm
sig_thetas = []
rms_tilts = []
for r0 in r0s:
    sig_theta = np.sqrt( 0.170*(d/r0)**(5/3)*(lam/d)**2 )
    PSF = psf(r0=r0,d=d,lam=lam)
    n = PSF.shape[0]
    G = PSF[n//2-1:n//2+1,:].sum()*PSF.dx[1] # y line integral. G is the spot size gain in units of 1/arcsec
    rms_tilt = 2*s.std(axis=1).mean()/G
    print(r0,sig_theta.to_da(u.arcsec),rms_tilt.to_da(u.arcsec))
    sig_thetas.append(sig_theta.to_da(u.arcsec))
    rms_tilts.append(rms_tilt.to_da(u.arcsec))
sig_thetas = u.Quantity(sig_thetas)
rms_tilts = u.Quantity(rms_tilts)
plt.figure()
plt.plot(r0s,sig_thetas,label='$\\sigma_\\theta$')
plt.plot(r0s,rms_tilts,label='$\\sigma_I / G$')
plt.legend()

np.sqrt( 0.170*(d/r0)**(5/3)*(lam/d)**2 ).to_da(u.arcsec)

reload(ta)
date = '2016-05-24'
dsn = [100,99]
ta.db_cache = dbc
ds = ta.Set(date,dsn)
ds.read()
ds.recon_phase()
ds.power_spectrum(plot=True)

ax = plt.gca()
ax.set_ylim(1,1e6)
ax.set_xlim(0.1,1000)

# transparency transparent window
win = fig.canvas.manager.window
win.attributes('-alpha',0.5)

from sauce2 import dimg
ol,cl = ds.data
a = 1000
speed=10
dimg.play(((cl.phase2d-cl.phase2d.mean(axis=0))*cl.ap)[0::speed,:,:],vmin=-a,vmax=a)
dimg.play(((ol.phase2d-ol.phase2d.mean(axis=0))*ol.ap)[0::speed,:,:],vmin=-a,vmax=a)

'''

legacy_code = False
if legacy_code:
    def findVol(date,dsn=None):
        """find the volume that has this data set.
        Returns the first volume found, along with the path to the file
        or, if dsn is None, the directory
        containing any telemetry data files
        """
        print('WARNING telemetry_analysis.findVol is depricated. Use finder.findVol instead')
        if not isinstance(date,str): #it could be a datetime.date
            date = str(date)
        if dsn is None:
            pass
        elif not isinstance(dsn,int): #assume it is a telemetry file name
            dsn = int(dsn[5:9])
        for vol in dataVols:
            paths = glob.glob(os.path.join(vol,'**/%s'%date),recursive=True)
            if len(paths) == 0: # empty, continue on to next volume in list
                continue
            path = paths[0]
            # otherwise, move on to checking for the data file
            if dsn is None:
                files = glob.glob(os.path.join(path,'**/Data_*.fits'),recursive=True)
                if len(files) == 0:
                    continue
                path = os.path.dirname(files[0])
                return vol,path
            files = glob.glob(os.path.join(path,'**/Data_%04d.fits'%dsn),recursive=True)
            if len(files) == 0:
                continue
            return vol,files[0]
        print('<telemetry_analysis.findVol> WARNING did not find data for %s in any of'%date)
        pp(dataVols)
        return None,None

    def dbase_old(top):
        '''Create a database (pandas dataFrame) of all the telemetry files
        in a directory and subdirectories
        
        Parameters:
        -----------
        top : str
            The name of the top-level directory. May have '.' and '~'
        '''
        global db
        topdir = os.path.abspath(os.path.expanduser(top))
        files = glob.glob(os.path.join(topdir,'**/*.fits'),recursive=True)
        telemFile = 'Data_[0-9]{4}.fits'
        datePattern = '[0-9]{4}-[0-9]{2}-[0-9]{2}$'
        files = [f for f in files if re.match(telemFile,os.path.basename(f))]
        nan = np.nan
        unk = 'unknown'
        rows = []
        for fn in files:
            path,name = os.path.split(fn)
            nodes = splitall(path)
            obs_date = nan
            for node in nodes:
                if re.match(datePattern,node):
                    obs_date = pd.to_datetime(node).date()
                    break
            with fits.open(fn) as hdu:
                hdr = hdu[0].header
                date = hdr.get('DATE',nan)
                if date is not nan:
                    date = pd.to_datetime(date).date()
                time = hdr.get('TIME',nan)
                if time is not nan:
                    time = '{}:{}:{}'.format(time[0:2],time[2:4],time[4:6])
                    time = pd.to_datetime(time).time()
                if time is not nan and date is not nan:
                    time = datetime.datetime.combine(date,time)
                else:
                    time = nan
                mode = hdr.get('MODE',unk)
                substate = hdr.get('SUBSTATE',unk)
                loop = hdr.get('LOOP',unk)
                rate = hdr.get('RATE',nan)
                gain = hdr.get('GAIN',nan)
                wgain = hdr.get('WGAIN',nan)
                gamma = hdr.get('TWEETER_',nan)
                wgamma = hdr.get('WOOFER_B',nan)
                cent = hdr.get('CENT',unk)
            row = [path,name,  obs_date,  time,  mode,  substate,  loop,  rate,  gain,  wgain,  gamma,  wgamma,  cent ]
            rows.append(row)
        columns=['dir','name','obs_date','time','mode','substate','loop','rate','gain','wgain','gamma','wgamma','cent']
        db = pd.DataFrame(rows,columns=columns)
        if db.empty:
            return db
        # sort by time and reindex
        db = db.sort_values(by='time').reset_index(drop=True)
        return db

    def dbaseVol(date,dsn):
        '''find the volume that has this date and dataset,
        and then create a database of that volume.
        
        Parameters:
        -----------
        date : str or datetime.date
            The observing date of interest
        dsn : str or int
            The data set number or telemetry filename as in Data_0411.fits
            (411 being the data set number)
        '''
        global db,db_cache
        if not isinstance(date,str): date = str(date)
        if not isinstance(dsn,int): dsn = int(dsn[5:9])
        if (db is None) or (date not in list(db.obs_date)):
            vol,f = findVol(date,dsn)
            if vol in db_cache:
                db = db_cache[vol]
            else:
                db = dbase(vol)
            db_cache[vol] = db
        return db

def tiltrms(r0,d,lam):
    '''
    calculate the expected value of rms tilt given seeing and wavelength.
    
    Arguments:
    ----------
    r0: Quantity, length-like
        Fried's seeing parameter
    d: Quantity, length-like
        subaperture "diameter" (there is a slight difference between circular
        and square subapertures, but we'll ignore that)
    lam: Quantity, length-like
        the wavelength of the light
    
    Returns:
    --------
    Quantity, angle-like
        The rms, single-axis, z-tilt of the wavefront on the subaperture
    
    Notes:
    ------
    The 1 axis tilt formula is
        tiltrms = ( np.sqrt( 0.170*(d/r0)**(5/3) )*(lam/d) ).to_da(u.arcsec)
    '''
    return ( np.sqrt( 0.170*(d/r0)**(5/3) )*(lam/d) ).to_da(u.arcsec)

from astrobot import atmos
import tqdm
import pandas as pd
seeing = atmos.seeing
pi = np.pi
i = 1j

# experiment results are here:
'''
# ======================================================================================
# These are the results of measuring the centroider transfer curves using simulated PSFs
# ======================================================================================
*** r0 vs tip rms ***
df comes from running experimentB()
Lookup table for r0 vs tip/tilt rms for COG centroider and wfs sensing wavelength of 700 nm:
df
       r00                 est_tipsrms                     tipsrms                xsrms         a         b        ab
   5.0 cm   0.6750038687545548 arcsec    0.541151564611388 arcsec   0.3366256587004669  0.672765  1.016361  0.683772
   7.0 cm   0.5099562265938733 arcsec   0.4118474697782501 arcsec  0.28232248434335133  1.973041  0.483158  0.953290
  10.0 cm   0.3788331125694943 arcsec  0.31229563751210165 arcsec   0.2608075275766453  2.390861  0.480590  1.149023
  12.0 cm  0.32543447679500953 arcsec   0.2759002793143123 arcsec  0.25221636638376277  2.438326  0.495004  1.206982
  15.0 cm   0.2702123636247057 arcsec  0.22524360967434137 arcsec  0.22543746836567127  2.592011  0.488040  1.265006
  17.0 cm  0.24344853444284326 arcsec  0.21069972450234628 arcsec  0.21550287781849767  2.648843  0.483626  1.281049
  20.0 cm   0.2126128957510849 arcsec  0.17513835751555906 arcsec   0.1918620366477874  2.571620  0.498050  1.280796
  22.0 cm  0.19637929574978932 arcsec   0.1755526359615217 arcsec  0.18684915210459102  2.658609  0.486372  1.293074

df.to_csv()
 [',r00,est_tipsrms,tipsrms,xsrms,a,b,ab',
 '0,5.0 cm,0.6750038687545548 arcsec,0.541151564611388 arcsec,0.3366256587004669,0.6727649012791247,1.0163606559737108,0.68377177638014',
 '1,7.0 cm,0.5099562265938733 arcsec,0.4118474697782501 arcsec,0.28232248434335133,1.9730410159913607,0.48315759179146456,0.9532897457921704',
 '2,10.0 cm,0.3788331125694943 arcsec,0.31229563751210165 arcsec,0.2608075275766453,2.390861316849044,0.480589529313541,1.1490229149184348',
 '3,12.0 cm,0.32543447679500953 arcsec,0.2759002793143123 arcsec,0.25221636638376277,2.4383255994196102,0.4950044004575572,1.206981901461018',
 '4,15.0 cm,0.2702123636247057 arcsec,0.22524360967434137 arcsec,0.22543746836567127,2.592010685535185,0.48804036156021063,1.2650058321365212',
 '0,17.0 cm,0.24344853444284326 arcsec,0.21069972450234628 arcsec,0.21550287781849767,2.648842558522112,0.48362590370083036,1.281048876126476',
 '1,20.0 cm,0.2126128957510849 arcsec,0.17513835751555906 arcsec,0.1918620366477874,2.5716198166258573,0.4980503240289077,1.2807960829496685',
 '2,22.0 cm,0.19637929574978932 arcsec,0.1755526359615217 arcsec,0.18684915210459102,2.6586094715504167,0.4863723246127235,1.2930740689153806',
 '']

*** Transfer Curve ***
running experimentA(... plan='scan') generates a transfer curve of centroider output vs
spot centroid in arcsconds, for a given r00.
Here are the spline fit (spl) and spline inverse (spli) for
====== r00 = 10 cm =====
---spl---
experimentA(ns=25,nexp=10,r00=10*u.cm,plan='scan')
tck = spl._eval_args
tck = (np.array([-2.34, -2.34, -2.34, -2.34, -1.17, -0.58,  0.  ,  0.58,  1.17,  2.34,  2.34,  2.34,  2.34]),
     np.array([-1.28, -1.47, -0.45, -0.54,  0.  ,  0.52,  0.51,  1.44,  1.28,  0.  ,  0.  ,  0.  , -1.29]),
     3)
spl = scipy.interpolate.UnivariateSpline._from_tck(tck)
usage:
    xs = spl(tip)
---spli---
tck = spli._eval_args
tck = (np.array([-1.31e+00, -1.31e+00, -1.31e+00, -1.31e+00, -1.23e+00, -1.09e+00, -9.04e-01, -7.29e-01, -5.96e-01, -5.29e-01, -4.94e-01, -4.40e-01, -3.30e-01, -1.76e-01, -6.74e-04,  1.73e-01,  3.25e-01,
         4.36e-01,  5.00e-01,  5.47e-01,  6.21e-01,  7.51e-01,  9.18e-01,  1.09e+00,  1.23e+00,  1.30e+00,  1.30e+00,  1.30e+00,  1.30e+00]),
        np.array([-2.15e+00, -2.59e+00, -1.41e+00, -1.94e+00, -1.50e+00, -1.40e+00, -1.22e+00, -1.06e+00, -7.25e-01, -5.16e-01, -3.67e-01, -1.82e-01, -1.26e-03,  1.82e-01,  3.68e-01,  5.27e-01,  7.43e-01,
                1.03e+00,  1.22e+00,  1.40e+00,  1.51e+00,  1.93e+00,  1.42e+00,  2.60e+00,  2.15e+00,  0.00e+00,  0.00e+00,  0.00e+00,  0.00e+00]),
        3)
spli = scipy.interpolate.UnivariateSpline._from_tck(tck)
usage:
    tip = spli(xs)
    
======= r00 = 20 cm =====
---spl---
tck = (np.array([-2.34, -2.34, -2.34, -2.34, -1.76, -1.17, -0.58,  0.  ,  0.39,  0.58,  1.17,  1.76,  2.34,  2.34,  2.34,  2.34]),
        np.array([-1.33, -1.44, -1.24, -0.37, -0.62, -0.08,  0.38,  0.53,  0.39,  1.27,  1.44,  1.32, -1.37, -1.47, -0.22, -0.49]),
        3)
---spli---
tck = (np.array([-1.34, -1.34, -1.34, -1.34, -1.15, -0.92, -0.69, -0.56, -0.49, -0.49, -0.46, -0.39, -0.21,  0.02,  0.25,  0.4 ,  0.47,  0.47,  0.51,  0.57,  0.73,  0.96,  1.18,  1.32,  1.35,  1.35,  1.35, 1.35]),
        np.array([-2.34, -1.29, -2.28, -0.53, -4.34,  3.47, -6.38,  1.41, -3.16,  1.95, -1.52,  1.49, -3.4 ,  6.97, -6.49,  4.32, -1.62,  3.55,  0.15,  1.65,  2.47,  0.56,  2.74,  2.14, -2.26, -1.46, -1.88, -0.38]),
        3
======== r00 = 15 cm ======
In [1058]: ta.experimentA(ns=25,nexp=10,r00=15*u.cm,plan='scan')
In [1059]: ta.spl._eval_args                                                                                
Out[1059]: 
(array([-2.34, -2.34, -2.34, -2.34, -1.17, -0.58,  0.  ,  0.58,  1.17,  1.76,  2.34,  2.34,  2.34,  2.34]),
 array([-1.29, -1.61, -0.36, -0.57,  0.05,  0.56,  0.41,  1.25,  1.43,  1.3 ,  0.  ,  0.  , -1.26, -1.69]),
 3)

In [1060]: ta.spli._eval_args                                                                               
Out[1060]: 
(array([-1.31, -1.31, -1.31, -1.31, -1.14, -0.92, -0.7 , -0.56, -0.51, -0.47, -0.47, -0.36, -0.2 ,  0.02,  0.23,  0.39,  0.46,  0.48,  0.51,  0.57,  0.73,  0.95,  1.17,  1.31,  1.35,  1.35,  1.35,
         1.35]),
 array([-2.34, -1.18, -2.14, -1.48, -1.27, -1.49, -0.8 , -1.18,  0.98, -1.31,  0.28, -0.11,  0.11,  0.51,  0.28,  0.9 ,  0.98,  1.32,  1.39,  1.35,  2.36,  0.79,  2.58,  2.15, -2.16, -1.52, -1.84,
        -0.53]),
 3)
===== r00 = 7 cm =========
In [1061]: ta.experimentA(ns=25,nexp=10,r00=7*u.cm,plan='scan')
In [1062]: ta.spl._eval_args                                                                                
Out[1062]: 
(array([-2.34, -2.34, -2.34, -2.34, -1.17,  0.  ,  0.58,  1.17,  2.34,  2.34,  2.34,  2.34]),
 array([-1.29, -1.18, -0.53, -0.22,  0.49,  0.54,  1.41,  1.19,  0.  ,  0.  ,  0.  ,  0.  ]),
 3)

In [1063]: ta.spli._eval_args                                                                               
Out[1063]: 
(array([-1.17, -1.17, -1.17, -1.17, -0.88, -0.52, -0.33,  0.19,  0.45,  0.73,  1.19,  1.19,  1.19,  1.19,  1.19,  1.19]),
 array([-2.23, -1.61, -1.68, -1.18,  0.  , -0.11,  0.72,  1.75,  1.42,  2.07,  1.9 ,  2.32, -2.27, -1.39, -2.08, -0.19]),
 3)
===== r00 = 22 cm ======
In [1066]: ta.experimentA(ns=25,nexp=10,r00=22*u.cm,plan='scan')
In [1067]: ta.spl._eval_args                                                                                
Out[1067]: 
(array([-2.34, -2.34, -2.34, -2.34, -1.76, -1.17, -0.58,  0.  ,  0.58,  1.17,  1.76,  2.34,  2.34,  2.34,  2.34]),
 array([-1.33, -1.45, -1.25, -0.35, -0.63,  0.02,  0.62,  0.37,  1.29,  1.43,  1.33,  0.  , -1.38, -1.49, -0.21]),
 3)

In [1068]: ta.spli._eval_args                                                                               
Out[1068]: 
(array([-1.34, -1.34, -1.34, -1.34, -1.15, -0.93, -0.7 , -0.56, -0.49, -0.48, -0.47, -0.38, -0.21,  0.02,  0.25,  0.4 ,  0.47,  0.48,  0.49,  0.58,  0.73,  0.96,  1.18,  1.32,  1.36,  1.36,  1.36,
         1.36]),
 array([-2.34e+00, -1.14e+00, -2.15e+00, -1.38e+00, -1.59e+00, -9.62e-01, -1.41e+00, -7.78e-01, -1.20e-01, -6.64e-01, -3.29e-02, -6.45e-04,  3.66e-02,  5.39e-01,  2.36e-01,  7.64e-01,  1.32e+00,
         1.02e+00,  1.55e+00,  1.31e+00,  2.35e+00,  8.45e-01,  2.63e+00,  2.15e+00, -2.16e+00, -1.44e+00, -1.95e+00, -3.16e-01]),
 3)
======  r00 = 12 cm ======
In [1069]: ta.experimentA(ns=25,nexp=10,r00=12*u.cm,plan='scan')
In [1070]: ta.spl._eval_args                                                                                
Out[1070]: 
(array([-2.34, -2.34, -2.34, -2.34, -1.17, -0.58,  0.  ,  0.58,  1.17,  2.34,  2.34,  2.34,  2.34]),
 array([-1.26, -1.57, -0.38, -0.57,  0.03,  0.56,  0.41,  1.6 ,  1.25,  0.  ,  0.  ,  0.  , -1.33]),
 3)

In [1071]: ta.spli._eval_args                                                                               
Out[1071]: 
(array([-1.28, -1.28, -1.28, -1.28, -1.11, -0.92, -0.57, -0.5 , -0.48, -0.44, -0.37,  0.23,  0.39,  0.45,  0.45,  0.5 ,  0.59,  0.72,  1.14,  1.28,  1.28,  1.32,  1.32,  1.32,  1.32]),
 array([-2.34e+00, -1.56e+00, -1.91e+00, -1.40e+00, -1.42e+00, -1.06e+00, -7.65e-01, -4.70e-01, -1.51e-01,  1.05e-02,  3.55e-01,  4.29e-01,  9.00e-01,  1.01e+00,  1.21e+00,  1.52e+00,  1.57e+00,
         1.90e+00,  2.43e+00, -2.66e+02,  2.14e+00,  0.00e+00,  0.00e+00,  0.00e+00, -2.33e+00]),
 3)

'''

def experimentA(ns = 10, r00 = 10*u.cm, plan = 'random',nexp=1):
    ''' experimental simulations of Kolmogorov + subaperture PSF
    and corresponding wavefront phase tips and centroider output.
    Expected tips rms is from equation 11 in Gavel, SPIE 2016.
    
    Parameters
    ----------
    ns : int
        number of simulated psfs in the experiment
    r00 : Quantity, length-like
        Fried seeing parameter, at the reference wavelength 550 nm
    plan : str
        Either 'random' or 'scan'.
        Random is random phase screens, so spot jumps around as expected
            in turbulence
        Scan generates one atmospheric spot then scans it across to generate
            a tranfer curve
    nexp : int
        Number of experiments to average over (used only in the plan='scan' case)
    '''
    try:
        if plan == 'scan' and not nexp > 4:
            raise Exception('nexp must be > 4 for a scan experiment')
        n = 512
        du = 1*u.cm
        dx = 6.5*u.arcsec/n # wide enough to contain all the cells
        d = 20*u.cm # size of subap
        centroider = 'COG'
        ws = [-1.5,-0.5,0.5,1.5]
        mode = '16x'
        pixelscale = {'16x':1.56*u.arcsec/u.pix, '8x':2.06*u.arcsec/u.pix}[mode]
        p = pixelscale*u.pix
        cells = []
        cells.append( u.Quantity.rectangle((n,n),[1,4]*p, origin=[-2,-2]*p, dx=dx) )
        cells.append( u.Quantity.rectangle((n,n),[1,4]*p, origin=[-1,-2]*p, dx=dx) )
        cells.append( u.Quantity.rectangle((n,n),[1,4]*p, origin=[0,-2]*p, dx=dx) )
        cells.append( u.Quantity.rectangle((n,n),[1,4]*p, origin=[1,-2]*p, dx=dx) )
        cellsum = u.Quantity(cells).sum(axis=0).asQ(cells[0])
        #ap = u.Quantity.circle((n,n),r=d/2,dx=du)
        ap = u.Quantity.rectangle((n,n),[1,1]*d,dx=du)
        lam = 700*u.nm
        k = 2*pi/lam
    
        seeing.r0 = r00
        a = atmos.Screen(n=n,du=du,seeing=seeing)
        ffs = []
        xs = []
        tips = []
        
        if plan == 'random':
            for enum in tqdm.tqdm(range(ns),ascii=not utf_enabled):
                ph = a.gen_screen().screen
                tip = ( ph.planeFit(ap=ap)[1]['tip']/(du/u.pix) ).to_da(u.arcsec)
                tips.append(tip)
                wf = ( ap*np.exp(i*k*ph) ).set_attributes(wavelength=lam)
                ff = ( wf.oft().abs()**2 ).normalized('integral').axto(u.arcsec)
                ff = ff.zoomto(6.5*u.arcsec)
                x = 0
                for w,cell in zip(ws,cells):
                    x += w*(cell*ff).integral().to('') # Hartmann centroider
                x /= (cellsum*ff).integral().to('') # intensity denomenator
                xs.append(x)
                ffs.append(ff)
        elif plan == 'scan':
            tips = np.linspace(-1.5,1.5,ns)*pixelscale*u.pix
            xss = []
            for exper in range(nexp):
                xs = []
                for tip in tqdm.tqdm(tips,ascii=not utf_enabled):
                    ph = a.gen_screen().screen
                    ph = ph - ph.planeFit(ap=ap)[0]
                    wf = ( ap*np.exp(i*k*ph) ).set_attributes(wavelength=lam)
                    ff = ( wf.oft().abs()**2 ).normalized('integral').axto(u.arcsec)
                    ff = ff.zoomto(6.5*u.arcsec)
                    shift = int(tip/ff.dx[0])
                    ff = np.roll(ff,shift,axis=1)
                    x = 0
                    for w,cell in zip(ws,cells):
                        x += w*(cell*ff).integral().to('') # Hartmann centroider
                    x /= (cellsum*ff).integral().to('') # intensity denomenator
                    xs.append(x)
                    ffs.append(ff)
                xss.append(xs)
            xss = u.Quantity(xss)
            xs = xss.mean(axis=0)
            # fit a spline to the data, and an inverse spline
            w = 1/xss.std(axis=0) # spline-fitting weights based on data variances
            spl = UnivariateSpline(tips,xs,w=w) # fit mean, given variances
            
            idx = np.argsort(xs)
            idx1 = []
            last = xs.min()
            for k in idx: # toss out the indices that are not monotonic, so we don't exchange data points
                if xs[k]>last:
                    idx1.append(k)
                    last = xs[k]
            idx = idx1
            spli =  UnivariateSpline(xs[idx],tips[idx],w=w[idx])
            
        xs = u.Quantity(xs)
        tips = u.Quantity(tips)
        
        # scatter plot correlating tips to centroid output
        plt.figure()
        
        if plan == 'random':
            plt.plot(tips,xs,'.')
        elif plan == 'scan':
            for xse in list(xss):
                plt.plot(tips,xse,'.')
            plt.plot(tips,spl(tips),label='spline fit')
            plt.plot(spli(xs),xs,label='inverse spline')
            plt.legend()
        
        title = 'COG $r_{00}$ = %s'%seeing.r0.round(2)
        plt.title(title)
        plt.xlabel('tip, arcsec')
        plt.ylabel('centroider output')
        plt.grid(1)
        
        if plan == 'random':
            # fit a sigmoid to the data
            xdata = tips.value
            ydata = xs.value
            
            r = optimize.curve_fit(sigmoid,xdata,ydata)
            
            a,b = r[0]
            xrange = [-1,1]*u.arcsec
            x = np.linspace(xrange[0],xrange[1],100)
            y = sigmoid(x,a/u.arcsec,b)
            formula = '$b\\frac{e^{ax}-e^{-ax}}{e^{ax}+e^{-ax}}$'
            label = '%s: a = %s, b = %s'%(formula,a.round(2),b.round(2))
            
            plt.plot(x,y,label=label)
            
            plt.legend()
            plt.grid(1)
            
            # expected rms tip given r0:
            r0 = ( seeing.r0*(lam/seeing.lam0)**(6/5) ).to(u.cm)
            est_tipsrms = ( np.sqrt( 0.170*(d/r0)**(5/3) )*(lam/d) ).to_da(u.arcsec)
        
            # histogram of tips
            tipsrms = tips.std()
            plt.figure()
            bins = int(np.round(np.clip(ns/10,3,50)))
            xrange = [-1,1]*u.arcsec
            binsize = (xrange[1]-xrange[0])/bins
            label = 'tips, rms = %s'%tipsrms.round(2)
            
            plt.hist(tips.value,range=tuple(xrange.value),bins=bins,label=label)
            
            x = np.linspace(xrange[0],xrange[1],100)
            y = u.Quantity( np.exp(-(1/2)*(x/est_tipsrms)**2) )
            y.dx = u.Quantity([x[1]-x[0],])
            y = y.normalized('integral')*ns*binsize
            label = 'expected, $r_0$ = %s, rms = %s'%(r0.round(2),est_tipsrms.round(2))
        
            plt.plot(x,y,label=label)
        
            plt.title(title)
            plt.xlabel('tip, arcsec')
            plt.legend()

    finally:
        globals().update(locals())

def experimentB():
    '''create a table of expected and experimental tip stdevs vs r0
    and the experimental centroider output stdev.
    a and b are the best-fit parameters of a sigmoid y = b*tanh(ax)
    where x = tip angle and y = centroider output
    '''
    global df
    r0s = [5,7,10,12,15,17,20,22]*u.cm
    columns = ['r00','est_tipsrms','tipsrms','xsrms','a','b']
    rows = []
    for r0 in r0s:
        experimentA(ns=100,r00 = r0)
        row = [r0,est_tipsrms,tipsrms,xs.std(),a,b]
        print(row)
        plt.pause(.1)
        rows.append(row)
    df = pd.DataFrame(rows,columns=columns)
    plt.plot( u.Quantity(df.xsrms), u.Quantity(df.r00),'x-')
    return df

def sigmoid(x,a,b):
    '''sigmoid function for fitting to the centroid function curve
    '''
    exp = np.exp
    return b*(exp(a*x)-exp(-a*x))/(exp(a*x)+exp(-a*x))

def splitall(path):
    '''split all the path nodes given a directory path
    '''
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path: # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts

# ========================================================
# code tests
# =========================================================

def test(date='2015-09-03',dsn=[411,412]):
    global s
    s = Set(date=date,dsn=dsn).read().recon_phase()
    s.power_spectrum(plot=True)

# good data sets
goodds = [('2021-04-28',[55,56]),
          ('2015-09-03',[411,412]),]
goodds = [{x:y for x,y in zip(['date','dsn'],z)} for z in goodds]

def test1():
    for g in goodds:
        ds = Set(**g)
        m = Model().prep(ds)
        m.fit()
        ds.plot()

def test2():
    global ds,m
    ds = Set()
    m = Model().prep(ds)
    m.fit()
    print(m.params)
    ds.plot()

def test3():
    date = '2021-07-16'
    dsn = [3,4,5,6,7,8,9]  # or dsn='all'
    ds = Set(date,dsn).fit()
    #[data.plot() for data in ds.datasets if data.loop == 'open']
    opens = [x for x in ds.datasets if x.loop == 'open']
    for data in opens: data.plot()
    print(ds.dbData)
    print(ds.dbModels)
    opens = ds.db[ds.db.loop == 'open']
    ds.plot(opens)

def tests():
    '''run all the tests
    '''
    ta = sys.modules[__name__]
    ts = [x for x in dir(ta) if 'test' in x and x != 'tests']
    for t in ts:
        getattr(ta,t)()
        plt.pause(.01)
    return

# --------------------------------------------------------
#   none of the following tests are expected to work given
#   the present state of code revision
def old_tes(alpha=1.0,sigma=0.,dewaffle=True,skip=5):
    global s
    s = Set()
    s.read()
    s.recon_phase(alpha=alpha,sig=sigma,dewaffle=dewaffle)
    
    p1 = s.disp_acts(skip=skip,remove_average=True,display=False)
    p1 = p1/p1.max()
    p2 = s.disp_phase(skip=skip,remove_average=True,display=False)
    p2 = p2/p2.max()
    
    dt = 1./float(s.rate)
    times = np.arange(0,s.nt,skip)*dt
    tunits = 'sec'
    # reconstruction acts are the opposite sign of phase
    label='alpha = '+str(alpha)+' sigma = '+str(sigma)
    dimg.play([-p1,p2],times=times,label=label,tunits=tunits)
    dimg.plt.xlabel('open loop | closed loop (residual)')
    dimg.plt.ylabel('phase | -acts')
    
def old_tes1(alpha=[0.0,1.0],sigma=[0],skip=5):
    global s,p
    print('loading data sets'); sys.stdout.flush()
    s = Set()
    s.read()
    p = []
    assert isinstance(alpha,list)
    assert isinstance(sigma,list)
    for al in alpha:
        if (al > 0): dewaffle = True
        else: dewaffle = False
        p1 = []
        for sig in sigma:
            print('processing alpha = '+str(al)); sys.stdout.flush()
            s.recon_phase(alpha=al,sig=sig,dewaffle=dewaffle)
            p1.append(s.p_ol[::skip])
        p.append(p1)
    n,d,ds = 32,30,8
    ap = img.circle((n,n),(n/2,n/2),d/2.) - img.circle((n,n),(n/2,n/2),ds/2.)
    p = p*ap
    for i in range(len(p)):
        for j in range(len(p[0])):
            p[i,j] = p[i,j] - np.average(p[i,j],axis=0)
    t = np.arange(0,s.nt,skip)*s.dt
    label = s.date+' '+str(s.dsn)+' alpha = '+str(alpha)+' sigma = '+str(sigma)
    dimg.play(p,label=label,times=t,tunits='sec')
    dimg.plt.xlabel('sigma (smoothing)')
    dimg.plt.ylabel('alpha (dewaffle)')

def old_tes2(pair=None):
    """plot open-close spectra on all data sets
    in the database
    """
    if (not 'db' in dir()):
        available()
    dboo = db[db['cent']=='COG'].sort(['date','time'])
    if (pair is not None):
        # are they in the database and an open/closed loop pair?
        if (pair[0] not in dboo.index or pair[1] not in dboo.index):
            print('ERROR '+str(pair)+' not in COG datasets')
            return
        if ((dboo.loc[pair[0]]['loop'] != 'open') or
           (dboo.loc[pair[1]]['loop'] != 'closed')):
            print('ERROR '+str(pair)+' not an open/closed loop pair')
            return
        _test2_helper(pair)
        return
        
    dirs = set(dboo['directory'])
    pairs = []
    for d in dirs:
        dbt = dboo[dboo['directory']==d]
        rates = set(dbt['rate'])
        for rate in rates:
            db2 = dbt[dbt['rate']==rate]
            dbol = db2[db2['loop']=='open']
            dbcl = db2[db2['loop']=='closed']
            while (len(dbol) > 0 and len(dbcl) > 0):
                # find the first open-closed loop pair
                pair = [dbol.index[0], dbcl.index[0]]
                pairs.append(pair)
                dbol = dbol.drop(pair[0])
                dbcl = dbcl.drop(pair[1])
    print('pairs: ',pairs)
    for pair in pairs:
        _test2_helper(pair)    
    return pairs

def old__tes2_helper(pair):
    directory = db.loc[pair[0]]['directory']
    dsn = [db.loc[pair[0]]['number'],
           db.loc[pair[1]]['number']]
    dsn = list(map(int,dsn))
    filePair = [db.loc[pair[0]]['file'],
                db.loc[pair[1]]['file']]
    print('evaluating data sets '+str(pair)+ \
          ' files '+ filePair[0]+', '+filePair[1]+' from '+directory)
    sys.stdout.flush()
    s = Set(date=directory,dsn=dsn)
    s.read()
    s.power_spectrum()
    plt.show()

def old_tes3():
    s = Set()
    s.read()
    s.power_spectrum(y='phases')
    f = s.ps_phases[0]
    df = f[0]
    pegf = 10. # Hz
    n0 = int(pegf/df)
    a = s.ps_phases[1][n0]
    plt.loglog(f,a*(f/pegf)**(-8./3.))
    xy = f[1], a*(f[1]/pegf)**(-8./3.)
    dimg.plt.annotate('-8/3',xy=xy)
    plt.loglog(f,a*(f/pegf)**(-4./3.))
    xy = f[1], a*(f[1]/pegf)**(-4./3.)
    dimg.plt.annotate('-4/3',xy=xy)
    
    s.power_spectrum(y='acts')
    a = s.ps_acts[1][n0]
    plt.loglog(f,a*(f/pegf)**(-8./3.))
    xy = f[1], a*(f[1]/pegf)**(-8./3.)
    dimg.plt.annotate('-8/3',xy=xy)
    plt.loglog(f,a*(f/pegf)**(-4./3.))
    xy = f[1], a*(f[1]/pegf)**(-4./3.)
    dimg.plt.annotate('-4/3',xy=xy)
    
    s.power_spectrum(y='slopes')
    a = s.ps_slopes[1][n0]
    plt.loglog(f,a*(f/pegf)**(-2./3.))
    xy = f[1], a*(f[1]/pegf)**(-2./3.)
    dimg.plt.annotate('-2/3',xy=xy)
    
    s.power_spectrum(y='x slopes')
    a = s.ps_x_slopes[1][n0]
    plt.loglog(f,a*(f/pegf)**(-2./3.))
    xy = f[1], a*(f[1]/pegf)**(-2./3.)
    dimg.plt.annotate('-2/3',xy=xy)
    plt.loglog(f,a*(f/pegf)**(-5./3.))
    xy = f[1], a*(f[1]/pegf)**(-5./3.)
    dimg.plt.annotate('-5/3',xy=xy)
    
    s.power_spectrum(y='y slopes')
    a = s.ps_y_slopes[1][n0]
    plt.loglog(f,a*(f/pegf)**(-2./3.))
    xy = f[1], a*(f[1]/pegf)**(-2./3.)
    dimg.plt.annotate('-2/3',xy=xy)
    plt.loglog(f,a*(f/pegf)**(-5./3.))
    xy = f[1], a*(f[1]/pegf)**(-5./3.)
    dimg.plt.annotate('-5/3',xy=xy)

    exec(compile(open('local/pymodules/komog_temporal.py', "rb").read(), 'local/pymodules/komog_temporal.py', 'exec'))

def old_tes_recon():
    sdum = Set().read()
    n = 16
    ns = 144
    G = fmatrix.grad(n=n,circulant=True)
    R = np.real(fmatrix.recon_Fourier(n=16))
    p = np.zeros((n,n))
    p[:,n/2:] = 1.0
    #ap = img.circle((16,16),r=7)
    #p *= ap
    p = np.matrix(p.flatten()).T
    s = G*p
    p_hat = R*s
    p_hat = np.array(p_hat).reshape((n,n))
    fp = np.fft.fft2(p_hat)
    fp = np.fft.fftshift(fp)
    fp = img.zeropad(fp,(2*n,2*n))
    fp = np.fft.ifftshift(fp)* (2*n/n)**2
    p_hat = np.real(np.fft.ifft2(fp))
    s2 = np.array(s).flatten()
    s = np.array([s2[sdum.smap],s2[sdum.smap+ns]]).flatten()
    s2 = s2.reshape(2,n,n)
    pr = sdum.recon_phase(s).reshape((2*n,2*n))
    return pr,p_hat,s2,s,p

def do_graphs(version='v2',kind='phases',save_graphs = False):
    """do the graphs for the paper
    """
    if 'running_jupyter' in globals():
        if running_jupyter:
            save_graphs = False
    date = '2016-05-24'
    figDir = os.path.join(home,'Desktop','SPIE_2016','Telemetry_Graphs')
    # create unique subdirectory using the current date and time
    subDir = datetime.datetime.now().isoformat().split('.')[0]
    figDir = os.path.join(figDir,subDir)
    os.makedirs(figDir)
    
    kinds = ['phases','zernikes','all']
    if kind not in kinds:
        raise Exception('kind must be one of %r'%kinds)
    dsns = [[7,6], [86,85], [100,99],[126,125], [134,133], [148,147]]
    for dsn in dsns:
        s = Set(date,dsn=dsn).read()
        s.recon_phase()
        print(f'r0 estimates for data set {dsn[0]}: tilt: {s.data[0].r0_tilt_var.round(2)}, phase: {s.data[0].r0_phase_var.round(2)}')
        if kind in ['phases','all']:
            figName = 'phases_powerSpec_%d,%d_%s.png'%(dsn[0],dsn[1],version)
            s.power_spectrum_plot('phases',r0=.13,v=4.,r1=.2,pegf=10.,dual_plot=False)
            plt.ylim(1,1.e6)
            plt.draw()
            try:
                plt.pause(.01)
            except:
                pass
            if save_graphs:
                plt.savefig(os.path.join(figDir,figName))
        if kind in ['zernikes','all']:
            r0 = 0.13
            figName = 'zernikes_%d_r0=%dcm_%s.png'%(dsn[0],np.round(r0*100).astype(int),version)
            s.zernike(r0=r0)
            plt.draw()
            try:
                plt.pause(.01)
            except:
                pass
            if save_graphs:
                plt.savefig(os.path.join(figDir,figName))
            
# -------------------------------------------
#   Help

system_help = help
python = 'python'
def help(m=None):
    '''get help on telemetry_analysis module routines, or any other python
    function. If you need the traditional python interactive help, use
    system_help()
    '''
    if m is None:
        pydoc.pager('telemetry_analysis - '+__doc__)
        return
    elif m == 'python':
        system_help()
    else:
        system_help(m)

# ---------------------------------------------------
#   Exectable
class parser(object):
    def __init__(self):
        params = dict(
            date = dict(pat='\d{4}-\d{2}-\d{2}|today',kind=1,key=['-d','--date']),
            dsn =  dict(pat='^\d+|^\[\d+',kind=1,key=['-n','--dsn']),
            star = dict(pat='^HD',kind=1,key=['-s','--star']),
            plot = dict(pat='',kind=0,key=['-p','--plot']),
            help = dict(pat='',kind=0,key=['-h','--help']),
        )
        params = pd.DataFrame(params).transpose()
        params['value'] = ''
        self.params = params
        return
    
    def parse(self,args):
        df= self.params
        df['value'] = '' # reset to default
        # if isinstance(args,str):
        #     args = shlex.split(args) # preserve spaces in quoted strings
        q = deque(args)
        if '--' in q:
            p = ''
            while p != '--': p = popleft(q)
        r = {}
        mode = None
        while len(q) > 0:
            arg = q.popleft()
            param = df[[arg in x for x in df['key']]]
            if not param.empty: # flag or key value pair
                param = param.iloc[0]
                if param.kind == 1: # has a succeding value
                    val = q.popleft()
                    df.at[param.name,'value'] = val
                else:
                    df.at[param.name,'value'] = 'True'
            else: # stand alone. Try to decypher based on format
                for ind,e in df.iterrows():
                    if re.match(e.pat,arg):
                        break
                else:
                    raise Exception(f'{tcolor.fail}not a valid argument: [{arg}]{tcolor.end}')
                df.at[e.name,'value'] = arg
        return dict(df['value'])
    
def run():
    global ds, date,star,dsn,star_info
    
    debug=False
    if debug: print(f'{tcolor.green}Running main program{tcolor.end}')
    if debug: print(f' sys.argv[1:] = {sys.argv[1:]} ')
    args = sys.argv[1:]
    if '--' in args: args.remove('--')
    if debug: print(f' args = {args} ')
    
    p = parser()
    r = p.parse(args)
    if debug: print(f'parsed as: {r}')
    if r['help']:
        pydoc.pager(__doc__)
        return
    date,dsn,star,plot = (r[x] for x in ['date','dsn','star','plot'])
    if dsn:
        if dsn != 'all':
            dsn = [int(x) for x in dsn.strip('[]').split(',')]
    if date and not dsn: dsn = 'all'
    if dsn and not date: date = 'today'
    plot = plot not in ['',None,0,'no','No',False,'False']
    # 
    if date and dsn:
        ds = Set(date,dsn)
        if not hasattr(ds,'data'): # failed to load data sets (e.g. no files)
            return
        print('processing ...',flush=True)
        for d in ds.datasets:
            if star: d.set_star(star)
            if d.loop == 'open': d.fit()
            else: d.power_spectrum()
            print(d.summary(),flush=True)
            
        # FITS card dump of seeing analysis results
        fits_cards = False
        keywords = True
        if fits_cards:
            try:
                print('')
                for d in ds.datasets:
                    if d.loop == 'open':
                        for x in d.summary(form='fits'):
                            print(x)
                print('')
            except:
                print(f'{tcolor.fail}failed to do a FITS card dump{tcolor.end}')

        if keywords:
            for d in ds.datasets:
                if d.loop == 'open':
                    d.summary(form='keywords')
                    
        if plot:
            if not sys.flags.interactive:
                plt.ioff()
                ds.plot()
                plt.show()
                plt.ion()
            else:
                ds.plot()
        
    elif star:  # star only, look it up
        star_info = photom.lookup_star(star,bail=False)
        if 'error' in star_info: print(star_info['error'])
        else: print(DotDict(star_info))
        
atexit.register(photom.save_star_cache)

if __name__ == '__main__':
    run()
