#
# bad pixel median
#
import os
import numpy as np
import scipy
import astropy.io.fits as pyfits

import matplotlib.pyplot as plt

from astrobot import fits
from astrobot import img
from astrobot import dimg

plt.ion()

n = 2048
field_center = [747,1090] # pixels [row,col]
field_radius = 450 # pixels
badCrit = 0.25 # criterion for bad pixels (fraction of normalized flat's average)

ap = img.circle((n,n),field_center,field_radius)

def ur_color():
    """Load 3 files with color information,
    subtract darks and de-bad.
    Return as 3-data-cube
    """
    ur_J = fits.readfits('s3392.fits')
    ur_J -= fits.readfits('s3394.fits')
    ur_H = fits.readfits('s3387.fits')
    ur_H -= fits.readfits('s3389.fits')
    ur_K = fits.readfits('s3397.fits')
    ur_K -= fits.readfits('s3399.fits')
    ur = []
    for u in [ur_K,ur_H,ur_J]:
        u = np.clip(u,0.,65000.)
        udb = bad_med(u,bad,ap)
        ur.append(udb)
    return ur

def pn_color():
    """Load 3 files with color information on
    Planetary Nebula IC2003
    De-bad and return as a 3-data-cube
    """
    ic_J = fits.readfits('s3414.fits')
    ic_J -= fits.readfits('s3416.fits')
    ic_J = img.shift(ic_J,(0,5))
    ic_H = fits.readfits('s3409.fits')
    ic_H -= fits.readfits('s3411.fits')
    ic_K = fits.readfits('s3419.fits')
    ic_K -= fits.readfits('s3421.fits')
    ic = []
    for u in [ic_K,ic_H,ic_J]:
        u = np.clip(u,0.,1000.)*65.
        udb = bad_med(u,bad,ap)
        ic.append(udb)
    return ic

def bad_med(a,bad,ap,w=3,verbose=False):
    """Replace bad pixels by the median of good-pixel neighbors
    a = original image
    bad = bad pixel map
    ap = aperture
    w = half-width of median filter
    """
    bad = bad*ap
    good = 1-bad
    bad_ind = np.nonzero(bad*ap)
    q = list(zip(bad_ind[0],bad_ind[1]))
    if verbose: print((a.shape))
    ar = a.copy()
    for xy in q:
        a_region = a[xy[0]-w:xy[0]+w+1,xy[1]-w:xy[1]+w+1].flatten()
        good_region = good[xy[0]-w:xy[0]+w+1,xy[1]-w:xy[1]+w+1].flatten()
        good_ones = np.take(a_region,np.nonzero(good_region))
        v = np.median(good_ones)
        ar[xy[0],xy[1]] = v
    return ar

def bad_find(d,ap,m=0,sd=1):
    """Determine the bad pixels given data, preferably dark or smooth
    flat-field data.
    d = data
    ap = aperture
    m = mean
    sd = std deviation
    """
    da = np.abs(d-m)*ap
    bad = np.where(da > 3*sd,1,0)
    pc = np.sum(bad)/np.sum(ap)
    print(('<bad_find> ' + str(pc*100)+' % of the pixels are bad'))
    return bad

def hist(a,nbins=50):
    """Display a histogram of the data that is in the aperture
    """
    ind = np.nonzero(ap.flatten())[0]
    u = a.flatten()[ind]
    plt.hist(u,nbins)

def avg_darks(date='15',debad=True):
    """Find and average the dark frames
    """
    files14 = [
        's1426.fits',
        's1427.fits',
        's1428.fits',
        's1429.fits',
        's1430.fits',
        's2511.fits',
        's2512.fits',
        's2513.fits',
        's2514.fits',
        's2515.fits',
    ]
    files15 = [
        's3574.fits',
        's3575.fits',
        's3576.fits',
        's3577.fits',
        's3578.fits',
        's3579.fits',
        's3580.fits',
        's3581.fits',
        's3582.fits'
    ]
    
    if (date == '14'): files = files14
    if (date == '15'): files = files15
    
    rejects = [ 's1430.fits', 's3578.fits']
    for r in rejects:
        if (r in files):
            files.remove(r)
    
    d_set = []
    k = 0
    for filename in files:
        d = fits.readfits(filename)
        k += 1
        if (debad):
            print(('de-badding '+str(k)+'/'+str(len(files))))
            d = bad_med(d,bad,ap)
        d_set.append(d)
    
    d_ave = np.average(np.array(d_set),0)
    return d_ave,d_set,files

def process(object='Uranus',band='H',date='15',dark=None,ctr=[1090,747],alpha=[0.1,0.0]):
    global files,u_set,ub,ubdr
    
    if (dark == None):
        print('<process> averaging darks')
        dark,d_set,f_set = avg_darks(date)
        
    files=[]
    for nr in range(5):
        files.append('s'+('%04d' % (nr+3387))+'.fits')
    
    u_set = []
    for filename in files:
        u_set.append(fits.readfits(filename)-dark)
        
    # remove bad pixels by median method
    ub = []
    k=0
    for u in u_set:
        k += 1
        print(('de-badding '+str(k)+'/'+str(len(u_set))))
        u = bad_med(u,bad,ap)
        ub.append(u)
    
    #return ub
    ##fix optical distortion
    print('<process> fixing distortion')
    ubdr = fix_distortion(ub,ctr=ctr,alpha=alpha)
    print('done fixing distortion')

    # register images to center of box-5

    # rough shift positions (edge of rings)
    p1 = np.array([
        (1195,792),
        (1320,669),
        (1074,666),
        (1076,910),
        (1320,910)
    ])
    p2 = np.array([
        (979,730),
        (1104,607),
        (859,606),
        (861,850),
        (1103,846)
    ])
    p = ((p1+p2)/2.).astype(int)
    #p = np.array([(760,1088),(639,1212),(638,967),(878,969),(878,1212)])
    p0 = p[0].copy()
    for k in range(len(p)):
        p[k] = -p[k] + p0
    ur = []
    #ctr = (p0[0],p0[1])
    #template_ap = img.circle((n,n),ctr,100) # this disk includes the whole planet
    #sub_template = img.circle((n,n),ctr,50) # this disk is inside the planet
    #thresh = np.sum(ubdr[0]*sub_template)/np.sum(sub_template)*0.5
    #x1 = np.arange(float(n))
    #x,y = np.meshgrid(x1,x1)
    #first = True
    for u,offset in zip(ubdr,p):
        u = img.shift(u,(offset[0],offset[1]))
        #uap = u*template_ap
        #template = np.where(uap > thresh,1,0 )
        #cx = np.sum(x*template)/np.sum(template)
        #cy = np.sum(y*template)/np.sum(template)
        #if (first):
        #    c0 = (cx,cy)
        #    first = False
        #else:
        #    c = (-cy+c0[1],-cx+c0[0])
        #    u = scipy.ndimage.interpolation.shift(u,c)
        #print str((cx,cy))
        ur.append(u)
    
    return ur

def fix_distortion(a_set,ctr = None, alpha=[0.02, 0.0]):
    """Assuming a 'barrel' model, fix the distortion in a set of images
    """
    x1 = (np.arange(float(n)) - float(n)/2. ) / (float(n)/2.) 
    x,y = np.meshgrid(x1,x1)
    if (ctr == None):
        ctr = [n/2,n/2]
    cx = (ctr[0] - float(n)/2 ) / (float(n)/2.)
    cy = (ctr[1] - float(n)/2 ) / (float(n)/2.)
    r = np.sqrt((x-cx)**2+(y-cy)**2)
    eps = 1.e-9
    th = np.arctan2(y-cy,x-cx)
    ru = (1-alpha[0]-alpha[1])*r + alpha[0]*r**3 + alpha[1]*r**5
    xp = ru*np.cos(th) + cx
    yp = ru*np.sin(th) + cy
    indx = np.fix(np.clip((xp+1.0)*n/2,0,n-1)).astype(int)
    indy = np.fix(np.clip((yp+1.0)*n/2,0,n-1)).astype(int)
    ind = indx + indy*n
    #ur = u.flatten()[ind].reshape((n,n))
    adr_set = []
    for a in a_set:
        adr = a.flatten()[ind].reshape((n,n))
        adr_set.append(adr)
    return adr_set

def ref_grid(m=2048,nl = 10,w=10):
    u = np.zeros((m,m))
    for k in range(0,m,m/nl):
        k1 = np.clip(k-w/2,0,m)
        k2 = np.clip(k+w/2,0,m)
        u[:,k1:k2] = 1.
        u[k1:k2,:] = 1.
    k = m/2
    u[:,k-w/2:k+w/2] = -1.
    u[k-w/2:k+w/2,:] = -1.
            
    return u

def ellipse(a,b,th,ctr=None,eps=0.1):
    u = np.zeros((n,n))
    x1 = np.arange(float(n))
    x,y = np.meshgrid(x1,x1)
    if (ctr == None):
        ctr = [n/2,n/2]
    cx = ctr[0]
    cy = ctr[1]
    x = x-cx
    y = y-cy
    xp = x*np.cos(th) + y*np.sin(th)
    yp = -x*np.sin(th) + y*np.cos(th)
    r = ((xp/a)**2 + (yp/b)**2 ).flatten()
    ind = np.where( (r > (1-eps)) & (r < (1+eps)) )
    q = r*0
    q[ind] = 1.
    q = q.reshape((n,n))
    return q

def planet_mask(ctr=(758,1086),r=60):
    return img.circle((n,n),ctr,r)

def ring_mask(ctr=(758,1086),r=70):
    return 1 - img.circle((n,n),ctr,r)

def moon_mask(ctr=(758,1086),r=120):
    return 1 - img.circle((n,n),ctr,r)

def show(u,a,b,thdeg,ctr,eb=200,eps=.02):
    degrees = np.pi/180.
    th = thdeg*degrees
    e = ellipse(a,b,th,ctr,eps)
    x1 = ctr[0]-200
    x2 = ctr[0]+200
    y1 = ctr[1]-100
    y2 = ctr[1]+100
    v = (eb*e+np.clip(u,600,1500))[y1:y2,x1:x2]
    dimg.show(v,origin='lower',fig=1)
    return e,v

def stack():
    v_set = []
    e_set = []
    e,v = show(ub[0],112.5,51.5,17.3,[1086,758.5],eb=200,eps=0.01)
    plt.draw()
    v_set.append(v)
    e_set.append(e)
    e,v = show(ub[1],112.5,51.6,16.6,[1210.5,637],eb=200,eps=0.01)
    plt.draw()
    v_set.append(v)
    e_set.append(e)
    e,v = show(ub[2],111,51.6,16.6,[965,636],eb=200,eps=0.01)
    plt.draw()
    v_set.append(v)
    e_set.append(e)
    e,v = show(ub[3],111.5,51.5,16.6,[968.5,878],eb=200,eps=0.01)
    plt.draw()
    v_set.append(v)
    e_set.append(e)
    e,v = show(ub[4],113.5,51.5,16.6,[1210.5,877],eb=200,eps=0.01)
    plt.draw()
    v_set.append(v)
    e_set.append(e)
    return v_set,e_set

degrees = np.pi/180.
p_set_H_15 = [
    ((1086,758.5),(112.5,51.5,16.6*degrees)),
    ((1210.5,637),(112.5,51.6,16.6*degrees)),
    ((965,636),(111,51.6,16.6*degrees)),
    ((968.5,878),(111,51.5,16.6*degrees)),
    ((1210.5,877),(113.5,50.5,16.5*degrees))
]

def combine(p_set):
    vf_set = []
    x1 = np.arange(float(n))
    xp,yp = np.meshgrid(x1,x1)
    xp0,yp0 = p_set[0][0]
    ap,bp,thp = p_set[0][1]
    xp -= xp0
    yp -= yp0
    xpu = np.cos(thp)*xp - np.sin(thp)*yp
    ypu = np.sin(thp)*xp + np.cos(thp)*yp
    vf_set.append(ub[0])

    for k in range(1,5):
        x0,y0 = p_set[k][0]
        a,b,th = p_set[k][1]
        xu = (xpu/ap)*a
        yu = (ypu/bp)*b
        x = np.cos(th)*xu + np.sin(th)*yu
        y = -np.sin(th)*xu + np.cos(th)*yu
        x += x0
        y += y0
        xi = np.clip(np.round(x),0,n-1).astype(int)
        yi = np.clip(np.round(y),0,n-1).astype(int)
        ind = (xi + yi*n).flatten()
        u = ub[k].flatten()[ind].reshape((n,n))
        vf_set.append(u)

    coadd = np.sum(np.array(vf_set),0)
    return coadd,vf_set

usage = """
ur = p.process()
u,vf_set = p.combine(p.p_set_H_15)
"""

'''
p_ap = p.planet_mask()
r_ap = p.ring_mask()
m_ap = p.moon_mask()
r_ap -= m_ap
sky = 3000.
u,vf_set = p.combine(p.p_set_H_15)
usr = u - sky
dimg.show([usr*p_ap*20 + usr*r_ap + vf_set[0]*m_ap])
'''
