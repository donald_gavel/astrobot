Analyze and Plot AO Telemetry Data
##################################

Unix command line
-----------------
    python -i -m astrobot.telemetry_analysis -- [--help] [-s <starname>]
        [-d <date>] [-n <dsn>] [--plot]

Options
-------
    -d, --date <date>
        The observation date. 'today' refers to the observation night,
        which in the wee hours of the morning is yesterday. Default is 'today'

    -h, --help
        Print this help and exit

    -n, --dsn <dsn>
        dsn refers to Data Set Number, which can be a single number or
        comma delimited set of numbers, as in '2,3,4'. An enclosing square
        bracket, e.g. '[2,3,4]' also works. The data set number refers to
        the particular telemetry file, such as '2' refers to Data_0002.fits.
        'all' is an option that refers to all the data sets of a particular
        observing date. 'all' is the default if only the date is provided.

    -p, --plot
        After gathering and analyzing the date, produce a plot. The
        program suspends until the plot is closed.

    -s, --star <starname>
        The name of the star (optional). If provided along with data set information this
        will allow analysis of the optical throughput to the wavefront sensor.*
        The star is looked up using the Simbad data service. If it is the only
        option provided, the star information will be displayed.

        *Throughput analysis works with data from 2016 and later, which has recorded intensity data.

Install
-------
    git clone https://donald_gavel@bitbucket.org/donald_gavel/astrobot.git

    pip install -e .

Example
-------
    python -i -m astrobot.telemetry_analysis -- '2015-09-03' '411,412' --plot

Use within Python
-----------------
    One can start up the python interpreter and use the code from there.
    The following unix command will drop the user into the
    telemetry_analysis module namespace:

        python -i -m astrobot.telemetry_analysis

    and from there:

        ds = Set('today',[4,5])
        ds.set_star('HD 121968')
        ds.fit()
        ds.plot()

    or 'chain' the commands:

        ds = Set('today',[4,5]).set_star('HD 121968').fit().plot()

Tests
-----
    The following command runs the self-tests:

        tests()
